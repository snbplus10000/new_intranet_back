# 인수인계 작업

## 한국어교원
	1. 특이사항 
		1.1. 국립국어원 기존 담당자 분께서 정년퇴직을 하셨습니다. 지금 현재 공석
		1.2. 전산실 측에서 한국어교원 테스트 서버 관련해서 전산실 가상화서버가 이용 가능하다. 
			새로오시는 전산실 담당자분에 정책에 따라 기존 네이버 클라우드 이용하시는게 가능/불가능 결정
			또한 가상화 서버 이용시 외부에서 접근 가능하는지 여부도 새로 오시는 분에 따라 결정 됨
		1.3. 위에 부분에 대해서 한국어교원측과 논의 필요
		1.4. 가상화 서버 이용시 외부에서 접근 안될 가능성이 크다함
		1.5. cms 견적 진행이 완료 / 설치는 4월경에 진행예정
		1.6. 1,2,3월 유지보수 전달 완료
		1.7. 3/4 ~ 3/13 2020년 1차 개인심사 기간
		1.8. 3/4 , 3/9 ~ 3/13 신복호 주임 출장
		1.9. 3/5 , 3/6 권경민 사원 출장
		1.10. 2019년 3차 개인심사 스캔 관련 완료(3.12)
			-> 스캔기 100장이 넘어가면 뒤섞여서 번호가 매겨지는 문제
		
## 지역어
	1. 특이사항 
		1.1. 2020년 지역어 종합정보 시스템 유지보수 사업 관련 상에 해당되는 작업 예정 날짜 진팀장님한테 전달
		1.2. 3/18 ~ 3/19 남대리님 관리자 및 사용자 디자인 작업
		1.3. 3/23 부터 지역어 작업 시작
		1.4. 3/31 서버 반영 요청(진팀장님)
		1.5. 1,2,3월 유지보수 전달 완료
		
		
## 서울대 박물관
	1.특이사항
		1.1. 웹취약점 2/21 ~ 3/18 까지 진행(약 200여개)
		1.2. 웹 쉘 탐지 에이전트 설치 (3.19 ~)
		    -> 웹 쉘 탐지 에이전트 설치 도중 오류 (해당 문의 완료)
			-> 외부포트 개방을 위한 설치(포트 개방 먼저 작업후 진행 / 포트개방 서울대 측에서 진행)
		1.3. 웹 쉘 탐지 에이전트 설치 완료
		
	2. 연락처
		2.1 권남규 연구원님(02-880-809$)
		
## 서울대 스누팔
	1. 특이사항
		1.1. 3/12 신복호 주임한테 보안취약사항 관련 질문 함
			-> XSS, 불안전한 직접 개체 참조, CSRF - 워드프레스인 경우 관련 플러그인이 있지만 드루팔은 잘 모르겠다라고 답변
			-> 정보유출과부적절한 에러처리, 불안정한 암호화 저장 -> Apache에서 설정해야한다고 말씀 드림 
		1.2. 3/12 ~ 3/18 권경민 사원한테 드루팔 관련 공부 하라고 하루에 2~3번씩 강조
		1.3. 남대리님과 같이 파악한 결과 게시판등  php로 된 커스터 마이징이 많아 회사에 아무도 처리 못한다는 결론이 나옴
		1.4. 진팀장님께 해당 사항을 설명 드리고 실장님한테 말씀 드려 달라고 요청 -> 실장님이 최종적으로 노팀장님한테 가능한지 여부를 월요일에 문의 예정

		
## 애드원 홀딩스
	1. 특이사항
		1.1. 3/3 애드원 홀딩스 영업관리 시스템을 작업해달라고 요청 (3월 말까지)
		1.2. 국립국어원으로 출장 가있는 동안 경민씨한테 작업 해달라고 요청
		- apm관련 문제 및 별도로 cafe24를 다시 생성하는 바람에 여러번 반복 작업함
		1.3. 서버 작업 및 로고 변경, 계약서 마크업, 조직사원 등록 및 view단 관련 오류 작업
		1.4. 엑셀파일 정보로 DB 마이그레이션 (조직도 정보 없어 부서관련 작업 불가)
		1.5. sms 업체 관련 조사 (문자통으로 결정)
		1.6. sms 관련 연동 남아있음
		
## 마당		
	1. 백엔드 작업 내용
	
	### ver 1.0.1(02/21)
		1. 반영: 2.21 1:20 pm ~ 2:30 pm 진행
		2. 작업자 : 신복호 주임
		3. 변경작업
			3.1.[휴가신청]
				3.1.1 휴가신청 vo 관련 작업
			3.2.[구매품의서]
				3.2.1 구매품의서 관련 유저 정보 관련 작업
				3.2.2 구매품의서 리스트 조회시 아이템 항목 합계값 관련 작업
			3.3 [법인카드결제]
				3.3.1 법인카드 작성자 관련 정보 작업
				3.3.2 회의록 관련 백엔드 작업
				3.3.3 apiOperation 변경 작업(Method 변경)
			3.4 [첨부파일]
				3.4.1 파일 다운로드 및 업로드 관련 변경(법인카드 결제 , 출장보고서, 게시판등 폴더 구분을 위한)
			3.5 [캘린더]
				3.5.1 부서별에 따른 클래스 적용 관련 기능 작업
			3.6 [게시판]
				3.6.1  게시판 관련 첨부파일 관련 변경 작업
		
	### ver 1.0.2(02/28)				
		1. 반영: 2.21 6:20 pm ~ 7:00 pm 진행
		2. 작업자 : 신복호 주임
		3. 변경 작업
			3.1 [직원] - 개인 정보
				3.1.1 유저 비밀번호 관련 백엔드 작업
				3.1.2 유저 개인정보 수정 관련 백엔드 수정 작업
				3.1.3 유저 관련 vo 관련 작업
			3.2 [관리자]
				3.2.1 차량 관련 - CRUD 관련 작업
				3.2.2 회사 유저 - CRUD 관련 작업
				3.2.3 법인카드 - CRUD 관련 작업
				3.2.4 직원 휴가 - CURD 관련 작업
				3.2.5 SMS - sms관련 백엔드 전체적인 작업(vo,controller,Mapper 등)
			3.3 [공지사항]
				3.3.1 mailSender 관련 작업
				3.3.2.공지사항 등록 및 수정
		
		4. 미완
			4.1 근태관리
				4.1.1 근태관리 - 관련 selenium 작업
				4.2.2 근태관리 - excel 파일 업로드시 db 데이터 저장
			4.2 로그인 세션
			-> 해당 문제는 db에서 세션 관리가 필요 (지금 당장 작업하기 어려움)
			
	### ver 1.0.3(03/02)
		1. 반영 03.02 5:20 pm ~ 6:20 pm
		2. 작업자 : 신복호 주임
		3. 작업
			3.1 자택근무 출근/퇴근 관련 백엔드 작업
		
	### ver 1.0.4(03/06)
		1. 반영 03.06 4:15 pm ~ 4:30 pm
		2. 작업자 : 신복호 주임
		3. 작업
			3.1 자택근무 출근/퇴근 관련 출근 및 퇴근 ip 분리로 인한 백엔드 작업
			3.2 메일 발송 관련 퇴사자 제외한 리스트 조회 변경
		
	### ver 1.0.5(03/13)
		1. 반영 03.06 11:15 am ~ 11:50 pm
		2. 작업자 : 신복호 주임
		3. 작업
			3.1 직원 연관된 가족관계, 학력, 자격증, 외국어, 개인정보 관련 백엔드 추가 작업
			3.2 공지사항 관련 체크시 전체 메일 발송 기능 수정
			3.3 구매품의서 아이템 관련 fk 값 관련 정보 수정
		
	2. 프론트 작업 내역
	
	## ver 1.0.1 (02/17)

	#### 반영 : 권경민

			1.기능 추가
				1.1. 구매신청 CRUD  추가
				1.2. 알람 기능 추가 (결재 대기 알람)
			2. 로고 수정
				2.1. 로고 변경
				2.2. 파피콘 변경
			3. 로그인 페이지
				3.1. text 수정 (Enter Email => Enter ID)
			4. 출장신청 
				4.1. 필수값 추가 (담당자명, 거래처명)
			5. 법인카드 정
				5.1. 항목 추가 (주유비)

	---

	## ver 1.0.2 (02/21)

	#### 반영 : 권경민

			1.추가
				1.1. 법인카드 정산 회의록 기능 추가
				1.2. 반려 사유 입력 기능 추가 (모든 결재 매뉴)
				1.3. 일일 업무 보고 수정 기능 추가
				1.4. 필수값 항목 체크 아이콘 추가
				1.5. 첨부파일 관련 기능 수정 (출장 신청,  출장 보고서, 법카 신청, 법카 정산, 공지사항, 서식 자료실)
				1.6. 게시판 텝 추가 (공지사항, 서식자료실)
				1.7. 공지사항, 서식자료실 CRUD 추가
			2. 업체관리
				2.1. 상세보기 관련 데이터 작업
				2.2. 리스트 담당자 한명만 표시
			3. 구매신청
				3.1. 예상금액 자동 계산 기능 (예상 단가 x 수량 = 예상 금액)
			4. 달력
				4.1. 달력에 일정 안보이던 오류 수정 ('결제완료' 오타가 원인)
				4.2. 달력 관련 팀별 색상 변경 및 관련 오류 수정

			5. 결재 text 오류
				5.1. 대표란 '0' 표기되던 오류 수정 (구마당 데이터 오류, 프론트 스크립트로 수정)
			6. 상세보기 select 태그 
				6.1. 상세보기 화면에서 select 선택 불가
			7. 첨부파일
				7.1. 첨부파일이 없을 시 상세보기에서는 링크 미노출
			8. 법인카드 정산
				8.1. 인원이 필요없는 항목에서는 인원 선택칸 숨기기 (회의록, 식비, 교통비 항목에서만 인원 선택칸 노출)
			9. 오타 수정, 스타일 통일
				
	---

	## ver 1.0.3 (02/28)

	#### 반영 : 권경민

			1.추가		
				1.1. 개인정보 수정 페이지 추가			
				1.2. 비밀번호 변경 기능 추가		
				1.3. 인쇄 기능 추가(휴가,법인카드정산,휴일근무,구매신청,출장신청,출장보고서)			
				1.4. 공지사항 CRUD 추가			
				1.5. 서식자료실 CRUD 추가			
				1.6. 문서 하단 회사 로고 추가			
				1.7. 관리자 메뉴 추가 			
					1.7.1. 법인카드 관리 CRUD 추가				
					1.7.2. 차량 관리 CRUD 추가				
					1.7.3. 인원 관리 마크업 추가 (메뉴 숨김)				
					1.7.4. 휴가 사용 관리 마크업 추가 (메뉴 숨김)				
					1.7.5. sms 관리 마크업 추가 (메뉴 숨김)			
			2. 오타 수정
			
	---

	### ver 1.0.4 (03/02)

	#### 반영 : 신복호
		1.추가		
			1.1. 매뉴 상단 재택근무 관련 출/퇴근 버튼 및 기능 작업
			1.2. 관리자-재택근무 리스트 및 상세 페이지 추가
			


	### ver 1.0.5 (03/06)

	#### 반영 : 신복호	
		1.추가		
			1.1. 유저페이지, 관리자페이지 공통레이아웃 모바일 대응, 상단 메뉴부분 ie 브라우저 대응, 페이징, 버튼 부분 모바일 대응 스타일로 변경(남상우 대리님 작업)
		
		2. 수정
			2.1. 출퇴근 관리 리스트 컬럼값 수정 (인당 한줄, 출근시간 ,퇴근시간 같은 줄에 나오도록 수정)
			2.2  출퇴근 관리 상세화면 관련 항목 변경(출/퇴근 시간, 출퇴근 ip, 비고 추가 구분 제거)
			2.3. 일일업무보고 수정 삭제 버튼 안보이던 오류 수정
			2.4. 일일업무보고 데이터 안뿌려지던 오류 수정
			2.5  출퇴근 관리 리스트 페이징 관련 오류 수정
			
			


	### ver 1.0.6 (03/13)

	#### 반영 : 권경민
		1. 추가
			1.1. 공지사항 메뉴 메일 발송 선택 기능 추가 (체크박스 추가)
			1.2. 공지사항, 서식자료실 메뉴의 등록, 수정 페이지에 웹 에디터 추가 
			1.3. 관리자 메뉴의 휴가관리 메뉴 추가
		
		2. 수정
			2.1. 공지사항, 서식자료실의 상세보기 페이지에 html 태그가 적용되지 않고 그대로 나오던 오류 수정
			2.2. 법인카드 정산 메뉴의 리스트 페이지 : 노트북 모니터에서 볼 때 "결재완료" text기 2줄로 표시되던 오류 수정
			2.3. 출장보고서 메뉴의 리스트 페이지 : text 수정 (출장 신청 -> 출장 보고)
			2.4. 업체관리 메뉴의 리스트 페이지 : 등록 버튼 스타일 오류 수정
			
			


	### ver 1.0.7 (03/16) 긴급

	#### 반영 : 권경민
		1. 수정
			1.1. 법인카드 정산 등록: 소스에 script태그가 없어 발생했던 오류 수정
			
	3. 미완성 된 부분
		3.1. 대체휴가 관련 SELENIUM 관련 작업(실장님 컴퓨터 고장나서 더이상 진행 X)
		3.2. 관리자 - 인사관리
		3.3. 유저 관련 오류 작업자
		
	4. 특이사항
		4.1. 마당 반영은 근무시간 이외에 해달라는 실장님의 오더가 떨어짐
			

## 경기문화재단
	1. 특이사항
		1.1. 경기문화재단 3/2 부터 시작
		1.2. 처음 서버 이전 전까진 개발팀에서 할일 없고 긴급건만 처리 하면 된다 했음 / 반대로 남대리님이 먼저 투입된다 하였음
		1.3. DB 관련 정보 없음 -> 요청
		1.4. 웹취약점 관련 작업 진행중(3/20~)
		
## 기타
	1. 특이사항
		1.1. 3월 백업에 AMS 한번만 백업해달라고 요청 -> AMS가 띄워지는지 까지 한번 해달라고 요청
		1.2. 김성윤 사원 5월 2번째주까지 연장(3/23 그주는 월요일 제외하고 4번 나머지주는 1주일에 3번 교원 가야됨)
		1.3. 삼성 SDS 개발은 다른업체에서 진행하는것으로 함 (단 노팀장님은 PL로 일부 참여할 가능성 있음)
		1.4. 기상 관련 제안 
		


