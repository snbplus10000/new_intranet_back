package com.snbplus.madang.contract;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.snbplus.madang.common.FileAttach;
import com.snbplus.madang.company.Company;
import com.snbplus.madang.user.User;

import lombok.Data;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * 계약서 - 계약 일반  VO / 관리 정보 VO
 * @author ATU
 *
 */
@Entity
@Table(name="tbl_contract")
@Data
public class Contract {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String state; // 상태

	private String gubun; // 구분
		
	private String workerName;	//계약자
	
	private String contractName;	//업체명
	
	private Integer contractYear;	//계약일
	
	private Integer pay;	//계약금액
	
	private String etc;	//비고
	
	private String contect;	//계약내용일반

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;
	
	private Integer creater;

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;	//최근수정일
	
	private Integer modifier;
	
	private String modifyReason; //변경 사유
	
	private String holdStartDate; // 보류시작날짜
	
	private String holdEndDate; // 보류 종료날짜
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	
	@ManyToOne
	@JoinColumn(name="COMPANY_ID", insertable=false, updatable=false)
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="USER_ID", insertable=false, updatable=false)
	private User user;

	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="contractId")
	private List<ContractFile> contractFile;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="contractId")
	private List<ContractApproval> contractApproval;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="contractId")
	private List<ContractPay> contractPay;
	


}
