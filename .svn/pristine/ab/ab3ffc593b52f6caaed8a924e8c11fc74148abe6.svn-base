package com.snbplus.madang.busiTrip;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.car.CarMapper;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/busiTripCar")
public class BusiTripCarController implements InterfaceController<BusiTripCar> {
	
	@Autowired
	BusiTripCarMapper busiTripCarMapper;
	
	@Autowired
	CarMapper carMapper;
	
	@Autowired
	UserMapper userMapper;

	@ApiOperation(value = "차량운행 리스트", httpMethod = "GET", notes = "차량운행 리스트")
	@Override
	public ResponseEntity<Page<BusiTripCar>> list(@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable) {

		 return new ResponseEntity<>(busiTripCarMapper.findByDelYn(pageable,"N")
				 .map(busiTripCar -> {

				 	User searchUser;
				 	Integer id = 0;
				 	User user = new User();
				 	if(busiTripCar.getModifier() != null && busiTripCar.getModifier() != 0) {
				 		id = busiTripCar.getModifier();

				 	}else {
				 		id = busiTripCar.getCreater();
				 	}
			 		searchUser = userMapper.findOne(id);
				 	user.setId(searchUser.getId());
				 	user.setName(searchUser.getName());
				 	user.setLoginId(searchUser.getLoginId());
				 	busiTripCar.setUser(user);
	

				 	return busiTripCar;
				 }), HttpStatus.OK);
	}

	@ApiOperation(value = "차량운행 상세화면", httpMethod = "GET", notes = "차량운행 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "차량운행 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiTripCar> view(@PathVariable Integer id) {

		BusiTripCar busiTripCar = busiTripCarMapper.findOne(id);
		
		//busiTripCar.getUser().setDeptChart(null);
		//busiTripCar.getUser().setHighDeptChart(null);
	

		return new ResponseEntity<>(busiTripCar, HttpStatus.OK);
	}

	@ApiOperation(value = "차량운행 저장", httpMethod = "PUT", notes = "차량운행 저장")
	@Override
	public ResponseEntity<BusiTripCar> save(@RequestBody BusiTripCar t) {
		User user = UserSingleton.getInstance();
		
		t.setCreater(user.getId());
		t.setCar(carMapper.findOne(t.getCarNo()));
		t.setUser(user);
		
		busiTripCarMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "차량운행 삭제", httpMethod = "DELETE", notes = "차량운행 삭제")
	@Override
	public ResponseEntity<BusiTripCar> delete(@PathVariable Integer id) {
	
		BusiTripCar busiTripCar = busiTripCarMapper.findOne(id);
		
		busiTripCar.setDelYn("Y");
		
		busiTripCarMapper.save(busiTripCar);
		
		return new ResponseEntity<>(busiTripCar,HttpStatus.OK);
	}

	@ApiOperation(value = "차량운행 수정", httpMethod = "PUT", notes = "차량운행 수정")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "차량운행 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiTripCar> update(@RequestBody BusiTripCar t) {

		User user = UserSingleton.getInstance();

		t.setModifier(user.getId());
		t.setCar(carMapper.findOne(t.getCarNo()));
		t.setUser(user);
		busiTripCarMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}
}
