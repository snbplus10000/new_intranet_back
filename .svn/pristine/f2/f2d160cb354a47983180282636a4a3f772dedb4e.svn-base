package com.snbplus.madang.user;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.snbplus.madang.common.FileAttach;
import com.snbplus.madang.common.FileUploadService;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.common.OrgChartMapper;
import com.snbplus.madang.common.RedirectException;
import com.snbplus.madang.config.MadangPasswordEncoder;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
	
	private static final String DEFAULT_REDIRECT_VIEW_URL = "/user/exterList";
	
	@Autowired
	MadangPasswordEncoder encoder;
	
    @Autowired
    UserMapper userMapper;
    
    @Autowired
    UserFileMapper userFileMapper;
    
    @Autowired
    FileUploadService fileUploadService;
    
    @Autowired
    OrgChartMapper chartMapper;

    @PostMapping("/info")
    public User user(Authentication authentication) {
    	
    	User user = UserSingleton.getInstance();
    	
    	user.getDeptChart().setDeptUser(null);
    	user.getDeptChart().setHighDeptUser(null);
    	user.getHighDeptChart().setDeptUser(null);
    	user.getHighDeptChart().setHighDeptUser(null);
    	
        return user;
    }
   
    
    
    @PostMapping("/save")  
    public String userSave(User user, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	User userSession = UserSingleton.getInstance();
    	user.setCreater(userSession.getId());
    	User userResult = new User();
    	
    	String memState = user.getMemState();
    	
    	if(memState != null && (memState.equals("정규직") || memState.equals("계약직") || memState.equals("인턴주"))) {
    		user.setGubun("I");
    	}else {
    		user.setGubun("O");
    	}
    	
    	String workState = user.getWorkState();
    	
    	if(workState != null && workState.equals("퇴사")) {
    		user.setRetireDate(null);
    	}
    	  	   	
    	String loginId = user.getLoginId();
    	
    	Optional<User> searchUser = userMapper.findByLoginId(loginId);
    	
    	
    	if(!searchUser.isPresent()) {
    		   		             	
    		String url = "/thumb";
        	String url2 = "/signature";
        	      	
        	MultipartFile thumbFiles = user.getThumbFiles();
        	UserFile thumbFile = new UserFile();
        	UserFile signFile = new UserFile();
        	if(thumbFiles != null &&!thumbFiles.isEmpty()) {
        		FileAttach thumbFileResult = fileUploadService.uploadFile(thumbFiles,url);
        		
        		UserFile thumbUserFile = new UserFile();
	        	thumbUserFile.setFileAttach(thumbFileResult);
	        	thumbUserFile.setKind("thumb");
	        	thumbFile = userFileMapper.save(thumbUserFile); 
	        	user.setThumbFile(thumbFile);
	        	
        		
        	}else {
        		user.setThumbFile(null);
        	}
        	MultipartFile signFiles = user.getSignFiles();
        	if(signFiles != null &&!signFiles.isEmpty()) {
        		FileAttach signFileResult = fileUploadService.uploadFile(signFiles,url2);
        		
            	UserFile signUserFile = new UserFile();
            	signUserFile.setFileAttach(signFileResult);
            	signUserFile.setKind("signature");
            	signFile = userFileMapper.save(signUserFile);
            	user.setSignFile(signFile);
        	}else {
        		user.setSignFile(null);
        	}
               	
        	userResult = userMapper.save(user); 

    	}else {
    		throw new RedirectException(DEFAULT_REDIRECT_VIEW_URL);
    	}
	  	
    	response.sendRedirect("/user/exterList");
    	
    	return "";
    }
    
    @PostMapping("/update") 
    public String userUpdate(User user, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	User userSession = UserSingleton.getInstance();

    	user.setModifier(userSession.getId());
    	
    	String memState = user.getMemState();
    	
    	if(memState != null && (memState.equals("정규직") || memState.equals("계약직") || memState.equals("인턴주"))) {
    		user.setGubun("I");
    	}else {
    		user.setGubun("O");
    	}
    	
    	String workState = user.getWorkState();
    	
    	if(workState != null && workState.equals("퇴사")) {
    		user.setRetireDate(null);
    	}
    	  	
    	String loginId = user.getLoginId();
    	
    	Optional<User> searchUser = userMapper.findByLoginId(loginId);
    
		User userInfo = searchUser.get();
		   		             	
    	String url = "/thumb";
    	String url2 = "/signature";
    	
    	MultipartFile thumbFiles = user.getThumbFiles();
    	UserFile thumbFile = new UserFile();
    	UserFile signFile = new UserFile();
    	if(!thumbFiles.isEmpty()) {
    		FileAttach thumbFileResult = fileUploadService.uploadFile(thumbFiles,url);
    		
    		UserFile thumbUserFile = new UserFile();
        	thumbUserFile.setFileAttach(thumbFileResult);
        	thumbUserFile.setKind("thumb");
        	thumbFile = userFileMapper.save(thumbUserFile); 
        	user.setThumbFile(thumbFile);
        	   		
    	}else {
    		user.setThumbFile(null);
    	}
    	MultipartFile signFiles = user.getSignFiles();
    	if(!signFiles.isEmpty()) {
    		FileAttach signFileResult = fileUploadService.uploadFile(signFiles,url2);
    		
        	UserFile signUserFile = new UserFile();
        	signUserFile.setFileAttach(signFileResult);
        	signUserFile.setKind("signature");
        	signFile = userFileMapper.save(signUserFile);
        	user.setSignFile(signFile);
    	}else {
    		user.setSignFile(null);
    	}
    	
    	user.setId(userInfo.getId());
    	User userResult = userMapper.save(user);
	  	
    	response.sendRedirect("/user/exterList");
    	
    	return "";
    }

	@ApiOperation(value = "부서에 따른 직원 list", httpMethod = "GET", notes = "부서에 따른 직원 list  ")
	@ApiResponses(value = {@ApiResponse(code = 400, message = "Server is Dead"), @ApiResponse(code = 200, message = "Run")})
	@GetMapping("/userList/{id}")
	public ResponseEntity<List<User>> orgChartUserList(@PathVariable Integer id){
		
		String workState ="재직중";
		OrgChart deptChart = chartMapper.findOne(id);
		List<User> userList = userMapper.findBydeptChartAndWorkState(deptChart,workState);
		
		for(User user : userList) {
			user.setDeptChart(null);
			user.setHighDeptChart(null);
		}
		
		return new ResponseEntity<>(userList,HttpStatus.OK);
	}
    
    
    
    
}
