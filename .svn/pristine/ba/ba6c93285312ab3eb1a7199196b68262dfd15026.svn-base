package com.snbplus.madang.busiCard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/busiCardCalc")
public class BusiCardCalcController implements InterfaceController<BusiCardCalc> {
	
	@Autowired
	BusiCardCalcMapper calcMapper;
	
	@Autowired
	UserMapper userMapper;

	@ApiOperation(value = "법인카드정산 리스트", httpMethod = "GET", notes = "법인카드정산 리스트")
	@Override
	public ResponseEntity<Page<BusiCardCalc>> list(Pageable pageable) {
		return new ResponseEntity<>(calcMapper.findByDelYn(pageable,"N")
				.map(busiCardCalc -> {
				
				 	User searchUser;
				 	Integer id = 0;
				 	User user = new User();
				 	if(busiCardCalc.getModifier() != null && busiCardCalc.getModifier() != 0) {
				 		id = busiCardCalc.getModifier();

				 	}else {
				 		id = busiCardCalc.getCreater();
				 	}
			 		searchUser = userMapper.findOne(id);
				 	user.setId(searchUser.getId());
				 	user.setName(searchUser.getName());
				 	user.setLoginId(searchUser.getLoginId());
				 	busiCardCalc.setUser(user);
					return busiCardCalc;
				}),HttpStatus.OK);

	}

	
	@ApiOperation(value = "법인카드정산 상세화면", httpMethod = "GET", notes = "법인카드정산 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "법인카드정산 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiCardCalc> view(@PathVariable Integer id) {
		
		return new ResponseEntity<>(calcMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드정산 저장", httpMethod = "PUT", notes = "법인카드정산 저장")
	@Override
	public ResponseEntity<BusiCardCalc> save(@RequestBody BusiCardCalc t) {
		
		calcMapper.save(t);
	
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드정산 삭제", httpMethod = "DELETE", notes = "법인카드정산 삭제")
	@Override
	public ResponseEntity<BusiCardCalc> update( @RequestBody BusiCardCalc t) {
		
		t.setDelYn("Y");
		calcMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드정산 수정", httpMethod = "PUT", notes = "법인카드정산 수정")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "법인카드정산 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiCardCalc> delete(@PathVariable Integer id) {
		
		User user = UserSingleton.getInstance();
		
		BusiCardCalc busiCardCalc = calcMapper.findOne(id);
		busiCardCalc.setDelYn("Y");
		busiCardCalc.setModifier(user.getId());
		
		calcMapper.save(busiCardCalc);
		
		return new ResponseEntity<>(busiCardCalc, HttpStatus.OK);
	}

}
