package com.snbplus.madang.company;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CompanyExportsMapperTest {
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	CompanyExportsMapper companyExportsMapper;
	
	@Test
	public void list() throws Exception {
		
		Company company = new Company();
		
		company.setId(1);
		
		Optional<Company> companyOptional = companyMapper.findByIdAndDelYn(1, "Y");
    	
	    companyOptional.orElseThrow(() -> new Exception("정보를 찾을수 없습니다."));
	    
	    int companyId = companyOptional.get().getId();
	    
	    List<CompanyExports> exportsList =  companyExportsMapper.findByCompanyIdAndDelYn(companyId,"N");
	    
	    exportsList.forEach(item -> log.info("{}", exportsList));
	    
	}
	
	 @Test
	 public void View() throws Exception {
		 
		 CompanyExports companyExports = companyExportsMapper.findOne(1);
	    
		int id = companyExports.getId();
		
		log.info("id :::" + id);
	 }
	
	 
	 @Test
	 public void save() {
		 
		 CompanyExports companyExports = new CompanyExports();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyExports.setCompanyId(company.getId());
		 companyExports.setDelYn("Y");
		 companyExports.setEmployeesNum(12);
		 companyExports.setEtc("기타");
		 
		 CompanyExports exportOptional = companyExportsMapper.save(companyExports);
	 }
	 
	 @Test
	 public void update() {
		 CompanyExports companyExports = new CompanyExports();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyExports.setCompanyId(company.getId());
		 
		 companyExports.setId(1);
		 companyExports.setDelYn("Y");
		 companyExports.setEmployeesNum(13);
		 companyExports.setEtc("기타2");
		 
		 CompanyExports exportOptional = companyExportsMapper.save(companyExports);
	 }

}
