package com.snbplus.madang.company;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CompanyMapperTest {
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	CompanyCommentMapper CompanyCommentMapper;
	
	@Autowired
	CompanyExportsMapper exportsMapper;
	
	@Autowired
	CompanyHistroyMapper historyMapper;
	
	@Autowired
	CompanyInfoMapper infoMapper;
	
	@Autowired
	CommentMapper commentMapper;
	
	@Autowired
	CompanySpec companySpec;
	
	@Autowired
	UserMapper userMapper;
	
	 @Test
	public void List() {
		
		companyMapper.findAll().forEach(item -> log.info("{}", item));
	}
		 
	 @Test
	 //@Transactional
	 public void View() throws Exception {
		 
		Optional<Company> companyOptional = companyMapper.findByIdAndDelYn(1, "N");
				
	    companyOptional.orElseThrow(() -> new Exception("정보를 찾을수 없습니다."));
	    
	    // List<CompanyComment> commentList = companyOptional.get().getCompanyComment();
	    
	    // int size = commentList.size();
	    	
	 }
	 
	 @Test
	 public void save() {
		 
		 Company company = new Company();			 

		 //company.setIestate("I");
		 company.setCapitalPay(1000);
		 company.setCompanyEnName("BKShin");
		 company.setCompanyName("신복호");
		 company.setCompanyType("T");
		 company.setContent("내용");
				 		 
		 
		 Company companyResult = companyMapper.save(company);
		 
		 if(companyResult != null) {

			 CompanyComment companyComment = new CompanyComment();
			 CompanyInfo info = new CompanyInfo();
			 CompanyExports exports = new CompanyExports();
			 CompanyHistroy history = new CompanyHistroy();
			 Comment comment = new Comment();
			 
			 comment.setKinds("company");
			 comment.setFromPeople("test1");
			 comment.setToPeople("test2");
			 comment.setVisibility("Y");
			 comment.setDelYn("N");
			 
			 Comment commentResult = commentMapper.save(comment);
			 
			 if(commentResult != null) {
				 
				 companyComment.setComment(commentResult);
				 companyComment.setCompany(companyResult);
				 companyComment.setDelYn("N");
				 
				 CompanyComment companyCommentResult = CompanyCommentMapper.save(companyComment);
			 }
			 
			 info.setCompanyId(companyResult.getId());
			 info.setPromotionYn("Y");
			 info.setWorkState("근무");
//			 info.setName("test");
//			 info.setRank("대리");
			 info.setDept("퍼블");
			 
			 CompanyInfo infoResult = infoMapper.save(info);
			 
			 
		 }else {
			 System.out.println("company 저장 안됨");
		 }
		 

		 
	 }
	 
	 @Test
	 public void update() {
		 Company company = new Company();
		 company.setId(1);
		 //company.setIestate("I");
		 company.setCapitalPay(1000);
		 company.setCompanyEnName("BKShin");
		 company.setCompanyName("신복호");
		 company.setCompanyType("T");
		 company.setContent("내용");
		 company.setDelYn("Y");
		 
		 Company companyResult = companyMapper.save(company);
	 }
 
    @Test
    public void 회사리스트() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<Company> spec = Specifications.where(companySpec.build("N",null,null));

        Page<Company> p = companyMapper.findAll(spec, pageable);

        log.info("회사리스트 - {}" , p.getContent());

    }
    
    @Test
    public void 회사리스트_회사이름like대() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Company> spec = Specifications.where(companySpec.build("N", "companyName", "대"));

        Page<Company> p = companyMapper.findAll(spec, pageable);

        log.info("size :::" + p.getSize());
        log.info("회사리스트_회사이름like대 - {}" ,p.getContent());
    }

    @Test
    public void 회사리스트_등록자like신() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Company> spec = Specifications.where(companySpec.build("N", "creater", "신"));

        Page<Company> p = companyMapper.findAll(spec, pageable);

        log.info("회사리스트_회사이름like신 - {}" ,p.getContent());

    }

}
