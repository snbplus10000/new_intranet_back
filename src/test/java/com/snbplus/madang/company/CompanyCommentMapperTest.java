package com.snbplus.madang.company;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CompanyCommentMapperTest {
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	CommentMapper commentMapper;
	
	@Autowired
	CompanyCommentMapper companyCommentMapper;
	
	@Test
	public void list() throws Exception {
		
		Company company = new Company();
		
		company.setId(1);
		
		Optional<Company> companyOptional = companyMapper.findByIdAndDelYn(1, "Y");
    	
	    companyOptional.orElseThrow(() -> new Exception("정보를 찾을수 없습니다."));
	    
	    int companyId = companyOptional.get().getId();
	    
	    List<CompanyComment> commentList =  companyCommentMapper.findByCompanyIdAndDelYnOrderByIdDesc(companyId ,"N");
	    
	    commentList.forEach(item -> log.info("{}", commentList));
	    
	}
	
	 @Test
	 public void View() throws Exception {
		 
		CompanyComment companyComment = companyCommentMapper.findOne(1);
	    
		int id = companyComment.getId();
		
		log.info("id :::" + id);
	 }
		 
	 @Test
	 public void save() {
		 
		 CompanyComment companyComment = new CompanyComment();
		 Comment comment = new Comment();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyComment.setCompany(company);
		 companyComment.setDelYn("Y");
		 
		 comment.setKinds("company");
		 comment.setFromPeople("test1");
		 comment.setToPeople("test2");
		 comment.setVisibility("Y");
		 comment.setDelYn("N");
		 
		 Comment commentResult = commentMapper.save(comment);
		 
		 if(commentResult != null) {
			 companyComment.setComment(commentResult);
			 CompanyComment commentOptional = companyCommentMapper.save(companyComment);
		 }

	 }
	 
	 @Test
	 public void update() {
		 CompanyComment companyComment = new CompanyComment();
		 Comment comment = new Comment();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyComment.setCompany(company);
		 companyComment.setDelYn("Y");
		 companyComment.setId(1);
		 
		 comment.setKinds("company");
		 comment.setFromPeople("test1");
		 comment.setToPeople("test2");
		 comment.setVisibility("Y");
		 comment.setDelYn("N");
		 
		 
		 Comment commentResult = commentMapper.save(comment);
		 
		 if(commentResult != null) {
			 companyComment.setComment(commentResult);
			 CompanyComment commentOptional = companyCommentMapper.save(companyComment);
		 }
	 }

}
