package com.snbplus.madang.company;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CompanyHistroyMapperTest {

	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	CompanyHistroyMapper companyHistoryMapper;
	
	@Test
	public void list() throws Exception {
		
		Company company = new Company();
		
		company.setId(1);
		
		Optional<Company> companyOptional = companyMapper.findByIdAndDelYn(1, "Y");
    	
	    companyOptional.orElseThrow(() -> new Exception("정보를 찾을수 없습니다."));
	    
	    int companyId = companyOptional.get().getId();
	    
	    List<CompanyHistroy> historyList =  companyHistoryMapper.findByCompanyIdAndDelYn(companyId,"N");
	    
	    
	}
	
	 @Test
	 public void View() throws Exception {
		 
		 CompanyHistroy companyHistory = companyHistoryMapper.findOne(1);
	    
		int id = companyHistory.getId();
		
		log.info("id :::" + id);
	 }
	
	 
	 @Test
	 public void save() {
		 
		 CompanyHistroy companyHistory = new CompanyHistroy();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyHistory.setCompanyId(company.getId());
		 
		 companyHistory.setDelYn("Y");
		 companyHistory.setArea("지역");
		 companyHistory.setEtc("기타");
		 
		 CompanyHistroy historyOptional = companyHistoryMapper.save(companyHistory);
	 }
	 
	 @Test
	 public void update() {
		 CompanyHistroy companyHistory = new CompanyHistroy();
		 
		 Company company = new Company();
		 company.setId(1);
		 companyHistory.setCompanyId(company.getId());
		 
		 companyHistory.setId(1);
		 companyHistory.setDelYn("Y");
		 companyHistory.setArea("지역2");
		 companyHistory.setEtc("기타2");
		 
		 CompanyHistroy historyOptional = companyHistoryMapper.save(companyHistory);
	 }
}
