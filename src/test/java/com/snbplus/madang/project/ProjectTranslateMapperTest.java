package com.snbplus.madang.project;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.method.P;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProjectTranslateMapperTest {

    @Autowired
    ProjectMapper projectMapper;

    @Autowired
    ProjectTranslateMapper projectTranslateMapper;

    /* 정상작동 */
    @Test
    public void list() throws Exception {

        Project project = new Project();

        project.setId(1);

        Optional<Project> projectOptional = projectMapper.findByIdAndDelYn(1,"N");

        projectOptional.orElseThrow(() -> new Exception("정보를 찾을 수 없습니다."));

        int projectId = projectOptional.get().getId();

        List<ProjectTranslate> translateList = projectTranslateMapper.findByProjectIdAndDelYn(projectId,"N");

        translateList.forEach(item -> log.info("{}", translateList));
    }

    /* 정상작동 */
    @Test
    public void view() throws Exception {

        ProjectTranslate projectTranslate = projectTranslateMapper.findOne(1);

        int id = projectTranslate.getId();

        log.info("id :::" + id);
    }

    /* 정상작동 */
    @Test
    public void save(){

        ProjectTranslate projectTranslate = new ProjectTranslate();

        Project project = new Project();
        project.setId(2);
        projectTranslate.setId(project.getId());

        projectTranslate.setCompanyName("Test1");
        projectTranslate.setTelNo("01012345678");
        projectTranslate.setRequestDate("2020-10-20");
        projectTranslate.setKorean("Y");
        projectTranslate.setEnglish("Y");
        projectTranslate.setChinese1("N");
        projectTranslate.setChinese2("N");
        projectTranslate.setJapanese("Y");
        projectTranslate.setVietnamese("N");
        projectTranslate.setTranslateVolume("100");
        projectTranslate.setTranslateFee("150000");
        projectTranslate.setCompleteDate("2020-10-25");
        projectTranslate.setCreater(2);
        projectTranslate.setDelYn("N");

        ProjectTranslate translateOptional = projectTranslateMapper.save(projectTranslate);
    }

    /* 정상작동 */
    @Test
    public void update(){
        ProjectTranslate projectTranslate = new ProjectTranslate();

        Project project = new Project();
        project.setId(1);
        projectTranslate.setId(project.getId());

        projectTranslate.setCompanyName("Test2");
        projectTranslate.setTelNo("01087654321");
        projectTranslate.setRequestDate("2020-10-25");
        projectTranslate.setKorean("N");
        projectTranslate.setEnglish("N");
        projectTranslate.setChinese1("Y");
        projectTranslate.setChinese2("Y");
        projectTranslate.setJapanese("N");
        projectTranslate.setVietnamese("Y");
        projectTranslate.setTranslateVolume("150");
        projectTranslate.setTranslateFee("200000");
        projectTranslate.setCompleteDate("2020-10-30");
        projectTranslate.setCreater(1);
        projectTranslate.setModifier(1);
        projectTranslate.setDelYn("N");
        projectTranslate.setProjectId(1);

        ProjectTranslate translateOptional = projectTranslateMapper.save(projectTranslate);
    }
}
