package com.snbplus.madang.project;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.company.Company;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProjectProcessMapperTest {

    @Autowired
    ProjectProcessMapper processMapper;

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    ProjectCommentMapper projectCommentMapper;

    @Autowired
    ProjectMapper projectMapper;

    @Autowired
    UserMapper userMapper;

    @Test
    public void List() {
        processMapper.findAll().forEach(item -> log.info("{}", item));
    }

    /* 정상작동 */
    @Test
    public void View() throws Exception {

        Optional<ProjectProcess> processOptional = processMapper.findByIdAndDelYn(1,"N");

        processOptional.orElseThrow(() -> new Exception("정보를 찾을 수 없습니다."));

    }

    /* 정상작동 */
    @Test
    public void save(){
        ProjectProcess process = new ProjectProcess();
        Optional<User> user = userMapper.findByLoginId("admin");

        Project project = new Project();
        Company company = new Company();

        company.setId(4);

//        project.setId(1);
        project.setCompany(company);
        project.setContent("테스트 내용");
        project.setCreater(user.orElseThrow(() -> new NoSuchElementException()));
        project.setDelYn("N");

        Project processResult = projectMapper.save(project);

        if(processResult != null) {

            ProjectComment projectComment = new ProjectComment();
            Comment comment = new Comment();

            comment.setKinds("projectTest");
            comment.setFromPeople("test3");
            comment.setToPeople("test4");
            comment.setVisibility("Y");
            comment.setDelYn("N");

            Comment commentResult = commentMapper.save(comment);

            if(commentResult != null) {
                projectComment.setComment(commentResult);
                projectComment.setProject(project);
                projectComment.setDelYn("N");

                ProjectComment projectCommentResult = projectCommentMapper.save(projectComment);
            }
        }else{
            System.out.println("process가 저장되지 않았습니다.");
        }
    }

    /* 정상작동 */
    @Test
    public void update(){
        ProjectProcess process = new ProjectProcess();
        process.setId(1);
        process.setTranslateYn("Y");
        process.setDeadLine("2020-10-25");
        process.setEtc("비비고고");
        process.setContent("테스트 수정 내용");
        process.setWorkUrl("workUrl 수정");
        process.setCompleteUrl("completeUrl 수정");
        process.setSiteAdminUrl("adminUrl 수정");
        process.setSiteAdminId("test3");
        process.setSiteAdminPw("4321");
        process.setWebHostUrl("webHostUrl 수정");
        process.setWebHostId("test4");
        process.setWebHostPw("8765");
        process.setThemeName("파크테마");
        process.setThemeLicenseCode("edocedoc");
        process.setCreater(1);
        process.setModifier(1);
        process.setDelYn("N");

        ProjectProcess processResult = processMapper.save(process);
    }
}
