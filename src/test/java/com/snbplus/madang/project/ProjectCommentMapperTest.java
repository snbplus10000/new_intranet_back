package com.snbplus.madang.project;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.company.Company;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProjectCommentMapperTest {

        @Autowired
        ProjectMapper projectMapper;

        @Autowired
        CommentMapper commentMapper;

        @Autowired
        ProjectCommentMapper projectCommentMapper;

        @Autowired
        ProjectCommentSpec projectCommentSpec;

        //* 정상작동 *//
        @Test
        public void list() throws Exception {

            Project project = new Project();

            project.setId(1);

            Optional<Project> projectOptional = projectMapper.findByIdAndDelYn(14,"N");

            projectOptional.orElseThrow(() -> new Exception("정보를 찾을 수 없습니다."));

            int projectId = projectOptional.get().getId();

            List<ProjectComment> commentList = projectCommentMapper.findByIdAndDelYnOrderByIdDesc(projectId, "N");

            commentList.forEach(item -> log.info("{}", commentList));

        }

        //* 정상작동 *//
       @Test
        public void View() throws Exception {

            ProjectComment projectComment = projectCommentMapper.findOne(1);

            int id = projectComment.getId();

            log.info("id :::" + id + ",내용 :::" + projectComment);

        }

        //* 정상작동이 안 됨 *//
      @Test
        public void save() {

            ProjectComment projectComment = new ProjectComment();
            Comment comment = new Comment();

            Project project = new Project();
            project.setId(2);
            projectComment.setDelYn("N");
            projectComment.setProject(project);

            Company company = new Company();
            company.setId(1);
            projectComment.setCompany(company);

            comment.setKinds("project");
            comment.setLocation("홈페이지");
            comment.setGubun("전화");
            comment.setFromPeople("test9");
            comment.setToPeople("test9");
            comment.setVisibility("Y");
            comment.setDelYn("N");

            Comment commentResult = commentMapper.save(comment);

            if(commentResult != null) {
                projectComment.setComment(commentResult);
                ProjectComment projectOptional = projectCommentMapper.save(projectComment);
            }
        }

        //* 정상작동 *//
/*        @Test
        public void update() {
            ProjectComment projectComment = new ProjectComment();
            Comment comment = new Comment();

            Project project = new Project();
            project.setId(1);
            projectComment.setProject(project);
            projectComment.setDelYn("Y");
            projectComment.setId(1);

            comment.setKinds("project");
            comment.setFromPeople("changeTest1");
            comment.setToPeople("changeTest2");
            comment.setVisibility("Y");
            comment.setDelYn("N");

            Comment commentResult = commentMapper.save(comment);

            if(commentResult != null) {
                projectComment.setComment(commentResult);
                ProjectComment projectOptional = projectCommentMapper.save(projectComment);
            }
        }*/

        @Test
        public void list_paging(){

            Pageable pageable = new PageRequest(0,5);

            Specification<ProjectComment> spec = Specifications.where(projectCommentSpec.spec("N",2,"project","홈페이지"));

            Page<ProjectComment> p = projectCommentMapper.findAll(spec, pageable).map(comment -> {
                /* 데이터 선별을 위해 불필요한 데이터를 null처리*/
                comment.setProject(null);
                return comment;
            });

            log.info("댓글리스트_구분_로케이션 - {}", p.getContent());
        }
}
