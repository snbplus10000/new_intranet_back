package com.snbplus.madang.project;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProjectMapperTest {

    @Autowired
    ProjectMapper projectMapper;

    @Autowired
    ProjectProcessMapper processMapper;

    @Autowired
    ProjectCommentMapper projectCommentMapper;

    @Autowired
    ProjectTranslateMapper translateMapper;

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    UserMapper userMapper;

    @Test
    public void save(){

        Project project = new Project();
        Optional<User> user = userMapper.findByLoginId("jinyk0811");

        project.setContractName("test");
        project.setBusinessName("수출바우처A");
        project.setKind("홈페이지");
        project.setState("진행");
        project.setContent("test 내용");
        project.setAmount(10000);
        project.setDelYn("N");
        project.setCreater(user.orElseThrow(() -> new NoSuchElementException()));


        project.setContractDate("2020-09-29");

        Project projectResult = projectMapper.save(project);

        if(projectResult != null) {
            ProjectComment projectComment = new ProjectComment();
            ProjectProcess process = new ProjectProcess();
            ProjectTranslate projectTranslate = new ProjectTranslate();
            Comment comment = new Comment();

            process.setTranslateYn("Y");
            process.setDeadLine("2020-10-20");
            process.setEtc("비비고");
            process.setContent("테스트 내용");
            process.setWorkUrl("https://www.naver.com");
            process.setCompleteUrl("https://www.daum.net");
            process.setSiteAdminUrl("https://www.google.com");
            process.setSiteAdminId("test");
            process.setSiteAdminPw("1234");
            process.setWebHostUrl("https://www.nbnb.com");
            process.setWebHostId("test2");
            process.setWebHostPw("5678");
            process.setThemeName("테마파크");
            process.setThemeLicenseCode("codecode");
            process.setDelYn("N");

            ProjectProcess processResult = processMapper.save(process);

            if(processResult != null) {

                comment.setKinds("projectTest");
                comment.setFromPeople("test3");
                comment.setToPeople("test4");
                comment.setVisibility("Y");
                comment.setDelYn("N");

                Comment commentResult = commentMapper.save(comment);

                if(commentResult != null) {
                    projectComment.setComment(commentResult);
                    projectComment.setProject(project);
                    projectComment.setDelYn("N");

                    ProjectComment projectCommentResult = projectCommentMapper.save(projectComment);
                }
            }

            projectTranslate.setCompanyName("Test1");
            projectTranslate.setTelNo("01012345678");
            projectTranslate.setRequestDate("2020-10-20");
            projectTranslate.setKorean("Y");
            projectTranslate.setEnglish("Y");
            projectTranslate.setChinese1("N");
            projectTranslate.setChinese2("N");
            projectTranslate.setJapanese("Y");
            projectTranslate.setVietnamese("N");
            projectTranslate.setTranslateVolume("100");
            projectTranslate.setTranslateFee("150000");
            projectTranslate.setCompleteDate("2020-10-25");
            projectTranslate.setCreater(1);
            projectTranslate.setDelYn("N");

            ProjectTranslate translateResult = translateMapper.save(projectTranslate);
        }else{
            System.out.println("저장이 되지 않았습니다.");
        }
    }

/*    @Test
    public void List() {
        projectMapper.findAll().forEach(item -> log.info("{}", item));
    }

    @Test
    public void View() throws Exception {

        Optional<Project> projectOptional = projectMapper.findByIdAndDelYn(1,"N");

        projectOptional.orElseThrow(() -> new Exception("정보를 찾을 수 없습니다."));
    }

    @Test
    public void save(){

    Project project = new Project();
    project.setId(1);
    project.setContractName("test");
    project.setBusinessName("수출바우처A");
    project.setKind("홈페이지");
    project.setState("진행");
    project.setContent("test 내용");
    project.setAmount(10000);
    project.setDelYn("N");
    project.setCreater(15);
    project.setModifier(15);
    project.setContractDate("2020-09-29");

    Project projectResult = projectMapper.save(project);

    if(projectResult != null) {
        ProjectProcess process = new ProjectProcess();
        ProjectComment projectComment = new ProjectComment();
        ProjectTranslate projectTranslate = new ProjectTranslate();
        Comment comment = new Comment();

        process.setId(1);
        process.setTranslateYn("Y");
        process.setDeadLine("2020-10-20");
        process.setEtc("비비고");
        process.setContent("테스트 내용");
        process.setWorkUrl("https://www.naver.com");
        process.setCompleteUrl("https://www.daum.net");
        process.setSiteAdminUrl("https://www.google.com");
        process.setSiteAdminId("test");
        process.setSiteAdminPw("1234");
        process.setWebHostUrl("https://www.nbnb.com");
        process.setWebHostId("test2");
        process.setWebHostPw("5678");
        process.setThemeName("테마파크");
        process.setThemeLicenseCode("codecode");
        process.setWorkProcess("진행");
        process.setDelYn("N");

        ProjectProcess processResult = processMapper.save(process);

        if(processResult != null) {

            comment.setKinds("projectTest");
            comment.setFromPeople("test3");
            comment.setToPeople("test4");
            comment.setVisibility("Y");
            comment.setDelYn("N");

            Comment commentResult = commentMapper.save(comment);

            if(commentResult != null) {
                projectComment.setComment(commentResult);
                projectComment.setProjectProcess(processResult);
                projectComment.setDelYn("N");

                ProjectComment projectCommentResult = projectCommentMapper.save(projectComment);
            }
        }

        projectTranslate.setCompanyName("Test1");
        projectTranslate.setTelNo("01012345678");
        projectTranslate.setRequestDate("2020-10-20");
        projectTranslate.setKorean("Y");
        projectTranslate.setEnglish("Y");
        projectTranslate.setChinese1("N");
        projectTranslate.setChinese2("N");
        projectTranslate.setJapanese("Y");
        projectTranslate.setVietnamese("N");
        projectTranslate.setTranslateVolume(100);
        projectTranslate.setTranslateFee(150000);
        projectTranslate.setCompleteDate("2020-10-25");
        projectTranslate.setCreater(1);
        projectTranslate.setDelYn("N");

        ProjectTranslate translateResult = translateMapper.save(projectTranslate);
    }else{
        System.out.println("저장이 되지 않았습니다.");
    }
    }

    @Test
    public void update() {
        Project project = new Project();
        project.setId(1);
        project.setContractName("변경된 테스트");
        project.setBusinessName("수출바우처B");
        project.setKind("쇼핑몰");
        project.setState("대기");
        project.setContent("변경된 테스트 내용");
        project.setAmount(20000);
        project.setDelYn("N");
        project.setCreater(15);
        project.setModifier(20);
        project.setContractDate("2020-09-28");

        Project projectResult = projectMapper.save(project);
    }*/
}
