package com.snbplus.madang.auth;

import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.user.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserAuthServiceTest {

    @Autowired
    UserAuthService userAuthService;

    @Test
    public void myTeamMemberList_대표() {

        User user = new User();
        /*OrgChart orgChart = new OrgChart();
        orgChart.setCode(1);*/

        Dept orgChart = new Dept();
        orgChart.setCode(1);

        user.setDeptChart(orgChart);

        log.info("" + userAuthService.myTeamMemberList(user).size());

    }

    @Test
    public void myTeamMemberList_실장() {

        User user = new User();
        Dept orgChart = new Dept();
        orgChart.setCode(7);

        user.setDeptChart(orgChart);

        log.info("" + userAuthService.myTeamMemberList(user).size());

    }

    @Test
    public void myTeamMemberList_팀장() {
        User user = new User();
        Dept orgChart = new Dept();
        orgChart.setCode(10);

        user.setDeptChart(orgChart);

        log.info("" + userAuthService.myTeamMemberList(user).size());
    }

    @Test
    public void devTeamMember_진팀장님(){
        User user = new User();

        user.setId(15);
        Dept orgChart = new Dept();
        orgChart.setCode(12);

        user.setDeptChart(orgChart);

        log.info("" + userAuthService.myTeamMemberList(user, "재직중").size());
    }

}