package com.snbplus.madang.holiWork;

import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.holiwork.HoliWork;
import com.snbplus.madang.holiwork.HoliWorkMapper;
import com.snbplus.madang.holiwork.HoliWorkSpec;
import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class HoliWorkMapperTest {
	
	@Autowired
	HoliWorkSpec workSpec;
	
	@Autowired
	HoliWorkMapper workMapper;
	
	@Autowired
	UserMapper userMapper;
	
    @Test
    public void 출장신청서() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<HoliWork> spec = Specifications.where(workSpec.build("N",null,null ));

        Page<HoliWork> p = workMapper.findAll(spec, pageable);

        log.info("출장신청서 - {}" , p.getContent());

    }
    
    @Test
    public void 출장신청서_상태like결재완료() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<HoliWork> spec = Specifications.where(workSpec.build("N", "state", "결재완료"));

        Page<HoliWork> p = workMapper.findAll(spec, pageable);

        log.info("출장신청서_상태like결재완료 - {}" ,p.getContent());
    }

    @Test
    public void 출장신청서_검색조건_작성자like노() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<HoliWork> spec = Specifications.where(workSpec.search("N", "creater", userMapper.findByNameLike("노").stream().map(user->user.getId()).collect(Collectors.toList())));
        
        log.info("spec :::"+spec.toString());

        Page<HoliWork> p = workMapper.findAll(spec, pageable);

        log.info("출장신청서_검색조건_작성자like노 - {}" ,p.getContent());

    }

}
