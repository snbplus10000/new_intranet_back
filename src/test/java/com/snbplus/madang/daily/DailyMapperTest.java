package com.snbplus.madang.daily;

import com.snbplus.madang.board.Board;
import com.snbplus.madang.board.BoardSpec;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class DailyMapperTest {

    @Autowired
    DailyMapper dailyMapper;

    @Autowired
    DailySpec dailySpec;

    @Test
    public void 일일보고_리스트() {
        Pageable pageable = new PageRequest(0, 10);

        Specification<Daily> spec = Specifications.where(dailySpec.build("N", Arrays.asList(27)));

        Page<Daily> p = dailyMapper.findAll(spec, pageable);

        log.info("일일보고_리스트 - {}" , p.getContent());
    }


    @Test
    public void 공지사항_검색조건_작성자like권경민() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Daily> spec = Specifications.where(dailySpec.build("N", "creater", "권경민", Arrays.asList(27)));

        Page<Daily> p = dailyMapper.findAll(spec, pageable);

        log.info("공지사항_검색조건_작성자like권경민 - {}" ,p.getContent());

    }

    @Test
    public void 공지사항_검색조건_작성자like신복호() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Daily> spec = Specifications.where(dailySpec.build("N", "creater", "신복호", Arrays.asList(27)));

        Page<Daily> p = dailyMapper.findAll(spec, pageable);

        log.info("공지사항_검색조건_작성자like신복호 - {}" ,p.getContent());

    }

}