package com.snbplus.madang.official;

import java.util.Optional;
import java.util.stream.Collectors;

import org.joda.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OfficialMapperTest {
	
	@Autowired
	OfficialMapper officialMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	OfficialSpec officialSpec;
	
    @Test
    public void 공문번호리스트() {

    	officialMapper.findAll().forEach(item -> log.info("{}", item));
    }
	
	@Test
    public void 공문번호() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<Official> spec = Specifications.where(officialSpec.build("N",null,null));

        Page<Official> p = officialMapper.findAll(spec, pageable);

        log.info("공문번호 - {}" , p.getContent());

    }
    
    @Test
    public void 공문번호_검색조건_작성자like박유현() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Official> spec = Specifications.where(officialSpec.search("N", "creater", userMapper.findByNameLike("박유현").stream().map(user->user.getId()).collect(Collectors.toList())));

        Page<Official> p = officialMapper.findAll(spec, pageable);

        log.info("공문번호_검색조건_작성자like박유현 - {}" ,p.getContent());

    }
    
    @Test
    public void update() throws Exception{
    	
    	String currentDate = LocalDate.now().toString();
    	   log.info("공문번호 - {}" , currentDate);

    }
    


}
