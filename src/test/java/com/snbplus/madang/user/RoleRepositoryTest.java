package com.snbplus.madang.user;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    public void findAllTest() {

        log.info("findAll Result {}", roleRepository.findAll());

    }

    @Test
    public void addRole() {
        Role role = new Role();
        role.setRoleName("테스트2");

        roleRepository.save(role);
    }

}