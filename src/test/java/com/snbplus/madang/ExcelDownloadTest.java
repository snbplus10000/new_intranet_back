package com.snbplus.madang;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ExcelDownloadTest {

	  private WebDriver driver;
	  private Map<String, Object> vars;
	  JavascriptExecutor js;
	  
	  @Before
	  public void setUp() {
		    driver = new ChromeDriver();
		    js = (JavascriptExecutor) driver;
		    vars = new HashMap<String, Object>();
	  }
	  
	  @After
	  public void tearDown() {
	    driver.quit();
	  }
	  @Test
	  public void project() {
		System.setProperty("webdriver.chrome.driver", "D:/workspace/dialect/WebContent/WEB-INF/webdriver/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
         options.addArguments("headless");
		  
	    driver.get("http://192.168.0.24/#/login");
	    driver.manage().window().setSize(new Dimension(945, 1020));
	    driver.findElement(By.id("id")).click();
	    driver.findElement(By.id("id")).sendKeys("bhshin");
	    driver.findElement(By.id("password")).sendKeys("tmakdlf2020!");
	    driver.findElement(By.id("password")).sendKeys(Keys.ENTER);
	    driver.switchTo().frame(1);
	    driver.findElement(By.cssSelector(".ng-scope:nth-child(8) .filterList > .ng-binding")).click();
	    driver.findElement(By.cssSelector(".txt")).click();
	    driver.findElement(By.cssSelector(".btnFunc:nth-child(2)")).click();
	    {
	      WebElement element = driver.findElement(By.cssSelector("div > .btnFunc:nth-child(3)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).clickAndHold().perform();
	    }
	    {
	      WebElement element = driver.findElement(By.cssSelector("div > .btnFunc:nth-child(3)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).perform();
	    }
	    {
	      WebElement element = driver.findElement(By.cssSelector("div > .btnFunc:nth-child(3)"));
	      Actions builder = new Actions(driver);
	      builder.moveToElement(element).release().perform();
	    }
	    driver.findElement(By.cssSelector("div > .btnFunc:nth-child(3)")).click();
	  }
	  
}
