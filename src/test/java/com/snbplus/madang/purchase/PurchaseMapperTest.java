package com.snbplus.madang.purchase;

import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class PurchaseMapperTest {
	
	@Autowired
	PurchaseMapper purchaseMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	PurchaseSpec purchaseSpec;
	
    @Test
    public void 구매품의서() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<Purchase> spec = Specifications.where(purchaseSpec.build("N",null,null));

        Page<Purchase> p = purchaseMapper.findAll(spec, pageable);

        log.info("구매품의서 - {}" , p.getContent());

    }
    
    @Test
    public void 구매품의서_상태like결재중() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Purchase> spec = Specifications.where(purchaseSpec.build("N", "state", "결재중"));

        Page<Purchase> p = purchaseMapper.findAll(spec, pageable);

        log.info("size :::" + p.getSize());
        log.info("구매품의서_상태like결재중 - {}" ,p.getContent());
    }

    @Test
    public void 구매품의서_검색조건_작성자like박유현() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Purchase> spec = Specifications.where(purchaseSpec.search("N", "creater", userMapper.findByNameLike("박유현").stream().map(user->user.getId()).collect(Collectors.toList())));

        Page<Purchase> p = purchaseMapper.findAll(spec, pageable);

        log.info("구매품의서_검색조건_작성자like박유현 - {}" ,p.getContent());

    }

}
