package com.snbplus.madang.busiTrip;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BusiTripCarMapperTest {
	
	@Autowired
	BusiTripCarMapper carMapper;
	
	@Autowired
	BusiTripCarSpec carSpec;
	
	@Autowired
	UserMapper userMapper;
	
    @Test
    public void 차량리스트() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiTripCar> spec = Specifications.where(carSpec.build("N"));

        Page<BusiTripCar> p = carMapper.findAll(spec, pageable);

        log.info("차량리스트 - {}" , p.getContent());
    }

    @Test
    public void 차량리스트_검색조건_소속like대한() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiTripCar> spec = Specifications.where(carSpec.build("N", "dept", "대한"));

        Page<BusiTripCar> p = carMapper.findAll(spec, pageable);

        log.info("차량리스트_검색조건_소속like대한 - {}" ,p.getContent());
    }

    @Test
    public void 차량리스트_검색조건_작성자like서() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiTripCar> spec = Specifications.where(carSpec.build("N", "creater", "서"));

        Page<BusiTripCar> p = carMapper.findAll(spec, pageable);

        log.info("차량리스트_검색조건_작성자like서 - {}" ,p.getContent());

    }
    
	
	
	

}
