package com.snbplus.madang.busiTrip;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BusiTripMapperTest {

    @Autowired
    BusiTripMapper busiTripMapper;

    @Test
    public void listPrintTest() {

        Pageable pageable = new PageRequest(0, 10);

        log.info("{}" , busiTripMapper.findByDelYn(pageable, "N").getContent());

    }

}