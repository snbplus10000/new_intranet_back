package com.snbplus.madang.busiTrip;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BusiTripTest {
	
	@Autowired
	BusiTripMapper busiTripMapper;
	
	@Autowired
	BusiTripMemberMapper busiTripMemberMapper;
	
	@Autowired
	BusiTripCarMapper busiTripCarMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@Test
	public void busiTripList() throws Exception{
		
		busiTripMapper.findAll().forEach(item -> log.info("{}", item));
	}
/*	
	@Test
	public void busiTripView() throws Exception{
		
		Optional<BusiTrip> busiTripOptional = busiTripMapper.findById(1);
		
		busiTripOptional.orElseThrow(() -> new Exception("정보를 찾을수 없습니다."));
		
		log.info("busiTrip" + busiTripOptional.get());
	
	}
	
	@Test
	public void busiTripWrite() throws Exception{
		
		BusiTrip busiTrip = new BusiTrip();
		BusiTripCar busiTripCar = new BusiTripCar();
		BusiTripMember busiTripMember = new BusiTripMember();
		User userResult = userMapper.findOne(20);
		
		List<BusiTripCar> carList = new ArrayList<BusiTripCar>();
		List<BusiTripMember> memberList = new ArrayList<BusiTripMember>();
		
		if(userResult != null) {
			
			busiTrip.setUser(userResult);
			busiTripMember.setUser(userResult);
		}
		
		busiTripCar.setUseGubun("1");
		busiTripCar.setStartGubun("2");
		busiTripCar.setStartDate("2019-01-01");
		busiTripCar.setStartDate("12:00 AM");
		
		carList.add(busiTripCar);
		memberList.add(busiTripMember);
				
		busiTrip.setYear("2019");
		busiTrip.setBusinessName("test");
		busiTrip.setCarNo("carNo");
		busiTrip.setTripArea("서울");
		
		busiTrip.setBusiTripCar(carList);
		busiTrip.setBusiTripMember(memberList);

		BusiTrip busiTripResult = busiTripMapper.save(busiTrip);
		
	}
	
*/
}
