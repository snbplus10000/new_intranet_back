package com.snbplus.madang.board;


import com.snbplus.madang.user.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.stereotype.Repository;

import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BoardMapperTest {

    @Autowired
    BoardMapper boardMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    BoardSpec boardSpec;

    @Test
    public void 공지사항() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Board> spec = Specifications.where(boardSpec.build("N", "notice"));

        Page<Board> p = boardMapper.findAll(spec, pageable);

        log.info("공지사항 - {}" , p.getContent());
    }

    @Test
    public void 공지사항_검색조건_제목like2018() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Board> spec = Specifications.where(boardSpec.build("N", "notice", "title", "2018년"));

        Page<Board> p = boardMapper.findAll(spec, pageable);

        log.info("공지사항_검색조건_제목like2018 - {}" ,p.getContent());
    }

    @Test
    public void 공지사항_검색조건_작성자like신복호() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Board> spec = Specifications.where(boardSpec.build("N", "notice", "creater", "신복호"));

        Page<Board> p = boardMapper.findAll(spec, pageable);

        log.info("공지사항_검색조건_작성자like신복호 - {}" ,p.getContent());

    }
    


}