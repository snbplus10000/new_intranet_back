package com.snbplus.madang.vacation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class VacationMapperTest {
	
	@Autowired
	VacationMapper vacationMapper;
	
	@Autowired
	VacationSpec vacationSpec;
	
	@Autowired
	UserMapper userMapper;
	
	    @Test
	    public void 휴가() {

	    	//보여줘야 하는 팀원 리스트
	       List<Integer> list = new ArrayList<Integer>();
	       
	       list.add(20);
	       list.add(23);
	       list.add(25);
	       list.add(27);
	    	
	        Pageable pageable = new PageRequest(0, 10);
	
	        Specification<Vacation> spec = Specifications.where(vacationSpec.search("N", list));
	
	        Page<Vacation> p = vacationMapper.findAll(spec, pageable);
	
	        log.info("휴가 - {}" , p.getContent());
	
	    }
	    
	    @Test
	    public void 휴가_이유like개인() {
			//보여줘야 하는 팀원 리스트
			List<Integer> list = new ArrayList<Integer>();

			list.add(20);
			list.add(23);
			list.add(25);
			list.add(27);
	
	        Pageable pageable = new PageRequest(0, 10);
	
	        Specification<Vacation> spec = Specifications.where(vacationSpec.build("N", "reason", "개인", list));
	
	        Page<Vacation> p = vacationMapper.findAll(spec, pageable);
	
	        log.info("휴가_이유like개인 - {}" ,p.getContent());
	    }
	
	    @Test
	    public void 휴가_검색조건_작성자like남 () {
			//보여줘야 하는 팀원 리스트
			List<Integer> list = new ArrayList<Integer>();

			list.add(13);
			list.add(16);
			list.add(18);
			list.add(20);
	
	        Pageable pageable = new PageRequest(0, 10);

	        Specification<Vacation> spec = Specifications.where(vacationSpec.build("N", "regName", "남", list));
	        
	        log.info("spec :::"+spec.toString());
	
	        Page<Vacation> p = vacationMapper.findAll(spec, pageable);
	
	        log.info("휴가_검색조건_작성자like남 - {}" ,p.getContent());
	
	    }
	    
	    @Test
	    public void 휴가병특_검색조건_해당연도해당월between201811() {
	    	
			String StartDate = "2018-11-01";
			String EndDate = "2018-11-31";
			//vacationMapper.findByStartDateAtBetweenOrderByIdDesc(StartDate,EndDate).forEach(item -> log.info("{}", item));
			vacationMapper.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate,EndDate,StartDate,EndDate).forEach(item -> log.info("{}", item));
	    	
	    	
	    }
	

}
