package com.snbplus.madang.common;

import com.snbplus.madang.common.constant.AreaCategory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CommonControllerTest {

    @Autowired
    CommonController commonController;

    @Test
    public void enumTest() throws Exception{

        System.out.println(Arrays.stream(commonController.getEnumInstance().get("area")).collect(Collectors.toList()));



        System.out.println(AreaCategory.valueOf("SEOUL"));
        System.out.println(AreaCategory.BUSAN.getKey());

        System.out.println(commonController.getEnumInstance().get("area"));
        Class enumClas = commonController.ENUM_CLASS.get("area");

        System.out.println(enumClas.isEnum());
        System.out.println(Enum.valueOf(enumClas, "BUSAN").name());
        Method[] methods = enumClas.getDeclaredMethods();
        for(Method m : methods) {
            System.out.println(m.getName());
        }

        Field[] fields = enumClas.getDeclaredFields();
        for(Field f : fields) {
            System.out.println(f.getName());
        }

        Field stringToEnumField = enumClas.getField("stringToEnum");
        System.out.println(((Map)stringToEnumField.get(enumClas)).keySet());

    }

}