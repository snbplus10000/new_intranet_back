package com.snbplus.madang.busiCard;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BusiCardCalcMapperTest {
	
    @Autowired
    UserAuthService userAuthService;
  
    @Autowired
    UserMapper userMapper;
    
    @Autowired
    BusiCardCalcSpec calcSpec;
    
    @Autowired
    BusiCardCalcMapper calcMapper;
    
    @Test
    public void 법인카드정산() {
    	
		   
       List<Integer> list = new ArrayList<Integer>();
       
       list.add(20);
       list.add(23);
       list.add(25);
       list.add(27);
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiCardCalc> spec = Specifications.where(calcSpec.build("N",null,null, list));

        Page<BusiCardCalc> p = calcMapper.findAll(spec, pageable);

        log.info("법인카드정산 - {}" , p.getContent());

    }
    
    @Test
    public void 법인카드정산_목적like야근() {
    	
        List<Integer> list = new ArrayList<Integer>();
        
        list.add(20);
        list.add(23);
        list.add(25);
        list.add(27);

        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiCardCalc> spec = Specifications.where(calcSpec.build("N", "purpose", "야근", list));

        Page<BusiCardCalc> p = calcMapper.findAll(spec, pageable);

        log.info("size :::" + p.getSize());
        log.info("법인카드정산_목적like야근 - {}" ,p.getContent());
    }

    @Test
    public void 법인카드정산_등록자like박() {

        List<Integer> list = new ArrayList<Integer>();
        
        list.add(20);
        list.add(23);
        list.add(25);
        list.add(27);
        
        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiCardCalc> spec = Specifications.where(calcSpec.build("N", "creater","박",list));

        Page<BusiCardCalc> p = calcMapper.findAll(spec, pageable);

        log.info("법인카드정산_등록자like박 - {}" ,p.getContent());

    }

}
