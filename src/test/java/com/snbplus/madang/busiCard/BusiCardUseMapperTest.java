package com.snbplus.madang.busiCard;

import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.user.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BusiCardUseMapperTest {

    @Autowired
    UserAuthService userAuthService;

    @Autowired
    BusiCardUseMapper busiCardUseMapper;
    
    @Autowired
    BusiCardUseSpec useSpec;

    @Test
    public void findByDelYnAAndCreaterTest() {

        User user = new User();
        /*OrgChart orgChart = new OrgChart();
        orgChart.setCode(10);
*/
        Dept orgChart = new Dept();
        orgChart.setCode(10);

        user.setDeptChart(orgChart);

        log.info("" + userAuthService.myTeamMemberList(user).size());

        List<Integer> userFilterList = userAuthService.myTeamMemberList(user).stream().map(userItem->userItem.getId()).collect(Collectors.toList());

        log.info("userFilterList"  + userFilterList);

        log.info("userFilterList"  + busiCardUseMapper.findByDelYnAndCreaterIn("N", userFilterList ) );

    }
    
    @Test
    public void 법인카드사용신청() {
    	
		   
       List<Integer> list = new ArrayList<Integer>();
       
       list.add(20);
       list.add(23);
       list.add(25);
       list.add(27);
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiCardUse> spec = Specifications.where(useSpec.build("N",null,null, list));

        Page<BusiCardUse> p = busiCardUseMapper.findAll(spec, pageable);

        log.info("법인카드사용신청 - {}" , p.getContent());

    }
    
    @Test
    public void 법인카드사용신청_등록자like신() {

        List<Integer> list = new ArrayList<Integer>();
        
        list.add(20);
        list.add(23);
        list.add(25);
        list.add(27);
        
        Pageable pageable = new PageRequest(0, 10);

        Specification<BusiCardUse> spec = Specifications.where(useSpec.build("N", "creater","신",list));

        Page<BusiCardUse> p = busiCardUseMapper.findAll(spec, pageable);

        log.info("법인카드사용신청_등록자like신 - {}" ,p.getContent());

    }

}