package com.snbplus.madang.outing;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.board.Board;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;

import static org.junit.Assert.*;

import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OutingMapperTest {

    @Autowired
    OutingMapper outingMapper;
    
    @Autowired
    UserMapper userMapper;
    
    @Autowired
    UserAuthService userAuthService;
    
    @Autowired
    OutingSpec outingSpec;

    @Test
    public void 출장신청서() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<Outing> spec = Specifications.where(outingSpec.build("N",null,null,"BT"));

        Page<Outing> p = outingMapper.findAll(spec, pageable);

        log.info("출장신청서 - {}" , p.getContent());

    }
    
    @Test
    public void 출장신청서_목적like국립국어원() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Outing> spec = Specifications.where(outingSpec.build("N", "purpose", "국립국어원","BT"));

        Page<Outing> p = outingMapper.findAll(spec, pageable);

        log.info("출장신청서_목적like국립국어원 - {}" ,p.getContent());
    }

    @Test
    public void 출장신청서_검색조건_작성자like박() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<Outing> spec = Specifications.where(outingSpec.search("N", "creater", userMapper.findByNameLike("박").stream().map(user->user.getId()).collect(Collectors.toList()),"BT"));

        log.info("spec :::"+spec.toString());

        Page<Outing> p = outingMapper.findAll(spec, pageable);

        log.info("출장신청서_검색조건_작성자like박 - {}" ,p.getContent());

    }
    
    @Test
    public void 출장병특_검색조건_해당연도해당월between201811() {
    	
		String StartDate = "2018-11-01";
		String EndDate = "2018-11-31";
		outingMapper.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate,EndDate,StartDate,EndDate).forEach(item -> log.info("{}", item));
    	
    	
    }
    

}