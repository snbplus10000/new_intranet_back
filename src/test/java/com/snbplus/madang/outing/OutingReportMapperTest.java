package com.snbplus.madang.outing;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OutingReportMapperTest {

    @Autowired
    OutingReportMapper reportMapper;
    
    @Autowired
    OutingMapper outingMapper;
    
    @Autowired
    UserMapper userMapper;
    
    @Autowired
    UserAuthService userAuthService;
    
    @Autowired
    OutingReportSpec reportSpec;

    @Test
    public void 출장보고서() {
    	
        Pageable pageable = new PageRequest(0, 10);

        Specification<OutingReport> spec = Specifications.where(reportSpec.build("N",null,null));

        Page<OutingReport> p = reportMapper.findAll(spec, pageable);

        log.info("출장보고서 - {}" , p.getContent());

    }
    
    @Test
    public void 출장보고서_목적like국립국어원() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<OutingReport> spec = Specifications.where(reportSpec.build("N", "purpose", "국립국어원"));

        Page<OutingReport> p = reportMapper.findAll(spec, pageable);

        log.info("size :::" + p.getSize());
        log.info("출장보고서_목적like국립국어원 - {}" ,p.getContent());
    }

    @Test
    public void 출장보고서_검색조건_작성자like신복호() {

        Pageable pageable = new PageRequest(0, 10);

        Specification<OutingReport> spec = Specifications.where(reportSpec.search("N", "creater", userMapper.findByNameLike("신복호").stream().map(user->user.getId()).collect(Collectors.toList())));

        Page<OutingReport> p = reportMapper.findAll(spec, pageable);

        log.info("출장보고서_검색조건_작성자like신복호 - {}" ,p.getContent());

    }
    
    @Test
    public void 출장보고서병특_검색조건_해당연도해당월between201811() {
    	
		String StartDate = "2018-11-01";
		String EndDate = "2018-11-31";
		//vacationMapper.findByStartDateAtBetweenOrderByIdDesc(StartDate,EndDate).forEach(item -> log.info("{}", item));
		reportMapper.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate,EndDate,StartDate,EndDate).forEach(item -> log.info("{}", item));
    	
    	
    }
	
	
}
