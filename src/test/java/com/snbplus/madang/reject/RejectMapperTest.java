package com.snbplus.madang.reject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RejectMapperTest {
	
	@Autowired
	RejectMapper rejectMapper;
	
	@Test
	public void 반려리스트() {
		
		Pageable pageable = new PageRequest(0, 10);
		
		Page<Reject> p = rejectMapper.findByDelYnAndReadState(pageable, "N", "N");
		
		log.info("반려리스트- {}" ,p.getContent());
		
	}

}
