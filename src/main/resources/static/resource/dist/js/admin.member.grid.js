$(function() {
	// Family Grid
	var familyDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/family",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.birth != null) {
								v.birth = kendo.toString(new Date(v.birth),'yyyy-MM-dd');
							}
						});
						
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/family",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				});
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/family",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/family",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
					regNum1 : {
					}
				}
			}
		}
	});

	$("#family-grid").kendoGrid({
		dataSource : familyDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'name',
			title : '성명'
		}, {
			field : "regNum1",
			title : "주민번호",
			format : "{0:c}",
		}, {
			field : "rel",
			title : "관계",
		}, {
			field : "acAbility",
			title : "학력",
		}, {
			field : "job",
			title : "직업",
		}, {
			field : "workplace",
			title : "근무처",
		}, {
			field : "withYn",
			title : "동거",
		}, {
			field : "depedentYn",
			title : "피부양자",
		}, {
			field : "fa",
			title : "가족수당",
		}, {
			field : "deathYn",
			title : "사망여부",
		}, {
			field : "birth",
			title : "생년월일",
            type: "datetime",
            format:"{0:yyyy-MM-dd}"
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	
	// School Grid
	var schoolDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/school",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.entDate != null) {
								v.entDate = kendo.toString(new Date(v.entDate),'yyyy-MM-dd');
							} 
							if(v.grdDate != null) {
								v.grdDate = kendo.toString(new Date(v.grdDate),'yyyy-MM-dd');
							}
						});
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/school",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/school",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/school",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
				}
			}
		}
	});

	$("#school-grid").kendoGrid({
		dataSource : schoolDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'acAbil',
			title : '학력명'
		}, {
			field : "finAcAbil",
			title : "최종학력",
			format : "{0:c}",
		}, {
			field : "entDate",
			title : "입학일자",
            format:"{0:yyyy-MM-dd}",
            parse: function(value) {
                return kendo.parseDate(value, "yyyy-MM-dd");
            },
		}, {
			field : "grdDate",
			title : "졸업(예정)일",
            format:"{0:yyyy-MM-dd}",
            parse: function(value) {
                return kendo.parseDate(value, "yyyy-MM-dd");
            },
		}, {
			field : "schName",
			title : "학교명",
		}, {
			field : "major",
			title : "전공명",
		}, {
			field : "subMajor",
			title : "부전공명",
		}, {
			field : "sciengiYn",
			title : "이공계여부",
		}, {
			field : "regionYn",
			title : "지방대여부",
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	
	// Licence Grid
	var licenceDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/licence",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.acqDate != null) {
								v.acqDate = kendo.toString(new Date(v.acqDate),'yyyy-MM-dd');
							} 
							if(v.expDate != null) {
								v.expDate = kendo.toString(new Date(v.expDate),'yyyy-MM-dd');
							}
						});
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/licence",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/licence",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/licence",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
				}
			}
		}
	});

	$("#licence-grid").kendoGrid({
		dataSource : licenceDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'cate',
			title : '분야'
		}, {
			field : "licence",
			title : "자격증명",
			format : "{0:c}",
		}, {
			field : "licenceNum",
			title : "자격증번호",
		}, {
			field : "pubOffice",
			title : "발행기관",
            format:"{0:yyyy-MM-dd}",
		}, {
			field : "acqDate",
			title : "취득일자",
		}, {
			field : "expDate",
			title : "유효일자",
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	
	// Language Grid
	var languageDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/language",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.acqDate != null) {
								v.acqDate = kendo.toString(new Date(v.acqDate),'yyyy-MM-dd');
							} 
						});
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/language",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/language",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/language",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
				}
			}
		}
	});

	$("#language-grid").kendoGrid({
		dataSource : languageDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'acqDate',
			title : '취득일자'
		}, {
			field : "cate",
			title : "외국어구분",
			format : "{0:c}",
		}, {
			field : "testName",
			title : "시험명",
		}, {
			field : "acqScore",
			title : "취득점수",
            format:"{0:yyyy-MM-dd}",
		}, {
			field : "score1",
			title : "구술/듣기점수",
		}, {
			field : "score2",
			title : "필기점수",
		}, {
			field : "ratio",
			title : "환산비율",
		}, {
			field : "validGrade",
			title : "유효성적",
		}, {
			field : "speakingRank",
			title : "스피킹등급",
		}, {
			field : "pubOffice",
			title : "인정기관",
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	
	// Salary Grid
	var salaryDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/salary",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.birth != null) {
								v.birth = kendo.toString(new Date(v.birth),'yyyy-MM-dd');
							}
						});
						
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/salary",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/salary",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/salary",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
					regNum1 : {
					}
				}
			}
		}
	});

	$("#salary-grid").kendoGrid({
		dataSource : salaryDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'year',
			title : '연도'
		}, {
			field : "salary",
			title : "연봉",
		}, {
			field : "basicPay",
			title : "기본급",
		}, {
			field : "dutyBenefit",
			title : "직책수당",
		}, {
			field : "overtimeBenefit",
			title : "시간외수당",
		}, {
			field : "transPay",
			title : "교통비",
		}, {
			field : "mealsPay",
			title : "식비",
		}, {
			field : "longBenefit",
			title : "근속수당",
		}, {
			field : "researchBenefit",
			title : "연구활동비",
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	

	// Holi Grid
	var holiDataSource = new kendo.data.DataSource({
		transport : {
			read : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				jQuery.ajax({
					type : "GET",
					url : "/admin/member/grid/" + id + "/holi",
					datatype : "JSON",
					contentType : "application/json",
					success : function(data) {
						if (data.result != "ok") {
							alert(data.msg);
							return;
						}
						
						$.each(data.json, function(i, v) {
							if(v.birth != null) {
								v.birth = kendo.toString(new Date(v.birth),'yyyy-MM-dd');
							}
						});
						
						options.success(data.json);
					}
				});
			},
			update : function(options) {
				console.log(options);
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "PUT",
						url : "/admin/member/grid/holi",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			destroy : function(options) {
				$.each(options.data.models, function(i, v) {
					jQuery.ajax({
						type : "DELETE",
						url : "/admin/member/grid/"+v.id+"/holi",
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			create : function(options) {
				var id = $(location).attr('pathname').split('/').pop();
				$.each(options.data.models, function(i, v) {
					var strJson = JSON.stringify(v);
					jQuery.ajax({
						type : "POST",
						url : "/admin/member/grid/"+id+"/holi",
						data : strJson,
						datatype : "JSON",
						contentType : "application/json",
						success : function(data) {
							if (data.result != "ok") {
								alert(data.msg);
								return;
							}
							options.success();
						}
					});
				})
			},
			parameterMap : function(options, operation) {
				if (operation !== "read" && options.models) {
					return {
						models : kendo.stringify(options.models)
					};
				}
			}
		},
		batch : true,
		pageSize : 20,
		schema : {
			model : {
				id : "id",
				fields : {
					name : {
						//editable : false,
						nullable : true
					},
					regNum1 : {
					}
				}
			}
		}
	});

	$("#holi-grid").kendoGrid({
		dataSource : holiDataSource,
		navigatable : true,
		height : 400,
		//toolbar : [ {name:"create", text:"추가"}, {name:"save", text:"저장"}, {name:"cancel", text:"취소"} ],
		columns : [ {
			hidden: true,
			field : 'id'
		}, {
			hidden: true,
			field : 'refId',
		}, {
			field : 'year',
			title : '연도'
		}, {
			field : "holiCnt",
			title : "연차개수",
		}, {
			field : "useCnt",
			title : "사용개수",
		}, {
			command : {text:"삭제", name:"destroy"},
			title : ""
		} ],
		editable : true
	});
	
	// button event
	$('.grid-add').on('click', function() {
		var grid = $(this).data('grid');
		var grid = $('#'+grid).data("kendoGrid");
		grid.addRow();
	});

	$('.grid-save').on('click', function() {
		var grid = $(this).data('grid');
		var grid = $('#'+grid).data("kendoGrid");
		grid.saveChanges();
	});

	$('.grid-cancel').on('click', function() {
		var grid = $(this).data('grid');
		var grid = $('#'+grid).data("kendoGrid");
		grid.cancelChanges();
	});
	
	function datepickerEditor(container, options) {
		$('<input type="text" name="'+options.field+'" class="kendo-datepicker"/>').appendTo(container);
		$('.kendo-datepicker').kendoDatePicker()
	}
});