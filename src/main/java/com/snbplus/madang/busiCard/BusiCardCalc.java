package com.snbplus.madang.busiCard;

import java.sql.Timestamp;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.user.User;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "busi_card_calc")
@Data
public class BusiCardCalc {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String useName; // 사용자

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creater", insertable=false, updatable=false)
	private User user;
	
	private String usedPlace; // 사용처
	
	private String startDate; //시작일
	
	private String endDate; //종료일
	
	private Integer amount; // 금액
	
	private String accTitle; //계정과목
	
	private String state; // 상태
	
	private String purpose; //목적
	
	private String comment; //비고
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	private String dept; // 부서명
	
	private String people; //인원
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부

	@ManyToOne
	@JoinColumn(name="cardId")
	private BusiCard card; // 사용카드

	@OneToMany(mappedBy = "calc", cascade=CascadeType.ALL, orphanRemoval = true)
	private List<BusiCardFile> files = new ArrayList<>();
	
	@Transient
	private Integer cardNo;

	@Transient
	private Approval approval;
	
	@Transient
	private String imgUrl;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;

	private Integer purchaseId;
}
