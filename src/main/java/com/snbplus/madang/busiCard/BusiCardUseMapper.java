package com.snbplus.madang.busiCard;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BusiCardUseMapper extends JpaRepository<BusiCardUse, Integer> , JpaSpecificationExecutor<BusiCardUse> {

	Page<BusiCardUse> findByDelYn(Pageable pageable, String delYn);

	Page<BusiCardUse> findByDelYnAndCreaterIn(Pageable pageable, String delYn, List<Integer> creater);

	List<BusiCardUse> findByDelYnAndCreaterIn(String delYn, List<Integer> creater);
	
	@Query(value = "SELECT b.* FROM busi_card_use b left outer join approval a"
			+ " on b.id = a.ref_id and a.ref_table = :refTable where b.del_yn = :delYn and b.creater in :userFilterList and a.step = :step"
			,nativeQuery = true)
	List<BusiCardUse> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn")String delYn, @Param("userFilterList")List<Integer> userFilterList, @Param("step")Integer step);

	List<BusiCardUse> findByCreaterAndState(Integer id, String state);

	@Query(value="select b from BusiCardUse b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardUse b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
	Page<BusiCardUse> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardUse b join fetch b.user u where b.state like %:state% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardUse b inner join b.user u where b.state like %:state% and b.delYn=:delYn")
	Page<BusiCardUse> findByStateContainingAndDelYn(@Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardUse b join fetch b.user u where b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardUse b inner join b.user u where b.delYn=:delYn")
	Page<BusiCardUse> findAllByDelYn(@Param("delYn")String delYn, Pageable pageable);

}
