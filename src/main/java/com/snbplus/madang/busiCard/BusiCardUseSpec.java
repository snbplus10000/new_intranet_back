package com.snbplus.madang.busiCard;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.user.UserMapper;

@Component
public class BusiCardUseSpec  {
	
	@Autowired
	UserMapper userMapper;

	public Specification<BusiCardUse> build(String delYn, String conditionKey, String conditionValue, List userAuthList) {
		
        if(conditionKey == null || conditionKey.equals("")) {
            return search(delYn,userAuthList);
        }else if(conditionKey != null && conditionKey.equals("creater")) {
        	return search(delYn,conditionKey,userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()),userAuthList);
        }

        return search(delYn, conditionKey, "%" + conditionValue + "%", userAuthList);

	}



	public Specification<BusiCardUse> search(String delYn, String conditionKey, String conditionValue, List userAuthList) {
		
        return (root, query, cb) ->{
	        Path<Object> path = root.get(conditionKey);
	        CriteriaBuilder.In<Object> in = cb.in(path);
	        for (Object conditionColumnValue : userAuthList) {
	             
	        	in.value(conditionColumnValue);
	             	                           	
	        }
	        return cb.and(
	                cb.equal(root.get("delYn"),  delYn),
	                cb.like(root.get(conditionKey), conditionValue),
                    in
	        );
        };
	}

	public Specification<BusiCardUse> search(String delYn, String conditionKey, List conditionValue, List userAuthList) {
		
        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : conditionValue) {
            	for(Object number : userAuthList) {
            		
            		if(number == conditionColumnValue) {
            			  in.value(conditionColumnValue);        
            			  break;
            		}
            	}
                 	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };
	}


	public Specification<BusiCardUse> search(String delYn, List userAuthList) {
		
        return (root, query, cb) ->{
	        Path<Object> path = root.get("creater");
	        CriteriaBuilder.In<Object> in = cb.in(path);
	        for (Object conditionColumnValue : userAuthList) {
	             
	        	in.value(conditionColumnValue);
	             	                           	
	        }
        
	      return  cb.and(
	                cb.equal(root.get("delYn"),  delYn),
                    in
	        );
        };
	}



}
