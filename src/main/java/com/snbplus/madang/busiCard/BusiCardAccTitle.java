package com.snbplus.madang.busiCard;

import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.newApproval.ApprovalInfo;
import com.snbplus.madang.user.User;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "busi_card_acc_title")
@Data
public class BusiCardAccTitle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String accTitle; // 사용자
}
