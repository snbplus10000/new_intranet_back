package com.snbplus.madang.busiCard;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@RestController
@RequestMapping("/busiCardUse")
@RequiredArgsConstructor
public class BusiCardUseController implements InterfaceController<BusiCardUse> {

	private final BusiCardUseMapper useMapper;

	private final BusiCardMapper cardMapper;

	private final UserMapper userMapper;

	private final UserAuthService userAuthService;

	private final BusiCardUseSpec usespec;

	@ApiOperation(value = "법인카드사용신청 리스트", httpMethod = "GET", notes = "법인카드사용신청 리스트")
	@Override
	public ResponseEntity<Page<BusiCardUse>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {

		Page<BusiCardUse> busiCardUseList = null;

		if(conditionKey == null){
			conditionKey="default";
		}
		if(conditionValue==null){
			conditionValue="";
		}

		switch (conditionKey) {
			case "creater":  busiCardUseList = useMapper.findByCreatorContainingAndDelYn(conditionValue, "N", pageable);
				break;
			case "state":  busiCardUseList = useMapper.findByStateContainingAndDelYn(conditionValue, "N", pageable);
				break;
			default: busiCardUseList = useMapper.findAllByDelYn("N",pageable);
				break;
		}

		return new ResponseEntity<>(busiCardUseList, HttpStatus.OK);
	}

	
	@ApiOperation(value = "법인카드사용신청 상세화면", httpMethod = "GET", notes = "법인카드사용신청 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "법인카드사용신청 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiCardUse> view(@PathVariable Integer id) {
		BusiCardUse t = useMapper.findOne(id);
		User user = userMapper.findOne(t.getCreater());
    	/*user.getDeptChart().setDeptUser(null);
    	user.getDeptChart().setHighDeptUser(null);
    	user.getHighDeptChart().setDeptUser(null);
    	user.getHighDeptChart().setHighDeptUser(null);*/
		t.setUser(user);
				
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드사용신청 저장", httpMethod = "PUT", notes = "법인카드사용신청 저장")
	@Override
	public ResponseEntity<BusiCardUse> save(@RequestBody BusiCardUse t) {
		User user = UserSingleton.getInstance();
		t.setUser(null);
		t.setCreater(user.getId());
		t.setCard(cardMapper.findOne(t.getCardNo()));
		t.setDelYn("N");
		useMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드사용신청 수정", httpMethod = "POST", notes = "법인카드사용신청 수정")
	@Override
	public ResponseEntity<BusiCardUse> update( @RequestBody BusiCardUse t) {
		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		t.setCard(cardMapper.findOne(t.getCardNo()));
		useMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드사용신청 삭제", httpMethod = "DELETE", notes = "법인카드사용신청 수정")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "법인카드사용신청 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<BusiCardUse> delete(@PathVariable Integer id) {
		
		User user = UserSingleton.getInstance();
		
		BusiCardUse busiCardUse = useMapper.findOne(id);
		busiCardUse.setDelYn("Y");
		busiCardUse.setModifier(user.getId());
		
		useMapper.save(busiCardUse);
		
		return new ResponseEntity<>(busiCardUse, HttpStatus.OK);
	}
	
	@GetMapping("/reviewList")
	@ApiOperation(value="법인카드사용신청 결재 대기 리스트", httpMethod = "GET", notes="법인카드사용신청 결재 대기 리스트")
	public ResponseEntity<List<BusiCardUse>> reviewList(){
		User curUser = UserSingleton.getInstance();
		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());
	
		return new ResponseEntity<>(useMapper.findByDelYnAndCreatorIn("BUSI_CARD_USE", "N", userFilterList, step).stream().map(busiCardUse ->{
			 
			Integer id = busiCardUse.getCreater();
			User user = userMapper.findOne(id);
			
			String imgUrl = "";
			
			if(user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null)  {
				imgUrl = "static/img/default-avatar.png";
			}else {
				imgUrl = "/intranet/api/file/image/"+user.getThumbFile().getFileAttach().getId();
			}
			busiCardUse.setImgUrl(imgUrl);
						 
			return busiCardUse;
			 
		}).collect(Collectors.toList()), HttpStatus.OK);
		
	}
	
	@GetMapping("/rejectList")
	@ApiOperation(value="법인카드 정산 반려 리스트", httpMethod = "GET", notes = "법인카드 정산 반려 리스트")
	public ResponseEntity<List<BusiCardUse>> rejectList(){
		
		return new ResponseEntity<>(useMapper.findByCreaterAndState(UserSingleton.getInstance().getId(),"반려"), HttpStatus.OK);
		
	}


}
