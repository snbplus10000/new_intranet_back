package com.snbplus.madang.busiCard;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BusiCardCalcMapper extends JpaRepository<BusiCardCalc, Integer>, JpaSpecificationExecutor<BusiCardCalc> {

	Page<BusiCardCalc> findByDelYn(Pageable pageable, String delYn);

	Page<BusiCardCalc> findByDelYnAndCreaterIn(Pageable pageable, String string, List<Integer> userFilterList);

	@Query(value = "SELECT b.* FROM busi_card_calc b left outer join approval a"
			+ " on b.id = a.ref_id and a.ref_table = :refTable where b.del_yn = :delYn and b.creater in :userFilterList and a.step = :step"
			,nativeQuery = true)
	List<BusiCardCalc> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn")String delYn, @Param("userFilterList")List<Integer> userFilterList, @Param("step")Integer step);

	List<BusiCardCalc> findByCreaterAndState(Integer id, String state);

	@Query(value="select b from BusiCardCalc b join fetch b.user u where b.accTitle like %:accTitle% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardCalc b inner join b.user u where b.accTitle like %:accTitle% and b.delYn=:delYn")
	Page<BusiCardCalc> findByAccTitleContainingAndDelYn(@Param("accTitle")String accTitle, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardCalc b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardCalc b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
	Page<BusiCardCalc> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardCalc b join fetch b.user u join fetch b.card c where concat(c.cardCom, ' - ', c.cardName) like %:cardName% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardCalc b inner join b.user u inner join b.card c where concat(c.cardCom, ' - ', c.cardName) like %:cardName% and b.delYn=:delYn")
	Page<BusiCardCalc> findByCardNameContainingAndDelYn(@Param("cardName")String cardName, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardCalc b join fetch b.user u where b.purpose like %:purpose% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardCalc b inner join b.user u where b.purpose like %:purpose% and b.delYn=:delYn")
	Page<BusiCardCalc> findByPurposeContainingAndDelYn(@Param("purpose")String purpose, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from BusiCardCalc b join fetch b.user u where b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from BusiCardCalc b inner join b.user u where b.delYn=:delYn")
	Page<BusiCardCalc> findAllByDelYn(@Param("delYn")String delYn, Pageable pageable);
}
