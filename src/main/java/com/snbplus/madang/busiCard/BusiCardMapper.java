package com.snbplus.madang.busiCard;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusiCardMapper extends JpaRepository<BusiCard, Integer> {

	Page<BusiCard> findByDelYn(Pageable pageable, String delYn);

}
