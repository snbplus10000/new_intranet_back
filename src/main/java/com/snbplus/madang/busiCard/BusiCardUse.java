package com.snbplus.madang.busiCard;

import java.sql.Timestamp;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "busi_card_use")
@Data
public class BusiCardUse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String useNem; // 사용자

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creater", insertable=false, updatable=false)
	private User user;
	
	private String recvNem; // 수령인
	
	private String usedPlace; // 사용처
	
	private String startDate; // 시작일 
	
	private String endDate; // 종료일
	
	private Integer expAmount; // 예상금액
	
	private String state; // 상태값
	
	private String purpose; // 사용목적
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부

	@ManyToOne
	@JoinColumn(name="cardId")
	private BusiCard card; // 사용카드
	
	@Transient
	private Integer cardNo;
	
	@Transient
	private String imgUrl;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;


}
