package com.snbplus.madang.busiCard;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.outing.OutingReport;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/busiCard")
public class BusiCardController implements InterfaceController<BusiCard> {

	@Autowired
	BusiCardMapper cardMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	UserAuthService userAuthService;

	@ApiOperation(value = "법인카드 리스트", httpMethod = "GET", notes = "법인카드 리스트")
	@Override
	public ResponseEntity<Page<BusiCard>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 9999) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {
		return new ResponseEntity<>(cardMapper.findByDelYn(pageable, "N").map(busiCard -> {

			User searchUser;
			Integer id = 0;
			User user = new User();
			id = busiCard.getCreater();
			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			busiCard.setUser(user);
			return busiCard;
		}), HttpStatus.OK);

	}

	@ApiOperation(value = "법인카드 상세화면", httpMethod = "GET", notes = "법인카드 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "법인카드 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<BusiCard> view(@PathVariable Integer id) {

		return new ResponseEntity<>(cardMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드 저장", httpMethod = "PUT", notes = "법인카드 저장")
	@Override
	public ResponseEntity<BusiCard> save(@RequestBody BusiCard t) {

		User user = UserSingleton.getInstance();
		t.setCreater(user.getId());
		cardMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드 수정", httpMethod = "POST", notes = "법인카드 삭제")
	@Override
	public ResponseEntity<BusiCard> update(@RequestBody BusiCard t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		cardMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "법인카드 삭제", httpMethod = "delete", notes = "법인카드 수정")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "법인카드 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<BusiCard> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		BusiCard busiCard = cardMapper.findOne(id);
		busiCard.setModifier(user.getId());
		busiCard.setDelYn("Y");

		cardMapper.save(busiCard);

		return new ResponseEntity<>(busiCard, HttpStatus.OK);
	}

}
