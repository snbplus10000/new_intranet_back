package com.snbplus.madang.busiCard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.common.FileAttach;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "busi_card_file")
@Data
public class BusiCardFile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@JoinColumn(name = "busiCardCalcId")
	@ManyToOne(fetch = FetchType.LAZY)
	@ToString.Exclude
	@JsonIgnore
	private BusiCardCalc calc;

	@JoinColumn(name = "fileId")
	@ManyToOne(fetch = FetchType.LAZY)
	private FileAttach fileAttach;
}
