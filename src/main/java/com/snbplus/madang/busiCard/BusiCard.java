package com.snbplus.madang.busiCard;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "busi_card")
@Data
public class BusiCard {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String cardNum1; // 카드번호1
	
	private String cardNum2; // 카드번호2
	
	private String cardNum3; // 카드번호3
	
	private String cardNum4; // 카드번호4
	
	private String expYear; // 만료년도
	
	private String expMonth; // 만료월
	
	private String cvcNum; // cvc
	
	private String cardCom; // 카드사
	
	private String cardName; // 카드명
	
	private String state; // 상태값
	
	private String comment; // 비고
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private User user;
	
	@Transient
	private Approval approval;
}
