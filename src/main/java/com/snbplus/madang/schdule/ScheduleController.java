package com.snbplus.madang.schdule;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
/**
 * 스케줄 관련 vo - 현재
 * @author bhshin
 *
 */
@Slf4j
@RestController
@RequestMapping("/schedule")
public class ScheduleController {
	
	@Autowired
	ScheduleMapper scheduleMapper;
	
	@GetMapping("")
	@ApiOperation(value = "스케줄 리스트", httpMethod = "GET", notes = "스케줄 리스트")
	public ResponseEntity<List<Schedule>> scheduleList(Pageable pageable) {
		
		return new ResponseEntity<>(scheduleMapper.findAllByDelYn(pageable, "N"), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@ApiOperation(value = "스케줄 상세화면", httpMethod = "GET", notes = "스케줄 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "스케줄 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	public ResponseEntity<Schedule> view(@PathVariable Integer id) {
		
		return new ResponseEntity<>(scheduleMapper.findOne(id), HttpStatus.OK);
	}

	
	@PutMapping("")
	@Transactional
	@ApiOperation(value = "스케줄 저장", httpMethod = "PUT", notes = "스케줄 저장")
	public ResponseEntity<Schedule> save(@RequestBody Schedule t) {
	
		User user =  UserSingleton.getInstance();
		t.setCreater(user.getId());
		scheduleMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}


	@PostMapping("/")
	@Transactional
	@ApiOperation(value = "스케줄 수정", httpMethod = "POST", notes = "스케줄 삭제")
	public ResponseEntity<Schedule> update(Schedule t) {
		
		User user =  UserSingleton.getInstance();
		t.setModifier(user.getId());
		scheduleMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}


	@DeleteMapping("/{id}")
	@ApiOperation(value = "스케줄 삭제", httpMethod = "DELETE", notes = "스케줄 수정")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "스케줄 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	public ResponseEntity<Schedule> delete(Integer id) {
		User user =  UserSingleton.getInstance();
		Schedule t = scheduleMapper.findOne(id);
		t.setModifier(user.getId());
		t.setDelYn("Y");
		scheduleMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

}
