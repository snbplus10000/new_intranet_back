package com.snbplus.madang.sms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/sms")
public class SmsController implements InterfaceController<Sms> {
	
	@Autowired
	SmsMapper smsMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@ApiOperation(value = "SMS 리스트", httpMethod = "GET", notes = "SMS 리스트")
	@Override
	public ResponseEntity<Page<Sms>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {

		return new ResponseEntity<>(smsMapper.findByDelYn(pageable,"N").map(sms ->{
			
			User searchUser = userMapper.findOne(sms.getCreater());
			User user = new User();
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			sms.setUser(user);
			return sms;
		}), HttpStatus.OK);
	}

	@ApiOperation(value = "SMS 상세화면", httpMethod = "GET", notes = "SMS 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "SMS 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<Sms> view(@PathVariable Integer id) {
		
		return new ResponseEntity<>(smsMapper.findOne(id),HttpStatus.OK);
	}

	@ApiOperation(value = "SMS 저장", httpMethod = "PUT", notes = "SMS 저장")
	@Override
	public ResponseEntity<Sms> save(@RequestBody Sms t) {
		
		User user = UserSingleton.getInstance();
		
		t.setCreater(user.getId());
		
		smsMapper.save(t);
		SmsInfo smsInfo = new SmsInfo();
		
		if(t.getId() != null && t.getId() != 0) {
			smsInfo.setId("snbkorea");
			smsInfo.setPw("korea2016!");
			smsInfo.setTxt(t.getMsg());
			smsInfo.setNum("01083214580");	//api 규칙상 설정된 번호로만 됨
			smsInfo.setRecv(t.getReceiver());
			//smsInfo.setReturn_url("http://madang.snbplus.co.kr:5000/api/sms/return");	//필요없어서 주석
		}
		String url = "http://daemon.smstong.co.kr:8888/";
		try {
			smsSend(smsInfo, url);
		}catch (Exception e) {
			return new ResponseEntity<>(t,HttpStatus.NOT_EXTENDED);
		}
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "SMS 수정", httpMethod = "POST", notes = "SMS 수정")
	@Override
	public ResponseEntity<Sms> update(@RequestBody Sms t) {
		
		User user = UserSingleton.getInstance();
		
		t.setModifier(user.getId());
		
		smsMapper.save(t);
		SmsInfo smsInfo = new SmsInfo();
		
		if(t.getId() != null && t.getId() != 0) {
			smsInfo.setId("snbkorea");
			smsInfo.setPw("korea2016!");
			smsInfo.setTxt(t.getMsg());
			smsInfo.setNum(t.getSender());
			smsInfo.setRecv(t.getReceiver());
			smsInfo.setReturn_url("http://madang.snbplus.co.kr:5000/api/sms/return");
		}
		/*
		String url = "http://smstong.co.kr/cps/";
		try {
			smsSend(smsInfo, url);
		}catch (Exception e) {
			return new ResponseEntity<>(t,HttpStatus.NOT_EXTENDED);
		}
		*/
		return new ResponseEntity<>(t,HttpStatus.OK);
	}
	@ApiOperation(value = "SMS 삭제", httpMethod = "DELETE", notes = "SMS 삭제")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "SMS 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<Sms> delete(@PathVariable Integer id) {
		
		User user = UserSingleton.getInstance();
		
		Sms t = smsMapper.findOne(id);
		
		t.setDelYn("Y");
		t.setModifier(user.getId());
		
		smsMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}
	
	public void smsSend(SmsInfo smsInfo, String url) throws IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(url);

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", smsInfo.getId()));
		params.add(new BasicNameValuePair("pw", smsInfo.getPw()));
		params.add(new BasicNameValuePair("from", smsInfo.getNum()));
		params.add(new BasicNameValuePair("to", smsInfo.getRecv()));
		params.add(new BasicNameValuePair("msg", new String(smsInfo.getTxt().getBytes("utf-8"), "iso-8859-1") ));
		params.add(new BasicNameValuePair("indexkey", "100000"));
		httpPost.setEntity(new UrlEncodedFormEntity(params));

		httpPost.setEntity(new UrlEncodedFormEntity(params));
		CloseableHttpResponse response = client.execute(httpPost);  

        BufferedReader reader = new BufferedReader(new InputStreamReader(
                response.getEntity().getContent()));
		
        String inputLine;
        StringBuffer sb = new StringBuffer();
        
        while ((inputLine = reader.readLine()) != null) {
        	sb.append(new String(inputLine.getBytes() ));
        }
        
        reader.close();
        System.out.println(sb.toString());

		client.close();
	}
	
	@GetMapping("/return")
	public ResponseEntity<SmsInfo> smsGet(@RequestBody SmsInfo t){
		
		String resultCodeString = t.getResult_code();
		Integer indexkey = t.getIndexkey();
		
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}


}
