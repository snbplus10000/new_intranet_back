package com.snbplus.madang.sms;

import lombok.Data;

@Data
public class SmsInfo {
	
	private String id; // 문자발송 아이디
	
	private String pw; // 문자 발송 passwod
	
	private String txt; // 보내는 내용
	
	private String num; // 문자 보내는 사랑
	
	private String recv; // 문자받는 사람
	
	private String return_url;
	
	private String result_code;
	
	private Integer indexkey;

}
