package com.snbplus.madang.userCard;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.busiCard.BusiCardCalc;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/userCardCalc")
public class UserCardCalcController implements InterfaceController<UserCardCalc> {

	@Autowired
	UserCardCalcMapper calcMapper;
	
	@Autowired
	UserMapper userMapper;

	@Autowired
	UserAuthService userAuthService;

	@Autowired
	UserCardCalcSpec calcSpec;
	
	@ApiOperation(value = "개인카드정산 리스트", httpMethod = "GET", notes = "개인카드정산 리스트")
	@Override
	public ResponseEntity<Page<UserCardCalc>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Page<UserCardCalc> busiCardCalcList = null;

		if(conditionKey == null){
			conditionKey="default";
		}
		if(conditionValue==null){
			conditionValue="";
		}

		switch (conditionKey) {
			case "creater":  busiCardCalcList = calcMapper.findByCreatorContainingAndDelYn(conditionValue, "N", pageable);
				break;
			case "state":  busiCardCalcList = calcMapper.findByStateContainingAndDelYn(conditionValue, "N", pageable);
				break;
			case "purpose":  busiCardCalcList = calcMapper.findByPurposeContainingAndDelYn(conditionValue, "N", pageable);
				break;
			default: busiCardCalcList = calcMapper.findAllByDelYn("N", pageable);
				break;
		}

		return new ResponseEntity<>(busiCardCalcList, HttpStatus.OK);

	}

	@ApiOperation(value = "개인카드정산 상세화면", httpMethod = "GET", notes = "개인카드정산 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "개인카드정산 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<UserCardCalc> view(@PathVariable Integer id) {
		UserCardCalc t = calcMapper.findOne(id);
		User user = userMapper.findOne(t.getCreater());
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		t.setUser(user);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "개인카드정산 저장", httpMethod = "PUT", notes = "개인카드정산 저장")
	@Override
	public ResponseEntity<UserCardCalc> save(@RequestBody UserCardCalc t) {

		User user = UserSingleton.getInstance();
		t.setUser(null);
		t.setCreater(user.getId());
		t.setDelYn("N");

		calcMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "개인카드정산 수정", httpMethod = "POST", notes = "개인카드정산 삭제")
	@Override
	public ResponseEntity<UserCardCalc> update(@RequestBody UserCardCalc t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		calcMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "개인카드정산 삭제", httpMethod = "DELETE", notes = "개인카드정산 수정")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "개인카드정산 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<UserCardCalc> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		UserCardCalc UserCardCalc = calcMapper.findOne(id);
		UserCardCalc.setDelYn("Y");
		UserCardCalc.setModifier(user.getId());

		calcMapper.save(UserCardCalc);

		return new ResponseEntity<>(UserCardCalc, HttpStatus.OK);
	}

	@GetMapping("/users")
	public ResponseEntity<List<User>> getLists() {
		List<User> lists = userMapper.findUserCalcCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@GetMapping("/reviewList")
	@ApiOperation(value = "개인카드정산 결재 대기 리스트", httpMethod = "GET", notes = "개인카드정산 결재 대기 리스트")
	public ResponseEntity<List<UserCardCalc>> reviewList() {
		User curUser = UserSingleton.getInstance();
		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(calcMapper.findByDelYnAndCreatorIn("USER_CARD_CALC", "N", userFilterList, step)
				.stream().map(UserCardCalc -> {

					Integer id = UserCardCalc.getCreater();
					User user = userMapper.findOne(id);

					String imgUrl = "";

					if (user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null) {
						imgUrl = "static/img/default-avatar.png";
					} else {
						imgUrl = "/intranet/api/file/image/" + user.getThumbFile().getFileAttach().getId();
					}
					UserCardCalc.setImgUrl(imgUrl);

					return UserCardCalc;

				}).collect(Collectors.toList()), HttpStatus.OK);

	}

	@GetMapping("/rejectList")
	@ApiOperation(value = "개인카드 정산 반려 리스트", httpMethod = "GET", notes = "개인카드 정산 반려 리스트")
	public ResponseEntity<List<UserCardCalc>> rejectList() {

		return new ResponseEntity<>(calcMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
				HttpStatus.OK);

	}

}
