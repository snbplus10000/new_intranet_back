package com.snbplus.madang.userCard;

import java.util.List;

import com.snbplus.madang.busiCard.BusiCardCalc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserCardCalcMapper extends JpaRepository<UserCardCalc, Integer>, JpaSpecificationExecutor<UserCardCalc> {

	@Query(value = "SELECT b.* FROM use_card_calc b left outer join approval a"
			+ " on b.id = a.ref_id and a.ref_table = :refTable where b.del_yn = :delYn and b.creater in :userFilterList and a.step = :step"
			,nativeQuery = true)
	List<UserCardCalc> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn")String delYn, @Param("userFilterList")List<Integer> userFilterList, @Param("step")Integer step);

	List<UserCardCalc> findByCreaterAndState(Integer id, String state);

	@Query(value="select b from UserCardCalc b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from UserCardCalc b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
	Page<UserCardCalc> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from UserCardCalc b join fetch b.user u where b.state like %:state% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from UserCardCalc b inner join b.user u where b.state like %:state% and b.delYn=:delYn")
	Page<UserCardCalc> findByStateContainingAndDelYn(@Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from UserCardCalc b join fetch b.user u where b.purpose like %:purpose% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from UserCardCalc b inner join b.user u where b.purpose like %:purpose% and b.delYn=:delYn")
	Page<UserCardCalc> findByPurposeContainingAndDelYn(@Param("purpose")String purpose, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from UserCardCalc b join fetch b.user u where b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from UserCardCalc b inner join b.user u where b.delYn=:delYn")
	Page<UserCardCalc> findAllByDelYn(@Param("delYn")String delYn, Pageable pageable);
}
