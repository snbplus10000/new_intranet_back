package com.snbplus.madang.car;

import com.snbplus.madang.user.User;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "tbl_car")
@Data
public class Car {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String kind; //차종
	
	private String carNo; //차번호

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";

	private Timestamp beginContract;

	private Timestamp endContract;

	private String contractShop;

	@CreatedDate
	@Column(updatable = false, nullable= false)
	@CreationTimestamp
	private Timestamp  created;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private Integer creater; // 입력자
	
	private Integer modifier; // 수정자

	@Transient
	private User user;

	@Transient
	private List<CarLog> log;

	private String carYear;

	private String carModel;

	private String carManagerName;

	private String carManagerPhone;

}
