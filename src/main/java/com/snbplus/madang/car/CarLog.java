package com.snbplus.madang.car;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "tbl_car_log")
@Data
public class CarLog {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private Integer ref;

    private Timestamp usedDate;

    private String content;

    private Integer travelDistance;

    private String repairShop;

    private Integer cost;

    private String comment;

    @CreatedDate
    @Column(updatable = false, nullable= false)
    @CreationTimestamp
    private Timestamp createdDate;

    private String delYn;
}
