package com.snbplus.madang.car;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarMapper extends JpaRepository<Car, Integer> {

	@Query(value = "SELECT * FROM tbl_car c WHERE id in (SELECT DISTINCT car_id FROM tbl_busi_trip_car where del_yn = 'N') AND del_yn = 'N'", nativeQuery = true)
	List<Car> busiCarLists();

	List<Car> findByDelYn(String delYn);
	
	Page<Car> findByDelYn(Pageable pageable, String delYn);
	
	List<Car> findByCarNoLike(String carNo);
}
