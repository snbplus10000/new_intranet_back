package com.snbplus.madang.car;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.busiTrip.BusiTripCarMapper;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Clock;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController implements InterfaceController<Car> {

	private final CarMapper carMapper;
	private final CarLogMapper carLogMapper;
	private final UserMapper userMapper;
	private final BusiTripCarMapper busiTripCarMapper;


	@ApiOperation(value = "차량 리스트", httpMethod = "GET", notes = "차량 리스트")
	@Override
	public ResponseEntity<Page<Car>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {
		return new ResponseEntity<>(carMapper.findByDelYn(pageable, "N").map(car -> {

			User user = userMapper.findOne(car.getCreater());
			/*user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);*/
			car.setUser(user);
			car.setLog(carLogMapper.findByRefAndDelYnOrderByCreatedDate(car.getId(), "N"));
			return car;

		}), HttpStatus.OK);
	}

	@ApiOperation(value = "차량 거리 조회", httpMethod = "GET", notes = "차량 리스트")
	@GetMapping("/distance/{carId}")
	public ResponseEntity<Integer> getLargestDistance(
			@PathVariable Integer carId) {
		return new ResponseEntity<>(busiTripCarMapper.findTheLargestDistance(carId), HttpStatus.OK);
	}

	@ApiOperation(value = "차량 상세화면", httpMethod = "GET", notes = "차량 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "차량 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Car> view(@PathVariable Integer id) {

		Car car = carMapper.findOne(id);
		List<CarLog> log = carLogMapper.findByRefAndDelYnOrderByCreatedDate(id, "N");
		User user = userMapper.findOne(car.getCreater());
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		car.setUser(user);
		car.setLog(log);

		return new ResponseEntity<>(car, HttpStatus.OK);
	}

	@ApiOperation(value = "차량 저장", httpMethod = "PUT", notes = "차량 저장")
	@Override
	public ResponseEntity<Car> save(@RequestBody Car t) {
		User user = UserSingleton.getInstance();
		t.setCreater(user.getId());
		List<CarLog> log = t.getLog();
		if(log != null) {
			for (CarLog c : log) {
				System.out.println("TEST");
				c.setRef(t.getId());
				carLogMapper.save(c);
			}
		}

		carMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "차량 수정", httpMethod = "POST", notes = "차량 수정")
	@Override
	public ResponseEntity<Car> update(@RequestBody Car t) {
		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		carMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "차량 삭제", httpMethod = "DELETE", notes = "차량 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "휴가신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Car> delete(@PathVariable Integer id) {

		Car t = carMapper.findOne(id);
		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		t.setDelYn("Y");
		carMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);

	}

}
