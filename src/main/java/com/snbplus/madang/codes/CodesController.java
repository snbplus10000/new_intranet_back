package com.snbplus.madang.codes;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.busiCard.BusiCardCalc;
import com.snbplus.madang.holiwork.HoliWork;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/codes")
public class CodesController{

	private final CodesMapper codesMapper;

	@GetMapping("/{code}")
	public ResponseEntity<Codes> getCode(@PathVariable String code){

		return new ResponseEntity<>(codesMapper.findByCode(code), HttpStatus.OK);
	}

	@PostMapping("/")
	public ResponseEntity<Codes> update(@RequestBody Codes t) {

		codesMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

}
