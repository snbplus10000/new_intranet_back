package com.snbplus.madang.codes;

import lombok.Data;

import javax.persistence.*;

/**
 * 코드 entity
 * @author juuck14
 *
 */
@Entity
@Table(name = "codes")
@Data
public class Codes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id; // pk

	@Column(nullable = false)
	private String code; // 코드
	
//	private String codeKoreaName; // 코드 국문명
	@Column(name = "code_k_name")
	private String codeKName; // 코드 국문명

	@Column(name = "code_e_name")
	private String codeEName;  // 코드 영문명
	
	private Integer codeValue;  // 코드 값(숫자인 경우)
	
	private String upcode; // 상위 코드

	@Column(nullable = false)
	private String delYn; // 삭제 여부

	private String remark; // 코드 설명

}
