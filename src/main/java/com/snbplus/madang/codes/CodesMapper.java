package com.snbplus.madang.codes;

import com.snbplus.madang.approval.Approval;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodesMapper extends JpaRepository<Codes, Integer> {

    Codes findByCode(String code);
}
