package com.snbplus.madang.vacation;


import com.snbplus.madang.user.User;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "vacation")
@Data
public class Vacation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;  // pk

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creater", insertable=false, updatable=false)
	private User user;
	
	private String dept; // 부서명
	
	private String regName; // 신청자명
	
	private String subName; // 대체자명
	
	private String sabun; // 사번
	
	private String position; //직책
	
	private String regRank; //직급
	
	private String dest; // 행선지
	
	private String holiTel; // 휴가중 연락처
	
	private String holiKind; // 휴가구분
	
	private String startDate; // 시작일
	
	private String endDate; // 종료일
	
	private double holiPeriod; // 기간

	private String isEmergant; //긴급

	private String approNo;

	private Integer fileId;

	private String fileName;
	
	@Column(length = 1024)
	private String reason; // 사유
	
	private String state; // 진행상태
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private double useCnt;
	
	@Transient
	private double holiCnt;
	
	@Transient
	private double restCnt;
	
	@Transient
	private String className;
	
	@Transient
	private String imgUrl;
}
