package com.snbplus.madang.vacation;

import java.util.List;

import com.snbplus.madang.userCard.UserCardCalc;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VacationMapper extends JpaRepository<Vacation, Integer>, JpaSpecificationExecutor<Vacation> {

	Page<Vacation> findByDelYn(Pageable pageable, String string);

	List<Vacation> findByCreaterAndStateAndStartDateBetweenOrderByStartDateDesc(Integer id, String state, String startDate, String endDate);

	Page<Vacation> findByDelYnAndCreaterIn(Pageable pageable, String delYn, List<Integer> creater);

	@Query(value = "SELECT v.* FROM vacation v left outer join approval a"
			+ " on v.id = a.ref_id and a.ref_table = :refTable where v.del_yn = :delYn and v.creater in :userFilterList and a.step = :step", nativeQuery = true)
	List<Vacation> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn") String delYn,
			@Param("userFilterList") List<Integer> userFilterList, @Param("step") Integer step);

	List<Vacation> findByCreaterAndState(Integer id, String state);

	List<Vacation> findByDelYnAndState(String delYn, String state);

	@Query(value = "SELECT * FROM vacation where creater = :creater and del_yn = 'N' and ((start_date >= :startDate and start_date <= :endDate) or (end_date >= :startDate and end_date <= :endDate)) order by creater, start_date, end_date asc ", nativeQuery = true)
	List<Vacation> findByCreaterAndStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(@Param("creater") Integer creater, @Param("startDate") String startDate,
																				 @Param("endDate") String endDate);

	@Query(value = "SELECT v.* FROM vacation v left join member_clone u on u.id = v.creater where u.mem_kind='병특' and v.state = '결재완료' and ((v.start_date >= :startDate and v.start_date <= :endDate) or (v.end_date >= :startDate2 and v.end_date <= :endDate2)) order by v.creater, v.start_date, v.end_date asc ", nativeQuery = true)
	List<Vacation> findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("startDate2") String startDate2,
			@Param("endDate2") String endDate2);

	@Query(value="select b from Vacation b join fetch b.user u where u.id in (:idList) and u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Vacation b inner join b.user u where u.id in (:idList) and u.name like %:creator% and b.delYn=:delYn")
	Page<Vacation> findByCreatorContainingAndDelYn(@Param("idList")List<Integer> idList, @Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from Vacation b join fetch b.user u where u.id in (:idList) and b.state like %:state% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Vacation b inner join b.user u where u.id in (:idList) and b.state like %:state% and b.delYn=:delYn")
	Page<Vacation> findByStateContainingAndDelYn(@Param("idList")List<Integer> idList, @Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from Vacation b join fetch b.user u where u.id in (:idList) and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Vacation b inner join b.user u where u.id in (:idList) and b.delYn=:delYn")
	Page<Vacation> findAllByDelYn(@Param("idList")List<Integer> idList, @Param("delYn")String delYn, Pageable pageable);

}
