package com.snbplus.madang.vacation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/vacationHoli")
public class VacationHoliController implements InterfaceController<VacationHoli> {

	@Autowired
	VacationHoliMapper holiMapper;
	
	@Autowired
	UserMapper userMapper;
	
	@ApiOperation(value = "공휴일 리스트", httpMethod = "GET", notes = "공휴일 리스트")
	@Override
	public ResponseEntity<Page<VacationHoli>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {
		return new ResponseEntity<>(holiMapper.findByDelYnOrderByHoliDateDesc(pageable,"N")
				.map(vacationHoli -> {
				
				 	User searchUser;
				 	Integer id = 0;
				 	User user = new User();
				 	if(vacationHoli.getModifier() != null && vacationHoli.getModifier() != 0) {
				 		id = vacationHoli.getModifier();

				 	}else {
				 		id = vacationHoli.getCreater();
				 	}
			 		searchUser = userMapper.findOne(id);
				 	user.setId(searchUser.getId());
				 	user.setName(searchUser.getName());
				 	user.setLoginId(searchUser.getLoginId());
				 	vacationHoli.setUser(user);
					return vacationHoli;
				}),HttpStatus.OK);

	}

	
	@ApiOperation(value = "공휴일 상세화면", httpMethod = "GET", notes = "공휴일 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "공휴일 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<VacationHoli> view(@PathVariable Integer id) {
		
		return new ResponseEntity<>(holiMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "공휴일 저장", httpMethod = "PUT", notes = "공휴일 저장")
	@Override
	public ResponseEntity<VacationHoli> save(@RequestBody VacationHoli t) {
		
		holiMapper.save(t);
	
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "공휴일 삭제", httpMethod = "DELETE", notes = "공휴일 삭제")
	@Override
	public ResponseEntity<VacationHoli> update( @RequestBody VacationHoli t) {
		
		t.setDelYn("Y");
		holiMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "공휴일 수정", httpMethod = "POST", notes = "공휴일 수정")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "공휴일 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<VacationHoli> delete(@PathVariable Integer id) {
		
		User user = UserSingleton.getInstance();
		
		VacationHoli vacationHoli = holiMapper.findOne(id);
		vacationHoli.setDelYn("Y");
		vacationHoli.setModifier(user.getId());
		
		holiMapper.save(vacationHoli);
		
		return new ResponseEntity<>(vacationHoli, HttpStatus.OK);
	}
	
	@GetMapping("/allList")
	@ApiOperation(value="공휴일 전체 리스트", httpMethod = "GET", notes="휴가 완료 전체 리스트")
	public ResponseEntity<List<VacationHoli>> copleteList(){
		
		List<VacationHoli> holiList = holiMapper.findByDelYn("N");
		
		
		for(VacationHoli holi : holiList) {
			holi.setClassName("event-azure");
		}
		
		return new ResponseEntity<>( holiList, HttpStatus.OK);
		
	}



}
