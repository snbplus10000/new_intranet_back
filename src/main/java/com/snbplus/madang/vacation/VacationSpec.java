package com.snbplus.madang.vacation;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;

import com.snbplus.madang.user.UserMapper;

@Component
@Slf4j
public class VacationSpec {
	
    @Autowired
    UserMapper userMapper;

    public Specification<Vacation> build(String delYn, String conditionKey, String conditionValue, List userAuthList){

        if(conditionKey == null || conditionKey.equals("")) {
            return search(delYn,userAuthList);
        }else if(conditionKey != null && conditionKey.equals("creater")) {
            return search(delYn,conditionKey,userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()),userAuthList);
        }

        return search(delYn, conditionKey, "%" + conditionValue + "%", userAuthList);

    }


	public Specification<Vacation> search(String delYn, String conditionKey, String conditionValue, List userAuthList) {

        log.info("userAuthList {}" , userAuthList);

        return (root, query, cb) -> {

            Path<Object> path = root.get("creater");
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : userAuthList) {
                 
            	in.value(conditionColumnValue);
                 	                           	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    cb.like(root.get(conditionKey), conditionValue),
                    in
            );
        };
	}
	
	public Specification<Vacation> searchMemKind(String delYn, String conditionKey, List userAuthList,
			List userSearchList, String conditionValue) {
        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : userAuthList) {
                 for(Object value : userSearchList) {
                	 if(value == conditionColumnValue) {
                		 in.value(conditionColumnValue);
                		 break;
                	 }
                 }
            	                	                           	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    cb.like(root.get("regName"), conditionValue),
                    in
            );
        };
	}



	public Specification<Vacation> search(String delYn, String conditionKey, List userAuthList,
			List userSearchList) {

        log.info("userAuthList result {}" , userAuthList);
        log.info("userSearchList result {}" , userSearchList);

	    userAuthList.retainAll(userSearchList);

	    log.info("userFilter result {}" , userAuthList);

        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : userAuthList) {
                in.value(conditionColumnValue);
                System.out.println(conditionColumnValue);
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };
	}
	
	
    public Specification<Vacation> search(String delYn,  List userAuthList) {
    	
        return (root, query, cb) -> {

            Path<Object> path = root.get("creater");
            CriteriaBuilder.In<Object> in = cb.in(path);
            for(Object conditionColumnValue : userAuthList) {
            	in.value(conditionColumnValue);
            }
            
             return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
                  
            );
        };
    }

}
