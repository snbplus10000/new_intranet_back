package com.snbplus.madang.vacation;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VacationHoliMapper extends JpaRepository<VacationHoli, Integer> {

	Page<VacationHoli> findByDelYnOrderByHoliDateDesc(Pageable pageable, String string);

	List<VacationHoli> findByDelYn(String delYn);

}
