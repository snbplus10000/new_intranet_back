package com.snbplus.madang.vacation;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.approval.ApprovalMapper;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.*;
import com.snbplus.madang.userCard.UserCardCalc;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.internal.EntityManagerImpl;
import org.hibernate.jpa.spi.AbstractEntityManagerImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/vacation")
@RequiredArgsConstructor
public class VacationController implements InterfaceController<Vacation> {

	private final VacationMapper vacationMapper;

	private final UserMapper userMapper;

	private final UserHoliMapper holiMapper;

	Calendar cal = Calendar.getInstance();
	Integer year = cal.get(cal.YEAR);

	private final UserAuthService userAuthService;

	private final VacationSpec vacationSpec;

	private final ApprovalMapper approvalMapper;

	@ApiOperation(value = "휴가신청 리스트", httpMethod = "GET", notes = "휴가신청 리스트")
	@Override
	public ResponseEntity<Page<Vacation>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Page<Vacation> vacationList = null;

		if(conditionKey == null){
			conditionKey="default";
		}
		if(conditionValue==null){
			conditionValue="";
		}

//		List<Integer> userAuthList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
//				.map(userItem -> userItem.getId()).collect(Collectors.toList());

		List<Integer> userAuthList = userAuthService.allMemberList().stream()
				.map(userItem -> userItem.getId()).collect(Collectors.toList());

		switch (conditionKey) {
			case "creater":  vacationList = vacationMapper.findByCreatorContainingAndDelYn(userAuthList, conditionValue, "N", pageable);
				break;
			case "state":  vacationList = vacationMapper.findByStateContainingAndDelYn(userAuthList, conditionValue, "N", pageable);
				break;
			default: vacationList = vacationMapper.findAllByDelYn(userAuthList, "N", pageable);
				break;
		}

		return new ResponseEntity<>(vacationList, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가신청 상세화면", httpMethod = "GET", notes = "휴가신청 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "휴가신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Vacation> view(@PathVariable Integer id) {
		Vacation t = vacationMapper.findOne(id);
		User user = userMapper.findOne(t.getCreater());
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		t.setUser(user);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가신청 저장", httpMethod = "PUT", notes = "휴가신청 저장")
	@Override
	public ResponseEntity<Vacation> save(@RequestBody Vacation t) throws NullPointerException {

		User user = UserSingleton.getInstance();
		t.setUser(null);
		t.setCreater(user.getId());
		vacationPeriodCalc(t, 'c');

		vacationMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가신청 수정", httpMethod = "POST", notes = "휴가신청 수정")
	@Override
	public ResponseEntity<Vacation> update(@RequestBody Vacation t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		vacationPeriodCalc(t, 'u');

		vacationMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가신청 삭제", httpMethod = "DELETE", notes = "휴가신청 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "휴가신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Vacation> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		Vacation Vacation = vacationMapper.findOne(id);
		Vacation.setDelYn("Y");
		Vacation.setModifier(user.getId());
		vacationPeriodCalc(Vacation, 'd');

		vacationMapper.save(Vacation);

		return new ResponseEntity<>(Vacation, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가 리스트", httpMethod = "GET", notes = "휴가 리스트")
	@GetMapping("/users")
	public ResponseEntity<List<User>> getLists() {
		List<User> lists = userMapper.findVacationCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@GetMapping("/person/{id}")
	@ApiOperation(value = "직원 해당연도 휴가 내용  리스트", httpMethod = "Get", notes = "직원 해당연도 휴가 내용  리스트")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "직원 고유의 pk", required = true, dataType = "number", defaultValue = "1") })
	public ResponseEntity<List<Vacation>> vacationListByIdYear(@PathVariable Integer id) {

		String startDate = year + "-01-01";
		String endDate = year + "-12-31";

		return new ResponseEntity<>(vacationMapper.findByCreaterAndStateAndStartDateBetweenOrderByStartDateDesc(id, "결재완료", startDate, endDate),
				HttpStatus.OK);
	}

	@GetMapping("/reviewList")
	@ApiOperation(value = "휴가신청 결재 대기 리스트", httpMethod = "GET", notes = "휴가신청 결재 대기 리스트")
	public ResponseEntity<List<Vacation>> reviewList() {

		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(vacationMapper.findByDelYnAndCreatorIn("VACATION", "N", userFilterList, step).stream().map(vacation ->{
			 
			Integer id = vacation.getCreater();
			User user = userMapper.findOne(id);

			String imgUrl = "";
			
			if(user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null)  {
				imgUrl = "static/img/default-avatar.png";
			}else {
				imgUrl = "/intranet/api/file/image/"+user.getThumbFile().getFileAttach().getId();
			}
			vacation.setImgUrl(imgUrl);
						 
			return vacation;
			 
		}).collect(Collectors.toList()),HttpStatus.OK);

	}

	@GetMapping("/rejectList")
	@ApiOperation(value = "휴가신청 정산 리스트", httpMethod = "GET", notes = "휴가신청 반려 리스트")
	public ResponseEntity<List<Vacation>> rejectList() {

		return new ResponseEntity<>(vacationMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
				HttpStatus.OK);

	}

	@GetMapping("/completeList")
	@ApiOperation(value = "휴가신청 완료 리스트", httpMethod = "GET", notes = "휴가 완료 전체 리스트")
	public ResponseEntity<List<Vacation>> completeList() {

		List<Vacation> vacationList = vacationMapper.findByDelYnAndState("N", "결재완료");

		for (Vacation v : vacationList) {

			Integer creater = v.getCreater();
			String className = "";

			User searchUser = userMapper.findOne(creater);
			if (searchUser.getDeptChart().getCode() == 12) { // 디크팀
				className = "event-red";
			} else if (searchUser.getDeptChart().getCode() == 10) { // 개발팀
				className = "event-rose";
			}
			/*
			 * else if(searchUser.getDeptChart().getCode() == 15) { className =
			 * "event-green"; }else if(searchUser.getDeptChart().getCode() == 17) {
			 * className = "event-orange"; }
			 */
			else {
				className = "event-default";
			}
			v.setClassName(className);
		}

		return new ResponseEntity<>(vacationList, HttpStatus.OK);

	}

	@GetMapping("/vacationCheck")
	@ApiOperation(value = "휴가신청 리스트", httpMethod = "GET", notes = "휴가 전체 리스트")
	@ApiImplicitParams({
	@ApiImplicitParam(name = "startDate", value = "직원 고유의 pk", required = true, dataType = "String") ,
	@ApiImplicitParam(name = "endDate", value = "직원 고유의 pk", required = true, dataType = "String") ,
	@ApiImplicitParam(name = "holiKind", value = "직원 고유의 pk", required = true, dataType = "String") ,
	@ApiImplicitParam(name = "id", value = "직원 고유의 pk", required = false, dataType = "Integer")
	})
	public Integer vacationList(@RequestParam String startDate, @RequestParam String endDate, @RequestParam String holiKind, @RequestParam Integer id){
		User user = UserSingleton.getInstance();
		List<Vacation> vacationCheck = vacationMapper.findByCreaterAndStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(user.getId(), startDate, endDate);

		//오전휴가와 오후휴가는 겹쳐도 되기 때문에 카운트 조정용 변수 추가
		int ignorable = 0;

		for(Vacation v : vacationCheck){
			if((holiKind.contains("오전") && v.getHoliKind().contains("오후"))
			|| (holiKind.contains("오후") && v.getHoliKind().contains("오전"))
			|| v.getId().equals(id)){
				ignorable++;
			}
		}
		return vacationCheck.size() - ignorable;
	}

	@GetMapping("/soliderList")
	@ApiOperation(value = "휴가신청 병특 리스트", httpMethod = "GET", notes = "휴가신청 병특 리스트")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "year", value = "병특리스트 관련 연도", required = true, dataType = "number", defaultValue = "2020"),
			@ApiImplicitParam(name = "month", value = "병특리스트 관련 월", required = true, dataType = "number") })
	public ResponseEntity<List<Vacation>> soliderServiceByMonth(@RequestParam(required = true) Integer year,
			@RequestParam(required = true) Integer month) {

		String StartDate = "";
		String EndDate = "";
		if (month < 10) {
			StartDate = year + "-0" + month + "-01";
			EndDate = year + "-0" + month + "-31";
		} else {
			StartDate = year + "-" + month + "-01";
			EndDate = year + "-" + month + "-31";
		}

		List<Vacation> vacationList = vacationMapper
				.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate, EndDate, StartDate, EndDate)
				.stream().map(Vacation -> {

					Approval approval = approvalMapper.findByRefTableAndRefId("VACATION", Vacation.getId());

					User user = userMapper.findOne(Vacation.getCreater());
					/*user.getDeptChart().setDeptUser(null);
					user.getDeptChart().setHighDeptUser(null);
					user.getHighDeptChart().setDeptUser(null);
					user.getHighDeptChart().setHighDeptUser(null);*/
					Vacation.setUser(user);
					if(user.getPosition().equals("팀장")){
						Vacation.setApproNo(approval.getAppName2());
					} else {
						Vacation.setApproNo(approval.getAppName4());
					}

					return Vacation;

				}).collect(Collectors.toList());

		return new ResponseEntity<>(vacationList, HttpStatus.OK);
	}

	public void vacationPeriodCalc(Vacation vacation, char mode) throws NullPointerException {

		Integer memberId = vacation.getCreater();

		// 유저가 입력한 휴가 기간
		double holiPeriod = vacation.getHoliPeriod();
		if(vacation.getHoliKind().contains("반차")) holiPeriod = 0.5;

		// update나 delete일 경우 기존에 삭감했던 연차 돌려주기
		if(mode=='u') holiPeriod -= vacationMapper.getOne(vacation.getId()).getHoliPeriod();
		if(mode=='d') holiPeriod -= vacationMapper.getOne(vacation.getId()).getHoliPeriod() * 2;

		// 남은 휴가 개수 처리
		String realYear = vacation.getStartDate().substring(0, 4);
		Optional<UserHoli> memberholiOptional = holiMapper.findByDelYnAndYearAndRefId("N", realYear, memberId);
		memberholiOptional.orElseThrow(() -> new NullPointerException("해당 년도에 대한 유저의 휴가정보를 등록해주세요."));

		UserHoli memberholi = memberholiOptional.get();

		double pastUseCnt = Double.valueOf(memberholi.getUseCnt());
		double useCnt = holiPeriod + pastUseCnt;

		memberholi.setUseCnt(useCnt);

		holiMapper.save(memberholi);
		
	}

}


