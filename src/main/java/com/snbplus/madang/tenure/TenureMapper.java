package com.snbplus.madang.tenure;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TenureMapper extends JpaRepository<Tenure, Integer> {
    Tenure findById(Integer id);
    @Query(value="select b from Tenure b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
            countQuery= "select count(b) from Tenure b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
    Page<Tenure> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

    @Query(value="select b from Tenure b join fetch b.user u where b.state like %:state% and b.delYn=:delYn order by b.id desc",
            countQuery= "select count(b) from Tenure b inner join b.user u where b.state like %:state% and b.delYn=:delYn")
    Page<Tenure> findByStateContainingAndDelYn(@Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

    @Query(value="select b from Tenure b join fetch b.user u where b.delYn=:delYn order by b.id desc",
            countQuery= "select count(b) from Tenure b inner join b.user u where b.delYn=:delYn")
    Page<Tenure> findAllByDelYn(@Param("delYn")String delYn, Pageable pageable);
}
