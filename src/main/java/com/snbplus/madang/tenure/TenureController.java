package com.snbplus.madang.tenure;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/tenure")
@RequiredArgsConstructor
public class TenureController implements InterfaceController<Tenure> {

    private final UserAuthService userAuthService;

    private final TenureMapper tenureMapper;
    
    @Override
    public ResponseEntity<Page<Tenure>> list(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
            @RequestParam(required = false) String conditionKey,
            @RequestParam(required = false) String conditionValue) {

        Page<Tenure> tenureList = null;

        if(conditionKey == null){
            conditionKey="default";
        }
        if(conditionValue==null){
            conditionValue="";
        }

//        List<Integer> userAuthList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
//                .map(userItem -> userItem.getId()).collect(Collectors.toList());

        switch (conditionKey) {
            case "creater":  tenureList = tenureMapper.findByCreatorContainingAndDelYn(conditionValue, "N", pageable);
                break;
            case "state":  tenureList = tenureMapper.findByStateContainingAndDelYn(conditionValue, "N", pageable);
                break;
            default: tenureList = tenureMapper.findAllByDelYn("N", pageable);
                break;
        }

        return new ResponseEntity<>(tenureList, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Tenure> view(@PathVariable Integer id) {
        Tenure t = tenureMapper.findById(id);
        if(t==null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Tenure> save(@RequestBody Tenure t) throws NullPointerException {

        User user = UserSingleton.getInstance();
        t.setUser(user);
        Long currentRow = tenureMapper.count();
        Long nextRow = currentRow+13;
        String orderNo = null;
        if(nextRow<10){
            orderNo="00"+nextRow;
        }else if(nextRow<100){
            orderNo="0"+nextRow;
        }else{
            orderNo= String.valueOf(nextRow);
        }
        t.setState("접수");
        t.setDelYn("N");
        t.setDocumentNumber("제 "+ LocalDateTime.now().getYear()+"-"+orderNo+"E 호");
        tenureMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Tenure> update(@RequestBody Tenure t) {

        User user = UserSingleton.getInstance();
        t.setModifier(user);
        t.setDelYn("N");
        tenureMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Tenure> delete(@PathVariable Integer id) {

        User user = UserSingleton.getInstance();
        Tenure t = tenureMapper.findById(id);
        if(t==null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        t.setDelYn("Y");
        t.setModifier(user);
        tenureMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }
}


