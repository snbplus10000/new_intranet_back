package com.snbplus.madang.tenure;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.snbplus.madang.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Entity
@Data
@NoArgsConstructor
@Table(name = "tenure")
public class Tenure {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="creator", nullable = false, insertable=true, updatable=false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="modifier", nullable = true, insertable=true, updatable=true)
    private User modifier;

    private String documentNumber;
    private String title;
    private String state;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
    private String address;
    private String rank;
    private String department;
    private String purpose;
    private String subjectOfSubmit;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate joinDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate lastWorkedDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfSubmit;
    @ColumnDefault("'N'")
    private String delYn;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    @CreatedDate
    private Timestamp createdAt;

    @Column(updatable = true)
    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd")
    @LastModifiedDate
    private Timestamp updatedAt;
}