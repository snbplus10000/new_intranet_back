package com.snbplus.madang.tenure;

import com.snbplus.madang.user.User;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class TenureDto {
    private Integer id;
    private User user;
    private String requestNumber;
    private String documentNumber;
    private String title;
    private String state;
    private String name;
    private String birthday;
    private String address;
    private String rank;
    private String department;
    private String purpose;
    private String subjectOfSubmit;
    private LocalDateTime joinDate;
    private LocalDateTime lastWorkedDate;
    private LocalDateTime dateOfSubmit;
    private String delYn;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
}