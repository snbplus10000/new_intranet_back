package com.snbplus.madang;

import com.snbplus.madang.config.FileStorageProperties;
import com.snbplus.madang.config.MadangPasswordEncoder;
import com.snbplus.madang.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@EnableScheduling
@SpringBootApplication
@RestController
@Configuration
@EnableConfigurationProperties({
        FileStorageProperties.class
})
@EnableAuthorizationServer
public class MadangApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(MadangApplication.class, args);
    }

    @ApiOperation(value = "서버 동작 확인", httpMethod = "POST", notes = "서버 동작을 확인하는 API")
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Server is Dead"), @ApiResponse(code = 200, message = "Run")})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "authorization header", required = true,
                    dataType = "string", paramType = "header", defaultValue = "bearer ")

    })
    @PostMapping("/api/run")
    public String isLive() {

        System.out.println("called...");
        return "running";
    }



}
