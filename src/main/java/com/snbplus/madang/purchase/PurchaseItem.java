package com.snbplus.madang.purchase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

@Entity
@Table(name = "purchase_item")
@Data
public class PurchaseItem {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer refId;

	private String itemName; // 항목

	private Integer quantity; // 수량

	private Integer unitPrice; // 예상단가

	private Integer expectedAmount; // 예상금액

	private String etc; // 비고

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

}
