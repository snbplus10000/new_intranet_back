package com.snbplus.madang.purchase;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.holiwork.HoliWork;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/purchase")
@RequiredArgsConstructor
public class PurchaseController implements InterfaceController<Purchase> {

    private final PurchaseMapper purchaseMapper;

    private final UserMapper userMapper;

    private final UserAuthService userAuthService;

    private final PurchaseSpec purchaseSpec;

    private final PurchaseItemMapper itemMapper;


    @ApiOperation(value = "구매품의서 리스트", httpMethod = "GET", notes = "구매품의서 리스트")
    @Override
    public ResponseEntity<Page<Purchase>> list(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
            @RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {

        Page<Purchase> purchaseList = null;

        if(conditionKey == null){
            conditionKey="default";
        }
        if(conditionValue==null){
            conditionValue="";
        }

        switch (conditionKey) {
            case "creater":  purchaseList = purchaseMapper.findByCreatorContainingAndDelYn(conditionValue, "N", pageable).map(purchase -> {
                Integer totalPrice=purchase.calculateTotalAmount();
                purchase.setTotalPrice(totalPrice);
                return purchase;
            });
                break;
            case "state":  purchaseList = purchaseMapper.findByStateContainingAndDelYn(conditionValue, "N", pageable).map(purchase -> {
                Integer totalPrice=purchase.calculateTotalAmount();
                purchase.setTotalPrice(totalPrice);
                return purchase;
            });
                break;
            default: purchaseList = purchaseMapper.findAllByDelYn("N", pageable).map(purchase -> {
                Integer totalPrice=purchase.calculateTotalAmount();
                purchase.setTotalPrice(totalPrice);
                return purchase;
            });
                break;
        }

        return new ResponseEntity<Page<Purchase>>(purchaseList, HttpStatus.OK);
    }

    @ApiOperation(value = "구매품의서 상세화면", httpMethod = "GET", notes = "구매품의서 상세화면")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "구매품의서 관련 pk", required = true, dataType = "number", defaultValue = "1")})
    @Override
    public ResponseEntity<Purchase> view(@PathVariable Integer id) {
        Purchase t = purchaseMapper.findOne(id);
        User searchUser = userMapper.findOne(t.getCreater());
        t.setUser(searchUser);
        t.getUser().setHighDeptChart(null);
        //t.getUser().setHighDeptChart(null);
        Integer totalPrice=t.calculateTotalAmount();
        t.setTotalPrice(totalPrice);

        try{
            User purchaserUser = userMapper.findOne(t.getPuschaser());
            /*purchaserUser.getDeptChart().setDeptUser(null);
            purchaserUser.getDeptChart().setHighDeptUser(null);*/
            User user2 = new User();
            user2.setRankName(purchaserUser.getRankName());
            user2.setName(purchaserUser.getName());
            user2.setDeptChart((purchaserUser.getDeptChart()));
            //user2.setDeptChart(purchaserUser.getDeptChart());
            t.setPurchaseUser(user2);
            //t.getPurchaseUser().setHighDeptChart(null);
            t.getPurchaseUser().setHighDeptChart(null);
        }catch(Exception e){
            log.error(e.getMessage());
            t.setPuschaser(null);
            t.setPurchaseUser(null);
        }finally {
            return new ResponseEntity<>(t, HttpStatus.OK);
        }

    }

    @ApiOperation(value = "구매품의서 저장", httpMethod = "PUT", notes = "구매품의서 저장")
    @Override
    public ResponseEntity<Purchase> save(@RequestBody Purchase t) {

        User user = UserSingleton.getInstance();
        t.setUser(null);
        t.setCreater(user.getId());
        t.setDelYn("N");
        // t.setUser(user);
        List<PurchaseItem> purchaseItemList = t.getPurchaseItem();
        t.setPurchaseItem(null);
        purchaseMapper.save(t);

        for (PurchaseItem item : purchaseItemList) {
            item.setRefId(t.getId());
        }

        if (!purchaseItemList.isEmpty()) {
            itemMapper.save(purchaseItemList);
        }


        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "구매품의서 수정", httpMethod = "POST", notes = "구매품의서 수정")
    @Override
    public ResponseEntity<Purchase> update(@RequestBody Purchase t) {
        User user = UserSingleton.getInstance();

        t.setModifier(user.getId());
        List<PurchaseItem> purchaseItemList = t.getPurchaseItem();
        for (PurchaseItem item : purchaseItemList) {
            item.setRefId(t.getId());
        }
        t.setPurchaseItem(purchaseItemList);

        purchaseMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "구매품의서 삭제", httpMethod = "DELETE", notes = "구매품의서 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "구매품의서 관련 pk", required = true, dataType = "number", defaultValue = "1")})
    @Override
    public ResponseEntity<Purchase> delete(@PathVariable Integer id) {
        User user = UserSingleton.getInstance();
        Purchase purchase = purchaseMapper.findOne(id);

        purchase.setDelYn("Y");
        purchase.setModifier(user.getId());
        purchaseMapper.save(purchase);

        return new ResponseEntity<>(purchase, HttpStatus.OK);
    }

    @ApiOperation(value = "휴가 리스트", httpMethod = "GET", notes = "휴가 리스트")
    @GetMapping("/users")
    public ResponseEntity<List<User>> getLists() {
        List<User> lists = userMapper.findPurchaseCreater();

        return new ResponseEntity<>(lists, HttpStatus.OK);
    }

    @GetMapping("/reviewList")
    @ApiOperation(value = "구매품의서 결재 대기 리스트", httpMethod = "GET", notes = "구매 품의서 결재 대기 리스트")
    public ResponseEntity<List<Purchase>> reviewList() {

        User curUser = UserSingleton.getInstance();
        Integer step = UserSingleton.getStep();

        List<Integer> userFilterList = userAuthService.myTeamMemberList(curUser).stream()
                    .map(userItem -> userItem.getId()).collect(Collectors.toList());

        return new ResponseEntity<>(purchaseMapper.findDelYnAndCreatorIn("PURCHASE", "N", userFilterList, step).stream().map(purchase -> {

            Integer id = purchase.getCreater();
            User user = userMapper.findOne(id);

            String imgUrl = "";

            if (user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null) {
                imgUrl = "static/img/default-avatar.png";
            } else {
                imgUrl = "/intranet/api/file/image/" + user.getThumbFile().getFileAttach().getId();
            }
            purchase.setImgUrl(imgUrl);
            return purchase;

        }).collect(Collectors.toList()),
                HttpStatus.OK);
    }

    @GetMapping("/rejectList")
    @ApiOperation(value = "구매품의서 정산 반려 리스트", httpMethod = "GET", notes = "구매품의서 정산 반려 리스트")
    public ResponseEntity<List<Purchase>> rejectList() {

        return new ResponseEntity<>(purchaseMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
                HttpStatus.OK);

    }

}
