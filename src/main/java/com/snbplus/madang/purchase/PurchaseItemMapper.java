package com.snbplus.madang.purchase;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseItemMapper extends JpaRepository<PurchaseItem, Integer> {

	List<PurchaseItem> findByRefIdAndDelYn(Integer id, String delYn);

	PurchaseItem findFirstByRefIdAndDelYn(Integer id, String delYn);

}
