package com.snbplus.madang.purchase;

import java.util.List;

import com.snbplus.madang.vacation.Vacation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseMapper extends JpaRepository<Purchase, Integer>, JpaSpecificationExecutor<Purchase> {

	Page<Purchase> findByDelYn(Pageable pageable, String DelYn);

	@Query(value = "SELECT p.* From purchase p left outer join approval a "
			+ "on p.id = a.ref_id and a.ref_table = :refTable where p.del_yn = :delYn "
			+ "and p.creater in :userFilterList and a.step = :step"
			, nativeQuery = true)
	List<Purchase>  findDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn")String delYn, @Param("userFilterList")List<Integer> userFilterList, @Param("step")Integer step);

	List<Purchase> findByCreaterAndState(Integer id, String state);

	@Query(value="select b from Purchase b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Purchase b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
	Page<Purchase> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from Purchase b join fetch b.user u where b.state like %:state% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Purchase b inner join b.user u where b.state like %:state% and b.delYn=:delYn")
	Page<Purchase> findByStateContainingAndDelYn(@Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from Purchase b join fetch b.user u where b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from Purchase b inner join b.user u where b.delYn=:delYn")
	Page<Purchase> findAllByDelYn(@Param("delYn")String delYn, Pageable pageable);

}
