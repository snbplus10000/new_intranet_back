package com.snbplus.madang.purchase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/purchaseItem")
public class PurchaseItemController implements JoinController<List<PurchaseItem>> {

	@Autowired
	PurchaseItemMapper itemMapper;

	@ApiOperation(value = "구매품의서 - 상세항목  리스트", httpMethod = "GET", notes = "구매품의서 - 상세항목 리스트")
	@Override
	public ResponseEntity<List<PurchaseItem>> list(Integer id) {
		
		return new ResponseEntity<>(itemMapper.findByRefIdAndDelYn(id,"N"), HttpStatus.OK);
	}


	@ApiOperation(value = "구매품의서 - 상세항목 저장/수정", httpMethod = "PUT", notes = "구매품의서 - 상세항목 저장/수정")
	@Override
	public ResponseEntity<List<PurchaseItem>> save(@RequestBody List<PurchaseItem> t) {
		
		t.forEach(item -> log.info("{}", item));
		
		for(PurchaseItem info : t) {
			itemMapper.save(info);
		}

		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "구매품의서 - 상세항목 삭제", httpMethod = "DELETE", notes = "구매품의서 - 상세항목 삭제")
	@Override
	public ResponseEntity<List<PurchaseItem>> delete(List<PurchaseItem> t) {
		
		for( PurchaseItem info :  t) {
			
			info.setDelYn("Y");
		}		
		itemMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}


}
