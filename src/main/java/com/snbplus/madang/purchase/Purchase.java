package com.snbplus.madang.purchase;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "purchase")
@Data
public class Purchase {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creater", insertable=false, updatable=false)
	private User user;

	private String state; // 상태

	private String dept;

	private String regName; // 작성자

	@Column(length = 1024)
	private String text; // 특이사항

	private String bank; // 은행명

	private String accountName; // 예금주

	private String accNum; // 계좌번호

	@ColumnDefault("''")
	private String deferReason=""; // 보류사유

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	private Integer puschaser; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "refId")
	private List<PurchaseItem> purchaseItem;

	@Transient
	private User purchaseUser;
	
	@Transient
	private Integer totalPrice;

	@Transient
	private String imgUrl;

	private String usedPlace;

	private String purpose;

	private Integer fileId;

	private String fileName;

	private String purchaseKind;

/*	@OneToOne
	@JoinColumn(name="approval_info_no")
	private ApprovalInfo approvalInfo;*/

	public Integer calculateTotalAmount() {
		return purchaseItem.stream()
				.filter(element -> element.getQuantity() != 0 && element.getUnitPrice() != 0)
				.mapToInt(element -> element.getQuantity() * element.getUnitPrice()).sum();
	}
}
