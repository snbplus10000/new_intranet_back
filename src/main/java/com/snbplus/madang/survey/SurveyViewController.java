package com.snbplus.madang.survey;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SurveyViewController {
	
	@GetMapping("/survey/view")
	public String SurveyView(Model model) {
		
		return "survey/survey";
	}
}
