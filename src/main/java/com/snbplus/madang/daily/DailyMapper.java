package com.snbplus.madang.daily;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DailyMapper extends JpaRepository<Daily, Integer> , JpaSpecificationExecutor<Daily> {

	Page<Daily> findByDelYn(Pageable pageable, String delYn);

	Page<Daily> findByDelYnAndCreaterIn(Pageable pageable, String string, List<Integer> userFilterList);

}
