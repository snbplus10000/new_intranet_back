package com.snbplus.madang.daily;

import java.util.List;
import java.util.stream.Collectors;

import com.snbplus.madang.board.Board;
import com.snbplus.madang.user.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.InterfaceSpec;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;

@Component
public class DailySpec {


	@Autowired
	UserMapper userMapper;

	public Specification<Daily> build(String delYn, String conditionKey, String conditionValue, List<Integer> userAuthList) {

		if(conditionKey == null || conditionValue.equals("")) {
			return search(delYn, userAuthList);
		}else {
			if(conditionKey.equals("creater")) {
				return search(delYn, conditionKey, userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()), userAuthList);
			}
		}

		return search(delYn, conditionKey, "%" + conditionValue + "%", userAuthList);

	}

	public Specification<Daily> build(String delYn, List<Integer> userAuthList) {

		return build(delYn, null, null, userAuthList);
	}

	public Specification<Daily> search(String delYn, List<Integer> userAuthList) {
		return (root, query, cb) ->{
			Path<Object> path = root.get("creater");
			CriteriaBuilder.In<Object> in = cb.in(path);
			for (Object conditionColumnValue : userAuthList) {

				in.value(conditionColumnValue);

			}
			return cb.and(
					cb.equal(root.get("delYn"),  delYn),
					in
			);
		};
	}

	public Specification<Daily> search(String delYn, String conditionKey, String conditionValue, List<Integer> userAuthList) {
		return (root, query, cb) ->{
			Path<Object> path = root.get("creater");
			CriteriaBuilder.In<Object> in = cb.in(path);
			for (Object conditionColumnValue : userAuthList) {

				in.value(conditionColumnValue);

			}
			return cb.and(
					cb.equal(root.get("delYn"),  delYn),
					cb.like(root.get(conditionKey), conditionValue),
					in
			);
		};
	}

	public  Specification<Daily> search(String delYn, String conditionKey, List conditionValue, List<Integer> userAuthList) {

		return (root, query, cb) -> {

			Path<Object> path = root.get(conditionKey);
			CriteriaBuilder.In<Object> in = cb.in(path);
			for (Object conditionColumnValue : conditionValue) {
				in.value(conditionColumnValue);
			}

			Path<Object> path2 = root.get("creater");
			CriteriaBuilder.In<Object> in2 = cb.in(path2);
			for (Object conditionColumnValue : userAuthList) {
				in2.value(conditionColumnValue);
			}

			return cb.and(
					cb.equal(root.get("delYn"),  delYn),
					in,in2
			);
		};


	}
	public Specification<Daily> search( List<Integer> userAuthList) {
		return (root, query, cb) ->{
			Path<Object> path = root.get("creater");
			CriteriaBuilder.In<Object> in = cb.in(path);
			for (Object conditionColumnValue : userAuthList) {

				in.value(conditionColumnValue);

			}
			return cb.and(
					in
			);
		};
	}



}
