package com.snbplus.madang.daily;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DailyDetailMapper extends JpaRepository<DailyDetail, Integer> {

	List<DailyDetail> findByDailyIdAndDelYn(Integer id, String delYn);

}
