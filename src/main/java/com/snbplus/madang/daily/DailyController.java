package com.snbplus.madang.daily;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/daily")
@RequiredArgsConstructor
public class DailyController implements InterfaceController<Daily> {

	private final DailyMapper dailyMapper;

	private final DailyDetailMapper dailyDetailMapper;

	private final UserMapper userMapper;

	private final UserAuthService userAuthService;

	private final DailySpec dailySpec;

	@ApiOperation(value = "일일보고 리스트", httpMethod = "GET", notes = "일일보고 리스트")
	@Override
	public ResponseEntity<Page<Daily>> list(
			@PageableDefault(sort = "inputDate", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {
		User curUser = UserSingleton.getInstance();

		List<Integer> userFilterList;

		userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		Specification<Daily> spec;
		if(curUser.getLoginId() != null && curUser.getLoginId().equals("jud")){
			spec = dailySpec.search(userFilterList);
		}else{
			spec = dailySpec.build("N", conditionKey, conditionValue, userFilterList);
		}

		return new ResponseEntity<>(dailyMapper.findAll(spec, pageable).map(daily -> {

			User searchUser;
			User user = new User();
			Integer id = daily.getCreater();
			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			daily.setUser(user);
			return daily;

		}), HttpStatus.OK);
	}

	@ApiOperation(value = "일일보고 상세화면", httpMethod = "GET", notes = "일일보고 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "일일보고 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Daily> view(@PathVariable Integer id) {
		return new ResponseEntity<>(dailyMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "일일보고 저장", httpMethod = "PUT", notes = "일일보고 저장")
	@Override
	public ResponseEntity<Daily> save(@RequestBody Daily t) {

		User user = UserSingleton.getInstance();
		t.setCreater(user.getId());
		t.setDept(user.getDeptChart());
		List<DailyDetail> detailList = t.getDailyDetail();
		t.setDailyDetail(null);

		dailyMapper.save(t);

		if (!detailList.isEmpty()) {
			for (DailyDetail detail : detailList) {
				detail.setDailyId(t.getId());
			}
			dailyDetailMapper.save(detailList);
		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "일일보고 삭제", httpMethod = "DELETE", notes = "일일보고 삭제")
	@Override
	public ResponseEntity<Daily> delete(@PathVariable Integer id) {

		Daily daily = dailyMapper.findOne(id);
		daily.setDelYn("Y");
		dailyMapper.save(daily);

		return new ResponseEntity<>(daily, HttpStatus.OK);
	}

	@ApiOperation(value = "일일보고 수정", httpMethod = "POST", notes = "일일보고 수정")
	@Override
	public ResponseEntity<Daily> update(@RequestBody Daily t) {

		User user = UserSingleton.getInstance();

		Daily dailyDetail = dailyMapper.findOne(t.getId());

		List<DailyDetail> detailList = t.getDailyDetail();
		t.setDailyDetail(null);

		t.setCreater(dailyDetail.getCreater());
		t.setModifier(user.getId());
		dailyMapper.save(t);

		if (!detailList.isEmpty()) {
			for (DailyDetail detail : detailList) {
				detail.setDailyId(t.getId());
			}
			dailyDetailMapper.save(detailList);
		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

}
