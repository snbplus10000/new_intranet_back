package com.snbplus.madang.daily;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/dailyDetail")
public class DailyDetailController implements JoinController<List<DailyDetail>> {
	
	@Autowired
	DailyDetailMapper detailMapper;
	
	@ApiOperation(value = "일일보고 세부사항 리스트", httpMethod = "GET", notes = "일일보고 세부사항 리스트")
	@Override
	public ResponseEntity<List<DailyDetail>> list(Integer id) {
		
		return new ResponseEntity<>(detailMapper.findByDailyIdAndDelYn(id,"N"), HttpStatus.OK);
	}


	@ApiOperation(value = "일일보고 세부사항 저장/수정", httpMethod = "PUT", notes = "일일보고 세부사항 저장/수정")
	@Override

	public ResponseEntity<List<DailyDetail>> save(@RequestBody List<DailyDetail> t) {
		
		List<DailyDetail> list = t;
		
		for(DailyDetail detail : list) {
			detailMapper.save(detail);
		}
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	
	@ApiOperation(value = "일일보고 세부사항 삭제", httpMethod = "DELETE", notes = "일일보고 세부사항 삭제")
	@Override
	public ResponseEntity<List<DailyDetail>> delete(List<DailyDetail> t) {
		
		
		for( DailyDetail detail :  t) {
			
			detail.setDelYn("Y");
		}		
		detailMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

}
