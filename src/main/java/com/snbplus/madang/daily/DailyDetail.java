package com.snbplus.madang.daily;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

/**
 * 일일보고 - 세부항목
 * @author bhshin
 *
 */
@Entity
@Table(name="tbl_daily_detail")
@Data
public class DailyDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String startTime; //시작시간

	private String endTime; // 종료시간

	private String location; // 지역

	private String gugun; // 구군

	private String companyName; // 업체명

	private String content; // 내용

	private String customerName; // 고객 이름

	private String customerRank; // 고객 직급

	private String phoneNo; // 고객 연락처

	private String email; // 고객 이메일

	private String homepage; // 홈페이지 url

	private String etc; // 비고

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N" ; // 삭제여부

	private Integer dailyId; //
}
