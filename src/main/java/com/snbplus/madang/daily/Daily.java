package com.snbplus.madang.daily;

import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.user.User;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
/**
 * 일일보고
 * @author bhshin
 *
 */
@Entity
@Table(name="tbl_daily")
@Data
public class Daily {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String inputDate; // 날짜
	
	private Integer creater; // 입력자
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 입력날짜
	
	private Integer modifier; // 수정자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정날짜
		
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinColumn(name="dailyId")
	private List<DailyDetail> dailyDetail;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dept")
	//private OrgChart dept; // 일일업무보고 당시 부서
	private Dept dept;

	@Transient
	private Integer userId;
	
	@Transient
	private User user; 
	
}

