package com.snbplus.madang;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

public interface InterfaceSpec<T> {

   
	public Specification<T> build(String delYn, String conditionKey, String conditionValue);
	
    public Specification<T> build(String delYn);
    
    public Specification<T> search(String delYn, String conditionKey, String conditionValue);

    public Specification<T> search(String delYn, String conditionKey, List conditionValue);

    public Specification<T> search(String delYn);
   

    
}
