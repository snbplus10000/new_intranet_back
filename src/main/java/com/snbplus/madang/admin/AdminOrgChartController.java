package com.snbplus.madang.admin;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.common.OrgChartMapper;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/admin/orgchart")
@RequiredArgsConstructor
public class AdminOrgChartController {

	private final OrgChartMapper chartMapper;

	@ApiOperation(value = "관리자 - 조직도 리스트", httpMethod = "GET", notes = "관리자 - 조직도 리스트")
	@GetMapping("")
	public ResponseEntity<List<OrgChart>> list(){
		return new ResponseEntity<>(chartMapper.findByDelYnOrderByCodeAsc("N"), HttpStatus.OK);
	}
	
	@ApiOperation(value = "관리자 - 조직도 수정", httpMethod = "POST", notes = "관리자 - 조직도 수정")
	@PostMapping("")
	public ResponseEntity<List<OrgChart>> save(@RequestBody List<OrgChart> list){
		
		for(OrgChart chart : list) {
			
			chartMapper.save(chart);
		}
		
		return new ResponseEntity<>(list, HttpStatus.OK);
		
		
	}
}
