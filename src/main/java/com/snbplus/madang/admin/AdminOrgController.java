package com.snbplus.madang.admin;

import java.util.ArrayList;
import java.util.List;

import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.vacation.Vacation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.DeptMapper;
import com.snbplus.madang.user.*;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/admin/dept")
@RequiredArgsConstructor
public class AdminOrgController {

    private final DeptMapper deptMapper;

    private final UserMapper userMapper;

    @ApiOperation(value = "관리자 - 조직도 리스트", httpMethod = "GET", notes = "관리자 - 조직도 리스트")
    @GetMapping("")
    public ResponseEntity<List<Dept>> list(){
        List<Dept> depts = deptMapper.findByDelYnOrderByCodeAsc("N");
        for (Dept d : depts) {
            if(d.getManager() != null) {
                User manager = userMapper.findOne(d.getManager());
                manager.setDeptChart(null);
                manager.setHighDeptChart(null);
                d.setUser(manager);

            }
        };

        return new ResponseEntity<>(depts, HttpStatus.OK);
    }


    @ApiOperation(value = "관리자 - 부서 저장", httpMethod = "PUT", notes = "관리자 - 부서 저장")
    @PutMapping("")
    public ResponseEntity<Dept> save(@RequestBody Dept t){
        deptMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "관리자 - 부서 삭제", httpMethod = "DELETE", notes = "관리자 - 부서 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "부서 관련 pk", required = true, dataType = "number", defaultValue = "1") })
    @DeleteMapping("/{id}")
    public ResponseEntity<List<Dept>> delete(@PathVariable Integer id) {
        List<Dept> depts = deptMapper.findByDelYnOrderByCodeAsc("N");

        for (Dept d : depts) {
            if(d.getDeptUpper() == id || d.getCode() == id) {
                d.setDelYn("Y");
            }
        }

        deptMapper.save(depts);

        return new ResponseEntity<>(depts, HttpStatus.OK);
    }

    @ApiOperation(value = "관리자 - 부서 보기", httpMethod = "GET", notes = "관리자 - 부서 보기")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id", value = "번호" , required = true, dataType = "number", defaultValue = "1"),
    })
    @GetMapping("/{id}")
    public ResponseEntity<List<Dept>> detail(@PathVariable Integer id) {
        List<Dept> depts = deptMapper.findByDelYnOrderByCodeAsc("N");
        List<Dept> filter = new ArrayList<>();
        
        Dept curr = deptMapper.findByCode(id);
        User manager = userMapper.findOne(curr.getManager());
        manager.setDeptChart(null);
        manager.setHighDeptChart(null);
        curr.setUser(manager);

        //상위 부서
        if(curr.getDeptUpper() == null) {
            filter.add(curr);
        }else {
            Dept high = deptMapper.findByCode(curr.getDeptUpper());
            User hManager = userMapper.findOne(high.getManager());
            hManager.setDeptChart(null);
            hManager.setHighDeptChart(null);
            high.setUser(hManager);
            filter.add(high);
            filter.add(curr);
        }

        return new ResponseEntity<>(filter, HttpStatus.OK);
    }

    //부서에 있는 유저 가져오기 (Select)
    @ApiOperation(value = "관리자 - 결재라인 담당자 리스트", httpMethod = "GET", notes = "관리자 - 결재라인 담당자 리스트")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id", value = "번호" , required = true, dataType = "number", defaultValue = "1"),
    })
    @GetMapping("/users/{id}")
    public ResponseEntity<List<User>> fetch(@PathVariable Integer id) {
//        List<Integer> departments = new ArrayList<>();
        List<User> users = userMapper.findByDelYnAndWorkState("N", "재직중", new Sort(Sort.Direction.ASC, "id"));
        List<User> results = new ArrayList<>();
        Integer highDeptCode = deptMapper.getOne(id).getDeptUpper();

        for (User u : users) {
            //박종범 실장님한테는 모든 부서에 권한을 줌
            if(u.getDeptChart().getCode() == id || u.getDeptChart().getCode() == highDeptCode || u.getLoginId() == "master") {
                u.setDeptChart(null);
                u.setHighDeptChart(null);
                results.add(u);
            }
        }
        return new ResponseEntity<>(results, HttpStatus.OK);
    }


    /*
    @ApiOperation(value = "부서 리스트 보기", httpMethod = "GET", notes = "전체 하위 부서 리스트")
    @GetMapping("/dept")
    public ResponseEntity<List<Dept>> fetchDept() {
        List<Dept> results = deptMapper.findByDelYnOrderByCodeAsc("N");
        List<Dept> lists = new ArrayList<>();
        for(Dept d : results) {
            if(d.getDeptUpper() != null) {
                lists.add(d);
            }
        }

        return new ResponseEntity<>(lists, HttpStatus.OK);
    }*/
}
