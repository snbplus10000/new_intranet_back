package com.snbplus.madang.contract;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 계약 - 담당자 정보
 * @author bhshin
 *
 */
@Entity
@Table(name="tbl_contract_info")
@Data
public class ContractInfo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private Integer contractId;
	
	@OneToOne
	@JoinColumn(name="user_id")
	private User user;

}
