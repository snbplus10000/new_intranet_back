package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/contract")
public class ContractController implements InterfaceController<Contract> {

    @Autowired
    ContractMapper contractMapper;
    
    @Autowired
    ContractFileMapper fileMapper;
    
    @ApiOperation(value = "계약 리스트", httpMethod = "GET", notes = "계약 리스트")
	@Override
	public ResponseEntity<Page<Contract>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {
		
		return new ResponseEntity<>(contractMapper.findByDelYn(pageable,"N"), HttpStatus.OK);
	}

	@ApiOperation(value = "계약 상세화면", httpMethod = "GET", notes = "업체 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "계약서 관련 PK", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<Contract> view(Integer id) {
		
		return new ResponseEntity<>(contractMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "계약서 저장", httpMethod = "PUT", notes = "계약서 저장")
	@Override
	public ResponseEntity<Contract> save(Contract t) {
		
		
		User user = UserSingleton.getInstance();
		
		t.setCreater(user.getId());
		
		contractMapper.save(t);
		
		List<ContractFile> fileList = t.getContractFile();
		
		if(fileList != null && fileList.size()>0) {
			
			for(ContractFile file : fileList) {
				
				file.setContractId(t.getId());
				
				fileMapper.save(file);
			}
		}
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value="계약서 수정", httpMethod = "PUT", notes = "계약서 수정")
	@Override
	public ResponseEntity<Contract> update(Contract t) {
		
		User user = UserSingleton.getInstance();
		
		t.setModifier(user.getId());
		
		contractMapper.save(t);
		
		List<ContractFile> fileList = t.getContractFile();
		
		if(fileList != null && fileList.size()>0) {
			
			for(ContractFile file : fileList) {
				
				file.setContractId(t.getId());
				
				fileMapper.save(file);
			}
		}
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}
	
	@ApiOperation(value="계약서 삭제", httpMethod = "DELETE", notes = "계약서 삭제")
	@Override
	public ResponseEntity<Contract> delete(Integer id) {
		
		User user = UserSingleton.getInstance();
		
		Contract contract = contractMapper.findOne(id);
 		
		contract.setModifier(user.getId());
		contractMapper.save(contract);
		
		return new ResponseEntity<>(contract, HttpStatus.OK);
	}



}
