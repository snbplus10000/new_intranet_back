package com.snbplus.madang.contract;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 계약서 - 실작업내용 + 의뢰하기 VO
 * @author 신복호
 *
 */
@Entity
@Table(name="tbl_contract_work")
@Data
public class ContractWork {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String contractPay; // 제작금액
	
	private String language; // 언어
	
	private String workDate; // 제작시기 
	
	private String designReport; // 디자인 보고
	
	private String etc; // 비고
	
	private String requestDate; // 의뢰일
	
	@Transient
	private String elapsedDate; // 경과일
		
	private String progressState; //진척도
	
	private String completeDate; // 완료일
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private Integer modifier; // 수정자

	@OneToOne
	@JoinColumn(name="userId")
	private User designUser; // 디자이너 
	
	@Transient
	private String[] languages;
	
	@ManyToOne
	@JoinColumn(name="contractId", insertable=false, updatable=false)	
	private Contract contract;
	
	@ManyToOne
	@JoinColumn(name="workUserId", insertable=false, updatable=false)
	private User workUserId;
	
	@Transient
	private User user; 

}
