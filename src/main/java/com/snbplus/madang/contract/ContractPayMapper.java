package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractPayMapper extends JpaRepository<ContractPay, Integer> {

	List<ContractPay> findByContractIdAndDelYn(Integer id, String delYn);

}
