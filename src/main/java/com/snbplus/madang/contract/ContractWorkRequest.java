package com.snbplus.madang.contract;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 계약서 - 실작업내용 - 외주의뢰하기 VO
 * @author ATU
 *
 */
@Entity
@Table(name="tbl_contract_work_request")
@Data
public class ContractWorkRequest {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String gubun; //구분
		
	private String workPay; // 작업비
	
	private String content; // 내용
	
	private String workType; // 제작형태
	
	private String shooting; //촬영
	
	private String translation;  //번역
	
	private String requestDate;  // 의뢰일
	
	private String deadlineDate;  // 마감일
	
	private String language; // 언어
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	@Transient
	private String[] languages;
	
	
	@ManyToOne
	@JoinColumn(name="userId", insertable=false, updatable=false)
	private User user;
	
	@OneToOne
	@JoinColumn(name="contractWorkId", insertable=false, updatable=false)
	private ContractWork contractWork;
	
	
	

}
