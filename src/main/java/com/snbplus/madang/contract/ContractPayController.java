package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;


public class ContractPayController implements JoinController<List<ContractPay>> {

	@Autowired
	public ContractPayMapper payMapper;

	@ApiOperation(value = "계약 - 외주/사업비 리스트", httpMethod = "GET", notes = "계약 - 외주/사업비 리스트")
	@Override
	public ResponseEntity<List<ContractPay>> list(Integer id) {
		
		return new ResponseEntity<>(payMapper.findByContractIdAndDelYn(id,"N"), HttpStatus.OK);
	}

	@ApiOperation(value = " 계약 - 외주/사업비 저장/수정", httpMethod = "PUT", notes = "계약 - 외주/사업비 저장/수정")
	@Override
	public ResponseEntity<List<ContractPay>> save(List<ContractPay> t) {
		
		for(ContractPay pay : t) {
			payMapper.save(pay);
		}
			
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "계약 - 외주/사업비 삭제", httpMethod = "DELETE", notes = "계약 - 외주/사업비 삭제")
	@Override
	public ResponseEntity<List<ContractPay>> delete(List<ContractPay> t) {
		
		for( ContractPay pay :  t) {
			
			pay.setDelYn("Y");
		}		
		payMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

}
