package com.snbplus.madang.contract;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractMapper extends JpaRepository<Contract, Integer> {

	public Page<Contract> findByDelYn(Pageable pageable, String DelYn);

}
