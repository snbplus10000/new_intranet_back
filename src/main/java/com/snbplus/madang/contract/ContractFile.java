package com.snbplus.madang.contract;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.snbplus.madang.common.FileAttach;

import lombok.Data;

/**
 * 계약서 - 계약 업체자료  VO
 * @author ATU
 *
 */
@Entity
@Table(name="tbl_contract_file")
@Data
public class ContractFile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String items;
	
	private String kind;
	
	private Integer contractId;

	@OneToOne
	@JoinColumn(name="file_id")
	private FileAttach fileAttach;

}
