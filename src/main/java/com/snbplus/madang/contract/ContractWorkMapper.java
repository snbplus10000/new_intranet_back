package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractWorkMapper extends JpaRepository<ContractWork, Integer> {

	List<ContractWork> findByContractIdAndDelYn(Integer id, String delYn);

}
