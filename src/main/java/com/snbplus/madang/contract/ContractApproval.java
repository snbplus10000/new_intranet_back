package com.snbplus.madang.contract;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;


/**
 * 계약서 - 승인내역 VO
 * @author 신복호
 *
 */
@Entity
@Table(name="tbl_contract_approval")
@Data
public class ContractApproval {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String gubun;	//항목
	
	private Integer approvalPay;	//승인금액
	
	private Integer burdenPay;	// 업체부담
	
	private Integer supportPay;	//지원금액
	
	private String etc;	//비고
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;
	
	@ManyToOne
	@JoinColumn(name="creater")
	private User creater;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	@ManyToOne
	@JoinColumn(name="modifier")
	private User modifier; // 수정자
	
	private Integer contractId;
	
	@Transient
	private User user; // 영업자 
	

	
	
}
