package com.snbplus.madang.contract;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractApprovalMapper extends JpaRepository<ContractApproval, Integer> {

}
