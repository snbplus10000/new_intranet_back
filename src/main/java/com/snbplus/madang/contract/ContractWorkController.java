package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping
public class ContractWorkController implements JoinController<List<ContractWork>> {

	@Autowired
	public ContractWorkMapper workMapper;

	@ApiOperation(value = "계약 - 외주/사업비 리스트", httpMethod = "GET", notes = "계약 - 외주/사업비 리스트")
	@Override
	public ResponseEntity<List<ContractWork>> list(Integer id) {
		
		return new ResponseEntity<>(workMapper.findByContractIdAndDelYn(id,"N"), HttpStatus.OK);
	}

	@ApiOperation(value = " 계약 - 외주/사업비 저장/수정", httpMethod = "PUT", notes = "계약 - 외주/사업비 저장/수정")
	@Override
	public ResponseEntity<List<ContractWork>> save(List<ContractWork> t) {
		
		for(ContractWork work : t) {
			workMapper.save(work);
		}
			
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "계약 - 외주/사업비 삭제", httpMethod = "DELETE", notes = "계약 - 외주/사업비 삭제")
	@Override
	public ResponseEntity<List<ContractWork>> delete(List<ContractWork> t) {
		
		for( ContractWork work :  t) {
			
			work.setDelYn("Y");
		}		
		workMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

}
