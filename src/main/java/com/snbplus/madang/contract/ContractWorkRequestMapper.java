package com.snbplus.madang.contract;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractWorkRequestMapper extends JpaRepository<ContractWorkRequest, Integer> {

	List<ContractWorkRequest> findByContractWorkIdAndDelYn(Integer id, String delYn);

}
