package com.snbplus.madang.outing;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OutingReportMapper
		extends JpaRepository<OutingReport, Integer>, JpaSpecificationExecutor<OutingReport> {

	Page<OutingReport> findByDelYn(Pageable pageable, String delYn);

	@Query(value = "SELECT o.* FROM outing_report o left outer join approval a"
			+ " on o.id = a.ref_id and a.ref_table = :refTable where o.del_yn = :delYn and o.creater in :userFilterList and a.step = :step", nativeQuery = true)
	List<OutingReport> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn") String delYn,
			@Param("userFilterList") List<Integer> userFilterList, @Param("step") Integer step);

	List<OutingReport> findByCreaterAndState(Integer id, String string);

	@Query(value = "SELECT v.* FROM outing_report v left outer join member_clone u on u.id = v.creater where u.mem_kind='병특' and v.state = '결재완료' and ((v.start_date >= :startDate and v.start_date <= :endDate) or (v.end_date >= :startDate2 and v.end_date <= :endDate2)) order by v.creater, v.start_date, v.end_date asc ", nativeQuery = true)
	List<OutingReport> findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(@Param("startDate") String startDate,
			@Param("endDate") String endDate, @Param("startDate2") String startDate2,
			@Param("endDate2") String endDate2);

}
