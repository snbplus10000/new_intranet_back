package com.snbplus.madang.outing;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.snbplus.madang.common.FileAttach;

import lombok.Data;

@Entity
@Table(name="outing_file")
@Data
public class OutingFile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String kind;
	
	@OneToOne
	@JoinColumn(name="file_id")
	private FileAttach fileAttach;
}
