package com.snbplus.madang.outing;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.approval.ApprovalMapper;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/outingReport")
@RequiredArgsConstructor
public class OutingReportController implements InterfaceController<OutingReport> {

	private final OutingReportMapper reportMapper;

	private final UserMapper userMapper;

	private final UserAuthService userAuthService;

	private final OutingReportSpec outingReportSpec;

	private final ApprovalMapper approvalMapper;

	@GetMapping("/users")
	public ResponseEntity<List<User>> getLists() {
		List<User> lists = userMapper.findOutingReportCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@ApiOperation(value = "출장보고서 리스트", httpMethod = "GET", notes = "출장보고서 리스트")
	@Override
	public ResponseEntity<Page<OutingReport>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Specification<OutingReport> spec = Specifications
				.where(outingReportSpec.build("N", conditionKey, conditionValue));

		return new ResponseEntity<>(reportMapper.findAll(spec, pageable).map(outingReport -> {

			Integer id = outingReport.getCreater();
			User user = userMapper.findOne(id);
			outingReport.setUser(user);

			return outingReport;
		}), HttpStatus.OK);

	}

	@ApiOperation(value = "출장보고서 상세화면", httpMethod = "GET", notes = "출장보고서 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "출장보고서 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<OutingReport> view(@PathVariable Integer id) {

		OutingReport outingReport = reportMapper.findOne(id);
		User user = userMapper.findOne(outingReport.getCreater());

		outingReport.setUser(user);

		return new ResponseEntity<>(outingReport, HttpStatus.OK);
	}

	@ApiOperation(value = "출장보고서 저장", httpMethod = "PUT", notes = "출장보고서 저장")
	@Override
	public ResponseEntity<OutingReport> save(@RequestBody OutingReport t) {

		User user = UserSingleton.getInstance();

		t.setCreater(user.getId());
		reportMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "출장보고서 수정", httpMethod = "post", notes = "출장보고서 수정")
	@Override
	public ResponseEntity<OutingReport> update(@RequestBody OutingReport t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		reportMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "출장보고서 삭제", httpMethod = "DELETE", notes = "출장보고서 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "출장보고서 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<OutingReport> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();
		OutingReport outingReport = reportMapper.findOne(id);
		outingReport.setDelYn("Y");
		outingReport.setModifier(user.getId());

		reportMapper.save(outingReport);

		return new ResponseEntity<>(outingReport, HttpStatus.OK);
	}

	@GetMapping("/reviewList")
	@ApiOperation(value = "출장 보고서 결재 대기 리스트", httpMethod = "GET", notes = "출장 보고서 결재 대기 리스트")
	public ResponseEntity<List<OutingReport>> reviewList() {
		User curUser = UserSingleton.getInstance();
		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(reportMapper.findByDelYnAndCreatorIn("OUTING_REPORT", "N", userFilterList, step)
				.stream().map(outingReport -> {

					Integer id = outingReport.getCreater();
					User user = userMapper.findOne(id);

					String imgUrl = "";

					if (user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null) {
						imgUrl = "static/img/default-avatar.png";
					} else {
						imgUrl = "/intranet/api/file/image/" + user.getThumbFile().getFileAttach().getId();
					}
					outingReport.setImgUrl(imgUrl);

					return outingReport;

				}).collect(Collectors.toList()), HttpStatus.OK);

	}

	@GetMapping("/rejectList")
	@ApiOperation(value = "출장 보고서 반려 리스트", httpMethod = "GET", notes = "출장 보고서 반려 리스트")
	public ResponseEntity<List<OutingReport>> rejectList() {

		return new ResponseEntity<>(reportMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
				HttpStatus.OK);

	}

	@GetMapping("/soliderList")
	@ApiOperation(value = "출장 보고서 병특 리스트", httpMethod = "GET", notes = "출장 보고서 병특 리스트")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "year", value = "병특리스트 관련 연도", required = true, dataType = "number", defaultValue = "2020"),
			@ApiImplicitParam(name = "month", value = "병특리스트 관련 월", required = true, dataType = "number") })
	public ResponseEntity<List<OutingReport>> soliderServiceByMonth(@RequestParam(required = true) Integer year,
			@RequestParam(required = true) Integer month) {

		String StartDate = "";
		String EndDate = "";
		if (month < 10) {
			StartDate = year + "-0" + month + "-01";
			EndDate = year + "-0" + month + "-31";
		} else {
			StartDate = year + "-" + month + "-01";
			EndDate = year + "-" + month + "-31";
		}

		List<OutingReport> reportList = reportMapper
				.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate, EndDate, StartDate, EndDate)
				.stream().map(outingReport -> {

					Approval approval = approvalMapper.findByRefTableAndRefId("OUTING_REPORT", outingReport.getId());

					User user = userMapper.findOne(outingReport.getCreater());
					/*user.getDeptChart().setDeptUser(null);
					user.getDeptChart().setHighDeptUser(null);
					user.getHighDeptChart().setDeptUser(null);
					user.getHighDeptChart().setHighDeptUser(null);*/
					outingReport.setUser(user);
					outingReport.setApproNo(approval.getAppName2());

					return outingReport;

				}).collect(Collectors.toList());

		return new ResponseEntity<>(reportList, HttpStatus.OK);

	}
}
