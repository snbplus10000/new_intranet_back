package com.snbplus.madang.outing;

import java.sql.Timestamp;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 기존 출장 보고서(이름 겹침)
 * @author bhshin
 *
 */
@Entity
@Table(name = "outing_report")
@Data
public class OutingReport {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String regName;  //신청인
	
	private String startDate;  //출장기간 시작
	
	private String endDate;  //출장기간 끝
	
	private String tripArea; //출장지역
	
	private String purpose;  //출장목적
		
	private String report;  //주요보고사항
	
	private Integer nemNum;  // 인원수
	
	private String memList;  //인원 명단
	
	private Integer fareExp; // 운임비
	
	private String fareCmt; // 운임내용 
	
	private Integer actExp; // 활동비
	
	private String actCmt; // 활동내역
	
	private Integer lodgeExp; // 숙박비
	
	private String lodgeCmt; // 숙박내용
	
	private Integer foodExp; // 식비
	
	private String foodCmt; // 식비내용
	
	private Integer totalExp; // 전체 금액
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	private String state;  //상태

	private String approNo;

	private String inFileId;
	private String inFileName;
	private String outFileId;
	private String outFileName;


	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private User user;
	
	@Transient
	private Approval approval;
	
	@Transient
	private String imgUrl;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;
}
