package com.snbplus.madang.outing;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 기존 출장 신청(이름 겹침)
 * @author bhshin
 *
 */
@Entity
@Table(name = "outing")
@Data
public class Outing {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String regName;
	
	private String startDate;
	
	private String endDate;
	
	private String tripArea;
	
	private String purpose;
	
	private Integer memNum;
	
	private String memList;
	
	private String referReason;
	
	private String state;
	
	private String preNo;
	
	private String approNo;
	
	private String companyName;
	
	private String managerName;
	
	private String managerRank;
	
	private String telNo;

	private String cate;
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	private String etc;
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private User user;
	
	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="outing_file_id")
	private List<OutingFile> outingFile;
	
	@Transient
	private String className;
	
	@Transient
	private String imgUrl;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;


}
