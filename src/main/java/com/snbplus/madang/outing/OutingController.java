package com.snbplus.madang.outing;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.approval.ApprovalMapper;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/outing")
@RequiredArgsConstructor
public class OutingController implements InterfaceController<Outing> {


	private final OutingMapper outingMapper;

	private final UserMapper userMapper;

	private final OutingFileMapper outingfileMapper;

	private final UserAuthService userAuthService;

	private final OutingSpec outingSpec;

	private final ApprovalMapper approvalMapper;


	@GetMapping("/outingUsers")
	public ResponseEntity<List<User>> getOutingLists() {
		List<User> lists = userMapper.findOutingCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@GetMapping("/busiTripUsers")
	public ResponseEntity<List<User>> getBusiTripLists() {
		List<User> lists = userMapper.findApplyCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@ApiOperation(value = "출장신청 리스트", httpMethod = "GET", notes = "출장신청 리스트")
	@GetMapping("/owList")
	public ResponseEntity<Page<Outing>> owList(
		@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
		@RequestParam(required = false) String conditionKey,
		@RequestParam(required = false) String conditionValue,
		@RequestParam(required = false) String cate)
		 {
			Specification<Outing> spec = Specifications.where(outingSpec.build("N", conditionKey, conditionValue ,cate));

			return new ResponseEntity<>(outingMapper.findAll(spec, pageable).map(outing -> {

				User searchUser;
				Integer id = 0;
				User user = new User();
				id = outing.getCreater();
				searchUser = userMapper.findOne(id);
				user.setId(searchUser.getId());
				user.setName(searchUser.getName());
				user.setLoginId(searchUser.getLoginId());
				outing.setUser(user);
				return outing;
			}), HttpStatus.OK);
	}

 	@ApiOperation(value = "출장신청 리스트", httpMethod = "GET", notes = "출장신청 리스트")
	@Override
	public ResponseEntity<Page<Outing>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue)
	{
		Specification<Outing> spec = Specifications.where(outingSpec.build("N", conditionKey, conditionValue, "TEST"));

		return new ResponseEntity<>(outingMapper.findAll(spec, pageable).map(outing -> {

			User searchUser;
			Integer id = 0;
			User user = new User();
			id = outing.getCreater();
			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			outing.setUser(user);
			return outing;
		}), HttpStatus.OK);
	}


	@ApiOperation(value = "출장신청 상세화면", httpMethod = "GET", notes = "출장신청 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "출장신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Outing> view(@PathVariable Integer id) {
		System.out.println("CHECKPOINT111");

		Outing outing = outingMapper.findOne(id);
		User user = userMapper.findOne(outing.getCreater());
		/*searchUser.getDeptChart().setDeptUser(null);
		searchUser.getDeptChart().setHighDeptUser(null);
		searchUser.getHighDeptChart().setDeptUser(null);
		searchUser.getHighDeptChart().setHighDeptUser(null);*/
		//user.setDeptChart(searchUser.getDeptChart());
		//user.setDeptChart(searchUser.getDeptChart());
		outing.setUser(user);
		System.out.println("CHECKPOINT2");
		System.out.println(user.getDeptChart().getManager());
		return new ResponseEntity<>(outing, HttpStatus.OK);
	}

	@ApiOperation(value = "출장신청 저장", httpMethod = "PUT", notes = "출장신청 저장")
	@Override
	public ResponseEntity<Outing> save(@RequestBody Outing t) {
		List<OutingFile> outingFile = t.getOutingFile();

		outingFile.forEach(data -> {
			if (data.getKind() != null) {
				outingfileMapper.save(outingFile);
				t.setOutingFile(outingFile);
			}
		});

		User user = UserSingleton.getInstance();
		t.setCreater(user.getId());
		outingMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "출장신청 수정", httpMethod = "post", notes = "출장신청 수정")
	@Override
	public ResponseEntity<Outing> update(@RequestBody Outing t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		outingMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "출장신청 삭제", httpMethod = "DELETE", notes = "출장신청 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "출장신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Outing> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		Outing outing = outingMapper.findOne(id);
		outing.setDelYn("Y");
		outing.setModifier(user.getId());

		outingMapper.save(outing);

		return new ResponseEntity<>(outing, HttpStatus.OK);
	}

	@GetMapping("/reviewList")
	@ApiOperation(value = "출장 신청서 결재 대기 리스트", httpMethod = "GET", notes = "출장 신청서 결재 대기 리스트")
	public ResponseEntity<List<Outing>> reviewList() {
		User curUser = UserSingleton.getInstance();
		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(
				outingMapper.findByDelYnAndCreatorIn("OUTING", "N", userFilterList, step).stream().map(outing -> {

					Integer id = outing.getCreater();
					User user = userMapper.findOne(id);

					String imgUrl = "";

					if (user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null) {
						imgUrl = "static/img/default-avatar.png";
					} else {
						imgUrl = "/intranet/api/file/image/" + user.getThumbFile().getFileAttach().getId();
					}
					outing.setImgUrl(imgUrl);

					return outing;

				}).collect(Collectors.toList()), HttpStatus.OK);

	}

	@GetMapping("/rejectList")
	@ApiOperation(value = "출장 신청서 반려 리스트", httpMethod = "GET", notes = "출장 신청서 반려 리스트")
	public ResponseEntity<List<Outing>> rejectList() {

		return new ResponseEntity<>(outingMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
				HttpStatus.OK);

	}

	@GetMapping("/completeList")
	@ApiOperation(value = "출장 신청서 완료 리스트", httpMethod = "GET", notes = "출장신청서 완료 리스트")
	public ResponseEntity<List<Outing>> completeList() {

		List<Outing> list = outingMapper.findByDelYnAndState("N", "결재완료");
		for (Outing v : list) {

			Integer creater = v.getCreater();
			String className = "";

			User searchUser = userMapper.findOne(creater);
			if (searchUser.getDeptChart().getCode() == 12) { // 디크팀
				className = "event-red";
			} else if (searchUser.getDeptChart().getCode() == 10) { // 개발팀
				className = "event-rose";
			}
			/*
			 * else if(searchUser.getDeptChart().getCode() == 15) { className =
			 * "event-green"; }else if(searchUser.getDeptChart().getCode() == 17) {
			 * className = "event-orange"; }
			 */
			else {
				className = "event-default";
			}
			v.setClassName(className);
		}

		return new ResponseEntity<>(list, HttpStatus.OK);

	}

	@GetMapping("/soliderList")
	@ApiOperation(value = "출장 신청서 병특 리스트", httpMethod = "GET", notes = "출장 신청서 병특 리스트")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "year", value = "병특리스트 관련 연도", required = true, dataType = "number", defaultValue = "2020"),
			@ApiImplicitParam(name = "month", value = "병특리스트 관련 월", required = true, dataType = "number") })
	public ResponseEntity<List<Outing>> soliderServiceByMonth(@RequestParam(required = true) Integer year,
			@RequestParam(required = true) Integer month) {

		String StartDate = "";
		String EndDate = "";
		if (month < 10) {
			StartDate = year + "-0" + month + "-01";
			EndDate = year + "-0" + month + "-31";
		} else {
			StartDate = year + "-" + month + "-01";
			EndDate = year + "-" + month + "-31";
		}

		List<Outing> outingList = outingMapper
				.findByStartDateAtBetweenOrEndDateAtBetweenOrderByIdDesc(StartDate, EndDate, StartDate, EndDate)
				.stream().map(outing -> {

					Approval approval = approvalMapper.findByRefTableAndRefId("OUTING", outing.getId());

					User user = userMapper.findOne(outing.getCreater());
					/*user.getDeptChart().setDeptUser(null);
					user.getDeptChart().setHighDeptUser(null);
					user.getHighDeptChart().setDeptUser(null);
					user.getHighDeptChart().setHighDeptUser(null);*/
					outing.setUser(user);
					outing.setApproNo(approval.getAppName2());

					return outing;

				}).collect(Collectors.toList());

		return new ResponseEntity<>(outingList, HttpStatus.OK);
	}

}
