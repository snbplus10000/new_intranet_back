package com.snbplus.madang.outing;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OutingFileMapper extends JpaRepository<OutingFile, Integer>{

}
