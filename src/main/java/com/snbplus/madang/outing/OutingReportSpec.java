package com.snbplus.madang.outing;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.user.UserMapper;

@Component
public class OutingReportSpec {

	@Autowired
    UserMapper userMapper;

	public Specification<OutingReport> build(String delYn, String conditionKey, String conditionValue) {
		
        if(conditionKey == null || conditionKey.equals("")) {
            return search(delYn);
        } else if(conditionKey.equals("memKind")) {           	
        	 return searchMemKind(delYn, conditionKey, userMapper.findByMemKind("병특").stream().map(user->user.getId()).collect(Collectors.toList()),userMapper.findByNameLike("%" + conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()));
        } else if(conditionKey != null && conditionKey.equals("creater")) {
        	return search(delYn, conditionKey, userMapper.findByNameLike("%" + conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()));
        }


        return search(delYn, conditionKey, "%" + conditionValue + "%");

	}

    public Specification<OutingReport> search(String delYn) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn)
                );

    }
    
    public Specification<OutingReport> search(String delYn, String conditionKey, String conditionValue) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn),

                        cb.like(root.get(conditionKey), conditionValue)
                );

    }
    
    public  Specification<OutingReport> search(String delYn, String conditionKey, List conditionValue) {

        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : conditionValue) {
                in.value(conditionColumnValue);           	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };


    }


    
    public Specification<OutingReport> searchMemKind(String delYn, String conditionKey, List userAuthList, List userSearchList) {
          	
        return (root, query, cb) -> {


            Path<Object> path = root.get("creater");
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : userAuthList) {
            	for(Object creater : userSearchList) {
            		if(conditionColumnValue == creater) {
            			   in.value(conditionColumnValue);
            		}
            	}
             
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };
    }
}
