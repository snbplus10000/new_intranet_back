package com.snbplus.madang.meetingRoom;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MeetingRoomMapper extends JpaRepository<MeetingRoom, Integer> {

	Page<MeetingRoom> findByDelYn(Pageable pageable, String delYn);

}
