package com.snbplus.madang.meetingRoom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/meetingRoom")
public class MeetingRoomController implements InterfaceController<MeetingRoom> {

	@Autowired
	MeetingRoomMapper roomMapper;
	
	@Autowired
	UserMapper userMapper;

	@ApiOperation(value = "회의실예약 리스트", httpMethod = "GET", notes = "회의실예약 리스트")
	@Override
	public ResponseEntity<Page<MeetingRoom>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {
		return new ResponseEntity<>(roomMapper.findByDelYn(pageable,"N")
				.map(MeetingRoom -> {
				
				 	User searchUser;
				 	Integer id = 0;
				 	User user = new User();
				 	if(MeetingRoom.getModifier() != null && MeetingRoom.getModifier() != 0) {
				 		id = MeetingRoom.getModifier();

				 	}else {
				 		id = MeetingRoom.getCreater();
				 	}
			 		searchUser = userMapper.findOne(id);
				 	user.setId(searchUser.getId());
				 	user.setName(searchUser.getName());
				 	user.setLoginId(searchUser.getLoginId());
				 	MeetingRoom.setUser(user);
					return MeetingRoom;
				}),HttpStatus.OK);

	}

	
	@ApiOperation(value = "회의실예약 상세화면", httpMethod = "GET", notes = "회의실예약 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "회의실예약 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<MeetingRoom> view(@PathVariable Integer id) {
		
		return new ResponseEntity<>(roomMapper.findOne(id), HttpStatus.OK);
	}

	@ApiOperation(value = "회의실예약 저장", httpMethod = "PUT", notes = "회의실예약 저장")
	@Override
	public ResponseEntity<MeetingRoom> save(@RequestBody MeetingRoom t) {
		
		roomMapper.save(t);
	
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "회의실예약 수정", httpMethod = "POST", notes = "회의실예약 수정")
	@Override
	public ResponseEntity<MeetingRoom> update( @RequestBody MeetingRoom t) {
		
		User user = UserSingleton.getInstance();
				
		t.setModifier(user.getId());
		
		roomMapper.save(t);
		
		return new ResponseEntity<>(t, HttpStatus.OK);
		

	}

	@ApiOperation(value = "회의실예약 삭제", httpMethod = "DELETE", notes = "회의실예약 삭제")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "회의실예약 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<MeetingRoom> delete(@PathVariable Integer id) {
		
		MeetingRoom MeetingRoom = roomMapper.findOne(id);
		MeetingRoom.setDelYn("Y");
		roomMapper.save(MeetingRoom);
		
		return new ResponseEntity<>(MeetingRoom, HttpStatus.OK);

	}

}
