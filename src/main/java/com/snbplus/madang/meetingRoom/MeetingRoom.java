package com.snbplus.madang.meetingRoom;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 회의실 관련 vo
 * @author bhshin
 *
 */
@Entity
@Table(name = "meeting_room")
@Data
public class MeetingRoom {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String writer;
	
	private String dept;
	
	private String comTel;
	
	private String perTel;
	
	private String userDate;
	
	private String startTime;
	
	private String endTime;
	
	private String place;
	
	private Integer attendance;
	
	private String comment;
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private User user;
}
