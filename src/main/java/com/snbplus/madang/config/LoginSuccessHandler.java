package com.snbplus.madang.config;

import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserDetail;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class LoginSuccessHandler implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    UserMapper userMapper;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {

        AuthenticationSuccessEvent appEvent = (AuthenticationSuccessEvent) event;
        UserDetails userDetails = (UserDetails) appEvent.getAuthentication().getPrincipal();

        UserDetail user = (UserDetail) userDetails;

        User loginUser = userMapper.findOne(user.getUser().getId());
        loginUser.setLastLoginTime(new Date());
        userMapper.save(loginUser);

    }
}
