package com.snbplus.madang.official;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficialMapper extends JpaRepository<Official, Integer>, JpaSpecificationExecutor<Official> {

	Optional<Official> findById(Integer id);

	Optional<Official> findFirstByApplyDateAndRuleAndDelYnOrderByCreatedDesc(String date, String rule, String delYn);

	Optional<Official> findFirstByApplyDateContainingAndRuleAndKindAndDelYnOrderByCreatedDesc(String date, String rule, String kind, String delYn);

}
