package com.snbplus.madang.official;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "official")
@Data
public class Official {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String kind; // 뮨서 종류

	private String title; // 문서 제목

	private String destination; // 수신처

	private String applyDate; // 시행일자

	private String drafter; // 기안자

	private String rule; // 문서생성규칙

	private String documentNo; // 문서번호

	private String orderNo;

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

	private String fileName;

	private Integer fileId;

	@Transient
	private User user;
}
