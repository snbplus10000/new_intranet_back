package com.snbplus.madang.official;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/official")
public class OfficialController implements InterfaceController<Official> {

	@Autowired
	OfficialMapper officialMapper;

	@Autowired
	OfficialSpec officialSpec;

	@Autowired
	UserMapper userMapper;

	@ApiOperation(value = "공문번호 리스트", httpMethod = "GET", notes = "공문번호 리스트")
	@Override
	public ResponseEntity<Page<Official>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Specification<Official> spec = Specifications.where(officialSpec.build("N", conditionKey, conditionValue));

		return new ResponseEntity<>(officialMapper.findAll(spec, pageable).map(official -> {
			User searchUser;
			Integer id = 0;
			User user = new User();

			id = official.getCreater();

			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			official.setUser(user);

			return official;
		}), HttpStatus.OK);
	}

	@ApiOperation(value = "공문번호 상세화면", httpMethod = "GET", notes = "공문번호 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "공문번호 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Official> view(@PathVariable Integer id) {
		Official t = officialMapper.findOne(id);
		User searchUser = userMapper.findOne(t.getCreater());
		/*searchUser.getDeptChart().setDeptUser(null);
		searchUser.getDeptChart().setHighDeptUser(null);*/
		User user = new User();
		user.setRankName(searchUser.getRankName());
		user.setName(searchUser.getName());
		user.setDeptChart(searchUser.getDeptChart());
		t.setUser(user);
		t.getUser().setHighDeptChart(null);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "공문번호 저장", httpMethod = "PUT", notes = "공문번호 저장")
	@Override
	public ResponseEntity<Official> save(@RequestBody Official t) {

		User user = UserSingleton.getInstance();
		String year = t.getApplyDate().substring(0, 4);
		String orderNo = "";
		Integer no = 0;

		if(t.getKind().equals("증명서")){
			Optional<Official> officialOptional = officialMapper
					.findFirstByApplyDateContainingAndRuleAndKindAndDelYnOrderByCreatedDesc(year, t.getRule(), t.getKind(), "N");

			if (officialOptional.isPresent()) {
				no = Integer.parseInt(officialOptional.get().getOrderNo()) + 1;
				orderNo = String.valueOf(no);

				if(orderNo.length() < 3) {
					orderNo = (orderNo.length() == 1 ? "00" + orderNo : "0" + orderNo);
				}

			} else {
				orderNo = "001";
			}
			t.setOrderNo(orderNo);

			String documentNo = "제 " + year + "-" + t.getOrderNo() + t.getRule().substring(0, 1) + " 호";
			t.setDocumentNo(documentNo);
		}else {

			Optional<Official> officialOptional = officialMapper
					.findFirstByApplyDateAndRuleAndDelYnOrderByCreatedDesc(t.getApplyDate(), t.getRule(), "N");

			if (officialOptional.isPresent()) {
				no = Integer.parseInt(officialOptional.get().getOrderNo()) + 1;
				orderNo = String.valueOf(no);
				orderNo = (orderNo.length() == 1 ? "00" + orderNo : "0" + orderNo);
			} else {
				orderNo = "001";
			}

			t.setOrderNo(orderNo);

			String documentNo = "S" + t.getApplyDate() + "-" + t.getOrderNo() + t.getRule();
			t.setDocumentNo(documentNo);
		}
		t.setCreater(user.getId());
		t.setDelYn("N");
		officialMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "공문번호 수정", httpMethod = "POST", notes = "공문번호 수정")
	@Override
	public ResponseEntity<Official> update(@RequestBody Official t) {


		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		officialMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "공문번호 삭제", httpMethod = "DELETE", notes = "공문번호 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "공문번호 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Official> delete(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();
		Official t = officialMapper.findOne(id);

		t.setDelYn("Y");
		t.setModifier(user.getId());
		officialMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

}
