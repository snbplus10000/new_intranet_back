package com.snbplus.madang.approval;

import com.snbplus.madang.alterVacation.AlterVacation;
import com.snbplus.madang.alterVacation.AlterVacationMapper;
import com.snbplus.madang.busiCard.BusiCardCalc;
import com.snbplus.madang.busiCard.BusiCardCalcMapper;
import com.snbplus.madang.busiCard.BusiCardUse;
import com.snbplus.madang.busiCard.BusiCardUseMapper;
import com.snbplus.madang.codes.CodesMapper;
import com.snbplus.madang.common.MailService;
import com.snbplus.madang.holiwork.HoliWork;
import com.snbplus.madang.holiwork.HoliWorkMapper;
import com.snbplus.madang.notification.Notification;
import com.snbplus.madang.notification.NotificationMapper;
import com.snbplus.madang.notification.NotificationMemberClone;
import com.snbplus.madang.official.Official;
import com.snbplus.madang.official.OfficialMapper;
import com.snbplus.madang.outing.Outing;
import com.snbplus.madang.outing.OutingMapper;
import com.snbplus.madang.outing.OutingReport;
import com.snbplus.madang.outing.OutingReportMapper;
import com.snbplus.madang.purchase.Purchase;
import com.snbplus.madang.purchase.PurchaseMapper;
import com.snbplus.madang.tenure.Tenure;
import com.snbplus.madang.tenure.TenureMapper;
import com.snbplus.madang.user.*;
import com.snbplus.madang.userCard.UserCardCalc;
import com.snbplus.madang.userCard.UserCardCalcMapper;
import com.snbplus.madang.vacation.Vacation;
import com.snbplus.madang.vacation.VacationMapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/approval")
public class ApprovalController {	

	private final ApprovalMapper approvalMapper;
	private final VacationMapper vacationMapper;
	private final BusiCardUseMapper cardUseMapper;
	private final BusiCardCalcMapper calcMapper;
	private final OutingMapper outingMapper;
	private final PurchaseMapper purchaseMapper;
	private final OutingReportMapper reportMapper;
	private final HoliWorkMapper workMapper;
	private final UserHoliMapper holiMapper;
	private final AlterVacationMapper alterVacationMapper;
	private final MailService mailService;
	private final UserMapper userMapper;
	private final UserCardCalcMapper userCardCalcMapper;
	private final NotificationMapper notificationMapper;
	private final TenureMapper tenureMapper;
	private final OfficialMapper officialMapper;
	private final CodesMapper codesMapper;

	@GetMapping("/{id}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "table", value = "연관 table", required = false, dataType = "String", defaultValue = ""),
			@ApiImplicitParam(name = "id", value = "연관 pk", required = false, dataType = "number", defaultValue = "0") })
	public ResponseEntity<Approval> view(@PathVariable Integer id, String table) {

		return new ResponseEntity<>(approvalMapper.findByRefTableAndRefId(table, id), HttpStatus.OK);
	}

	@PostMapping("/")
	@Transactional
	public ResponseEntity<Approval> update(@RequestBody Approval approval, HttpServletRequest request) throws Exception {

		Integer id = 0;
		String state = "";

		User user = UserSingleton.getInstance();
		Integer userId = user.getId();
		Integer defaultId = 0;

		approval.setRefId(approval.getRefId());
		String name = user.getName();

		Date now = (Date) Calendar.getInstance().getTime();

		// now.setHours(now.getHours() +9);

		Vacation vacation;
		BusiCardUse busiCardUse;
		Outing outing;
		Purchase purchase;
		BusiCardCalc busiCardCalc;
		OutingReport outingReport;
		HoliWork holiWork;
		AlterVacation alterVacation;
		UserCardCalc userCardCalc;
		Tenure tenure;

		Approval approvalResult = null;

		approvalResult = approvalMapper.findByRefTableAndRefId(approval.getRefTable(), approval.getRefId());

		if(approval.getDeferReason() != null && !approval.getDeferReason().equals("")) {
			approvalResult.setDeferReason(approval.getDeferReason());
		}

		if(approval.getCancel().equals("Y")) { // 반려일 경우
			approvalResult.setStep(5);
			if(user.getLoginId().equals("master")) {
				approvalResult.setAppId4(null);
				approvalResult.setAppDate4(null);
				approvalResult.setAppName4("");
				approvalResult.setAppId3(null);
				approvalResult.setAppDate3(null);
				approvalResult.setAppName3("");
			}else if(user.getPosition().equals("실장") || user.getLoginId().equals("snbkim")) {
				approvalResult.setAppId3(userId);
				approvalResult.setAppDate3(now);
				approvalResult.setAppName3(name);
			}else if(user.getPosition().equals("팀장")) {
				approvalResult.setAppId2(userId);
				approvalResult.setAppDate2(now);
				approvalResult.setAppName2(name);
			}
			approvalMapper.save(approvalResult);
			id = approvalResult.getRefId();
			state = "반려";
		} else { // 반려가 아닐 경우
			Boolean emergent = approval.getEmergent();
			User userRef = userMapper.findOne(approval.getUserRef());
			switch (approval.getStep()) {
				case 0: //일반 상신
					if (approval.getCancel().equals("R") && approvalResult != null) {
						approvalResult.setStep(0);
						approvalResult.setAppId1(defaultId);
						approvalResult.setAppDate1(null);
						approvalResult.setAppName1(null);
						id = approval.getRefId();
						state = "접수";
						approvalMapper.save(approvalResult);
					}else {
						approval.setStep(1);
						approval.setAppId1(userId);
						approval.setAppDate1(now);
						approval.setAppName1(name);
						id = approval.getRefId();
						state = "결재중";
						if((approval.getUserRef()==13 || approval.getUserRef()==14) && (userRef.getId()==13 || userRef.getId()==14) && approval.getRefTable().equals("TENURE")){
							approval.setStep(4);
							state = "결재완료";
						}
						approvalMapper.save(approval);
						saveNotification(user, approval, "상신");
					}
					break;
				case 1:
					//1차 결재
					Integer secondId = userRef.getSecondApproval();
					id = approval.getRefId();

					//구매품의 총 금액이 기준 금액보다 적으면 2차 결재 전결
					Purchase t = purchaseMapper.findOne(id);
					Integer totalPrice = 0;
					if(t != null){
						totalPrice = t.calculateTotalAmount();
					}

					if(approval.getRefTable().equals("PURCHASE") && totalPrice <= codesMapper.findByCode("PC001").getCodeValue()){
						approvalResult.setStep(4); //완료 처리
						approvalResult.setAppId2(userId);
						approvalResult.setAppDate2(now);
						approvalResult.setAppName2(name);
						state = "결재완료";
					} else if(secondId == null || secondId == 0) {
						//emergent 여부에 따라 전결 (대표)란만 바뀜
						//긴급과 1차 결재자가 같은지 확인
						if(!approval.getRefTable().equals("TENURE") && userRef.getFirstApproval() == userRef.getEmergencyApproval()) {
							approvalResult.setStep(4); //완료 처리
							if(emergent.booleanValue() == true) {
								approvalResult.setAppId2(userId);
								approvalResult.setAppDate2(now);
								approvalResult.setAppName2(name);
								approvalResult.setAppId4(userId);
								approvalResult.setAppName4(name);
								approvalResult.setAppDate4(now);
							}else {
								approvalResult.setAppId2(userId);
								approvalResult.setAppDate2(now);
								approvalResult.setAppName2(name);
							}
							state = "결재완료";
						}else {
							state = "결재중";
							approvalResult.setAppId2(userId);
							approvalResult.setAppDate2(now);
							approvalResult.setAppName2(name);
							approvalResult.setStep(3); //긴급결재 분기처리
						}
					}else {
						approvalResult.setAppId2(userId);
						approvalResult.setAppDate2(now);
						approvalResult.setAppName2(name);
						approvalResult.setStep(2);
						state = "결재중";
					}
					approvalMapper.save(approvalResult);
					break;
				case 2:
					//2차 결재
					System.out.println("CHECKPOINT");
					System.out.println(emergent);
					approvalResult.setAppId3(userId);
					approvalResult.setAppDate3(now);
					approvalResult.setAppName3(name);
					id = approval.getRefId();

					if(emergent.booleanValue() == true) {
						approvalResult.setStep(3);
						state = "결재중";
					}else {
						approvalResult.setStep(4);
						state = "결재완료";
					}

					approvalMapper.save(approvalResult);
					break;
				case 3: //긴급 결재
						approvalResult.setAppId4(userId);
						approvalResult.setAppDate4(now);
						approvalResult.setAppName4(name);
						id = approval.getRefId();
						approvalResult.setStep(4);

						state = "결재완료";
						approvalMapper.save(approvalResult);
					break;
				default:
					break;
			}
		}

		// 반려이거나 결재완료이거나 접수인 경우 알림 발송
		if(state.equals("반려") || state.equals("결재완료") || state.equals("접수")) {
			saveNotification(user, approvalMapper.findOne(approval.getId()), state);
		}

		if(approvalResult == null) {
			approvalResult = approval;
		}
		if (approval.getRefTable().equals("VACATION")) {

			vacation = vacationMapper.findOne(id);
			vacation.setState(state);
			vacation.setModifier(user.getId());
			vacation.setDeferReason(approval.getDeferReason());
			vacationMapper.save(vacation);

			// 반려가 됐을 경우, 휴가 신청 후 삭감되었던 휴가기간을 되돌려준다.
			if(state.equals("반려")) {
				// 유저의 휴가 정보 가져오기
				String realYear = vacation.getStartDate().substring(0, 4);
				Integer memberId = vacation.getCreater();
				Optional<UserHoli> memberholiOptional = holiMapper.findByDelYnAndYearAndRefId("N", realYear, memberId);

				double period = vacation.getHoliPeriod();
				memberholiOptional.ifPresent(holi -> {
					holi.setUseCnt(holi.getUseCnt() - period);
					holiMapper.save(holi);
				});

			}
			approvalResult.setRegistrant(vacation.getCreater());
			
		} else if (approval.getRefTable().equals("BUSI_CARD_USE")) {

			busiCardUse = cardUseMapper.findOne(id);
			busiCardUse.setState(state);
			busiCardUse.setModifier(user.getId());
			busiCardUse.setDeferReason(approval.getDeferReason());
			cardUseMapper.save(busiCardUse);
			approvalResult.setRegistrant(busiCardUse.getCreater());
			
		} else if (approval.getRefTable().equals("OUTING")) {
			// 출장 신청
			outing = outingMapper.findOne(id);
			outing.setState(state);
			outing.setModifier(user.getId());
			outing.setDeferReason(approval.getDeferReason());
			outingMapper.save(outing);
			approvalResult.setRegistrant(outing.getCreater());
			
		} else if (approval.getRefTable().equals("PURCHASE")) {

			purchase = purchaseMapper.findOne(id);
			purchase.setState(state);
			purchase.setModifier(user.getId());
			purchase.setDeferReason(approval.getDeferReason());
			purchaseMapper.save(purchase);
			approvalResult.setRegistrant(purchase.getCreater());
			
			//메일 전송 로직
			purchase.setUser(userMapper.findOne(purchase.getCreater()));
			if (purchase.getPuschaser() != null)
				purchase.setPurchaseUser(userMapper.findOne(purchase.getPuschaser()));
			mailService.purchaseApprovalMailSend(purchase);
		} else if (approval.getRefTable().equals("BUSI_CARD_CALC")) {

			busiCardCalc = calcMapper.findOne(id);
			busiCardCalc.setState(state);
			busiCardCalc.setModifier(user.getId());
			busiCardCalc.setDeferReason(approval.getDeferReason());
			calcMapper.save(busiCardCalc);			
			approvalResult.setRegistrant(busiCardCalc.getCreater());
			
		} else if (approval.getRefTable().equals("OUTING_REPORT")) {
			// 출장 보고서
			outingReport = reportMapper.findOne(id);
			outingReport.setState(state);
			outingReport.setModifier(user.getId());
			outingReport.setDeferReason(approval.getDeferReason());
			reportMapper.save(outingReport);
			approvalResult.setRegistrant(outingReport.getCreater());

		} else if (approval.getRefTable().equals("HOLI_WORK")) {

			holiWork = workMapper.findOne(id);
			holiWork.setState(state);
			holiWork.setModifier(user.getId());
			holiWork.setDeferReason(approval.getDeferReason());
			workMapper.save(holiWork);
			approvalResult.setRegistrant(holiWork.getCreater());

		}else if(approval.getRefTable().equals("ALTER_VACATION")) {
			alterVacation = alterVacationMapper.findOne(id);
			alterVacation.setState(state);
			alterVacation.setModifier(user.getId());
			alterVacation.setDeferReason(approval.getDeferReason());
			alterVacationMapper.save(alterVacation);
			approvalResult.setRegistrant(alterVacation.getCreater());
		}else if(approval.getRefTable().equals("USER_CARD_CALC")) {
			userCardCalc = userCardCalcMapper.findOne(id);
			userCardCalc.setState(state);
			userCardCalc.setModifier(user.getId());
			userCardCalc.setDeferReason(approval.getDeferReason());
			userCardCalcMapper.save(userCardCalc);
	 		approvalResult.setRegistrant(userCardCalc.getCreater());
		}else if (approval.getRefTable().equals("TENURE")) {
			tenure = tenureMapper.findById(id);
			tenure.setState(state);
			tenure.setModifier(user);
			if(state.equals("결재완료")){
				Official tenureOfficial = new Official();
				tenureOfficial.setApplyDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
				tenureOfficial.setCreater(tenure.getUser().getId());
				tenureOfficial.setDelYn("N");
				tenureOfficial.setDestination(tenure.getSubjectOfSubmit());
				tenureOfficial.setDocumentNo(tenure.getDocumentNumber());
				tenureOfficial.setDrafter(tenure.getUser().getName());
				tenureOfficial.setKind("증명서");
				tenureOfficial.setOrderNo("001");
				tenureOfficial.setRule("E");
				tenureOfficial.setTitle(tenure.getTitle());
				officialMapper.save(tenureOfficial);
			}
			approvalResult.setRegistrant(tenure.getUser().getId());
		}

		return new ResponseEntity<>(approvalResult, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Transactional
	public ResponseEntity<Approval> delete(@PathVariable Integer id) {
		Approval approval = approvalMapper.findOne(id);
		approval.setDelYn("Y");
		approvalMapper.save(approval);
		return new ResponseEntity<>(approval, HttpStatus.OK);
	}

	//알림 저장 로직
	public void saveNotification(User user, Approval approval, String status) {
		List<User> receivers = userMapper.findByAuth(1); // 권한이 Admin인 user들 받는 사람으로 기본 설정
		User applicant = userMapper.findOne(approval.getAppId1()); // (상신자 Id == 신청자 Id)로 신청자 얻어오기

		if(status.equals("반려")) {
			receivers.add(applicant); // 반려됐을 경우 받는 사람에 신청자 추가
		} else if(status.equals("결재완료")) {
			/*
			List<User> deptUsers = applicant.getDeptLoad().getDeptUser(); // 동일 부서 직원들 얻어오는 로직
			receivers.addAll(deptUsers); // 결재완료됐을 경우 받는 사람에 신청자 및 동일 부서 직원들 추가
			*/
		} else if(status.equals("상신")) {//상신의 경우 1차 결재자에게도 알림 전송
			User firstApproval = null;

			if(approval.getRefTable().equals("TENURE")){
				firstApproval = applicant.getTenureFirstApproval() != null ? userMapper.findOne(applicant.getTenureFirstApproval()) : null;
			} else {
				firstApproval = applicant.getFirstApproval() != null ? userMapper.findOne(applicant.getFirstApproval()) : null;
			}

			User finalFirstApproval = firstApproval;
			if(firstApproval != null && receivers.stream().filter(receiver -> receiver.getId().equals(finalFirstApproval.getId())).collect(Collectors.toList()).size() == 0){
				receivers.add(firstApproval);
			}
		}
		// 알림 저장을 위한 객체 설정
		Notification notification = new Notification();
		notification.setApproval(approval); // approval_id 컬럼 값 설정
		notification.setStatus(status.equals("접수")?"상신취소":status); // 승인상태
		notification.setCreater(user); // 알림 생성자
		if(approval.getRefTable().equals("BUSI_CARD_CALC")) {
			notification.setKind("법인카드정산");
			notification.setLink("apply/calc/read");
		} else if(approval.getRefTable().equals("VACATION")) {
			notification.setKind("휴가");
			notification.setLink("apply/vacation/read");
		} else if(approval.getRefTable().equals("OUTING")) {
			notification.setKind("출장");
			notification.setLink("apply/tripApply/read");
		} else if(approval.getRefTable().equals("OUTING_REPORT")) {
			notification.setKind("출장보고서");
			notification.setLink("apply/tripReport/read");
		} else if(approval.getRefTable().equals("USER_CARD_CALC")) {
			notification.setKind("개인카드정산");
			notification.setLink("apply/userCalc/read");
		} else if(approval.getRefTable().equals("PURCHASE")) {
			notification.setKind("구매품의");
			notification.setLink("apply/purchase/read");
		} else if(approval.getRefTable().equals("HOLI_WORK")) {
			notification.setKind("휴일근무");
			notification.setLink("apply/holiwork/read");
		} else if(approval.getRefTable().equals("BUSI_CARD_USE")) {
			notification.setKind("법인카드사용");
			notification.setLink("apply/use/read");
		} else if(approval.getRefTable().equals("TENURE")) {
			notification.setKind("재직증명서신청");
			notification.setLink("apply/tenure/read");
		}
		notification.setLink("/" + notification.getLink() + "/" + approval.getRefId());
		receivers.stream().filter(u -> !u.getId().equals(user.getId())).forEach(receiver -> {
			NotificationMemberClone notificationMemberClone = new NotificationMemberClone();
			notificationMemberClone.setNotification(notification); //notification_id 컬럼 값 설정
			notificationMemberClone.setReadYn("N"); // 읽음여부 N으로 설정
			notificationMemberClone.setReceiver(receiver); // 받는 사람 설정
			notification.getNotificationList().add(notificationMemberClone);
		});

		notificationMapper.save(notification);
	}
}
