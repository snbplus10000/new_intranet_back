package com.snbplus.madang.approval;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 승인 관련 entity
 * @author bhshin
 *
 */
@Entity
@Table(name = "approval")
@Data
public class Approval {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id; // pk
	
	@Column(unique = true )
	private String refTable; // 참조 테이블
	
	@Column(unique = true )
	private Integer refId; // 참조 ID 
	
	private Integer step;  // 단계
	
	private Integer appId1;  // 결재1(담당자)
	
	private String appName1; // 성명1
	
	private Date appDate1; // 결제일1

	private Integer appId2; // 결재2 (중간결재자)

	private String appName2; //성명 2

	private Date appDate2; //결재일2

	private Integer appId3; // 결재3(팀장)
	
	private String appName3; // 성명3
	 
	private Date appDate3;  // 결제일3
	
	private Integer appId4;// 결재3(실장)
	
	private String appName4; // 성명4
	
	private Date appDate4;  // 결제일4

/*
	private Integer appId5; // 결재5(대표)

	private String appName5; // 성명5

	private Date appDate5; // 결제일5*/
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private String cancel;


	@Transient
	private int userRef;

	@Transient
	private Boolean emergent;

	@Transient
	private String deferReason=""; //보류사유
	
	@Transient
	private Integer registrant;

}
