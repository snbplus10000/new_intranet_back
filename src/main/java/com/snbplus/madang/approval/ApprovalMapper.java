package com.snbplus.madang.approval;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApprovalMapper extends JpaRepository<Approval, Integer> {

	Approval findByRefTableAndRefId(String table, Integer id);

}
