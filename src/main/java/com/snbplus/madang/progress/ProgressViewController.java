package com.snbplus.madang.progress;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProgressViewController {
	
	@GetMapping("/progress/list")
	public String progressList(Model model) {
		
		return "progress/progressList";
	}
}
