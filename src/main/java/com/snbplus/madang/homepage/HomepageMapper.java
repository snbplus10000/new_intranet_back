package com.snbplus.madang.homepage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomepageMapper extends JpaRepository<Homepage, Integer> {

}
