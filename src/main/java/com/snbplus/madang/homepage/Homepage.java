package com.snbplus.madang.homepage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

/**
 * 홈페이지 - 관리정보 vo
 * @author bhshin
 *
 */
@Entity
@Table(name = "tbl_homepage")
@Data
public class Homepage {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String state; // 상태
	
	private String changeReason; // 보류
	
	private String holdStartDate;
	
	private String holdEndDate;
	
	private String progress;
	
	private String result;
	
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	

}
