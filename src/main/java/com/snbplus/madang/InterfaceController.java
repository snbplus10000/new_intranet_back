package com.snbplus.madang;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


/**
 * join의 최상단에 대한 generic화를 위한 인터페이스
 * example - company, daily, busiTripCar 등
 * @author bhshin
 *
 * @param <T>
 */
public interface InterfaceController<T> {

	@GetMapping("")
	@ApiImplicitParams({
		@ApiImplicitParam(name="page", value = "페이지 번호" , required = false, dataType = "number", defaultValue = "1"),
		@ApiImplicitParam(name="size", value = "페이지 사이즈" , required = false, dataType = "number", defaultValue = "10")
	})
	ResponseEntity<Page<T>> list( Pageable pageable, String conditionKey,  String conditionValue);


	/*@GetMapping("")
	@ApiImplicitParams({
			@ApiImplicitParam(name="page", value = "페이지 번호" , required = false, dataType = "number", defaultValue = "1"),
			@ApiImplicitParam(name="size", value = "페이지 사이즈" , required = false, dataType = "number", defaultValue = "10")
	})
	ResponseEntity<Page<T>> list2( Pageable pageable, String conditionKey,  String conditionValue, String cate);*/


	@GetMapping("/{id}")
	@ApiImplicitParams({
		@ApiImplicitParam(name="id", value = "번호" , required = false, dataType = "number", defaultValue = "1"),
		
	})
	ResponseEntity<T> view(@PathVariable Integer id);
	
	@PutMapping("")
	@Transactional
	ResponseEntity<T> save(@RequestBody T t);
	
	@PostMapping("/")
	@Transactional
	ResponseEntity<T> update(@RequestBody T t);
	
	@DeleteMapping("/{id}")
	ResponseEntity<T> delete(@PathVariable Integer id);
	
	
}
