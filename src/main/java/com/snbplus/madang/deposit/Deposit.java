package com.snbplus.madang.deposit;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.contract.Contract;
import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name="tbl_deposit")
@Data
public class Deposit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String state;
	
	private String companyName;
	
	private String workContent;
	
	private String contractName;
	
	private Integer contractPay;
	
	private Integer collectPay;
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;
	
	@ManyToOne
	@JoinColumn(name="creater")
	private User creater; // 등록자;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	@ManyToOne
	@JoinColumn(name="modifier")
	private User modifier; // 수정자
	
	
	@Transient
	private Contract contrack;
	

}
