package com.snbplus.madang.deposit;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DepositViewController {

	@GetMapping("/deposit/list")
	public String depositList(Model model) {
		
		return "deposit/depositList";
	}
	
	@GetMapping("/deposit/view")
	public String depositView(Model model, Integer no) {
		
		return "deposit/depositView";
	}
}
