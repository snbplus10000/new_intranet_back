package com.snbplus.madang.deposit;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Entity
@Table(name="tbl_deposit_detail")
@Data
public class DepositDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private Integer year;
	
	private String businessName;
	
	private String companyName;
	
	private String workContent;
	
	private String item;
	
	private String depositName;
	
	private Integer collectPay;

	private String depositDate;

	private Integer depositId;
	
//	@ManyToOne
//	@JoinColumn(name="DEPOSIT_ID", insertable=false, updatable=false)
	@Transient
	private Deposit deposit;
}
