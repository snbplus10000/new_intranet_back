package com.snbplus.madang.reject;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.common.MailService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/reject")
public class RejectController implements InterfaceController<Reject> {

	private final RejectMapper rejectMapper;

	private final UserMapper userMapper;

	private final MailService mailService;

	@ApiOperation(value = "반려 리스트", httpMethod = "GET", notes = "반려 리스트")
	@Override
	public ResponseEntity<Page<Reject>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 30) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {
		return null;
	}

	@ApiOperation(value = "반려 상세화면", httpMethod = "GET", notes = "반려 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "반려 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Reject> view(@PathVariable Integer id) {

		Reject t = rejectMapper.findOne(id);
		t.setReadState("Y");
		rejectMapper.save(t);
		User createUser = userMapper.findOne(t.getRegistrant());
		User rejectUser = userMapper.findOne(t.getCreater());

		/*createUser.getDeptChart().setDeptUser(null);
		createUser.getDeptChart().setHighDeptUser(null);
		createUser.getHighDeptChart().setDeptUser(null);
		createUser.getHighDeptChart().setHighDeptUser(null);

		rejectUser.getDeptChart().setDeptUser(null);
		rejectUser.getDeptChart().setHighDeptUser(null);
		rejectUser.getHighDeptChart().setDeptUser(null);
		rejectUser.getHighDeptChart().setHighDeptUser(null);*/

		t.setCreateUser(createUser);
		t.setRejectUser(rejectUser);

		return new ResponseEntity<>(t, HttpStatus.OK);

	}

	@ApiOperation(value = "반려 저장", httpMethod = "PUT", notes = "반려 저장")
	@Override
	public ResponseEntity<Reject> save(@RequestBody Reject t) {

		User user = UserSingleton.getInstance();

		t.setCreater(user.getId());
		t.setDelYn("N");
		t.setReadState("N");
		rejectMapper.save(t);

		t.setRejectUser(userMapper.findOne(t.getCreater()));
		t.setCreateUser(userMapper.findOne(t.getRegistrant()));

		try {
			mailService.rejectApprovalMailSend(t);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(t, HttpStatus.OK);
		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "반려 수정", httpMethod = "POST", notes = "반려 수정")
	@Override
	public ResponseEntity<Reject> update(@RequestBody Reject t) {
		User user = UserSingleton.getInstance();

		t.setModifier(user.getId());
		rejectMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "반려 삭제", httpMethod = "DELETE", notes = "반려 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "반려 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Reject> delete(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();
		Reject reject = rejectMapper.findOne(id);

		reject.setDelYn("Y");
		reject.setModifier(user.getId());
		rejectMapper.save(reject);

		return new ResponseEntity<>(reject, HttpStatus.OK);
	}

	@ApiOperation(value = "자신의 반려 리스트 조회", httpMethod = "GET", notes = "자신의 반려 리스트 조회")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "반려 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@GetMapping("/list")
	public ResponseEntity<List<Reject>> rejectList() {

		User userResult = UserSingleton.getInstance();

		return new ResponseEntity<>(
				rejectMapper.findByDelYnAndReadStateAndRegistrant("N", "N", userResult.getId()).stream().map(reject -> {
					Integer id = 0;
					id = reject.getCreater();
					User rejectUser = userMapper.findOne(id);

					/*rejectUser.getDeptChart().setDeptUser(null);
					rejectUser.getDeptChart().setHighDeptUser(null);
					rejectUser.getHighDeptChart().setDeptUser(null);
					rejectUser.getHighDeptChart().setHighDeptUser(null);*/

					String rejectimgurl = "";

					if (rejectUser.getThumbFile() == null) {
						rejectimgurl = "static/img/default-avatar.png";
					} else {
						if (rejectUser.getThumbFile().getFileAttach() == null) {
							rejectimgurl = "static/img/default-avatar.png";
						} else {
							rejectimgurl = "/intranet/api/file/image/"
									+ rejectUser.getThumbFile().getFileAttach().getId();
						}
					}
					reject.setImgUrl(rejectimgurl);
					reject.setRejectUser(rejectUser);

					User createUser = userMapper.findOne(reject.getRegistrant());

					/*createUser.getDeptChart().setDeptUser(null);
					createUser.getDeptChart().setHighDeptUser(null);
					createUser.getHighDeptChart().setDeptUser(null);
					createUser.getHighDeptChart().setHighDeptUser(null);*/

					String createimgurl = "";

					if (rejectUser.getThumbFile() == null) {
						createimgurl = "static/img/default-avatar.png";
					} else {
						if (rejectUser.getThumbFile().getFileAttach() == null) {
							createimgurl = "static/img/default-avatar.png";
						} else {
							createimgurl = "/intranet/api/file/image/"
									+ rejectUser.getThumbFile().getFileAttach().getId();
						}
					}
					reject.setImgUrl(createimgurl);
					reject.setCreateUser(createUser);

					return reject;
				}).collect(Collectors.toList()), HttpStatus.OK);
	}

}
