package com.snbplus.madang.reject;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RejectMapper extends JpaRepository<Reject, Integer> {

	Page<Reject> findByDelYnAndReadState(Pageable pageable, String delYn, String readStete);

	List<Reject> findByDelYnAndReadStateAndRegistrant(String delYn, String readStete, Integer registrant);

}
