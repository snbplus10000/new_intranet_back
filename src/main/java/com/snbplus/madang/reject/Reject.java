package com.snbplus.madang.reject;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "reject")
@Data
public class Reject {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String url;

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String readState = "N"; // 알람 확인 여부

	private String createDate; // 결제 등록 날짜

	private String kind; // 결제 종류

	private String deferReason = ""; // 반려 사유

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	private Integer registrant; // 결제 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

	@Transient
	private User createUser;

	@Transient
	private User rejectUser;

	@Transient
	private String imgUrl;

}
