package com.snbplus.madang.newApproval;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

/**
 * 결재 step 에 따른 entity
 * @author baebull70
 *
 */

@Data
@Entity
@Table(name = "tbl_approval_step")
public class ApprovalStep {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Integer no; // pk

    private String approver; // 결재자 

    private Integer creater;

    private Integer modifier;

    @Column( nullable = false, updatable = false )
    @CreatedDate
	@CreationTimestamp
    private Timestamp appDate;  // 승인일 (timestamp)

    @Column( nullable = false, updatable = false )
	@CreatedDate
	@CreationTimestamp
    private Timestamp created;  // 등록일
    
    @LastModifiedDate
	@Column( updatable = true )
	@UpdateTimestamp
	private Timestamp modified; // 수정일
    
    @Column(nullable = false)
	@ColumnDefault("'N'")
    private String delYn ="N"; // 삭제여부
    
    private String state; // 결재 상태 

    @ManyToOne
    @JoinColumn(name="approval_info_no")
    private ApprovalInfo approvalInfo; // 결재 정보 아이디 (fk)

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name="parent_id", nullable = true)
    private ApprovalStep approvalStep; // 프로세스에 따른 화면을 보여주기 위해
	
}