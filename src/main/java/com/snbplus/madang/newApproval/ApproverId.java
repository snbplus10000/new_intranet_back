package com.snbplus.madang.newApproval;
import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;
/**
 * 승인 관련 entity
 * @author baebull70
 *
 */
@Data
@Embeddable
class ApproverId implements Serializable {

    @ManyToOne
    @JoinColumn(name="approval_no")
    private ApprovalDefine approvalDefine;

    public Integer step;

}