package com.snbplus.madang.newApproval;

import java.sql.Timestamp;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

/**
 * 결재 정보 
 * @author baebull70
 *
 */
@Entity
@Table(name = "tbl_approval_infomation")
@Data
public class ApprovalInfo {

    @Id
    @GeneratedValue( strategy = GenerationType.AUTO )
    private Integer approvalInfoNo;

    @ManyToOne
    @JoinColumn(name="tbl_approval_no")
    private ApprovalDefine approvalDefine;

    private Integer creater;

    private Integer modifier;
 
    @Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
    private Timestamp created;  // 등록일
    
    @LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

    @Column(nullable = false)
	@ColumnDefault("'N'")
    private String delYn ="N"; // 삭제여부
    
}