package com.snbplus.madang.company;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

/**
 * 업체리스트 - 이력사항 VO
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_company_history")
@Data
public class CompanyHistroy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String gubun; //구분
	
	private String area; // 관할지역

	@ColumnDefault("1900")
	private String historyYear; // 연도
	
	private String businessName; // 사업명
	
	private String supportGubun; // 지원구분
	
	private String state; // 선/탈
	
	private String etc; // 비고
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	private Integer companyId; // fk(company pk)


}
