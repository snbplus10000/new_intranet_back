package com.snbplus.madang.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/companyInfo")
public class CompanyInfoController implements JoinController<List<CompanyInfo>> {

	@Autowired
	CompanyInfoMapper infoMapper;

	@ApiOperation(value = "업체 리스트 - 담당자 정보  리스트", httpMethod = "GET", notes = "업체 리스트 - 담당자 정보 리스트")
	@Override
	public ResponseEntity<List<CompanyInfo>> list(Integer id) {
		
		return new ResponseEntity<>(infoMapper.findByCompanyIdAndDelYn(id,"N"), HttpStatus.OK);
	}


	@ApiOperation(value = "업체 리스트 - 담당자 정보 저장/수정", httpMethod = "PUT", notes = "업체 리스트 - 담당자 정보 저장/수정")
	@Override
	public ResponseEntity<List<CompanyInfo>> save(@RequestBody List<CompanyInfo> t) {
		
		for(CompanyInfo info : t) {
			infoMapper.save(info);
		}

		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "업체 리스트 - 담당자 정보 삭제", httpMethod = "DELETE", notes = "업체 리스트 - 담당자 정보 삭제")
	@Override
	public ResponseEntity<List<CompanyInfo>> delete(List<CompanyInfo> t) {
		
		
		for( CompanyInfo info :  t) {
			
			info.setDelYn("Y");
		}		
		infoMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

}
