package com.snbplus.madang.company;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyHistroyMapper extends JpaRepository<CompanyHistroy, Integer> {

	 List<CompanyHistroy> findByCompanyIdAndDelYn(Integer id, String delYn);
}
