package com.snbplus.madang.company;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyCommentMapper extends JpaRepository<CompanyComment, Integer > {


	 List<CompanyComment> findByCompanyIdAndDelYnOrderByIdDesc(Integer id, String delYn);


	
}
