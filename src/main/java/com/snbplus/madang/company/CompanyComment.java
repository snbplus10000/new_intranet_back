package com.snbplus.madang.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.snbplus.madang.common.Comment;

import lombok.Data;

/**
 * 업체리스트  - 회사 코멘트 VO
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_company_comment")
@Data
public class CompanyComment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	@OneToOne
	@JoinColumn(name="comment_id")
	private Comment comment;
	
	@ManyToOne
	@JoinColumn(name="company_id", nullable = false)
	private Company company;



}
