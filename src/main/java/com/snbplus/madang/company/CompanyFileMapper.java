package com.snbplus.madang.company;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyFileMapper extends JpaRepository<CompanyFile, Integer> {

}
