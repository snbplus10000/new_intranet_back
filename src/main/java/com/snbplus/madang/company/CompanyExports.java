package com.snbplus.madang.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;

/**
 * 업체리스트 - 직수출 실적 및 결산내역 VO
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_company_exports")
@Data
public class CompanyExports {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String closeYear; // 결산연도
	
	private Integer employeesNum; //종업원 수
	
	private Integer sales; //매출액(백만원)
	
	private Integer exports; // 수출액(천불)
	
	private String etc; //특이사항
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";

	private Integer companyId;

}
