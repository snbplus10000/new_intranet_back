package com.snbplus.madang.company;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyMapper extends JpaRepository<Company, Integer>, JpaSpecificationExecutor<Company> {
	
	Optional<Company> findByIdAndDelYn(Integer id, String delYn);

	Page<Company> findByDelYn(Pageable pageable, String delYn);
	
	List<Company> findByDelYn(String delYn);
		
	
	
}
