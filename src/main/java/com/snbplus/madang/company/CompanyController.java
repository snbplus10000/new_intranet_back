package com.snbplus.madang.company;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.holiwork.HoliWork;
import com.snbplus.madang.outing.Outing;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyController implements InterfaceController<Company> {

	@Autowired
	CompanyMapper companyMapper;

	@Autowired
	CompanyCommentMapper companyCommentMapper;

	@Autowired
	CommentMapper commentMapper;

	@Autowired
	CompanyFileMapper companyFileMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	CompanySpec companySpec;

	@ApiOperation(value = "업체 리스트", httpMethod = "GET", notes = "업체 리스트")
	@Override
	public ResponseEntity<Page<Company>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Specifications<Company> spec = Specifications.where(companySpec.build("N", conditionKey, conditionValue));

		return new ResponseEntity<>(companyMapper.findAll(spec, pageable).map(company -> {
			User searchUser;
			Integer id = 0;
			User user = new User();
			id = company.getCreater();
			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			company.setUser(user);
			// company.getCompanyComment(null);
			
			return company;
		}), HttpStatus.OK);
	}

	@ApiOperation(value = "업체 상세화면", httpMethod = "GET", notes = "업체 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "업체 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Company> view(@PathVariable Integer id) {
		Company company = companyMapper.findOne(id);

		// if(company != null){
		// 	List<CompanyComment> list1 = company.getCompanyComment();

		// 	if(list1 != null && list1.size() >0){
				
		// 		for(CompanyComment comments : list1){

		// 			comments.getComment().getUser().getDeptChart().setDeptUser(null);;
		// 			comments.getComment().getUser().getDeptChart().setHighDeptUser(null);
		// 			comments.getComment().getUser().getHighDeptChart().setDeptUser(null);;
		// 			comments.getComment().getUser().getHighDeptChart().setHighDeptUser(null);
		// 		}
		// 	}
		// }

		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	@ApiOperation(value = "업체 저장", httpMethod = "PUT", notes = "업체 저장")
	@Override
	public ResponseEntity<Company> save(@RequestBody Company t) {

		CompanyFile companyFile = t.getCompanyFile();

		if (companyFile.getKind() != null) {
			companyFileMapper.save(companyFile);
			t.setCompanyFile(companyFile);
		}

		User user = UserSingleton.getInstance();

		t.setCreater(user.getId());
		t.setDelYn("N");
		companyMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "업체 수정", httpMethod = "UPDATE", notes = "업체 수정")
	@Override
	public ResponseEntity<Company> update(@RequestBody Company t) {
		User user = UserSingleton.getInstance();
		CompanyFile companyFile = t.getCompanyFile();

		if (companyFile != null && companyFile.getKind() != null) {
			companyFileMapper.save(companyFile);
			t.setCompanyFile(companyFile);
		}

		t.setModifier(user.getId());
		companyMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "업체 삭제", httpMethod = "DELETE", notes = "업체 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "업체 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Company> delete(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();
		Company company = companyMapper.findOne(id);

		company.setDelYn("Y");
		company.setModifier(user.getId());
		companyMapper.save(company);

		return new ResponseEntity<>(company, HttpStatus.OK);
	}

	@GetMapping("/allList")
	@ApiOperation(value = "업체 전체 리스트", httpMethod = "GET", notes = "업체 전체 리스트")
	public ResponseEntity<List<Company>> allList() {

		List<Company> CompanyList = companyMapper.findByDelYn("N").stream().map(company -> {
			company.setCompanyHistory(null);
			company.setCompanyExports(null);
			company.setCompanyFile(null);

			return company;
		}).collect(Collectors.toList());

		return new ResponseEntity<>(CompanyList, HttpStatus.OK);
	}

}
