package com.snbplus.madang.company;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyExportsMapper extends JpaRepository<CompanyExports, Integer> {
	
	 List<CompanyExports> findByCompanyIdAndDelYn(Integer id, String delYn);

}
