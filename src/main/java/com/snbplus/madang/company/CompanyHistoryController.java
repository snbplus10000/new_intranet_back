package com.snbplus.madang.company;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/companyHistory")
public class CompanyHistoryController implements JoinController<List<CompanyHistroy>> {
	
	@Autowired
	CompanyHistroyMapper historyMapper;

	@Override
	public ResponseEntity<List<CompanyHistroy>> list(Integer id) {
		
		return new ResponseEntity<>(historyMapper.findByCompanyIdAndDelYn(id,"N"), HttpStatus.OK);
	}


	@Override
	public ResponseEntity<List<CompanyHistroy>> save(@RequestBody List<CompanyHistroy> t) {
		
		
		for(CompanyHistroy history : t) {
			historyMapper.save(history);
		}

		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<CompanyHistroy>> delete(List<CompanyHistroy> t) {
		
		
		for( CompanyHistroy history :  t) {
			
			history.setDelYn("Y");
		}		
		historyMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

}
