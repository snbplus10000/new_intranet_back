package com.snbplus.madang.company;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.outing.Outing;
import com.snbplus.madang.user.UserMapper;

@Component
public class CompanySpec {
    @Autowired
    UserMapper userMapper;

	public Specification<Company> build(String delYn, String conditionKey, String conditionValue) {
		
        if(conditionKey == null || conditionKey.equals("")) {
            return search(delYn);
        }else if(conditionKey != null && conditionKey.equals("creater")) {
        	return search(delYn, conditionKey, userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()));
        }else if(conditionKey != null && conditionKey.equals("infoName")) {
        	return searchJoin(delYn, conditionKey, conditionValue);
        }

        return search(delYn, conditionKey, "%" + conditionValue + "%");

	}

    public Specification<Company> search(String delYn) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn)
                );

    }
    
    public Specification<Company> search(String delYn, String conditionKey, String conditionValue) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn),

                        cb.like(root.get(conditionKey), conditionValue)
                );

    }
    
    public  Specification<Company> search(String delYn, String conditionKey, List conditionValue) {

        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : conditionValue) {
                in.value(conditionColumnValue);           	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };


    }
    
    public Specification<Company> searchJoin(String delYn, String conditionKey, String conditionValue){
    	
    	return (root,query,cb) ->{
    		Join<Object, Object> companyInfo = root.join("comPanyInfo");

    		
    		return cb.equal(companyInfo.get("infoName"), conditionValue);
    	};
    }
}
