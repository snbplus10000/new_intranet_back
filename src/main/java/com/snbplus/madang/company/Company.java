package com.snbplus.madang.company;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.FileAttach;
import com.snbplus.madang.contract.Contract;
import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 업체리스트 - 기본정보 + 부가정보 VO
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_company")
@Data
public class Company {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String industryCode;  //표준산업 분류코드
	
	private String rating; //알리바바 등급
	
 	private String companyName; //회사 이름
	
	private String companyEnName; // 회사 영문이름
	
	private String registerNo; //사업자 등록번호
	
	private String representName;  // 대표자명
	
	private String representPhoneNo; // 대표자 휴대폰번호
	
	private String foundationDate; // 창립일
	
	private String representNo; // 대표전화
	
	private String representFax; //대표 FAX
	
	private String representEmail; // 대표 email
	
	private String area; // 본사 -지역
	
	private String headOfficeAddress; //본사주소
	
	private String plantAddress; // 공장 주소
	
	private String engAddress; // 영문주소
	
	private String homepageAddress; //홈페이지 주소
	
	private String homepageExplain; // 홈페이지 설명
	
	private String content; // 내용
	
	private String tradeManagerYn; //무역담당자 여부
	
	private String tradeManagerName; // 무역담당자 이름
	
	private String tradeState; // 수출입여부
	
	private Integer capitalPay; //  자본금(백만원)
	
	private String companyType; // 기업형태
	
	private String mainIndustry; // 주업종
	
	private String subIndustry; // 부업종
	
	private String tradeCountry;  // 일반 교역국가
	
	private String mainItem; // 주요 취급품목

	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "company", cascade= CascadeType.PERSIST)
	// @OrderBy("id DESC")
	// @BatchSize(size = 5)
	// private List<CompanyComment> companyComment;
	
	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="companyId")
	private List<CompanyExports> companyExports;
	
	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="companyId")
	private List<CompanyHistroy> companyHistory;
	
	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="companyId")
	private List<CompanyInfo> companyInfo;
	
//	@OneToMany(fetch=FetchType.LAZY, mappedBy = "company")
	@Transient
	private List<Contract> contract;
	
	@Transient
	private User user; // 영업자 
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="company_file_id")
	private CompanyFile companyFile;
	
	@Transient
	private FileAttach FileAttach;
	
	@Transient
	private List<Comment> comments;

	

}
