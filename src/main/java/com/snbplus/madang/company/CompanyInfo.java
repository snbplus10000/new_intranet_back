package com.snbplus.madang.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import lombok.Data;


/**
 * 업체 리스트 - 담당자 정보 VO
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_company_info")
@Data
public class CompanyInfo {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String promotionYn; // 홍보대상 여부
	
	private String workState; //
	
	private String infoName; // 이름
	
	private String infoRank; // 직급
	
	private String rankCode; //직급코드
	
	private String dept; // 부서
	
	private String deptCode; // 부서코드
	
	private String telNo; // 전화
	
	private String phoneNo; // 휴대폰
	
	private String email; // 이메일
	
	private String etc; //비고
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N";	//삭제 여부
	
	private Integer companyId;

}
