package com.snbplus.madang.company;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;
import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/comment/company")
public class CompanyCommentController implements JoinController<List<CompanyComment>> {

	@Autowired
	UserMapper userMapper;
	
	@Autowired
	CompanyCommentMapper companyCommentMapper;

	@Autowired
	CommentMapper commentMapper;

	@ApiOperation(value = "회사 코멘트 리스트", httpMethod = "GET", notes = "회사 코멘트 리스트")
	@GetMapping("/{id}")
	@Override
	public ResponseEntity<List<CompanyComment>> list(@PathVariable Integer id) {
	
		return new ResponseEntity<>(companyCommentMapper.findByCompanyIdAndDelYnOrderByIdDesc(id,"N").stream().map(companyComment -> {

			User searchUser;
			Integer userId = 0;
			User user = new User();
			userId = companyComment.getComment().getCreater();
			searchUser = userMapper.findOne(userId);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setRankName(searchUser.getRankName());
			/*searchUser.getDeptChart().setDeptUser(null);
			searchUser.getDeptChart().setHighDeptUser(null);
			searchUser.getHighDeptChart().setDeptUser(null);
			searchUser.getHighDeptChart().setHighDeptUser(null);*/
			companyComment.getComment().setUser(user);

			return companyComment;
		}).collect(Collectors.toList()),HttpStatus.OK);

	}

	//not used
	@Override
	@PutMapping("/notused")
	public ResponseEntity<List<CompanyComment>> save(@RequestBody List<CompanyComment> t) {
		
		for(CompanyComment comment : t) {
			companyCommentMapper.save(comment);
		}

		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "회사 코멘트 저장/수정", httpMethod = "PUT", notes = "회사 코멘트 저장/수정")
	@PutMapping("/")
	public ResponseEntity<CompanyComment> saveSingle(@RequestBody Map<String, String> commentMap) {
		
		Comment comment = new Comment();
		User user = UserSingleton.getInstance();

		comment.setCreater(user.getId());
		comment.setDelYn("N");
		comment.setKinds("company");
		comment.setComment(commentMap.get("comment"));
		commentMapper.save(comment);

		Company company = new Company();
		company.setId(Integer.parseInt(commentMap.get("companyId")));

		CompanyComment companyComment = new CompanyComment();
		companyComment.setCompany(company);
		companyComment.setComment(comment);
		companyCommentMapper.save(companyComment);

		return new ResponseEntity<>(companyComment,HttpStatus.OK);
	}

	//notused
	@Override
	@DeleteMapping("/nouse")
	public ResponseEntity<List<CompanyComment>> delete(List<CompanyComment> t) {

		for( CompanyComment comment :  t) {
			
			comment.setDelYn("Y");
		}		
		companyCommentMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "회사 코멘트 삭제", httpMethod = "DELETE", notes = "회사 코멘트 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<CompanyComment> deleteSingle(@PathVariable Integer id) {
		
		CompanyComment companyComment = companyCommentMapper.findOne(id);
		User user = UserSingleton.getInstance();

		companyComment.getComment().setModifier(user.getId());
		companyComment.getComment().setDelYn("Y");
		companyComment.setDelYn("Y");
		companyCommentMapper.save(companyComment);
		
		
		return new ResponseEntity<>(companyComment,HttpStatus.OK);
	}


}
