package com.snbplus.madang;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * join의 최상단이 아닌 것들에 대한 generic화를 위한 인터페이스 example - companyInfo, dailyDetail 등
 * 
 * @author bhshin
 *
 * @param <T>
 */
public interface JoinController<T> {

	@GetMapping("")
	ResponseEntity<T> list(Integer id);

	@PutMapping("")
	@Transactional
	ResponseEntity<T> save(@RequestBody T t);

	@DeleteMapping("")
	@Transactional
	ResponseEntity<T> delete(@RequestBody T t);
	
	


}
