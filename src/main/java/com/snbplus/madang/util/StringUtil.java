package com.snbplus.madang.util;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.UUID;

import org.springframework.util.StringUtils;

public class StringUtil {
	private static String digits = "0123456789abcdef";

	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0));
	}

	public static boolean isEmptyOrWhitespace(String str) {
		return (isEmpty(str) || isEmpty((str.trim())));
	}

	public static boolean isNullOrEmpty(String str) {
		return isEmpty(str);
	}

	public static String getSalt() {
		// String today = DateUtil.getDateString(); //날짜를 추가하여 이용할 수도 있음.
		// For salt (random)
		String SALTCHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuffer salt = new StringBuffer();
		java.util.Random rnd = new java.util.Random();
		// build a random 9 chars salt
		while (salt.length() < 9) {
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.substring(index, index + 1));
		}
		String saltStr = salt.toString();
		
		// session.setAttribute("ran",saltStr); // Salt String is stored in
		// session so that we can retrieve in the serverside which is used to
		// calculate MD5 hash of salted Password.
		return saltStr;
	}

	public static String getUUID() {
		return UUID.randomUUID().toString();
	}

	public static String getUUIDByTimeBased() {
		
		long currentTime = System.currentTimeMillis();

		return new UUID(currentTime, currentTime).toString();
	}

	public static String getCamelCase(String underScore) {
		String[] array = underScore.split("_");
		StringBuffer camelCase = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			camelCase.append(StringUtils.capitalize(array[i].toLowerCase()));
		}
		return camelCase.toString();
	}

	public static String convert2CamelCase(String underScore) {

		// '_' 가 나타나지 않으면 이미 camel case 로 가정함.
		// 단 첫째문자가 대문자이면 camel case 변환 (전체를 소문자로) 처리가
		// 필요하다고 가정함. --> 아래 로직을 수행하면 바뀜
		if (underScore.indexOf('_') < 0 && Character.isLowerCase(underScore.charAt(0))) {
			return underScore;
		}
		StringBuilder result = new StringBuilder();
		boolean nextUpper = false;
		int len = underScore.length();

		for (int i = 0; i < len; i++) {
			char currentChar = underScore.charAt(i);
			if (currentChar == '_') {
				nextUpper = true;
			} else {
				if (nextUpper) {
					result.append(Character.toUpperCase(currentChar));
					nextUpper = false;
				} else {
					result.append(Character.toLowerCase(currentChar));
				}
			}
		}
		return result.toString();
	}
	
	public static String currency(String number) {
		String result = DecimalFormat.getCurrencyInstance(Locale.KOREA).format(number);
		return result;
	}

	public static String currency(double number) {
		String result = DecimalFormat.getCurrencyInstance(Locale.KOREA).format(number);
		return result;
	}
	
	public static String currency(int number) {
		String result = DecimalFormat.getCurrencyInstance(Locale.KOREA).format(number);
		return result;
	}

}
