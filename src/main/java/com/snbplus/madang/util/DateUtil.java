package com.snbplus.madang.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.stream.IntStream;

public class DateUtil {
	
	/**
	 * startYear <= endYear 조건에 맞아야 함
	 *  
	 * @param startYear //작은 연도
	 * @param endYear // 큰연도
	 * @return
	 */
	public static ArrayList<Integer> yearList(Integer startYear, Integer endYear){
		
		ArrayList<Integer> list = IntStream.rangeClosed(startYear, endYear )
											.map(i -> -i).sorted().map(i -> -i)												
											.collect(ArrayList<Integer>::new, ArrayList::add, ArrayList::addAll);
		
		return list;
	}
	
	public static String currentYear() {
		
		LocalDate now = LocalDate.now();
		String format = now.format(DateTimeFormatter.ofPattern("yyyy"));
		
		return format;

	}


}
