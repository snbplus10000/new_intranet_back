package com.snbplus.madang.holiwork;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import com.snbplus.madang.vacation.Vacation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/holiwork")
public class HoliWorkController implements InterfaceController<HoliWork> {

	@Autowired
	HoliWorkMapper workMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	UserAuthService userAuthService;

	@Autowired
	HoliWorkSpec workSpec;

	@ApiOperation(value = "휴일근무 리스트", httpMethod = "GET", notes = "휴일근무 리스트")
	@Override
	public ResponseEntity<Page<HoliWork>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {

		Page<HoliWork> holiWorkList = null;

		if(conditionKey == null){
			conditionKey="default";
		}
		if(conditionValue==null){
			conditionValue="";
		}

		switch (conditionKey) {
			case "creater":  holiWorkList = workMapper.findByCreatorContainingAndDelYn(conditionValue, "N", pageable);
				break;
			case "state":  holiWorkList = workMapper.findByStateContainingAndDelYn(conditionValue, "N", pageable);
				break;
			default: holiWorkList = workMapper.findAllHoliWorkByDelYn("N", pageable);
				break;
		}

		return new ResponseEntity<Page<HoliWork>>(holiWorkList, HttpStatus.OK);
	}

	@ApiOperation(value = "휴일근무 상세화면", httpMethod = "GET", notes = "휴일근무 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "휴일근무 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<HoliWork> view(@PathVariable Integer id) {
		HoliWork t = workMapper.findOne(id);
		User searchUser;
		User user = new User();
		user = userMapper.findOne(t.getCreater());
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		t.setUser(user);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴일근무 저장", httpMethod = "PUT", notes = "휴일근무 저장")
	@Override
	public ResponseEntity<HoliWork> save(@RequestBody HoliWork t) {

		User user = UserSingleton.getInstance();
		t.setUser(null);
		t.setCreater(user.getId());
		workMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴일근무 수정", httpMethod = "POST", notes = "휴일근무 수정")
	@Override
	public ResponseEntity<HoliWork> update(@RequestBody HoliWork t) {

		User user = UserSingleton.getInstance();

		t.setModifier(user.getId());

		workMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "휴일근무 삭제", httpMethod = "DELETE", notes = "휴일근무 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "휴일근무 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<HoliWork> delete(@PathVariable Integer id) {

		HoliWork holiWork = workMapper.findOne(id);

		User user = UserSingleton.getInstance();

		user.setModifier(user.getId());
		holiWork.setDelYn("Y");
		workMapper.save(holiWork);

		return new ResponseEntity<>(holiWork, HttpStatus.OK);
	}

	@ApiOperation(value = "휴가신청 유저 리스트", httpMethod = "GET", notes = "휴가신청 유저 리스트")
	@GetMapping("/users")
	public ResponseEntity<List<User>> getLists() {
		List<User> lists = userMapper.findHoliCreater();

		return new ResponseEntity<>(lists, HttpStatus.OK);
	}

	@GetMapping("/reviewList")
	@ApiOperation(value = "휴일근무 결재 대기 리스트", httpMethod = "GET", notes = "휴일근무 결재 대기 리스트")
	public ResponseEntity<List<HoliWork>> reviewList() {
		User curUser = UserSingleton.getInstance();
		Integer step = UserSingleton.getStep();

		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
					.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(
				workMapper.findByDelYnAndCreatorIn("HOLI_WORK", "N", userFilterList, step).stream().map(holiwork -> {

					Integer id = holiwork.getCreater();
					User user = userMapper.findOne(id);

					String imgUrl = "";

					if (user == null || user.getThumbFile() == null || user.getThumbFile().getFileAttach() == null) {
						imgUrl = "static/img/default-avatar.png";
					} else {
						imgUrl = "/intranet/api/file/image/" + user.getThumbFile().getFileAttach().getId();
					}
					holiwork.setImgUrl(imgUrl);

					return holiwork;

				}).collect(Collectors.toList()), HttpStatus.OK);

	}

	@GetMapping("/rejectList")
	@ApiOperation(value = "휴일근무 정산 반려 리스트", httpMethod = "GET", notes = "휴일근무 정산 반려 리스트")
	public ResponseEntity<List<HoliWork>> rejectList() {

		return new ResponseEntity<>(workMapper.findByCreaterAndState(UserSingleton.getInstance().getId(), "반려"),
				HttpStatus.OK);

	}

	@GetMapping("/completeList")
	@ApiOperation(value = "휴일근무 완료 리스트", httpMethod = "GET", notes = "휴일근무 완료 리스트")
	public ResponseEntity<List<HoliWork>> completeList() {

		List<HoliWork> holiworkList = workMapper.findByDelYnAndState("N", "결재완료");

		for (HoliWork v : holiworkList) {

			Integer creater = v.getCreater();
			String className = "";

			User searchUser = userMapper.findOne(creater);
			if (searchUser.getDeptChart().getCode() == 12) { // 디크팀
				className = "event-red";
			} else if (searchUser.getDeptChart().getCode() == 10) { // 개발팀
				className = "event-rose";
			}
			/*
			 * else if(searchUser.getDeptChart().getCode() == 15) { className =
			 * "event-green"; }else if(searchUser.getDeptChart().getCode() == 17) {
			 * className = "event-orange"; }
			 */
			else {
				className = "event-default";
			}
			v.setClassName(className);
		}

		return new ResponseEntity<>(holiworkList, HttpStatus.OK);
	}

}
