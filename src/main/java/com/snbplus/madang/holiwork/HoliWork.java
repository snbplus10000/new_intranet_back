package com.snbplus.madang.holiwork;

import java.sql.Timestamp;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 *  휴가 근무 관리
 * @author bhshin
 *
 */
@Entity
@Table(name = "holi_work")
@Data
public class HoliWork {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="creater", insertable=false, updatable=false)
	private User user;
	
	private String regName;
	
	private String schDate;
	
	private String startTime;
	
	private String endTime;
	
	private String content;
	
	private String reason;
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	private String state;

	@ColumnDefault("''")
	private String workKind;

	private Integer creater; // 등록자
	
	@CreatedDate
	@Column(updatable = false, nullable= false)
	@CreationTimestamp
	private Timestamp  created;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private String className;
	
	@Transient
	private String imgUrl;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;
}
