package com.snbplus.madang.holiwork;

import java.util.List;

import com.snbplus.madang.vacation.Vacation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snbplus.madang.outing.OutingReport;

@Repository
public interface HoliWorkMapper extends JpaRepository<HoliWork, Integer>, JpaSpecificationExecutor<HoliWork> {

	Page<HoliWork> findAllByDelYn(Pageable pageable, String string);

	@Query(value = "SELECT h.* FROM holi_work h left outer join approval a"
			+ " on h.id = a.ref_id and a.ref_table = :refTable where h.del_yn = :delYn and h.creater in :userFilterList and a.step = :step"
			,nativeQuery = true)
	List<HoliWork> findByDelYnAndCreatorIn(@Param("refTable") String refTable, @Param("delYn")String delYn, @Param("userFilterList")List<Integer> userFilterList, @Param("step")Integer step);

	List<HoliWork> findByCreaterAndState(Integer id, String state);

	List<HoliWork> findByDelYnAndState(String delYn, String state);

	@Query(value="select b from HoliWork b join fetch b.user u where u.name like %:creator% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from HoliWork b inner join b.user u where u.name like %:creator% and b.delYn=:delYn")
	Page<HoliWork> findByCreatorContainingAndDelYn(@Param("creator")String creator, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from HoliWork b join fetch b.user u where b.state like %:state% and b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from HoliWork b inner join b.user u where b.state like %:state% and b.delYn=:delYn")
	Page<HoliWork> findByStateContainingAndDelYn(@Param("state")String state, @Param("delYn")String delYn, Pageable pageable);

	@Query(value="select b from HoliWork b join fetch b.user u where b.delYn=:delYn order by b.id desc",
			countQuery= "select count(b) from HoliWork b inner join b.user u where b.delYn=:delYn")
	Page<HoliWork> findAllHoliWorkByDelYn(@Param("delYn")String delYn, Pageable pageable);

	

}
