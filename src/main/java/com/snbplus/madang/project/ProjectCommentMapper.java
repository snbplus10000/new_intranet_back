package com.snbplus.madang.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

import java.util.List;

@Repository
public interface ProjectCommentMapper extends JpaRepository<ProjectComment, Integer>, JpaSpecificationExecutor<ProjectComment> {

    /* 특정 글의 댓글 목록*/
    List<ProjectComment> findByIdAndDelYnOrderByIdDesc(Integer id, String delYn);

    /* 프로젝트 등록하기 - 코멘트 정보*/
    List<ProjectComment> findByCommentIdAndDelYnOrderByIdDesc(Integer id, String delYn);
    
    ProjectComment findTop1ByProjectIdAndDelYnOrderByIdDesc(Integer projectId, String delYn);
    
}
