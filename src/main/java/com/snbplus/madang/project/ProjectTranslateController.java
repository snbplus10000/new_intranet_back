package com.snbplus.madang.project;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;



@Slf4j
@RestController
@RequestMapping("/projectTranslate")
public class ProjectTranslateController implements InterfaceController<ProjectTranslate> {

    @Autowired
    ProjectMapper projectMapper;

    @Autowired
    ProjectTranslateMapper translateMapper;

    @Autowired
    ProjectEstimateFileMapper estimateFileMapper;

    @Autowired
    ProjectTranslateFileMapper translateFileMapper;

    @ApiOperation(value = "사업관리 - 번역의뢰 상세화면", httpMethod = "GET", notes = "사업관리 - 번역의뢰 상세화면")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사업관리 pk", required = true, dataType = "number", defaultValue = "1")})
    @Override
    public ResponseEntity<ProjectTranslate> view(@PathVariable Integer id) {

        ProjectTranslate translate = translateMapper.findOne(id);

        return new ResponseEntity<>(translate, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 번역의뢰 저장", httpMethod = "PUT", notes = "사업관리 - 번역의뢰 저장")
    @Override
    public ResponseEntity<ProjectTranslate> save(@RequestBody ProjectTranslate t) {
        ProjectEstimateFile estimateFile = t.getEstimateFile();
        ProjectTranslateFile translateFile = t.getTranslateFile();

        if (estimateFile != null && estimateFile.getKind() != null) {
            estimateFileMapper.save(estimateFile);
            t.setEstimateFile(estimateFile);
            estimateFileMapper.flush();
        }

        if (translateFile != null && translateFile.getKind() != null) {
            translateFileMapper.save(translateFile);
            t.setTranslateFile(translateFile);
            translateFileMapper.flush();
        }
        User user = UserSingleton.getInstance();
        t.setCreater(user.getId());
        t.setDelYn("N");
        translateMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 번역의뢰 수정", httpMethod = "UPDATE", notes = "사업관리 - 번역의뢰 수정")
    @Override
    public ResponseEntity<ProjectTranslate> update(@RequestBody ProjectTranslate t) {
        ProjectEstimateFile estimateFile = t.getEstimateFile();
        ProjectTranslateFile translateFile = t.getTranslateFile();

        if (estimateFile != null && estimateFile.getKind() != null) {
            estimateFileMapper.save(estimateFile);
            t.setEstimateFile(estimateFile);
        }

        if (translateFile != null && translateFile.getKind() != null) {
            translateFileMapper.save(translateFile);
            t.setTranslateFile(translateFile);
        }
        User user = UserSingleton.getInstance();
        t.setModifier(user.getId());
        translateMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 번역의뢰 삭제", httpMethod = "DELETE", notes = "사업관리 - 번역의뢰 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사업관리 pk", required = true, defaultValue = "1")
    })
    @Override
    public ResponseEntity<ProjectTranslate> delete(@PathVariable Integer id) {

        ProjectTranslate translate = translateMapper.findOne(id);

        translate.setDelYn("Y");
        User user = UserSingleton.getInstance();
        translate.setModifier(user.getId());
        translateMapper.save(translate);

        return new ResponseEntity<>(translate, HttpStatus.OK);
    }

    // NotUsed
    @Override
    public ResponseEntity<Page<ProjectTranslate>> list(Pageable pageable, String conditionKey, String conditionValue) {
        return null;
    }
    
	@PutMapping("/test")
	public ResponseEntity<ProjectTranslate> savetest(@RequestBody String t) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		ProjectTranslate translate = mapper.readValue(t, ProjectTranslate.class);
		
		return new ResponseEntity<>(translate, HttpStatus.OK);
	}
}
