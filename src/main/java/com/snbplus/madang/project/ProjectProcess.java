package com.snbplus.madang.project;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * 프로젝트 진행 관련
 * @author BH SHIN
 *
 */
@Entity
@Table(name="tbl_project_process")
@Data
public class ProjectProcess {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer projectId;

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String translateYn = "N"; // 번역여부

	private String deadLine; // 마감일
	
	private String etc; // 비고
	
	private String content; // 내용
	
	private String workUrl;  // 작업 url
	
	private String completeUrl; // 완료 사이트 url
	
	private String siteAdminUrl; // site admin Url
	
	private String siteAdminId; // site admin id;
	
	private String siteAdminPw; // site admi password;
	
	private String webHostUrl; // web Host url;
	
	private String webHostId; // web Host Id;
	
	private String webHostPw; // web Host password
	
	private String themeName; // 테마명
	
	private String themeLicenseCode; // 테마 라이센스 코드
	
	@Transient
	private List<MultipartFile> multipartFiles;

	@Transient
	private String workProcess;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="process_file1_id")
	private ProjectProcessFile processFile1;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="process_file2_id")
	private ProjectProcessFile processFile2;
	

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@Column(updatable = true)
	@LastModifiedDate
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부
	


}