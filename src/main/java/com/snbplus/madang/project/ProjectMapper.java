package com.snbplus.madang.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectMapper extends JpaRepository<Project, Integer>, JpaSpecificationExecutor<Project> {

    Optional<Project> findByIdAndDelYn(Integer id, String delYn);

    List<Project> findByKind(String kind);

    List<Project> findByState(String state);

}
