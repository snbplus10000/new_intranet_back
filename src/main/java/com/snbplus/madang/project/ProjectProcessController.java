package com.snbplus.madang.project;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/projectProcess")
@RequiredArgsConstructor
public class ProjectProcessController implements InterfaceController<ProjectProcess> {

    private final ProjectProcessMapper processMapper;

    private final ProjectProcessFileMapper processFileMapper;

    @ApiOperation(value = "사업관리 - 진행관련 상세화면", httpMethod = "GET", notes = "사업관리 - 진행관련 상세화면")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사업관리 pk", required = true, dataType = "number", defaultValue = "1")
    })
    @Override
    public ResponseEntity<ProjectProcess> view(@PathVariable Integer id) {

        ProjectProcess process = processMapper.findOne(id);

        return new ResponseEntity<>(process, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 저장", httpMethod = "PUT", notes = "사업관리 - 진행관련 저장")
    @Override
    public ResponseEntity<ProjectProcess> save(@RequestBody ProjectProcess t) {
    	
    	ProjectProcessFile processFile1 = t.getProcessFile1();
    	ProjectProcessFile processFile2 = t.getProcessFile2();
    	
    	if(processFile1 != null && processFile1.getKind() != null) {
    		processFileMapper.save(processFile1);
    		t.setProcessFile1(processFile1);	
    	}

    	if(processFile2 != null && processFile2.getKind() != null) {
    		processFileMapper.save(processFile2);
    		t.setProcessFile2(processFile2);
    	}

        User user = UserSingleton.getInstance();

        t.setCreater(user.getId());
        t.setDelYn("N");
        
        processMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 수정", httpMethod = "UPDATE", notes = "사업관리 - 진행관련 수정")
    @Override
    public ResponseEntity<ProjectProcess> update(@RequestBody ProjectProcess t) {
    	
    	ProjectProcessFile processFile1 = t.getProcessFile1();
    	ProjectProcessFile processFile2 = t.getProcessFile2();
    	
    	if(processFile1 != null && processFile1.getKind() != null) {
    		processFileMapper.save(processFile1);
    		t.setProcessFile1(processFile1);	
    	}

    	if(processFile2 != null && processFile2.getKind() != null) {
    		processFileMapper.save(processFile2);
    		t.setProcessFile2(processFile2);
    	}

        User user = UserSingleton.getInstance();


        t.setModifier(user.getId());
        processMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 삭제", httpMethod = "DELETE", notes = "사업관리 - 진행관련 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "진행관련 pk", required = true, dataType = "number", defaultValue = "1")})
    @Override
    public ResponseEntity<ProjectProcess> delete(@PathVariable Integer id) {
        ProjectProcess process = processMapper.findOne(id);

        User user = UserSingleton.getInstance();

        process.setDelYn("Y");
        process.setModifier(user.getId());
        processMapper.save(process);

        return new ResponseEntity<>(process, HttpStatus.OK);
    }

    // NotUsed
    @Override
    public ResponseEntity<Page<ProjectProcess>> list(Pageable pageable, String conditionKey, String conditionValue) {
        return null;
    }
    
	@PutMapping("/test")
	public ResponseEntity<ProjectProcess> savetest(@RequestBody String t) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		ProjectProcess translate = mapper.readValue(t, ProjectProcess.class);
		
		return new ResponseEntity<>(translate, HttpStatus.OK);
	}
}
