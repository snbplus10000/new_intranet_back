package com.snbplus.madang.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectTranslateMapper extends JpaRepository<ProjectTranslate, Integer> {

    List<ProjectTranslate> findByProjectIdAndDelYn(Integer id, String delYn);
}
