package com.snbplus.madang.project;

import com.snbplus.madang.JoinController;
import com.snbplus.madang.common.Comment;
import com.snbplus.madang.common.CommentMapper;
import com.snbplus.madang.common.MailService;
import com.snbplus.madang.company.Company;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/projectComment")
@RequiredArgsConstructor
public class ProjectCommentController implements JoinController<List<ProjectComment>> {

    private final ProjectCommentMapper projectCommentMapper;

    private final CommentMapper commentMapper;

    private final UserMapper userMapper;

    private final ProjectCommentSpec projectCommentSpec;

    private final ProjectMapper projectMapper;

    private final MailService mailService;

    // NotUsed
    @ApiOperation(value = "사업진행 - 코멘트 정보 저장/수정", httpMethod = "PUT", notes = "사업진행 - 코멘트 정보 저장/수정")
    @Override
    public ResponseEntity<List<ProjectComment>> save(@RequestBody List<ProjectComment> t) {

        for(ProjectComment comment : t) {
            projectCommentMapper.save(comment);
        }

        return new ResponseEntity<>(t,HttpStatus.OK);
    }

    @ApiOperation(value = "사업진행 - 코멘트 개별 정보 저장", httpMethod = "PUT", notes = "사업진행 - 코멘트 개별 정보 저장")
    @PutMapping("/create")
    public ResponseEntity<ProjectComment> saveSingle(@RequestBody Map<String, String> commentMap) {

        Comment comment = new Comment();
        User user = UserSingleton.getInstance();

        comment.setCreater(user.getId());
        comment.setDelYn("N");
        comment.setVisibility("Y");
        comment.setFromPeople(user.getName());
        comment.setKinds("project");
        /* 입력되는 요소 추가 */
        comment.setComment(commentMap.get("comment"));
        comment.setToPeople(commentMap.get("toPeople"));
        comment.setGubun(commentMap.get("gubun"));
        comment.setLocation(commentMap.get("location"));
        commentMapper.save(comment);

        Optional<Project> project = projectMapper.findByIdAndDelYn(Integer.parseInt(commentMap.get("projectId")), "N");

        Company company = new Company();
        company.setId(Integer.parseInt(commentMap.get("companyId")));

        ProjectComment projectComment = new ProjectComment();
        projectComment.setProject(project.orElseThrow(() -> new NoSuchElementException()));
        if(commentMap.get("sendMail").equals("true")) mailService.commentMailSend(project.get(), comment); //메일 전송 로직
        projectComment.setComment(comment);
        /* 201019 선별을 위한 company 추가 */
        projectComment.setCompany(company);
        projectCommentMapper.save(projectComment);

        return new ResponseEntity<>(projectComment, HttpStatus.OK);
    }

    @ApiOperation(value = "사업진행 - 코멘트 개별 정보 수정", httpMethod = "POST", notes = "사업진행 - 코멘트 개별 정보 수정")
    @PostMapping("/update")
    public ResponseEntity<ProjectComment> updateSingle(@RequestBody Map<String, String> commentMap) {

        Integer commentId = Integer.parseInt(commentMap.get("commentId"));
        Comment comment = commentMapper.findOne(commentId);

        User user = UserSingleton.getInstance();

        comment.setModifier(user.getId());
        comment.setDelYn("N");
        comment.setVisibility("Y");
        comment.setFromPeople(user.getName());
        comment.setKinds("project");
        /* 입력되는 요소 추가 */
        comment.setComment(commentMap.get("comment"));
        comment.setToPeople(commentMap.get("toPeople"));
        comment.setGubun(commentMap.get("gubun"));
        comment.setLocation(commentMap.get("location"));
        commentMapper.save(comment);

        Project project = new Project();
        project.setId(Integer.parseInt(commentMap.get("projectId")));

        Integer projectCommentId = Integer.parseInt(commentMap.get("projectCommentId"));
        ProjectComment projectComment = projectCommentMapper.findOne(projectCommentId);

        projectComment.setProject(project);
        projectComment.setComment(comment);
        projectCommentMapper.save(projectComment);

        return new ResponseEntity<>(projectComment, HttpStatus.OK);
    }

    // NotUsed
    @Override
    public ResponseEntity<List<ProjectComment>> delete(List<ProjectComment> t) {

        for(ProjectComment comment : t) {
            comment.setDelYn("Y");
        }
        projectCommentMapper.save(t);

        return new ResponseEntity<>(t,HttpStatus.OK);
    }


    @ApiOperation(value = "사업진행 - 코멘트 개별 정보 삭제", httpMethod = "DELETE", notes = "사업진행 - 코멘트 개별 정보 삭제")
    @DeleteMapping("/{id}")
    public ResponseEntity<ProjectComment> deleteSingle(@PathVariable Integer id){

        ProjectComment projectComment = projectCommentMapper.findOne(id);
        User user = UserSingleton.getInstance();

        projectComment.getComment().setModifier(user.getId());
        projectComment.getComment().setDelYn("Y");
        projectComment.setDelYn("Y");
        projectCommentMapper.save(projectComment);

        return new ResponseEntity<>(projectComment,HttpStatus.OK);
    }


    @ApiOperation(value = "사업진행 종류별 코멘트 리스트", httpMethod = "GET", notes = "사업진행 종류별 코멘트 리스트")
    @GetMapping("/page")
    public ResponseEntity<Page<ProjectComment>> pageList(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
            @RequestParam(required = true) Integer companyId,
            @RequestParam(required = false) String location){

        Specification<ProjectComment> spec = Specifications.where(projectCommentSpec.build("N",companyId,"project",location));

        return new ResponseEntity(projectCommentMapper.findAll(spec, pageable).map(comment -> {

            //* 데이터 선별을 위해 불필요한 데이터를 null처리 *//

            return comment;

        }), HttpStatus.OK);

    }

    @ApiOperation(value = "사업진행 종류별 코멘트 리스트", httpMethod = "GET", notes = "사업진행 종류별 코멘트 리스트")
    @GetMapping("/all")
    public ResponseEntity<Page<ProjectComment>> allList(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, value = Integer.MAX_VALUE) Pageable pageable,
            @RequestParam(required = true) Integer companyId,
            @RequestParam(required = false) String location){

        Specification<ProjectComment> spec = Specifications.where(projectCommentSpec.build("N",companyId,"project",location));

        return new ResponseEntity(projectCommentMapper.findAll(spec, pageable).map(comment -> {

            //* 데이터 선별을 위해 불필요한 데이터를 null처리 *//

            return comment;

        }), HttpStatus.OK);

    }

    // NotUsed
    @ApiOperation(value = "사업진행 - 코멘트 정보 리스트", httpMethod = "GET", notes = "사업진행 - 코멘트 개별 정보 리스트")
    @Override
    public ResponseEntity<List<ProjectComment>> list(@PathVariable Integer id) {
        return new ResponseEntity<>(projectCommentMapper.findByIdAndDelYnOrderByIdDesc(id,"N"), HttpStatus.OK);
    }


    @ApiOperation(value = "사업진행 코멘트 개별 정보 조회", httpMethod = "GET", notes = "사업진행 코멘트 개별 정보 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "코멘트 pk", required = true, dataType = "number")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ProjectComment> viewSingle(@PathVariable Integer id){
        ProjectComment comment = projectCommentMapper.findOne(id);
        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

}
