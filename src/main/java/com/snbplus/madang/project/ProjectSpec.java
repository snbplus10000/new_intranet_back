package com.snbplus.madang.project;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class ProjectSpec {

    public Specification<Project> build(String delYn, String kind, String conditionValue1, String conditionValue2) {

    	String conditionKey = "";
    	if((conditionValue1 == null || conditionValue1.equals("")) && (conditionValue2 == null || conditionValue2.equals(""))) {
    		if(kind == null || kind.equals("")){
                return search(delYn);
            }else{
                return search(delYn, kind);
            }
    	}else if((conditionValue1 != null && !conditionValue1.equals("")) && (conditionValue2 == null || conditionValue2.equals(""))) {

    		conditionKey = "state";

            if(kind == null || kind.equals("")){
                return search(delYn, conditionKey, "%" + conditionValue1 + "%");
            }else{
                return search(delYn, kind, conditionKey, "%" + conditionValue1 + "%");
            }


    	}else if((conditionValue1 == null || conditionValue1.equals("")) && (conditionValue2 != null && !conditionValue2.equals(""))) {

    		conditionKey = "businessName";

            if(kind == null || kind.equals("")){
                return search(delYn, conditionKey, "%" + conditionValue2 + "%");
            }else{
                return search(delYn, kind, conditionKey, "%" + conditionValue2 + "%");
            }

    	}else {

            if(kind == null || kind.equals("")){
                return searchAnd(delYn, "%" + conditionValue1 + "%" , "%" + conditionValue2 + "%");
            }else{
                return searchAnd(delYn, kind, "%" + conditionValue1 + "%" , "%" + conditionValue2 + "%");
            }


    	}
    }

    public Specification<Project> search(String delYn) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn)
                );
    }

    public Specification<Project> search(String delYn, String kind) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn),
                        cb.equal(root.get("kind"), kind)
                );
    }

    public Specification<Project> search(String delYn, String kind, String conditionKey, String conditionValue){
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn),
                        cb.equal(root.get("kind"), kind),
                        cb.like(root.get(conditionKey), conditionValue)
                );
    }
    
    public Specification<Project> search(String delYn, String conditionKey, String conditionValue){
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn),
                        cb.like(root.get(conditionKey), conditionValue)
               );
    }

    public Specification<Project> searchAnd(String delYn, String kind, String conditionValue1, String conditionValue2){
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn),
                        cb.equal(root.get("kind"), kind),
                        cb.like(root.get("state"), conditionValue1),
                        cb.like(root.get("businessName"), conditionValue2)
                );
    }

    public Specification<Project> searchAnd(String delYn, String conditionValue1, String conditionValue2){
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"), delYn),
                        cb.like(root.get("state"), conditionValue1),
                        cb.like(root.get("businessName"), conditionValue2)
                );
    }
}
