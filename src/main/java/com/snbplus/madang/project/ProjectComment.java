package com.snbplus.madang.project;

import com.snbplus.madang.common.Comment;
import com.snbplus.madang.company.Company;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

import static javax.persistence.FetchType.LAZY;

/**
 *  프로젝트 댓글 관리
 * @author hsAhn
 *
 */
@Entity
@Table(name = "tbl_project_comment")
@Data
public class ProjectComment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String delYn = "N";

    @OneToOne
    @JoinColumn(name="comment_id")
    private Comment comment;
    
    @ManyToOne
    @JoinColumn(name="project_id", nullable = false)
    private Project project;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name="company_id", nullable = false)
    private Company company;

}