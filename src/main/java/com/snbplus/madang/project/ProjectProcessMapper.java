package com.snbplus.madang.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectProcessMapper extends JpaRepository<ProjectProcess, Integer> {

    /* test에서 사용*/
    Optional<ProjectProcess> findByIdAndDelYn(Integer id, String delYn);


}
