package com.snbplus.madang.project;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.snbplus.madang.common.FileAttach;

import lombok.Data;
/**
 * 프로젝트 기본 내용 파일
 * @author BH SHIN
 *
 */
@Entity
@Table(name="tbl_project_process_file")
@Data
public class ProjectProcessFile {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String kind; // 종류
	
	@OneToOne
	@JoinColumn(name="file_id")
	private FileAttach fileAttach;
}
