package com.snbplus.madang.project;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.sql.Timestamp;
/**
 * 프로젝트 번역 
 * @author S&B Plus
 *
 */
@Entity
@Table(name="tbl_project_translate")
@Data
public class ProjectTranslate {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String companyName; // 업체명
	
	private String telNo; // 연락처
	
	private String requestDate; // 의뢰일
	
    @Column(nullable = false)
    @ColumnDefault("'false'")
//    private boolean korean = false; // 언어 - 한국어
	private String korean = "false"; // 언어 - 한국어
    
    @Column(nullable = false)
	@ColumnDefault("'false'")
//    private boolean english = false; // 언어 - 영어
	private String english = "false";
    
    @Column(nullable = false)
	@ColumnDefault("'false'")
//    private boolean chinese1 = false; // 언어 - 중국어 간체
	private String chinese1 = "false";

    
    @Column(nullable = false)
	@ColumnDefault("'false'")
//    private boolean chinese2 = false; // 언어 - 중국어 번체
	private String chinese2 = "false";
    
    @Column(nullable = false)
	@ColumnDefault("'false'")
//    private boolean japanese = false; // 언어 - 일본어
	private String japanese = "false";
    
    @Column(nullable = false)
	@ColumnDefault("'false'")
//    private boolean vietnamese = false; // 언어 - 베트남어
	private String vietnamese = "false";
    
    private String translateVolume; // 번역량
    
    private String translateFee; // 번역비
    
    private String completeDate; // 완료예정일
    
	@Transient
	private MultipartFile estimateOriFile;
	
	@Transient
	private MultipartFile translateOriFile;
    
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="estimate_file_id")
    private ProjectEstimateFile estimateFile; // 견적서 파일
    
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="translate_file_id")
    private ProjectTranslateFile translateFile; // 번역본 파일
    
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부
	
	private Integer projectId;
}
