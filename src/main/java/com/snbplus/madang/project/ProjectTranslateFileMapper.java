package com.snbplus.madang.project;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectTranslateFileMapper extends JpaRepository<ProjectTranslateFile, Integer> {
}
