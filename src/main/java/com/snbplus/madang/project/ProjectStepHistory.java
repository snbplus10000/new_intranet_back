package com.snbplus.madang.project;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@ToString
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tbl_project_step_history")
public class ProjectStepHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @CreatedDate
    private String startDate;

    private String endDate;

    private String step;

    public void updateEndDate() {
        endDate = "";
    }

    public static ProjectStepHistory createStep(String step) {
        ProjectStepHistory stepHistory = new ProjectStepHistory();
        stepHistory.step = step;
        return stepHistory;
    }
}
