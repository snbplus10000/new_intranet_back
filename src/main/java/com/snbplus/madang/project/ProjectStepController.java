package com.snbplus.madang.project;

import com.snbplus.madang.InterfaceController;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/projectStep")
@RequiredArgsConstructor
public class ProjectStepController implements InterfaceController<ProjectStep> {

    private final ProjectStepMapper stepMapper;

    @ApiOperation(value = "사업관리 - 진행관련 상세화면", httpMethod = "GET", notes = "사업관리 - 진행관련 상세화면")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "사업관리 pk", required = true, dataType = "number", defaultValue = "1")
    })
    @Override
    public ResponseEntity<ProjectStep> view(@PathVariable Integer id) {

        ProjectStep step = stepMapper.findOne(id);

        return new ResponseEntity<>(step, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 저장", httpMethod = "PUT", notes = "사업관리 - 진행관련 저장")
    @Override
    public ResponseEntity<ProjectStep> save(@RequestBody ProjectStep t) {

        stepMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 수정", httpMethod = "UPDATE", notes = "사업관리 - 진행관련 수정")
    @Override
    public ResponseEntity<ProjectStep> update(@RequestBody ProjectStep t) {

        stepMapper.save(t);

        return new ResponseEntity<>(t, HttpStatus.OK);
    }

    @ApiOperation(value = "사업관리 - 진행관련 삭제", httpMethod = "DELETE", notes = "사업관리 - 진행관련 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "진행관련 pk", required = true, dataType = "number", defaultValue = "1")})
    @Override
    public ResponseEntity<ProjectStep> delete(@PathVariable Integer id) {
        ProjectStep projectStep = stepMapper.findOne(id);

        stepMapper.save(projectStep);

        return new ResponseEntity<>(projectStep, HttpStatus.OK);
    }

    // NotUsed
    @Override
    public ResponseEntity<Page<ProjectStep>> list(Pageable pageable, String conditionKey, String conditionValue) {
        return null;
    }
}

