package com.snbplus.madang.project;

import com.snbplus.madang.company.Company;
import com.snbplus.madang.user.ProjectManager;
import com.snbplus.madang.user.User;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


/**
 * 프로젝트 기본 내용
 * @author BH SHIN
 *
 */
@Data
@Entity
@Table(name="tbl_project")
public class Project {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="companyId")
	private Company company;

	private String contractName; // 계약자명
	
	private String businessName; // 사업명
	
	private String kind; // 작업구분
	
	private String state; // 상태
	
	private String content; // 내용
	
	private Integer amount; // 금액
	
	private String contractDate; // 계약일

	private String contractCompletionDate; //계약완료일

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "creater")
	private User creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부
	

	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="projectId")
	private List<ProjectProcess> processList;
	
	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="projectId")
	private List<ProjectTranslate> translateList;

	@OneToMany(mappedBy = "project", orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ProjectManager> managers;

	@OneToMany(fetch = FetchType.LAZY,  cascade= CascadeType.ALL)
	@JoinColumn(name="projectId")
	private List<ProjectStep> projectStep;

	@Transient
	private User user;
	
	@Transient
	private Integer choiceCompanyId;

	/* 가장 최근에 달린 댓글의 날짜 */
	@Transient
	private Timestamp commentModified;

	@Transient
	private String companyName;

	private String step;

}
