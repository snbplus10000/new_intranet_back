package com.snbplus.madang.project;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

@Entity
@Table(name = "tbl_project_step")
@Data
public class ProjectStep {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer projectId;

    private String step1;

    private String date1;

    private String step2;

    private String date2;

    private String step3;

    private String date3;

    private String step4;

    private String date4;

    private String step5;

    private String date5;

    private String step6;

    private String date6;

    private String step7;

    private String date7;
}
