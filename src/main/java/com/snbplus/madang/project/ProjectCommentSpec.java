package com.snbplus.madang.project;

import javax.persistence.criteria.Join;

import com.snbplus.madang.company.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.common.CommentMapper;

import java.util.stream.Collectors;

@Component
public class ProjectCommentSpec{

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    ProjectCommentMapper projectCommentMapper;

	public Specification<ProjectComment> build(String delYn, Integer companyId, String kinds, String location) {

		if(location == null || location.equals("")) {
			return spec(delYn, companyId, kinds);
		}else{
			return spec(delYn, companyId, kinds, location);
		}

	}

    /* 최근 코멘트*/
    public Specification<ProjectComment> spec(String delYn, Integer companyId, String kinds){
    	return (root,query,cb) ->{
    		Join<Object, Object> comment = root.join("comment");
			Join<Object, Object> company = root.join("company");
    		return cb.and(
    					cb.equal(root.get("delYn"), delYn),
    					cb.equal(company.get("id"), companyId),
    					cb.equal(comment.get("kinds"), kinds)
    				);
    	};
    }

    /* 삭제여부와 CommentId,  location으로 필터링*/
    public Specification<ProjectComment> spec(String delYn, Integer companyId, String kinds, String location){
    	
    	return (root,query,cb) ->{
    		Join<Object, Object> comment = root.join("comment");
    		Join<Object, Object> company = root.join("company");
    		query.orderBy(cb.desc(root.get("id")));
    		return cb.and(
    					cb.equal(root.get("delYn"), delYn),
    					cb.equal(company.get("id"), companyId),
						cb.equal(comment.get("kinds"), kinds),
						cb.equal(comment.get("location"), location)
    				);
    	};
    }
    
    //todo 업체기준 댓글 가져오기
//	public Specification<ProjectComment> spec(String delYn, Integer id, String location){
//
//		return (root,query,cb) ->{
//			Join<Object, Object> comment = root.join("comment");
//			Join<Object, Object> project = root.join("project");
//			query.orderBy(cb.desc(root.get("id")));
//			return cb.and(
//					cb.equal(root.get("delYn"), delYn),
//					cb.equal(comment.get("location"), location),
//					cb.equal(project.get("id"), id)
//			);
//		};
//	}



}
