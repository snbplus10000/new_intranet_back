package com.snbplus.madang.project;

import com.snbplus.madang.common.FileAttach;
import lombok.Data;

import javax.persistence.*;

/**
 * 프로젝트 번역본 파일
 * @author S&B Plus
 *
 */

@Entity
@Table(name = "tbl_project_translate_file")
@Data
public class ProjectTranslateFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String kind; // 종류

    @OneToOne
    @JoinColumn(name = "file_id")
    private FileAttach fileAttach;

}
