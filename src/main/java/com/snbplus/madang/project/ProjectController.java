package com.snbplus.madang.project;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.company.Company;
import com.snbplus.madang.company.CompanyMapper;
import com.snbplus.madang.user.ProjectManager;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/project")
@RequiredArgsConstructor
public class ProjectController implements InterfaceController<Project> {

	private final ProjectMapper projectMapper;
	
	private final CompanyMapper companyMapper;

	private final UserMapper userMapper;

	private final ProjectSpec projectSpec;
	
	private final ProjectCommentMapper commentMapper;


	@ApiOperation(value = "사업진행 - 의뢰내용 리스트", httpMethod = "GET", notes = "사업진행 - 의뢰내용 리스트")
	@GetMapping("/")
	public ResponseEntity<Page<Project>> list1(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
			@RequestParam(required = false) String kind,
			@RequestParam(required = false) String conditionValue1,
			@RequestParam(required = false) String conditionValue2) {

		Specification<Project> spec = Specifications.where(projectSpec.build("N",kind,conditionValue1,conditionValue2));

//        유저가 선택한 조건에 맞는 프로젝트 리스트를 가져오기
        List<Project> projects = projectMapper.findAll(spec);
        Page<Project> projectPage = null;

		User curUser = UserSingleton.getInstance();

		if(!curUser.getAuth().getId().equals(1)){ // 권한이 ADMIN이 아닐 경우
			List<ProjectManager> userProjectManagers = curUser.getManagers();
			List<Project> readableProjects = new ArrayList<>();
            // 자신이 프로젝트의 담당자이거나 글 작성자인 프로젝트만 readaboe 리스트에 저장
            projects.stream().filter(p -> {
                for (ProjectManager pm : userProjectManagers)
                    if(pm.getProject().getId().equals(p.getId())) return true;
                if(p.getCreater().getId().equals(curUser.getId())) return true;
                return false;
            }).forEach(p -> readableProjects.add(p));
			projectPage = new PageImpl<>(readableProjects, pageable, readableProjects.size()); //만들어둔 프로젝트 리스트 페이지 객체에 대입
		} else {
            projectPage = projectMapper.findAll(spec, pageable);
        }

		return new ResponseEntity(projectPage.map(project -> {

				/* 최근컨텍일을 위한 작업*/
				ProjectComment comment = commentMapper.findTop1ByProjectIdAndDelYnOrderByIdDesc(project.getId(), "N");
				if(comment != null) {
					project.setCommentModified(comment.getComment().getModified());
				}else {
					project.setCommentModified(null);
				}

				/* Company 데이터 선별을 위한 작업 */
				Company company = project.getCompany();
				project.setCompany(null);
				project.setCompanyName(company.getCompanyName());


				/* Project 데이터 선별을 위한 작업 */
				project.setTranslateList(null);
				return project;
			}), HttpStatus.OK);
	}

	@ApiOperation(value = "사업진행 - 의뢰내용 상세화면", httpMethod = "GET", notes = "사업진행 - 의뢰내용 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "의뢰내용 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<Project> view(@PathVariable Integer id) {
		Project t = projectMapper.findOne(id);
		Integer choiceCompanyId = t.getCompany().getId();
		t.setId(id);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "사업진행 - 의뢰내용 저장", httpMethod = "PUT", notes = "사업진행 - 의뢰내용 저장")
	@Override
	public ResponseEntity<Project> save(@RequestBody Project t) {

		User user = UserSingleton.getInstance();
		
		Company company = companyMapper.findOne(t.getChoiceCompanyId());

		t.setCreater(user);
		t.setCompany(company);
		t.getManagers().forEach(manager -> manager.setProject(t));

		projectMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "사업진행 - 의뢰내용 수정", httpMethod = "POST", notes = "사업진행 - 의뢰내용 수정")
	@Override
	public ResponseEntity<Project> update(@RequestBody Project t) {
		User user = UserSingleton.getInstance();
		Company company = companyMapper.findOne(t.getChoiceCompanyId());

		t.setStep(t.getStep());
		t.setCompany(company);
		t.setModifier(user.getId());
		t.getManagers().forEach(manager -> manager.setProject(t));

		projectMapper.save(t);
		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "사업진행 - 의뢰내용 삭제", httpMethod = "DELETE", notes = "사업진행 - 의뢰내용 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "의뢰내용 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@Override
	public ResponseEntity<Project> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		Project project = projectMapper.findOne(id);
		project.setDelYn("Y");
		project.setModifier(user.getId());

		projectMapper.save(project);
		return new ResponseEntity<>(project, HttpStatus.OK);
	}

	// NotUsed
	@Override
	public ResponseEntity<Page<Project>> list(Pageable pageable, String conditionKey, String conditionValue) {
		return null;
	}
	
	@GetMapping("/test")
	public ResponseEntity<Project> saves( @RequestBody String t) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		Project project = mapper.readValue(t, Project.class);
		
		return new ResponseEntity<>(project, HttpStatus.OK);
	}
}
