package com.snbplus.madang.tax;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="tbl_tax_issued")
@Data
public class TaxIssued {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private Integer year;
	
	private String companyName;
	
	private String workContent;
	
	private String item;
	
	private String supplyPay;
	
	private String applyDate;
	
	private String issuedDate;
	
	private Integer taxNo;

}
