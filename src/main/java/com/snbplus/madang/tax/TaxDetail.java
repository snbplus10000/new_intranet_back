package com.snbplus.madang.tax;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="tbl_tax_detail")
@Data
public class TaxDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private Integer year;

	private String businessName;
	
	private String companyName;
	
	private String workContent;
	
	private String state;
	
	
	@ManyToOne
	@JoinColumn(name="tax_id")
	private Tax tax;

}
