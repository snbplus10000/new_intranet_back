package com.snbplus.madang.tax;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TaxViewController {
	
	@GetMapping("/tax/list")
	public String taxList(Model model) {
		
		
		return "tax/taxList";
	}
	
	@GetMapping("/tax/view")
	public String taxView(Model model, Integer id) {
		
		return "tax/taxView";
	}

}
