package com.snbplus.madang.auth;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "tbl_auth")
@Data
public class Auth {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id; // pk

    private String name; // 권한 이름

    private String description; //권한 설명

    private String menus; //메뉴 리스트

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String useYn; //사용 여부

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String delYn; //삭제 여부

    @Column(nullable = false, updatable = false)
    @CreatedDate
    @CreationTimestamp
    private Timestamp regdate;  // 등록일

    @LastModifiedDate
    @Column(updatable = true)
    @UpdateTimestamp
    private Timestamp moddate; // 수정일


}
