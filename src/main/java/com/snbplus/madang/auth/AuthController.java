package com.snbplus.madang.auth;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    @Autowired
    AuthMapper authMapper;

    @Autowired
    MenuMapper menuMapper;

    @ApiOperation(value = "메뉴 리스트", httpMethod = "GET", notes = "메뉴 리스트")
    @GetMapping("/menuList")
    public ResponseEntity<List<Menu>> list(@RequestParam Integer id) {
        String menu = authMapper.findByIdAndUseYnAndDelYn(id, "Y", "N").getMenus();
        List<Menu> menuLists;
        if(menu.equals("all")) {
            menuLists = menuMapper.getByUseYnAndDelYn("Y", "N");
        }else {
            String[] lists = menu.split(",");
            menuLists = new ArrayList<>();

            for (String m : lists) {
                Menu menuSpecific = menuMapper.findByIdAndUseYnAndDelYn(Integer.parseInt(m), "Y", "N");
                menuLists.add(menuSpecific);
            }
        }
        return new ResponseEntity<>(menuLists, HttpStatus.OK);
    }

    @ApiOperation(value = "메뉴 전체 리스트", httpMethod = "GET", notes = "메뉴 전체 리스트")
    @GetMapping("/menuAll")
    public ResponseEntity<List<Menu>> listAll() {
        List<Menu> menuLists = menuMapper.getByDelYn("N");
        return new ResponseEntity<>(menuLists, HttpStatus.OK);
    }

    @ApiOperation(value = "메뉴 사용 리스트", httpMethod = "GET", notes = "메뉴 사용 리스트")
    @GetMapping("/menuUsed")
    public ResponseEntity<List<Menu>> listUsed(){
        List<Menu> menuLists= menuMapper.getByUseYnAndDelYn("Y", "N");
        return new ResponseEntity<>(menuLists, HttpStatus.OK);
    }

    @GetMapping("/getMenu")
    public ResponseEntity<Menu> getMenu(@RequestParam (value="index", required=true) Integer id) {
        Menu menu = menuMapper.findOne(id);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @GetMapping("/getAuthDetail")
    public ResponseEntity<Auth> getAuthDetail(@RequestParam (value="index", required=true) Integer id) {
        Auth auth = authMapper.findOne(id);
        return new ResponseEntity<>(auth, HttpStatus.OK);
    }
    @GetMapping("/getAuth")
    public ResponseEntity<List<Auth>> getAuth() {
        List<Auth> lists = authMapper.findByDelYn("N");
        return new ResponseEntity<>(lists, HttpStatus.OK);
    };

    @GetMapping("/getAuthUsed")
    public ResponseEntity<List<Auth>> getAuthUsed() {
        List<Auth> lists = authMapper.findByUseYnAndDelYn("Y", "N");
        return new ResponseEntity<>(lists, HttpStatus.OK);
    }
    
    @ApiOperation(value = "메뉴 사용여부 토글 업데이트", httpMethod = "PUT", notes = "메뉴 사용여부 업데이트")
    @PutMapping("/updateMenuUse")
    public ResponseEntity<Menu> updateMenuUse(@RequestBody Map<String, Object> params) {
        Integer index = (Integer) params.get("index");
        String value = (String) params.get("value");
        Menu menu = menuMapper.findOne(index);
        menu.setUseYn(value);
        menuMapper.save(menu);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @ApiOperation(value = "권한 사용여부 토글 업데이트", httpMethod = "PUT", notes = "권한 사용여부 업데이트")
    @PutMapping("/updateAuthUse")
    public ResponseEntity<Auth> updateAuthUse(@RequestBody Map<String, Object> params) {
        Integer index = (Integer) params.get("index");
        String value = (String) params.get("value");
        Auth auth = authMapper.findOne(index);
        auth.setUseYn(value);
        authMapper.save(auth);
        return new ResponseEntity<>(auth, HttpStatus.OK);
    }

    @ApiOperation(value = "권한의 데이터 업데이트")
    @PutMapping("/updateAuth")
    public ResponseEntity<Auth> updateAuth(@RequestBody Auth auth) {
        authMapper.save(auth);
        return new ResponseEntity<>(auth, HttpStatus.OK);
    }

    @ApiOperation(value = "메뉴 상세페이지 데이터 업데이트", httpMethod = "PUT", notes = "메뉴 상세페이지 데이터 업데이트")
    @PutMapping("/updateMenu")
    public ResponseEntity<Menu> updateMenu(@RequestBody Menu menu) {
        menuMapper.save(menu);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @ApiOperation(value = "메뉴 추가")
    @PutMapping("/addMenu")
    public ResponseEntity<Menu> addMenu(@RequestBody Menu menu) {
        Integer group = menu.getParent();
        Integer sort = menuMapper.getLastSort(group);
        if(sort == null) {
            sort = 1;
        }
        menu.setSort(sort + 1);
        menu.setUseYn("Y");
        menu.setDelYn("N");
        menuMapper.save(menu);
        return new ResponseEntity<>(menu, HttpStatus.OK);

    }

    @ApiOperation(value = "메뉴 삭제", httpMethod = "PUT", notes = "메뉴 삭제")
    @PutMapping("/deleteMenu")
    public ResponseEntity<Menu> deleteMenu(@RequestBody Map<String, String> params) {
        Integer index = Integer.parseInt(params.get("index"));
        Menu menu = menuMapper.findOne(index);
        menu.setDelYn("Y");
        menuMapper.save(menu);
        return new ResponseEntity<>(menu, HttpStatus.OK);
    }

    @ApiOperation(value = "권한 삭제", httpMethod = "PUT", notes = "메뉴 삭제")
    @PutMapping("/deleteAuth")
    public ResponseEntity<Auth> deleteAuth(@RequestBody Map<String, String> params) {
        Integer index = Integer.parseInt(params.get("index"));
        Auth auth = authMapper.findOne(index);
        auth.setDelYn("Y");
        authMapper.save(auth);
        return new ResponseEntity<>(auth, HttpStatus.OK);
    }
}
