package com.snbplus.madang.auth;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import javax.persistence.*;

@Entity
@Table(name = "tbl_menu")
@Data
public class Menu {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id; // pk

    private String icon; //메뉴 아이콘

    private String name; // 메뉴 이름

    private String path; //메뉴 경로

    private Integer sort; //메뉴 순서

    private Integer parent; //상위 메뉴 fk 값

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String useYn; //사용 여부

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String delYn; //삭제 여부
}
