package com.snbplus.madang.auth;

import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserAuthService {
    private final UserMapper userMapper;

    //현재 내가 접근 가능한 맴버 리스트
    public List<User> myTeamMemberList(User user, String workState) {
        System.out.println("CHECKPOINT");
        return userMapper.findByWorkState(workState).stream().filter(usert -> {
            /*
            return user.getDeptChart().getCode().equals(usert.getDeptChart().getCode()) || user.getDeptChart().getCode().equals(usert.getDeptChart().getParentCode()) ||
                    user.getLoginId().equals("jinyk0811") || user.getDeptChart().getCode().equals(1) || user.getDeptChart().getCode().equals(2) || user.getDeptChart().getCode().equals(7);
            */
            try {
                return (user.getDeptChart() != null && user.getHighDeptChart() != null && usert.getDeptChart() != null && usert.getHighDeptChart() != null)
                        && (user.getDeptChart().getCode().equals(usert.getDeptChart().getCode()))
                        || (user.getHighDeptChart().getCode().equals(usert.getDeptChart().getCode()))
                        || (user.getDeptChart().getCode().equals(usert.getHighDeptChart().getCode()))
                        || (user.getId() == usert.getDeptChart().getManager())
                        || user.getLoginId() == "snbkim"
                        || user.getLoginId() == "master"
                        || user.getDeptChart().getCode().equals(1)
                        || user.getDeptChart().getCode().equals(2)
                        || user.getDeptChart().getCode().equals(7);
            }catch(Exception e) {
                System.out.println("CHECKPOINT");
                System.out.println(usert);
                System.out.println(user);
                return false;
            }

        }).collect(Collectors.toList());
    }

    //개발팀과 디크팀 리스트
    public List<User> devTeamMemberList(User user, String workState) {

        return userMapper.findByWorkState(workState).stream().filter(usert -> {

            /*
            return  user.getDeptChart().getParentCode().equals(usert.getDeptChart().getParentCode()) ||
                    user.getDeptChart().getCode().equals(1) ||
                    user.getDeptChart().getCode().equals(2) ||
                    user.getDeptChart().getCode().equals(7);
            */
            return user.getDeptChart().getDeptUpper().equals(usert.getDeptChart().getDeptUpper()) ||
                    user.getDeptChart().getCode().equals(1) ||
                    user.getDeptChart().getCode().equals(2) ||
                    user.getDeptChart().getCode().equals(7);

        }).collect(Collectors.toList());
    }


    public List<User> myTeamMemberList(User user) {

        return myTeamMemberList(user, "재직중");

    }

    public List<User> serviceTeamMemberList(User user, String workState, String duty) {

        return userMapper.findByDutyAndWorkState(duty, workState);
    }

    public List<User> devTeamMemberList(User user) {
        return devTeamMemberList(user, "재직중");
    }

    public List<User> serviceTeamMemberList(User user) {
        return serviceTeamMemberList(user, "재직중", "서비스지원");
    }

    public List<User> allMemberList() {
        return userMapper.findByWorkState("재직중");
    }
}