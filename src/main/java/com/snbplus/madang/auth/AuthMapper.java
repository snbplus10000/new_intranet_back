package com.snbplus.madang.auth;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.snbplus.madang.outing.OutingReport;

@Repository
public interface AuthMapper extends JpaRepository<Auth, Integer>, JpaSpecificationExecutor<Auth> {
    List<Auth> findByDelYn(String delYn);
    List<Auth> findByUseYnAndDelYn(String useYn, String delYn);
    Auth findByIdAndUseYnAndDelYn(Integer id, String useYn, String delYn);
}
