package com.snbplus.madang.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuMapper extends JpaRepository<Menu, Integer>, JpaSpecificationExecutor<Menu> {
    Menu findByIdAndUseYnAndDelYn(Integer id, String useYn, String delYn);
    List<Menu> getByDelYn(String delYn);
    List<Menu> getByUseYnAndDelYn(String useYn, String delYn);
    @Query(value="SELECT MAX(sort) FROM tbl_menu WHERE parent = :idx", nativeQuery = true)
    Integer getLastSort(@Param("idx") Integer idx);
}