package com.snbplus.madang.appointment;

import com.snbplus.madang.user.Appointment;
import com.snbplus.madang.vacation.Vacation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointmentMapper extends JpaRepository<Appointment, Integer>, JpaSpecificationExecutor<Vacation> {

	Page<Appointment> findByDelYn(Pageable pageable, String string);

}
