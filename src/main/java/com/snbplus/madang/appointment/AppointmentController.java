package com.snbplus.madang.appointment;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.DeptMapper;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.common.OrgChartMapper;
import com.snbplus.madang.user.Appointment;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/appointment")
@RequiredArgsConstructor
public class AppointmentController implements InterfaceController<Appointment> {

    private final AppointmentMapper appointmentMapper;

    private final UserMapper userMapper;

    private final OrgChartMapper orgChartMapper;

    private final DeptMapper deptMapper;

    @Override
    @ApiOperation(value = "인사발령 리스트", httpMethod = "GET", notes = "인사발령 리스트")
    public ResponseEntity<Page<Appointment>> list(
            @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable,
            @RequestParam(required = false) String conditionKey,
            @RequestParam(required = false) String conditionValue) {
        return new ResponseEntity<>(
            appointmentMapper.findAll(pageable).map(appointment -> {
                return appointment;
            }), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Appointment> view(@PathVariable Integer id) {
        return new ResponseEntity<>(appointmentMapper.getOne(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Appointment> save(@RequestBody Appointment appointment) {
        User user = userMapper.findOne(appointment.getMember().getId());
        //OrgChart dept = orgChartMapper.findOne(appointment.getAppDep().getCode());
        //OrgChart highDept = orgChartMapper.findOne(dept.getParentCode());


        Dept dept = deptMapper.findOne(appointment.getAppDep().getCode());
        Dept highDept;
        if(dept.getDeptUpper() != null) {
            highDept = deptMapper.findOne(dept.getDeptUpper());
        } else {
            highDept = null;
        }

        user.setDeptChart(dept);
        user.setHighDeptChart(highDept);
        user.setRankName(appointment.getRankName());
        appointmentMapper.save(appointment);
        userMapper.save(user);
        return new ResponseEntity<>(appointment, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Appointment> update(Appointment appointment) {
        return null;
    }

    @Override
    public ResponseEntity<Appointment> delete(Integer id) {
        return null;
    }
}
