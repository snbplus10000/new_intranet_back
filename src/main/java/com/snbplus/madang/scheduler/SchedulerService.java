package com.snbplus.madang.scheduler;

import com.snbplus.madang.board.Board;
import com.snbplus.madang.common.MailService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserHoli;
import com.snbplus.madang.user.UserHoliMapper;
import com.snbplus.madang.user.UserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class SchedulerService {

    @Autowired
    UserHoliMapper holiMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    MailService mailService;

    @Scheduled(cron = "0 0 8 1 * ?")
    public void run() throws AddressException, IOException {

        //월초에 재직기간 1년 미만인 사람 연차 + 1
        List<User> list = userMapper.findLessThan1Year();

        if (list.size() > 0) {
            for (User user : list) {
                Calendar calendar = Calendar.getInstance();
                String year = String.valueOf(calendar.get(Calendar.YEAR));
                Optional<UserHoli> userHoliOptional = holiMapper.findByRefIdAndYear(user.getId(), year);
                UserHoli userHoli = new UserHoli();
                if (userHoliOptional.isPresent()) {
                    userHoli = userHoliOptional.get();
                    userHoli.setHoliCnt(userHoli.getHoliCnt() + 1);
                    userHoli.setModifier(1);
                } else {
                    userHoli.setCreater(1);
                    userHoli.setHoliCnt(1);
                    userHoli.setRefId(user.getId());
                    userHoli.setUseCnt(0);
                    userHoli.setYear(year);
                }
                holiMapper.save(userHoli);
            }

            List<User> receivers = userMapper.findByAuth(1);

            Board board = new Board();
            board.setTitle("[마당] 월차 추가 안내");
            String contents = String.join(", ", list.stream().map(u -> u.getName()).collect(Collectors.toList()));
            contents += "님의 월차가 한개 추가됩니다.";
            board.setContents(contents);
            mailService.allUserMailSend("", receivers, board);
        }
    }
}