package com.snbplus.madang.notification;

import com.snbplus.madang.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface NotificationMapper extends JpaRepository<Notification, Integer> {
    List<Notification> findByCreatedBetweenOrderByIdDesc(Date start, Date end);
}
