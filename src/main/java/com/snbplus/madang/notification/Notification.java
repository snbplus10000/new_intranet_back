package com.snbplus.madang.notification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.approval.Approval;
import com.snbplus.madang.user.User;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "notification")
@Data
public class Notification {
    @Id @GeneratedValue
    private Long id;

    @JoinColumn(name = "approval_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Approval approval;

    private String status;

    private String link;

    private String kind;

    @Setter(value = AccessLevel.NONE)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "notification")
    @JsonIgnore
    @ToString.Exclude
    private List<NotificationMemberClone> notificationList = new ArrayList<>();

    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    @Setter(value = AccessLevel.NONE)
    private Timestamp created;

    @JoinColumn(name = "creater")
    @ManyToOne(fetch = FetchType.LAZY)
    private User creater;
}
