package com.snbplus.madang.notification;

import com.snbplus.madang.user.User;
import lombok.Data;

import javax.persistence.*;

@Table(name = "notification_member_clone")
@Entity
@Data
public class NotificationMemberClone {
    @Id @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receiver")
    private User receiver;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "notificationId")
    private Notification notification;

    private String readYn;
}
