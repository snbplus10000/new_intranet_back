package com.snbplus.madang.notification;

import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.core.Local;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/notification")
@RequiredArgsConstructor
public class NotificationController {

	private final NotificationMemberCloneMapper notificationMemberCloneMapper;

	private final NotificationMapper notificationMapper;

	private final UserMapper userMapper;

	@GetMapping("/load")
	public ResponseEntity<List<NotificationMemberClone>> load(Pageable pageable) {
		User curUser = UserSingleton.getInstance();

		User user = userMapper.findOne(curUser.getId()); //실시간 알림 업데이트를 위한 user DB에서 가져와서 갱신하기
		List<NotificationMemberClone> not = notificationMemberCloneMapper.findByReceiverAndReadYnOrderByIdDesc(user, "N", pageable);
		return new ResponseEntity<>(not, HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> getCount() {
		User curUser = UserSingleton.getInstance();

		User user = userMapper.findOne(curUser.getId()); //실시간 알림 업데이트를 위한 user DB에서 가져와서 갱신하기
		Long count = notificationMemberCloneMapper.findCount(user.getId(), "N");

		return new ResponseEntity<>(count, HttpStatus.OK);
	}

	@PutMapping("/deleteAll")
	public void deleteAll() {
		User curUser = UserSingleton.getInstance();

		User user = userMapper.findOne(curUser.getId()); //실시간 알림 업데이트를 위한 user DB에서 가져와서 갱신하기
		List<NotificationMemberClone> not = notificationMemberCloneMapper.findByReceiverAndReadYnOrderByIdDesc(user, "N");
		for (NotificationMemberClone n: not) {
			n.setReadYn("Y");
			notificationMemberCloneMapper.save(n);
		}
	}

	@ApiOperation(value = "활동내역 리스트", httpMethod = "GET", notes = "활동내역 리스트")
	@GetMapping("/history")
	public ResponseEntity<List<NotificationMemberClone>> historyList() {
		User curUser = UserSingleton.getInstance();
		User user = userMapper.findOne(curUser.getId());
		Date oneMonthBefore = Date.from(LocalDateTime.now().minusMonths(1).atZone(ZoneId.systemDefault()).toInstant());
		Date now = Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
		List<NotificationMemberClone> res = new ArrayList<>();

		List<Notification> lists = notificationMapper.findByCreatedBetweenOrderByIdDesc(oneMonthBefore, now);
		for(Notification n: lists) {
			NotificationMemberClone temp = notificationMemberCloneMapper.findByNotificationAndReceiver(n, user);
			if (temp != null) {
				res.add(temp);
			}
		}

		return new ResponseEntity<>(res, HttpStatus.OK);
	}

	@ApiOperation(value = "알림 읽었을 경우", httpMethod = "POST", notes = "알림 읽었을 경우")
	@PostMapping("")
	public void readNotification(@RequestBody NotificationMemberClone n) {
		notificationMemberCloneMapper.save(n);
	}
}
