package com.snbplus.madang.notification;

import com.snbplus.madang.user.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationMemberCloneMapper extends JpaRepository<NotificationMemberClone, Integer> {
    List<NotificationMemberClone> findByReceiverAndReadYnOrderByIdDesc(User receiver, String readYn, Pageable pageable);
    List<NotificationMemberClone> findByReceiverAndReadYnOrderByIdDesc(User receiver, String readYn);
    List<NotificationMemberClone> findByReceiverOrderByIdDesc(User receiver);
    NotificationMemberClone findByNotificationAndReceiver(Notification notification, User receiver);
    @Query(value = "SELECT COUNT(*) FROM notification_member_clone WHERE receiver = :receiver AND read_yn = :readYn", nativeQuery = true)
    Long findCount(@Param("receiver") Integer receiver, @Param("readYn") String readYn);
}
