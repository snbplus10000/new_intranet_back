package com.snbplus.madang.busiTrip;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface BusiTripCarMapper extends JpaRepository<BusiTripCar, Integer>, JpaSpecificationExecutor<BusiTripCar> {
	Page<BusiTripCar> findByDelYn(Pageable pageable, String delYn);
	
	List<BusiTripCar> findDistanceByDelYnAndCarId(String delYn, Integer carId);
	
	List<BusiTripCar> findDistanceByDelYnAndCarIdAndCreatedLessThan(String delYn, Integer carId, Timestamp created);

	@Query(value = "SELECT max(after_distance) FROM tbl_busi_trip_car c where car_id=:carId and del_yn='N' GROUP BY car_id;", nativeQuery = true)
	Integer findTheLargestDistance(@Param("carId") Integer carId);
}
