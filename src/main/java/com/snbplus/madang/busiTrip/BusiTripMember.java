package com.snbplus.madang.busiTrip;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 출장/외근보고 - 미팅참석자
 * @author 신복호
 *
 */
@Entity
@Table(name="tbl_busi_trip_member")
@Data
public class BusiTripMember {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String etc;
	
	private Integer busiTripId;
	
	
	@ManyToOne
	@JoinColumn(name="user_id", nullable = false)
	private User user;
	
	

}
