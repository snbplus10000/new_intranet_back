package com.snbplus.madang.busiTrip;

import java.util.Arrays;
import java.util.Calendar;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.car.CarMapper;
import com.snbplus.madang.common.CommonCodeMapper;
import com.snbplus.madang.common.constant.CommentGubunCategory;
import com.snbplus.madang.common.constant.DrivingGubunCategory;
import com.snbplus.madang.common.constant.WorkGubunCategory;
import com.snbplus.madang.company.CompanyMapper;
import com.snbplus.madang.util.DateUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/busiTrip")
public class BusiTripController  {
	
	@Autowired
	BusiTripMapper busiTripMapper;
	
	@Autowired
	BusiTripCarMapper busiTripCarMapper;
	
	@Autowired
	CompanyMapper companyMapper;
	
	@Autowired
	CommonCodeMapper commonCodeMapper;
	
	@Autowired
	BusiTripMemberMapper busiTripMemberMapper;
	
	@Autowired
	CarMapper carMapper;
	
	@GetMapping("/busiTrip/list")
	public String busiTripList(Model model,@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
		
		Page<BusiTrip> busiTripList = busiTripMapper.findByDelYn(pageable,"N");
		
		model.addAttribute("busiTripList", busiTripList);
			
		return "busiTrip/busiTripList";
		
	}
	
	@GetMapping("/busiTrip/cal")
	public String busiTripCal(Model model) {
			
		return "busiTrip/busiTripCal";
		
	}
	
	@GetMapping("/busiTrip/write")
	public String busiTripWrite(Model model){
		
//		companyMapper.findAll().stream().filter(company -> company.getDelYn().equals("N")).forEach(item -> log.info("{}", item));
		
		model.addAttribute("companyList", companyMapper.findByDelYn("N"));
		model.addAttribute("carList", carMapper.findByDelYn("N"));
		
		return "busiTrip/busiTripWrite";
		
	}
	
	@GetMapping("/busiTrip/view")
	public String busiTripView(Model model) {
		
		model.addAttribute("companyList", companyMapper.findByDelYn("N"));
		model.addAttribute("yearList", DateUtil.yearList(Calendar.getInstance().get(Calendar.YEAR)-20 , 2019));
		model.addAttribute("workGubunList", Arrays.stream(WorkGubunCategory.class.getEnumConstants()).collect(Collectors.toList()));
		model.addAttribute("gubunList2", Arrays.stream(DrivingGubunCategory.class.getEnumConstants()).collect(Collectors.toList()));
		model.addAttribute("commentGubunList", Arrays.stream(CommentGubunCategory.class.getEnumConstants()).collect(Collectors.toList()));
		model.addAttribute("carList", carMapper.findByDelYn("N"));
		return "busiTrip/busiTripView";
	}


}
