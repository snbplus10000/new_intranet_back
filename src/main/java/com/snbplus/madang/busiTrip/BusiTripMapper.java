package com.snbplus.madang.busiTrip;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusiTripMapper extends JpaRepository<BusiTrip, Integer> {

	Page<BusiTrip> findByDelYn(Pageable pageable, String delYn);

	Optional<BusiTrip> findById(int id);

}
