package com.snbplus.madang.busiTrip;

import com.snbplus.madang.car.Car;
import com.snbplus.madang.user.User;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "tbl_busi_trip_car")
@Data
@EntityListeners(AuditingEntityListener.class)
public class BusiTripCar {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Transient
	private Integer busiTripId;
	
	private String driveDate;  // 운행일자
	
	private String dept; // 동승자 소속
	
	private String name; // 동승자 성명

	private String departure; // 출발지

	private String destination; //도착지
	
	private double beforeDistance; // 주행 전 계기판의 거리
	
	private double afterDistance; // 주행 후 계기판의 거리
	
	private double distance; // 주행거리(Km)
	
	private double glDistance; // 출퇴근용(Km)
	
	private double nomalDistance; // 일반 업무용(Km)
	
	private String etc; // 비고

	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;

	private Integer creater;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;

	private Integer modifier;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="carId")
	private Car car;
	
	@Column(nullable = false)
	@ColumnDefault("'N'")	
	private String delYn ="N";
	
	@Transient
	private Integer carNo;
	
	@Transient
	private User user; 


}
