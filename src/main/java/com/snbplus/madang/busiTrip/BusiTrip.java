package com.snbplus.madang.busiTrip;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.web.multipart.MultipartFile;

import com.snbplus.madang.company.Company;
import com.snbplus.madang.user.User;

import lombok.Data;

/**
 *  출장/외근보고 - 업무내용 + 방문결과
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_busi_trip")
@Data
public class BusiTrip {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String year; // 사업연도
	
	private String businessName; //사업번호
	
	private String carNo; //차번호
	
	private String tripArea; // 방문지역
	
	private String tripPlace; // 방문처
	
	private String workGubun; // 업무구분
	
	private String startTime; // 출발시간
	
	private String endTime; // 도착시간
	
	private String meetingStartTime; //미팅시작시간
	
	private String meetingEndTime; // 미팅종료시간
	
	private String content; // 업무내용
	
	private String tripResult; //방문결과
		
	private Integer creater;
	
	@CreatedDate
	@Column(nullable=false, updatable = false)
	@CreationTimestamp
	private Timestamp  created;
	
	private Integer modifier;
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private String meetingStartDate;
	
	private String meetingEndDate;
	
	private String tripPurpose;
	
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	@Transient
	private User user;
	
	@OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name= "busiTripId")
	private List<BusiTripCar> busiTripCar;
	
/*	@OneToMany(fetch = FetchType.LAZY, cascade=CascadeType.PERSIST)
	@JoinColumn(name= "busiTripId")
	private List<BusiTripMember> busiTripMember;*/

	private String busiTripId;

	@OneToOne
	@JoinColumn(name="busi_trip_file_id")
	private BusiTripFile busiTripFile;
	
	@Transient
	private MultipartFile busiTripFiles;

}
