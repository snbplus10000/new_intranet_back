package com.snbplus.madang.busiTrip;

import com.snbplus.madang.InterfaceSpec;
import com.snbplus.madang.car.CarMapper;
import com.snbplus.madang.user.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BusiTripCarSpec implements InterfaceSpec<BusiTripCar> {
	
	private final UserMapper userMapper;

	private final CarMapper carMapper;

	@Override
	public Specification<BusiTripCar> build(String delYn, String conditionKey, String conditionValue) {
        if(conditionKey == null || conditionKey.equals("")) {
            return search(delYn);
        }else if(conditionKey != null && conditionKey.equals("creater")) {
        	return search(delYn,conditionKey,userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()));
        }else if(conditionKey != null && conditionKey.equals("car")) {
			return search(delYn,conditionKey,carMapper.findByCarNoLike("%"+ conditionValue + "%").stream().map(car->car.getId()).collect(Collectors.toList()));
		}

        return search(delYn, conditionKey, "%" + conditionValue + "%");
	}

	@Override
	public Specification<BusiTripCar> build(String delYn) {
		
		return build(delYn,null,null);
	}

	@Override
	public Specification<BusiTripCar> search(String delYn, String conditionKey, String conditionValue) {
		
        return (root, query, cb) ->
        cb.and(
                cb.equal(root.get("delYn"),  delYn),
                cb.like(root.get(conditionKey), conditionValue)
        );
	}

	@Override
	public Specification<BusiTripCar> search(String delYn, String conditionKey, List conditionValue) {
		
        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : conditionValue) {
                in.value(conditionColumnValue);           	
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    in
            );
        };
	}

	@Override
	public Specification<BusiTripCar> search(String delYn) {
		
        return (root, query, cb) ->
        cb.and(
                cb.equal(root.get("delYn"),  delYn)
        );
	}



}
