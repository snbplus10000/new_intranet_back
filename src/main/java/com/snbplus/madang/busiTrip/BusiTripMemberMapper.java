package com.snbplus.madang.busiTrip;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusiTripMemberMapper extends JpaRepository<BusiTripMember, Integer> {

}
