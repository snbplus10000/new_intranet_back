package com.snbplus.madang.common;

import com.snbplus.madang.board.Board;
import com.snbplus.madang.project.Project;
import com.snbplus.madang.purchase.Purchase;
import com.snbplus.madang.purchase.PurchaseItem;
import com.snbplus.madang.purchase.PurchaseItemMapper;
import com.snbplus.madang.reject.Reject;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserSingleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailService {
	
	private final JavaMailSender mailSender;

	private final PurchaseItemMapper purchaseItemMapper;

	private final String mailFrame = "<!DOCTYPE html>\r\n"+
			"<html lang='en'>\r\n"+

			"<head>\r\n"+
			"<meta charset='UTF-8'>\r\n"+
			"<meta name='viewport' content='width=device-width, initial-scale=1.0'>\r\n"+
			"<title>Document</title>\r\n"+
			"</head>\r\n"+
			"<body>\r\n"+
			"<div class='dropdown-menu show'>\r\n"+
			"<div style='max-width:500px; margin:0 auto; border-radius:10px; overflow: hidden; border:1px solid #eee'>\r\n"+
			"<div><img src='http://madang.snbplus.co.kr/static/img/logo.png' style='width:20%%; text-align:center'></div> \r\n"+
			"<div style='border: 0.5px solid black'> </div>\r\n" +
			"<div class='noti-a noti-wrap' style='display: flex;box-sizing:border-box;padding:15px 20px;border-top:1px solid #eeeeee;align-items: center; flex-flow:row-reverse nowrap; background-color: #f8f8f8;'>\r\n"+
			"<div class='noti-profile' style='display: flex;flex-flow:row-reverse nowrap;align-items: center; width:100%%;justify-content: end;'>  \r\n"+
			"<div class='photo' style='width: 80px;height: 80px;overflow: hidden;border-radius: 50%%;z-index: 5;border: 3px solid #dddddd;;margin-left: 20px;'><img src='%s' style='width:100%%;'></div>\r\n"+
			"<div>\r\n"+
			"<a href='http://madang.snbplus.co.kr/#/%s' target='_blank' style='color:#222222; text-decoration:none;'>\r\n"+
			"<div><strong class='noti-name' sylte='font-size:16px;'>%s</strong>\r\n"+
			"<div class='noti-con'  style='font-size:14px;'>%s</div>\r\n"+
			"</div>\r\n"+
			"<p style='font-size:12px;'>클릭시 해당 게시글로 이동합니다.</p>"+
			"</a>\r\n"+
			"</div>\r\n"+
			"</div>\r\n"+
			"</div>\r\n"+
			"</div>\r\n"+
			"</div>\r\n"+
			"</div>\r\n"+
			"</body>\r\n"+
			"</html>";

	public void allUserMailSend(String cate, List<User> userList, Board board) throws IOException, AddressException {

		InternetAddress[] toAddr = new InternetAddress[userList.size()];
		  
		for(Integer i = 0; i<userList.size(); i++)
			toAddr[i] = new	InternetAddress(userList.get(i).getLoginId()+"@snbplus.co.kr");
		 

		String title = board.getTitle();
		String contents = "<!DOCTYPE html>\r\n" +
				"<html lang=\"ko\">\r\n" +
				"\r\n" +
				board.getContents()+ "\r\n" +
				"</html>\r\n" +
				"";

		mailSend("master@snbplus.co.kr", toAddr, title, contents, true);
	}
	
	public void  rejectApprovalMailSend(Reject t) {

		String email = t.getCreateUser().getLoginId()+"@snbplus.co.kr";
		email += ",master@snbplus.co.kr";
		String imgurl = "";
		
		if(t.getRejectUser().getThumbFile() == null || t.getRejectUser().getThumbFile().getFileAttach().getPath().equals("") ) {
			imgurl = "http://madang.snbplus.co.kr/static/img/default-avatar.png";
		}else {
			imgurl = "http://madang.snbplus.co.kr/intranet/api/file/image/"+t.getRejectUser().getThumbFile().getFileAttach().getId();
		}
	
		String title = "[S&B PLUS](마당) "+t.getCreateDate()+" 에 신청하셨던 "+t.getKind() + " 이 반려되었습니다.";

		String address = t.getUrl().substring(1);
		String contents = String.format(mailFrame, imgurl, address,
				t.getRejectUser().getName()+"님께서", "<strong>"+t.getCreateUser().getName()+"님 의 <strong>["+t.getKind()+"]["+t.getCreateDate()+"]</strong>을 반려하였습니다.");
		mailSend(email, null, title, contents, true);
	}

	public void  purchaseApprovalMailSend(Purchase t) throws IOException, AddressException {

		PurchaseItem purchaseItem = purchaseItemMapper.findFirstByRefIdAndDelYn(t.getId(), "N");

		Date date = new Date();
		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-M-dd");

		String email = "hello@snbplus.co.kr";
		if(t.getPurchaseUser() != null && !t.getPurchaseUser().getLoginId().equals("hello"))
			email += "," + t.getPurchaseUser().getLoginId()+"@snbplus.co.kr";

		String imgurl = "";
		if(t.getUser().getThumbFile() == null || t.getUser().getThumbFile().getFileAttach()==null ) {
			imgurl = "http://madang.snbplus.co.kr/static/img/default-avatar.png";
		}else {
			imgurl = "http://madang.snbplus.co.kr/intranet/api/file/image/"+t.getUser().getThumbFile().getFileAttach().getId();
		}

		String title = "[S&B PLUS] "+t.getUser().getName()+"님께서 구매신청 하셨습니다.";
		String contents = String.format(mailFrame, imgurl, "apply/purchase/read/" + t.getId(),
				t.getUser().getName()+"님 께서 구매신청하신", "<strong>["+purchaseItem.getItemName()+"] 물품이 ["+sdFormat.format(date)+"]</strong>에 결재완료 되었습니다.");
		mailSend(email, null, title, contents, true);
	}

	public void commentMailSend(Project p, Comment comment) {
		String imgurl = "";
		User user = UserSingleton.getInstance();
		if(user.getThumbFile() == null || user.getThumbFile().getFileAttach().getPath().equals("") ) {
			imgurl = "http://madang.snbplus.co.kr/static/img/default-avatar.png";
		}else {
			imgurl = "http://madang.snbplus.co.kr/intranet/api/file/image/"+user.getThumbFile().getFileAttach().getId();
		}
		String title = "[S&B PLUS]"+user.getName()+"님께서 " +p.getCompany().getCompanyName() + "-" + p.getBusinessName() + "  프로젝트에 댓글을 등록하셨습니다.";
		String content = String.format(mailFrame, imgurl, "project/read/" + p.getId(),
				title, comment.getComment());

		List<String> email = p.getManagers()
				.stream().map(m -> m.getUser().getLoginId() + "@snbplus.co.kr")
				.collect(Collectors.toList());
		email.add(p.getCreater().getLoginId() + "@snbplus.co.kr");
		email.add("master@snbplus.co.kr");
		email.add("jinyk0811@snbplus.co.kr");
		String to = String.join(",", email);

		mailSend(to, null, title, content, true);
	}

	private void mailSend(String to, InternetAddress[] cc, String title, String contents, boolean isHTML) {
		MimeMessage message = mailSender.createMimeMessage();
		try {
			InternetAddress[] emailList = InternetAddress.parse(to);
			MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
			messageHelper.setFrom("snbplus10000@gmail.com");  // 보내는사람 생략하거나 하면 정상작동을 안함
			messageHelper.setTo(emailList); // 받는사람 이메일
			if(cc!=null) messageHelper.setCc(cc);
			messageHelper.setSubject(title); // 메일제목은 생략이 가능하다
			messageHelper.setText(contents, isHTML);  // 메일 내용
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		mailSender.send(message);
	}
}
