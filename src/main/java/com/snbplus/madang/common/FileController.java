package com.snbplus.madang.common;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.tools.ant.taskdefs.Length.FileMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/file")
@Slf4j
public class FileController {

    @Autowired
    FileUploadService fileUploadService;

    @Autowired
    FileAttachMapper mapper;
    
    
    @PostMapping("/upload/{path}")
    public List<FileAttach> fileUpload(MultipartRequest request, @PathVariable("path") String path) {

        List<FileAttach> fileAttachList = new ArrayList<>();
        if(path != null && path.equals("thumb")) {
        	 request.getFileNames().forEachRemaining(name -> fileAttachList.add(fileUploadService.uploadFile(request.getFile(name), "/file/image" + "/" + path + "/", path)));
        }else {
        	 request.getFileNames().forEachRemaining(name -> fileAttachList.add(fileUploadService.uploadFile(request.getFile(name), "/file/download" + "/" + path + "/", path)));
        }
       
       
        return fileAttachList;

    }
    
    @GetMapping("/download/{path}/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, @PathVariable String path, HttpServletRequest request){
        // Load file as Resource
        Resource resource = fileUploadService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    @GetMapping("/download/{no}")
    public ResponseEntity<Resource> downloadFile(@PathVariable Integer no, HttpServletRequest request) throws UnsupportedEncodingException {

        FileAttach fileAttach = mapper.findOne(no);

        // Load file as Resource
        Resource resource = fileUploadService.loadFileAsResource(fileAttach.getRefType(), fileAttach.getFileName() );

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + URLEncoder.encode(fileAttach.getOrgFileName(),"UTF-8") + "\"")
                .body(resource);
    }


    @GetMapping(value = "/image/{no}")
    public ResponseEntity<Resource> image (@PathVariable Integer no, HttpServletRequest request)  throws UnsupportedEncodingException {
    	
        FileAttach fileAttach = mapper.findOne(no);

        // Load file as Resource
        Resource resource = fileUploadService.loadFileAsResource(fileAttach.getRefType(), fileAttach.getFileName() );

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + URLEncoder.encode(fileAttach.getOrgFileName(),"UTF-8") + "\"")
                .body(resource);
    	
    }
 /*   
    @PostMapping("/readExcel/{path}")
    public void readExcel(MultipartRequest request, @PathVariable("path") String path) throws IOException, InvalidFormatException {
    	
    	//Workbook workbook = excelService.readExcel(request);

    }
*/
  
}
