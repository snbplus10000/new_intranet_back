package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 업무 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("workGubun")
public enum WorkGubunCategory {
	
	COMPANY("업체방문"),
	OFFICE("관공서방문"),
	OUTSIDE("외근");

	public static final Map<String, WorkGubunCategory> stringToEnum = new ConcurrentHashMap<String, WorkGubunCategory>();
	static {
		for (WorkGubunCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}


	private String value;
	
	WorkGubunCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
	

}
