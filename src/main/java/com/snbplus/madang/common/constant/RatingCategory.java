package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 알리바바 등급 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("rating")
public enum RatingCategory {
	
	GOLD("골드"),
	SILVER("실버"),
	NOT("해당없음");

	public static final Map<String, RatingCategory> stringToEnum = new ConcurrentHashMap<String, RatingCategory>();
	static {
		for (RatingCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;
	
	RatingCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
	
}
