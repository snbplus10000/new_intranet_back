package com.snbplus.madang.common.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.snbplus.madang.common.EnumRequestMapping;

/**
 * 일반 작업 진척도 상태
 * @author bhshin
 *
 */
@EnumRequestMapping("normalKind")
public enum NormalKindCategory {

	PLAN("기획"),
	DRAFT("시안"),
	DEVELOP("개발"),
	MODIFY("수정"),
    TRANSLATE("번역"),
	LANGUAGE_CHANGE("언변"),
	COMPLETE("완료");
	
	
    public static final Map<String, NormalKindCategory> stringToEnum = new ConcurrentHashMap<String, NormalKindCategory>();
    static {
        for (NormalKindCategory set: values()){
            stringToEnum.put(set.value, set);
        }
    }

    private String value;

    NormalKindCategory(String value) {
        this.value = value;
    }

    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }

}
