package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 비지니스 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("business")
public enum BusinessCategory {

    HOMEPAGE("홈페이지"),
    BRAND("브랜드");

    public static final Map<String, BusinessCategory> stringToEnum = new ConcurrentHashMap<String, BusinessCategory>();
    static {
        for (BusinessCategory set: values()){
            stringToEnum.put(set.value, set);
        }
    }

    private String value;

    BusinessCategory(String value) {
        this.value = value;
    }

    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }

}
