package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 직책 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("position")
public enum PositionCategory {
	
	S("최고권한자"),
	A("부서장"),
	B("영업팀"),
	C("팀장"),
	D("팀원"),
	E("외주"),
	F("ETC");

	public static final Map<String, PositionCategory> stringToEnum = new ConcurrentHashMap<String, PositionCategory>();
	static {
		for (PositionCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;
	
	PositionCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}

}
