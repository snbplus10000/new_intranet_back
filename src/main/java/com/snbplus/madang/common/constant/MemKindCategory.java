package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 직원 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("menState")
public enum MemKindCategory {

	
	NORMAL("일반"),
	SOLDIER("병특"),
	ETC("기타");

	public static final Map<String, MemKindCategory> stringToEnum = new ConcurrentHashMap<String, MemKindCategory>();
	static {
		for (MemKindCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;
	
	MemKindCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}

}
