package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 상테 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("state")
public enum StateCategory {
	
	SELECT("선정"),
	LEAVE("탈락"),
	NO("해당없음");

	public static final Map<String, StateCategory> stringToEnum = new ConcurrentHashMap<String, StateCategory>();
	static {
		for (StateCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;
	
	StateCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
	
}
