package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 코멘트 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("comment")
public enum CommentGubunCategory {
	
	TEL("전화"),
	EMIAL("이메일"),
	CONSALT("방문상담");

	public static final Map<String, CommentGubunCategory> stringToEnum = new ConcurrentHashMap<String, CommentGubunCategory>();
	static {
		for (CommentGubunCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;
	
	CommentGubunCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
	

}
