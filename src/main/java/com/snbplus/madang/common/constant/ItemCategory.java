package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 품목 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("Item")
public enum ItemCategory {

	LEVY("부담금"),
	BURDEM_TAX("부담세"),
	BALANCE("잔금"),
	ETC("기타");

	public static final Map<String, ItemCategory> stringToEnum = new ConcurrentHashMap<String, ItemCategory>();
	static {
		for (ItemCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;

	ItemCategory(String value){
		this.value = value;
	}
	
    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }
	
	
	
}
