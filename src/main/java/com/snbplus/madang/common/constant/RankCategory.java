package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 직위 카테고리 
 * @author bhshin
 *
 */
@EnumRequestMapping("rank")
public enum RankCategory {
		
	PRESIDENT("사장"),
	HEAD_MANAGER("본부장"),
	DIRECTOR("이사"),
	GENERAL_MANAGER("실장"),
	DEPUTY_GENERAL_MANAGER("차장"),
	MANAGER("과장"),
	ASSISTANCT_MANAGER("대리"),
	SENIOR_STAFF("주임"),
	STAFF("직원");

	public static final Map<String, RankCategory> stringToEnum = new ConcurrentHashMap<String, RankCategory>();
	static {
		for (RankCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;
	
	RankCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
	
	
}
