package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 운전 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("drive")
public enum DrivingGubunCategory {
	
	INPUT("입고"),
	OUTPUT("출고"),
	DRIVING("주행");

	public static final Map<String, DrivingGubunCategory> stringToEnum = new ConcurrentHashMap<String, DrivingGubunCategory>();
	static {
		for (DrivingGubunCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;
	
	DrivingGubunCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
		
}
