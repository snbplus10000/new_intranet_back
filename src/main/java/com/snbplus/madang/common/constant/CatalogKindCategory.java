package com.snbplus.madang.common.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.snbplus.madang.common.EnumRequestMapping;

@EnumRequestMapping("catalogKind")
public enum CatalogKindCategory {

	PLAN("기획"),
	DRAFT("시안"),
	DEVELOP("개발"),
	MODIFY("수정"),
	TRANSLATE("번역"),
	PRINT("인쇄"),
	COMPLETE("완료");
	
	
    public static final Map<String, CatalogKindCategory> stringToEnum = new ConcurrentHashMap<String, CatalogKindCategory>();
    static {
        for (CatalogKindCategory set: values()){
            stringToEnum.put(set.value, set);
        }
    }

    private String value;

    CatalogKindCategory(String value) {
        this.value = value;
    }

    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }

}
