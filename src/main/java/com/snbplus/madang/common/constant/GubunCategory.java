package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("gubun")
public enum GubunCategory {
	
	SUR_BUSINESS("지원사업"),
	CERTIFICATE("인증"),
	ETC("기타");

	public static final Map<String, GubunCategory> stringToEnum = new ConcurrentHashMap<String, GubunCategory>();
	static {
		for (GubunCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;

	GubunCategory(String value){
		this.value = value;
	}
	
    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }
}
