package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 번역 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("language")
public enum LanguageTypeCategory {

    KR("한국어"),
    EN("영어"),
    CN("중국어 간체"),
    TW("중국어 번체"),
    JP("일본어"),
    VI("베트남어"),
    ES("스페인어"),
    PT("포르투갈어"),
    FR("프랑스어");

    public static final Map<String, LanguageTypeCategory> stringToEnum = new ConcurrentHashMap<String, LanguageTypeCategory>();
    static {
        for (LanguageTypeCategory set: values()){
            stringToEnum.put(set.value, set);
        }
    }

    private String value;

    LanguageTypeCategory(String value) {
        this.value = value;
    }

    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }

}
