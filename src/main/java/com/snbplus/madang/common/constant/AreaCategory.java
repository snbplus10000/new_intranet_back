package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 지역 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("area")
public enum AreaCategory {


	SEOUL("서울"),
	INCHEON("인천"),
	DEAJEON("대전"),
	DAEGU("대구"),
	GWANGJU("광주"),
	ULSAN("울산"),
	BUSAN("부산"),
	SEJONG("세종"),
	GYEONGGI("경기"),
	GANGWON("강원"),
	CHUNGBUK("충북"),
	CHUNGNAM("충남"),
	JEONBUK("전북"),
	JEONNAM("전남"),
	GYEONGBUK("경북"),
	GYEONGNAM("경남"),
	JEJU("제주");


	public static final Map<String, AreaCategory> stringToEnum = new ConcurrentHashMap<String, AreaCategory>();
	static {
		for (AreaCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;

	AreaCategory(String value){
		this.value = value;
	}
	
    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }

}
