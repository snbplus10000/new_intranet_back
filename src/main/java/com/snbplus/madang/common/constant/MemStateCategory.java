package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 직원 구분 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("menState")
public enum MemStateCategory {
	
	FULLTIME("정규직"),
	CONTRACT("계약직"),
	INTERNSHIP("인턴직"),
	OUTSOURCING("외주"),
	ETC("기타");

	public static final Map<String, MemStateCategory> stringToEnum = new ConcurrentHashMap<String, MemStateCategory>();
	static {
		for (MemStateCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;
	
	MemStateCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}

}
