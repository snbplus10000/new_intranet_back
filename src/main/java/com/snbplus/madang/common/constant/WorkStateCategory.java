package com.snbplus.madang.common.constant;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 재직 상태 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("workState")
public enum WorkStateCategory {
	
	WORKING("재직중"),
	RETIRE("퇴직"),
	DISPATCH("파견"),
	LEAVE("휴직"),
	ETC("기타");


	public static final Map<String, WorkStateCategory> stringToEnum = new ConcurrentHashMap<String, WorkStateCategory>();
	static {
		for (WorkStateCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}
	
	private String value;
	
	WorkStateCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
}
