package com.snbplus.madang.common.constant;

public enum Role {

    Admin("관리자권한"),
    AlternativeMilitaryServiceManager("병역특례관리자"),
    DEFAULT("일반권한");

    private String value;

    Role(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return name();
    }

}
