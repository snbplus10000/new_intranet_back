package com.snbplus.madang.common.constant;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.snbplus.madang.common.EnumRequestMapping;

@EnumRequestMapping("workProcess")
public enum WorkProcessCategory {

	KR("한국어"),
    EN("영어"),
    CN("중국어 간체"),
    TW("중국어 번체"),
    JP("일본어"),
    VI("베트남어");

    public static final Map<String, WorkProcessCategory> stringToEnum = new ConcurrentHashMap<String, WorkProcessCategory>();
    static {
        for (WorkProcessCategory set: values()){
            stringToEnum.put(set.value, set);
        }
    }

    private String value;

    WorkProcessCategory(String value) {
        this.value = value;
    }

    public String getKey() {
        return name();
    }

    public String getValue() {
        return value;
    }
}
