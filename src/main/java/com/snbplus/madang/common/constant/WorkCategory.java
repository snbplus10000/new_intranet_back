package com.snbplus.madang.common.constant;

import org.springframework.web.bind.annotation.RequestMapping;

import com.snbplus.madang.common.EnumRequestMapping;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 제작형태 카테고리
 * @author bhshin
 *
 */
@EnumRequestMapping("work")
public enum WorkCategory {

	WORDPRESS("워드프레스"),
	SHOPPING("쇼핑몰"),
	NORMAL("일반");

	public static final Map<String, WorkCategory> stringToEnum = new ConcurrentHashMap<String, WorkCategory>();
	static {
		for (WorkCategory set: values()){
			stringToEnum.put(set.value, set);
		}
	}

	private String value;
	
	WorkCategory(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getKey() {
		 return name();
	}
}
