package com.snbplus.madang.common;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.view.RedirectView;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RedirectExceptionHandler {

    @ExceptionHandler(RedirectException.class)
    public RedirectView handleException(RedirectException ex) {
        return new RedirectView(ex.redirectUrl);
    }

}
