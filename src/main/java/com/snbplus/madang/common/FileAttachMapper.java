package com.snbplus.madang.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileAttachMapper extends JpaRepository<FileAttach, Integer> {

    public Optional<FileAttach> findById(Integer id);

}
