package com.snbplus.madang.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReplyMapper extends JpaRepository<Reply, Integer> {

}
