package com.snbplus.madang.common;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeptMapper extends JpaRepository<Dept, Integer> {

    Optional<Dept> findByOrgNameOrderByCode(String name);

    List<Dept> findByDelYnOrderByCodeAsc(String delYn);

    Dept findByCode (Integer key);

}
