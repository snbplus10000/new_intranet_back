package com.snbplus.madang.common;

public class RedirectException extends Exception {

    String redirectUrl;

    public RedirectException(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

}
