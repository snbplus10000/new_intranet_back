package com.snbplus.madang.common;

public class OptionalAndDefault<O,T> {

    private O target;
    private T defaultValue;

    private OptionalAndDefault(O target, T defaultValue) {
        this.target = target;
        this.defaultValue = defaultValue;
    }

    public static <O,T> OptionalAndDefault<O, T> ValueOf(O o, T t){
        return new OptionalAndDefault(o, t);
    }

    public O getValue() {
        return target;
    }

    public O elseThenThrow(Exception exception) throws Exception{
        if(target == null || target.equals(defaultValue)) {
            throw exception;
        }

        return target;
    }
}
