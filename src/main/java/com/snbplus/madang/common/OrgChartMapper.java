package com.snbplus.madang.common;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrgChartMapper extends JpaRepository<OrgChart, Integer> {
	
	Optional<OrgChart> findByOrgNameOrderByCode(String name);

	List<OrgChart> findByLvlOrderByCode(int no);

	List<OrgChart> findByLvlInOrderByCode(int no, int no2);

	List<OrgChart> findByLvlInOrderByCode(List<Integer> list);

	List<OrgChart> findByDelYnOrderByCodeAsc(String delYn);


}
