package com.snbplus.madang.common.filter;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ExceptionHadlingController {
	
	@ResponseBody
	@ExceptionHandler(Exception.class)
	public void handlError(Model model,HttpServletRequest request, Exception exception) throws Exception {
		
		throw new Exception("FALED");
			
	}
}
