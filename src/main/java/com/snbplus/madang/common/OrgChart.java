package com.snbplus.madang.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.user.User;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name="tbl_org_chart")
@Data
public class OrgChart {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer code; // 코드

	private String orgName; // 조직 이름 
	
	private Integer rootCode; // 최상위 코드
	
	private Integer parentCode; // 부모코드
	
	private Integer lvl; // 단계
	
	private Integer sorting; // sorting
	
	@CreatedDate
	@Column(nullable=false , updatable = false)
	@CreationTimestamp
	private Timestamp  created;
	
	@LastModifiedDate
	@Column(updatable= true)
	@UpdateTimestamp
	private Timestamp modified;

	/*@OneToMany(fetch =  FetchType.LAZY, mappedBy = "deptChart")
    @Where(clause = "retire_date is null")
    @OrderBy("order_no ASC")
	@JsonIgnore
	@ToString.Exclude
    private List<User> deptUser;

    @OneToMany(fetch =  FetchType.LAZY, mappedBy = "highDeptChart")
    @Where(clause = "retire_date is null")
    @OrderBy("order_no ASC")
	@JsonIgnore
	@ToString.Exclude
    private List<User> highDeptUser;*/

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
}
