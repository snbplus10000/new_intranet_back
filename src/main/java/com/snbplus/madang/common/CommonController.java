package com.snbplus.madang.common;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.snbplus.madang.car.Car;
import com.snbplus.madang.car.CarMapper;
import com.snbplus.madang.common.constant.AreaCategory;
import com.snbplus.madang.common.constant.MemKindCategory;
import com.snbplus.madang.company.CompanyMapper;
import com.snbplus.madang.util.DateUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CommonController {

	private static final String ENUM_PACKAGE = "com.snbplus.madang.common.constant";
	private Map<String, Object[]> ENUM_CLASS_MAP;
	public Map<String, Class> ENUM_CLASS = new HashMap();
	@Autowired
	OrgChartMapper orgChartMapper;

	@Autowired
	CarMapper carMapper;

	@Autowired
	CompanyMapper companyMapper;

	@Autowired
	ZipMapper zipMapper;

	public static void main(String[] args) throws Exception {

	}

	public synchronized Map<String, Object[]> getEnumInstance() throws Exception {

		if (ENUM_CLASS_MAP == null || ENUM_CLASS_MAP.isEmpty()) {

			log.info("Enum Code Info Map Init");

			ENUM_CLASS_MAP = new HashMap();

			ClassLoader loader = CommonController.class.getClassLoader();
			ClassPath p = ClassPath.from(loader);
			System.out.println(ENUM_PACKAGE);
			ImmutableSet<ClassPath.ClassInfo> list = p.getTopLevelClassesRecursive(ENUM_PACKAGE);

			list.stream().forEach(info -> {

				try {
					Class enumClass = Class.forName(info.getName());



					if (enumClass.isEnum()) {
						if (enumClass.getAnnotation(EnumRequestMapping.class) != null) {
							ENUM_CLASS_MAP.put(
									((EnumRequestMapping) enumClass.getAnnotation(EnumRequestMapping.class)).value(),
									enumClass.getEnumConstants());
							ENUM_CLASS.put(((EnumRequestMapping) enumClass.getAnnotation(EnumRequestMapping.class)).value(), info.load());
						}
						;
					}

				} catch (ClassNotFoundException e) {
					log.error(e.getMessage());
				}

			});

		}

		return ENUM_CLASS_MAP;
	}

	@ApiOperation(value = "enum 관련 데이터", httpMethod = "GET", notes = "enum 관련 데이터")
	@GetMapping("/code/enum/{code}")
	public ResponseEntity<List> workGubunList(@PathVariable String code) throws Exception {

		return new ResponseEntity<>(Arrays.stream(getEnumInstance().get(code)).collect(Collectors.toList()),
				HttpStatus.OK);
	}

	@ApiOperation(value = "enum 결과 데이터", httpMethod = "GET", notes = "enum 결과 데이터")
	@GetMapping("/code/enum/{code}/values")
	public ResponseEntity<List> getEnumValues(@PathVariable String code) throws Exception {

		Class enumClas = ENUM_CLASS.get(code);
		Field stringToEnumField = enumClas.getField("stringToEnum");
		return new ResponseEntity<>(new ArrayList<>(((Map)stringToEnumField.get(enumClas)).keySet()),
				HttpStatus.OK);
	}


	@ApiOperation(value = "연도 list", httpMethod = "GET", notes = "현재 연도부터 20년 정도 데이터 호출")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/code/yearList")
	public ResponseEntity<List> yearList() {

		int nYear;
		Calendar calendar = new GregorianCalendar(Locale.KOREA);
		nYear = calendar.get(Calendar.YEAR);

		return new ResponseEntity<>(DateUtil.yearList(Calendar.getInstance().get(Calendar.YEAR) - 20, nYear),
				HttpStatus.OK);
	}

	@ApiOperation(value = "상위부서 list", httpMethod = "GET", notes = "상위부서 list  (code : value / orgName :name)")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/code/highDeptList")
	public ResponseEntity<List<OrgChart>> highDeptList() {

		List<Integer> list = new ArrayList<Integer>();
		list.add(0);
		list.add(1);

		return new ResponseEntity<>(orgChartMapper.findByLvlInOrderByCode(list), HttpStatus.OK);
	}

	@ApiOperation(value = "하위부서 list", httpMethod = "GET", notes = "하위부서 list (code : value / orgName :name)")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/code/deptList")
	public ResponseEntity<List<OrgChart>> deptList() {

		return new ResponseEntity<>(orgChartMapper.findByLvlOrderByCode(2), HttpStatus.OK);
	}

	@ApiOperation(value = "차 list", httpMethod = "GET", notes = "차 list (id : value / carNo : name)")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/code/carList")
	public ResponseEntity<List<Car>> carList() {

		return new ResponseEntity<>(carMapper.findByDelYn("N"), HttpStatus.OK);
	}

	/*
	 * @ApiOperation(value = "시도 list", httpMethod = "GET", notes = "시도 list")
	 * 
	 * @ApiResponses(value = {@ApiResponse(code = 400, message =
	 * "Server is Dead"), @ApiResponse(code = 200, message = "Run")})
	 * 
	 * @GetMapping("/code/sidoList") public ResponseEntity<List> sidoList(){
	 * 
	 * List<Zip> list = zipMapper.findDistinctSido();
	 * 
	 * list.forEach(item -> log.info("{}", list));
	 * 
	 * return new ResponseEntity<>(zipMapper.findDistinctSido(),HttpStatus.OK); }
	 */


    @ApiOperation(value = "지역 카테고리 value", httpMethod = "GET", notes = "enum 관련 데이터")
    @GetMapping("/code/enum/area/value")
    public ResponseEntity<List> areaEnumValues() throws Exception{
        return new ResponseEntity<List>(Arrays.stream(AreaCategory.values()).map(i -> i.getValue()).collect(Collectors.toList()), HttpStatus.OK);
    }
    
    @ApiOperation(value = "병특 카테고리 value", httpMethod = "GET", notes = "enum 관련 데이터")
    @GetMapping("/code/enum/memKind/value")
    public ResponseEntity<List> memKindEnumValues() throws Exception{
        return new ResponseEntity<List>(Arrays.stream(MemKindCategory.values()).map(i -> i.getValue()).collect(Collectors.toList()), HttpStatus.OK);
    }
    
    @ApiOperation(value = "카다로그 작업 진척도 카테고리 value", httpMethod = "GET", notes = "enum 관련 데이터")
    @GetMapping("/code/enum/catalogKind/value")
    public ResponseEntity<List> CatalogEnumValues() throws Exception{
        return new ResponseEntity<List>(Arrays.stream(AreaCategory.values()).map(i -> i.getValue()).collect(Collectors.toList()), HttpStatus.OK);
    }
    
    @ApiOperation(value = "일반 작업 진척도 카테고리 value", httpMethod = "GET", notes = "enum 관련 데이터")
    @GetMapping("/code/enum/normalKind/value")
    public ResponseEntity<List> NormalEnumValues() throws Exception{
        return new ResponseEntity<List>(Arrays.stream(MemKindCategory.values()).map(i -> i.getValue()).collect(Collectors.toList()), HttpStatus.OK);
    }


 
}
