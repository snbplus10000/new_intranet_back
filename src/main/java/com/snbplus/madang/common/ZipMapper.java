package com.snbplus.madang.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipMapper extends JpaRepository<Zip, Integer> {
	
	//List<Zip> findDistinctSido(); 
	
	//List<String> findAllDistinctGugunBySido(String sido);

}
