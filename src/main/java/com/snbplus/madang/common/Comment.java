package com.snbplus.madang.common;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 공통 코멘트
 * @author 신복호
 *
 */
@Entity
@Table(name = "tbl_comment")
@Data
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String fromPeople; // from
	
	private String toPeople; // to
	
	private String visibility; // 공개여부
	
	private String gubun; // 구분
	
	private String kinds; // 종류
	
	private String comment; // 코멘트
	
	@CreatedDate
	@Column(nullable=false ,updatable = false)
	@CreationTimestamp
	private Timestamp  created;
	 
	private Integer creater;
	
	@LastModifiedDate
	@Column(updatable=true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private Integer modifier;
	
	@ColumnDefault("'N'")
	private String delYn ="N";
	
	private String alert; // 알람
	
	@ManyToOne
	@JoinColumn(name="user")
	private User user;

	/* 200928 위치 컬럼 추가*/
	private String location;


}
