package com.snbplus.madang.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommonCodeMapper extends JpaRepository<CommonCode, Integer> {
	
	List<CommonCode> findByKindsCodeOrderByOrderNo(String name);

}
