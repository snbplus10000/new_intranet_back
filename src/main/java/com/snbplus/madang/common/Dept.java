package com.snbplus.madang.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.user.User;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name="tbl_org")
@Data
public class Dept {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer code; // 코드

    private String orgName; // 조직 이름

    private Integer deptUpper; //상위 조직

    private Integer manager;

    @CreatedDate
    @Column(nullable=false , updatable = false)
    @CreationTimestamp
    private Timestamp  created;

    @LastModifiedDate
    @Column(updatable= true)
    @UpdateTimestamp
    private Timestamp modified;

    @Column(nullable = false)
    @ColumnDefault("'N'")
    private String delYn ="N"; // 삭제여부

    @Transient
    private User user;
}
