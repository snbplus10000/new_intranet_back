package com.snbplus.madang.common;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name="tbl_common_code")
@Data
public class CommonCode {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String kinds; // 종류
		
	private String kindsCode; // 종류코드
	
	private String name; // 이름
	
	private String nameCode; // 이름 코드
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; //삭제 여부
	
	private Integer orderNo; // 순서
	
	@CreatedDate
	@Column(nullable = false, updatable =  false)
	@CreationTimestamp
	private Timestamp  created;
	
	private Integer creater; // 입력자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;
	
	private Integer modifier; // 수정자
	
	@Transient
	private User user; 


}
