package com.snbplus.madang.common;

import com.snbplus.madang.config.FileStorageProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.transaction.Transactional;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
@Slf4j
public class FileUploadService {

	private final Path fileStorageLocation;
	
	private final FileAttachMapper fileAttachMapper;

	String uploadDir;

	@Autowired
	public FileUploadService(FileStorageProperties fileStorageProperties, FileAttachMapper fileAttachMapper) {

		uploadDir = fileStorageProperties.getUploadDir();
		this.fileAttachMapper = fileAttachMapper;

		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
				.toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new RuntimeException("Could not create the directory where the uploaded files will be stored.", ex);
		}
	}

	@Transactional
	public  FileAttach uploadFile(MultipartFile file, String url, String path) {
		FileAttach fileAttach = new FileAttach();
		UUID uuid = UUID.randomUUID();

		String resolveFileName = StringUtils.cleanPath(uuid.toString() + file.getOriginalFilename());
		String filePath = ServletUriComponentsBuilder.fromCurrentContextPath().path(url).path(resolveFileName).toUriString();
		String fileName = file.getOriginalFilename();
		try {
			Path targetLocation = this.fileStorageLocation.resolve(resolveFileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			//String fileName = StringUtil.getUUIDByTimeBased().replace("-", "");
			String orgFileName = file.getOriginalFilename();
			int pos = orgFileName.lastIndexOf(".");
			String ext = orgFileName.substring(pos+1);

			fileAttach.setFileName(resolveFileName);
			fileAttach.setOrgFileName(fileName);
			fileAttach.setSize((int)(long)file.getSize());
			fileAttach.setExt(ext);
			fileAttach.setPath(filePath);

			log.info("File upload - {}" , fileAttach);

			fileAttachMapper.save(fileAttach);

		}catch (Exception e) {
			e.printStackTrace();
			fileAttach = null;
		}

		return fileAttach;
	}
	
	public Resource loadFileAsResource( String fileName) {
		try { 
					
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();

			Resource resource = new UrlResource(filePath.toUri());
			if(resource.exists()) {
				return resource;
			} else {
				throw new RuntimeException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new RuntimeException("File not found " + fileName, ex);
		}
	}

	public Resource loadFileAsResource(String refType, String fileName) {
		try { 

			if(refType != null && !refType.equals("")) {
				fileName = refType + "/"+ fileName;
			}
			
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();

			Resource resource = new UrlResource(filePath.toUri());
			if(resource.exists()) {
				return resource;
			} else {
				throw new RuntimeException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new RuntimeException("File not found " + fileName, ex);
		}
	}
}
