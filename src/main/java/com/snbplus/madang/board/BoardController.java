package com.snbplus.madang.board;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.mail.internet.AddressException;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.google.common.base.Optional;
import com.snbplus.madang.common.MailService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/board")
public class BoardController {

	@Autowired
	BoardMapper boardMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	BoardFileMapper boardFileMapper;

	@Autowired
	MailService mailService;

	@Autowired
	BoardSpec boardSpec;

	@ApiOperation(value = "게시판 리스트", httpMethod = "GET", notes = "게시판 리스트")
	@GetMapping("/{cate}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "페이지 번호", required = false, dataType = "number", defaultValue = "1"),
			@ApiImplicitParam(name = "size", value = "페이지 사이즈", required = false, dataType = "number", defaultValue = "10"),
			@ApiImplicitParam(name = "code", value = "페이지 코드", required = false, dataType = "number", defaultValue = "common") })
	public ResponseEntity<Page<Board>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@PathVariable String cate, @RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {
		if(conditionKey == null){
			conditionKey="title";
		}
		if(conditionValue==null){
			conditionValue="";
		}

		Specification<Board> spec = Specifications.where(boardSpec.build("N", cate, conditionKey, conditionValue));
		return new ResponseEntity<>(boardMapper.findAll(spec, pageable).map(board -> {
			User searchUser;
			Integer id = 0;
			User user = new User();
			id = board.getCreater();
			searchUser = userMapper.findOne(id);
			user.setId(searchUser.getId());
			user.setName(searchUser.getName());
			user.setLoginId(searchUser.getLoginId());
			board.setUser(user);
			return board;
		}), HttpStatus.OK);
	}

	@ApiOperation(value = "게시판 상세화면", httpMethod = "GET", notes = "게시판 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "게시판 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@GetMapping("/{cate}/{id}")
	public ResponseEntity<Board> view(@PathVariable String cate, @PathVariable Integer id) {

		Board board = boardMapper.findOne(id);
		board.setHits(board.getHits() + 1);
		boardMapper.save(board);

		User boardUser = userMapper.findOne(board.getCreater());

		User user = new User();
		user.setName(boardUser.getName());
		user.setId(boardUser.getId());

		board.setUser(user);

		return new ResponseEntity<>(board, HttpStatus.OK);
	}

	@ApiOperation(value = "게시판 저장", httpMethod = "PUT", notes = "게시판 저장")
	@PutMapping("/{cate}")
	@Transactional
	public ResponseEntity<Board> save(@PathVariable String cate, @RequestBody Board t)
			throws AddressException, IOException {

		log.info("게시판 저장 " + t);

		User user = UserSingleton.getInstance();

		t.setCreater(user.getId());
		boardMapper.save(t);

		if (cate != null && (cate.equals("notice") || cate.equals("as"))  && t.getCheckYn().equals("Y")) {

			List<User> userList = userMapper.findByDelYnAndWorkState("N", "재직중", new Sort(Sort.Direction.ASC, "id"));

			mailService.allUserMailSend(cate, userList, t);

		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "게시판 수정", httpMethod = "POST", notes = "게시판 수정")
	@PostMapping("/{cate}")
	@Transactional
	public ResponseEntity<Board> update(@PathVariable String cate, @RequestBody Board t)
			throws AddressException, IOException {

		User user = UserSingleton.getInstance();

		for (BoardFile file : t.getFileList()) {

			Optional<BoardFile> boardFileOptional = boardFileMapper.findByDelYnAndBoardIdAndOrderNo("N", t.getId(),
					file.getOrderNo());
			if (boardFileOptional.isPresent()) {
				BoardFile boardFileResult = boardFileOptional.get();
				boardFileResult.setFileAttach(file.getFileAttach());
				boardFileMapper.save(boardFileResult);
			} else {

				boardFileMapper.save(file);
			}
		}

		t.setModifier(user.getId());
		List<BoardFile> fileList = boardFileMapper.findByBoardId(t.getId());
		t.setFileList(fileList);
		boardMapper.save(t);

		if (cate != null && (cate.equals("notice") || cate.equals("as")) && t.getCheckYn().equals("Y")) {

			List<User> userList = userMapper.findByDelYnAndWorkState("N", "재직중", new Sort(Sort.Direction.ASC, "id"));

			mailService.allUserMailSend(cate, userList, t);

		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "게시판 삭제", httpMethod = "DELETE", notes = "게시판 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "게시판 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@DeleteMapping("/{cate}/{id}")
	public ResponseEntity<Board> delete(@PathVariable String cate, @PathVariable Integer id) {
		User user = UserSingleton.getInstance();
		Board board = boardMapper.findOne(id);

		board.setDelYn("Y");
		for (BoardFile boardFile : board.getFileList()) {
			boardFile.setDelYn("Y");
		}
		board.setModifier(user.getId());
		boardMapper.save(board);

		return new ResponseEntity<>(board, HttpStatus.OK);
	}

}
