package com.snbplus.madang.board;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

/**
 * 보드
 *
 * @author bhshin
 *
 */
@Entity
@Table(name = "board")
@Data
public class Board {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String cate; // 게시판 종류

	private String title; // 제목

	private String contents; // 내용

	@ColumnDefault("0")
	private Integer hits = 0; // 조회수

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	private String state; // as게시판일 경우 접수 상태

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "boardId")
	private List<BoardFile> fileList;

	@Transient
	private User user;

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String checkYn = "N";

}
