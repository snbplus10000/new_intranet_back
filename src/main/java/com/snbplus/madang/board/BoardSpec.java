package com.snbplus.madang.board;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.snbplus.madang.user.UserMapper;

@Component
public class BoardSpec   {

    @Autowired
    UserMapper userMapper;

    public Specification<Board> build(String delYn, String cate, String conditionKey, String conditionValue) {

        if(conditionKey == null || conditionValue.equals("")) {
            return search(delYn, cate);
        }else {
        	if(conditionKey.equals("creater")) {
        		return search(delYn, cate, conditionKey, userMapper.findByNameLike("%"+ conditionValue + "%").stream().map(user->user.getId()).collect(Collectors.toList()));
        	}
        }

        return search(delYn, cate, conditionKey, "%" + conditionValue + "%");

    }
    
    public Specification<Board> build(String delYn, String cate) {

        return build(delYn,cate, null, null);
    }


    public Specification<Board> search(String delYn, String cate, String conditionKey, String conditionValue) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn),
                        cb.equal(root.get("cate"), cate),
                        cb.like(root.get(conditionKey), conditionValue)
                );

    }
    
    public  Specification<Board> search(String delYn, String cate, String conditionKey, List conditionValue) {
	
        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : conditionValue) {
                in.value(conditionColumnValue);           	
            }

            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    cb.equal(root.get("cate"), cate),
                    in
            );
        };


    }



    public Specification<Board> search(String delYn, String cate) {
        return (root, query, cb) ->
                cb.and(
                        cb.equal(root.get("delYn"),  delYn),
                        cb.equal(root.get("cate"), cate)
                );

    }
    
    public Specification<Board> searchMemKind(String delYn, String cate, String conditionKey, List list) {
          	
        return (root, query, cb) -> {

            Path<Object> path = root.get(conditionKey);
            CriteriaBuilder.In<Object> in = cb.in(path);
            for (Object conditionColumnValue : list) {
                in.value(conditionColumnValue);
            }
            return cb.and(
                    cb.equal(root.get("delYn"),  delYn),
                    cb.equal(root.get("cate"), cate),
                    in
                    
            );
        };
    }
}