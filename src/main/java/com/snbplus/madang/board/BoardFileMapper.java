package com.snbplus.madang.board;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

@Repository
public interface BoardFileMapper extends JpaRepository<BoardFile, Integer> {

	List<BoardFile> findByDelYn(String delYn);

	Optional<BoardFile> findByDelYnAndBoardIdAndOrderNo(String delYn, Integer boardId, Integer orderNo);

	List<BoardFile> findByBoardId(Integer id);

}
