package com.snbplus.madang.board;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.snbplus.madang.common.FileAttach;

import lombok.Data;

@Entity
@Table(name = "board_file")
@Data
public class BoardFile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String kind; // 종류
	
	private Integer boardId; //(fk)
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	private Integer orderNo; // 순서 정렬
	
	@OneToOne
	@JoinColumn(name="file_id")
	private FileAttach fileAttach;
	


}
