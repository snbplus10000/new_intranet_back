package com.snbplus.madang.user;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.snbplus.madang.auth.AuthMapper;
import com.snbplus.madang.common.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import com.snbplus.madang.config.MadangPasswordEncoder;
import com.snbplus.madang.config.Sha256;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

	private static final String DEFAULT_REDIRECT_VIEW_URL = "/user/exterList";


	// password encoder
	@Autowired
	MadangPasswordEncoder encoder;

	@Autowired
	UserMapper userMapper;

	@Autowired
	UserFileMapper userFileMapper;

	@Autowired
	UserSalaryMapper salaryMapper;

	@Autowired
	UserSchoolMapper schoolMapper;

	@Autowired
	UserLicenseMapper licenseMapper;

	@Autowired
	UserLanguageMapper languageMapper;

	@Autowired
	UserInfoMapper infoMapper;

	@Autowired
	UserFamilyMapper familyMapper;

	@Autowired
	FileUploadService fileUploadService;

	@Autowired
	OrgChartMapper chartMapper;

	@Autowired
	DeptMapper deptMapper;

	@Autowired
	UserHoliMapper holiMapper;

	@Autowired
	AuthMapper authMapper;

	@ApiOperation(value = "로그인 한 유저 정보", httpMethod = "POST", notes = "로그인 한 유저 정보 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PostMapping("/info")
	public User user(Authentication authentication) {



		User user = UserSingleton.getInstance();
		User userResult = userMapper.findOne(user.getId());

		Optional<UserHoli> holiOptional = holiMapper.findByDelYnAndYearAndRefId("N",
				Integer.toString(LocalDate.now().getYear()), user.getId());

		if (holiOptional.isPresent()) {
			userResult.setUserHoli(holiOptional.get());
		} else {
			userResult.setUserHoli(null);
		}


		/*userResult.getDeptChart().setDeptUser(null);
		userResult.getDeptChart().setHighDeptUser(null);
		userResult.getHighDeptChart().setDeptUser(null);
		userResult.getHighDeptChart().setHighDeptUser(null);*/

		return userResult;
	}

	@ApiOperation(value = "결재 정보 저장", httpMethod = "POST", notes = "결재 정보 저장 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PostMapping("/updateApproval")
	public void approvalSave(@RequestBody List<Object> user, HttpServletRequest request) {
		//get modifer
		User userSession = UserSingleton.getInstance();

		for(int i = 0; i < user.size(); i++) {
			List<Integer> u = (List<Integer>) (user.get(i));
			userMapper.updateApproval(u.get(0), u.get(1), u.get(2), u.get(3));
		}

	}


	@PostMapping("/updateTenureApproval")
	public String updateTenureApproval(@RequestBody List<Integer> user) {
		userMapper.updateTenureApproval(user.get(0), user.get(1));

		return "success";
	}

	@ApiOperation(value = "유저 정보", httpMethod = "POST", notes = "유저 정보 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PostMapping("/detail/{no}")
	public User userDetail(@PathVariable Integer no) {

		User userResult = userMapper.findOne(no);
		/*userResult.getDeptChart().setDeptUser(null);
		userResult.getDeptChart().setHighDeptUser(null);
		userResult.getHighDeptChart().setDeptUser(null);
		userResult.getHighDeptChart().setHighDeptUser(null);*/

		userResult.setUserSalary(salaryMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setUserInfo(infoMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setUserLanguage(languageMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setUserLicense(licenseMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setUserSchool(schoolMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setUserFamily(familyMapper.findByDelYnAndRefId("N", userResult.getId()));
		userResult.setHoliList(holiMapper.findByDelYnAndRefId("N", userResult.getId()));

		return userResult;
	}

	@ApiOperation(value = "유저 정보 저장", httpMethod = "PUT", notes = "유저 정보 저장 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PutMapping("/save")
	public ResponseEntity<User> userSave(@RequestBody User user, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		User userSession = UserSingleton.getInstance();
		user.setCreater(userSession.getId());

		String memState = user.getMemState();

		if (memState != null && (memState.equals("정규직") || memState.equals("계약직") || memState.equals("인턴주"))) {
			user.setGubun("I");
		} else {
			user.setGubun("O");
		}

		String workState = user.getWorkState();

		if (workState != null && workState.equals("퇴사")) {
			user.setRetireDate(null);
		}

		String salt = Sha256.getSaltValue();
		String pass = Sha256.encrypt(user.getPass());
		user.setPass(Sha256.encrypt(pass, salt));
		user.setSalt(salt);
		user.setAuth(authMapper.findOne(2)); //user
		userMapper.save(user);

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@ApiOperation(value = "유저 정보 변경", httpMethod = "POST", notes = "유저 정보 변경 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PostMapping("/update")
	public ResponseEntity<User> userUpdate(@RequestBody User user, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		User userSession = UserSingleton.getInstance();
		user.setModifier(userSession.getId());
		String memState = user.getMemState();

		if (memState != null && (memState.equals("정규직") || memState.equals("계약직") || memState.equals("인턴주"))) {
			user.setGubun("I");
		} else {
			user.setGubun("O");
		}

		String workState = user.getWorkState();

//		if (workState != null && workState.equals("퇴사")) {
//			user.setRetireDate(null);
//		}

		UserFile userfile = user.getThumbFile();
		if (userfile != null) {
			userFileMapper.save(userfile);
		}

		user.setThumbFile(userfile);

		String loginId = user.getLoginId();
		Optional<User> searchUser = userMapper.findByLoginId(loginId);
		User userInfo = searchUser.get();
		user.setId(userInfo.getId());

		userMapper.save(user);

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@ApiOperation(value = "비밀번호 변경", httpMethod = "POST", notes = "비밀번호 변경 ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@PostMapping("/changePassword")
	public ResponseEntity<User> changePassword(@RequestBody User user) {

		User searchUser = userMapper.findOne(UserSingleton.getInstance().getId());
		searchUser.setModifier(user.getId());
		User userResult = new User();

		boolean matches = encoder.matches(user.getPass(), searchUser.getPass() + "  " + searchUser.getSalt());

		if (matches) {

			String salt = Sha256.getSaltValue();
			String newPass = Sha256.encrypt(user.getNewPass());
			String passwordSalt = Sha256.encrypt(newPass, salt);
			searchUser.setPass(passwordSalt);
			searchUser.setSalt(salt);
			userMapper.save(searchUser);
			userResult.setResult(true);
		} else {
			userResult.setResult(false);
		}

		return new ResponseEntity<>(userResult, HttpStatus.OK);

	}

	@ApiOperation(value = "부서에 따른 직원 list", httpMethod = "GET", notes = "부서에 따른 직원 list  ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/userList/{id}")
	public ResponseEntity<List<User>> orgChartUserList(@PathVariable Integer id) {

		String workState = "재직중";
		//OrgChart deptChart = chartMapper.findOne(id);
		Dept deptChart = deptMapper.findOne(id);
		List<User> userList = new ArrayList<>(); //userMapper.findBydeptChartAndWorkState(deptChart, workState);

		/*
		for (User user : userList) {
			user.setDeptLoad(null);
			user.setHighDeptLoad(null);
		}*/

		return new ResponseEntity<>(userList, HttpStatus.OK);
	}

	@ApiOperation(value = "재직중인 직원 리스트", httpMethod = "GET", notes = "재직중인 직원 list  ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/userLists")
	public ResponseEntity<List<User>> userList() {
		List<User> userList = new ArrayList<>();
		userList = userMapper.findByDelYnAndWorkState("N", "재직중", new Sort(Sort.Direction.ASC, "id"));

		return new ResponseEntity<>(userList, HttpStatus.OK);

	}


	@ApiOperation(value = "직원 전체 리스트", httpMethod = "GET", notes = "직원 전체 리스트(관리자)  ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/allList")
	public ResponseEntity<Page<User>> UserList(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable) {

		return new ResponseEntity<>(userMapper.findByDelYn(pageable, "N").map(user -> {
/*			user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);*/
			user.setUserHoli(null);

			return user;
		}), HttpStatus.OK);

	}


	@ApiOperation(value = "해당 부서의 부장 가져오기", httpMethod = "GET", notes = "해당 부서의 부장 가져오기")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/teamLeader/{code}")
	public ResponseEntity<User> TeamLeader(@PathVariable String code) {
		Dept dept = deptMapper.findByCode(Integer.parseInt(code));
		return new ResponseEntity<>(userMapper.findByDelYnAndDeptChartAndPositionAndWorkState("N", dept, "팀장", "재직중"), HttpStatus.OK);
	}

}
