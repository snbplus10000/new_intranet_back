package com.snbplus.madang.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.project.Project;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "tbl_project_manager")
@Data
public class ProjectManager {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(value = AccessLevel.NONE)
    @Column(name = "tbl_project_manager_id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    @ToString.Exclude
    @JsonIgnore
    private Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private User user;
}
