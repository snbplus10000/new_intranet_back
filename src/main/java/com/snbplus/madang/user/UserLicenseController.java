package com.snbplus.madang.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userLicense")
public class UserLicenseController implements JoinController<List<UserLicense>> {

	@Autowired
	UserLicenseMapper licenseMapper;

	@ApiOperation(value = "직원 리스트 - 담당자 자격증 리스트", httpMethod = "GET", notes = "직원 리스트 - 담당자 자격증 리스트")
	@Override
	public ResponseEntity<List<UserLicense>> list(Integer id) {

		return new ResponseEntity<>(licenseMapper.findByDelYnAndRefId("N", id), HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 자격증 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 자격증 등록/수정")
	@Override
	public ResponseEntity<List<UserLicense>> save(@RequestBody List<UserLicense> t) {

		User user = UserSingleton.getInstance();

		for (UserLicense license : t) {

			if (license.getId() != null && license.getId() != 0) {
				license.setModifier(user.getId());
			} else {
				license.setCreater(user.getId());
			}
		}

		licenseMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 자격증 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 자격증 삭제")
	@Override
	public ResponseEntity<List<UserLicense>> delete(@RequestBody List<UserLicense> t) {

		User user = UserSingleton.getInstance();

		for (UserLicense license : t) {
			license.setDelYn("Y");
			license.setModifier(user.getId());
		}

		licenseMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 자격증 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 자격증 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserLicense> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();

		UserLicense result = licenseMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		licenseMapper.save(result);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
