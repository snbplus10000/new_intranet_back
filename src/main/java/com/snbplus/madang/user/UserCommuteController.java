package com.snbplus.madang.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.company.Company;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userCommute")
public class UserCommuteController implements InterfaceController<UserCommute> {
	
	
	@Override
	public ResponseEntity<Page<UserCommute>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {
		
		return null;
	}

	@Override
	public ResponseEntity<UserCommute> view(Integer id) {
		
		return null;
	}

	@Override
	public ResponseEntity<UserCommute> save(UserCommute t) {
		
		return null;
	}

	@Override
	public ResponseEntity<UserCommute> update(UserCommute t) {
		
		return null;
	}

	@Override
	public ResponseEntity<UserCommute> delete(Integer id) {
		
		return null;
	}

}
