package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Entity
@Table(name = "member_family")
@Data
public class UserFamily {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer refId;
	
	private String name; // 성명
	
	private String regNum1; // 주민번호앞
	
	private String regNum2; // 주민번호 뒤
	
	private String rel; // 관계
	
	private String acAbility; // 학력
	
	private String job; // 직업
	
	private String workplace; // 근무처
	
 	private String withYn; // 동거처
	
	private String dependentYn; //피부양자
	
	private String fa; // 가족수장
	
	private String deathYn; // 사망여부
	
	private String birth; // 생년월일
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
}
