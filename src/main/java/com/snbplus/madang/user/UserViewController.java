package com.snbplus.madang.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.snbplus.madang.common.CommonCodeMapper;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.common.OrgChartMapper;
import com.snbplus.madang.common.constant.MemStateCategory;
import com.snbplus.madang.common.constant.PositionCategory;
import com.snbplus.madang.common.constant.RankCategory;
import com.snbplus.madang.common.constant.WorkStateCategory;

@Controller
public class UserViewController {
	
	private static final String DEFAULT_REDIRECT_VIEW_URL = "/user/exterList";
	
    @Autowired
    UserMapper userMapper;
    
    @Autowired
    CommonCodeMapper commonCodeMapper;
    
    @Autowired
    OrgChartMapper orgChartMapper;

	@GetMapping("/login")
	public String user(Model model) {
		return "login/login";
	}

	@GetMapping("/login_error")
	public String loginError(Model model) {

		model.addAttribute("error", true);
		return "login/login";
	}
    
    @GetMapping("/user/interList")
    public String internalList(Model model){
    	   	 	
    	List<OrgChart> chartList = orgChartMapper.findAll();
    	
    	model.addAttribute("chartList", chartList);
    	   	    	   	   	
    	return "user/interList";
    }
    
    @GetMapping("/user/exterList")
    public String extonalList(Model model,  @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
    	
    	Page<User> userList = userMapper.findByGubunOrderByIdDesc(pageable, "O");
    	
    	model.addAttribute("userList", userList);
    	
    	return "user/exterList";
    }
    
    @GetMapping("/user/waitList")
    public String waitingList(Model model,  @PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 10) Pageable pageable) {
     	
    	Page<User> userList = userMapper.findByGubunOrderByIdDesc(pageable, "W");
    	
    	model.addAttribute("userList", userList);
    	
    	return "user/waitList";
    	   	
    }
    
    @GetMapping("/user/view")
    public String userView(Model model, Integer id) throws Exception  {
    		
    	List<Integer> list = new ArrayList<Integer>();
    	list.add(0);
    	list.add(1);
    	
    	List<OrgChart> highDeptList = orgChartMapper.findByLvlInOrderByCode(list);
    	List<OrgChart> deptList = orgChartMapper.findByLvlOrderByCode(2);
    	User userResult = userMapper.findOne(id);
    	
    	model.addAttribute("rankList", Arrays.stream(RankCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("deptList", deptList);
    	model.addAttribute("positionList", Arrays.stream(PositionCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("highDeptList", highDeptList);
    	model.addAttribute("memStateList", Arrays.stream(MemStateCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("workStateList", Arrays.stream(WorkStateCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("user", userResult);
	
    	return "user/userView";
    }
    
    @GetMapping("/user/write")
    public String userWrite(Model model) {
    	
    	List<Integer> list = new ArrayList<Integer>();
    	list.add(0);
    	list.add(1);
    	
    	List<OrgChart> highDeptList = orgChartMapper.findByLvlInOrderByCode(list);
    	List<OrgChart> deptList = orgChartMapper.findByLvlOrderByCode(2);
    	
    	model.addAttribute("rankList", Arrays.stream(RankCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("deptList", deptList);
    	model.addAttribute("positionList", Arrays.stream(PositionCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("highDeptList", highDeptList);
    	model.addAttribute("memStateList", Arrays.stream(MemStateCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	model.addAttribute("workStateList", Arrays.stream(WorkStateCategory.class.getEnumConstants()).collect(Collectors.toList()));
    	
    	return "user/userWrite";
    }


}
