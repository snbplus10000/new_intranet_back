package com.snbplus.madang.user;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.ToString;

@Data
@Entity
@Table(name = "user_role")
@ToString(exclude = "roleList")
public class UserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userRoleId;

    private Integer userId;

    @Transient
    private Integer[] rolesId;

    @ManyToMany
    private List<Role> roleList;

}
