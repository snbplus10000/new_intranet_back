package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Entity
@Table(name = "member_info")
@Data
public class UserInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer refId;
	
	private String subDept; // 겸직부서
	
	private String subPosition; // 겸직직책
	
	private String subDuty; //겸직업무
	
	private String comTel; // 사내번호
	
	private String homeTel; // 자택번호
	
	private String phone; // 휴대폰
	
	private String eTel1; // 긴급번호1
	
	private String eTel2; // 긴급번호2
	
	private String email; // 이메일
	
	private String wedAnny; // 결혼기념일
	
	private String religion; //종교
	
	private String lSight;  // 시력좌
	
	private String rSight;  // 시력우
	
	private String famRel; // 형재관계
	
	private String height; // 신장
	
	private String bldType; //혈액형
	
	private String hobby; // 취미
	
	private String specialty; // 특기
	
	private String uniformSize;  // 근무복사이즈
	
	private String carNum; // 차량번호
	
	private String carKind; // 차종
	
	private String carColor; // 차색
	
	private String carOwner; //차주
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부

}
