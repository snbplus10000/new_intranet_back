package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLicenseMapper extends JpaRepository<UserLicense, Integer> {

	List<UserLicense> findByDelYnAndRefId(String delYn, Integer refId);

}
