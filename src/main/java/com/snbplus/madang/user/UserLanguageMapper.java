package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLanguageMapper extends JpaRepository<UserLanguage, Integer> {

	List<UserLanguage> findByDelYnAndRefId(String delYn, Integer refId);

}
