package com.snbplus.madang.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userSchool")
public class UserSchoolController implements JoinController<List<UserSchool>> {

	@Autowired
	UserSchoolMapper schoolMapper;

	@ApiOperation(value = "직원 리스트 - 담당자 학력 리스트", httpMethod = "GET", notes = "직원 리스트 - 담당자 학력 리스트")
	@Override
	public ResponseEntity<List<UserSchool>> list(Integer id) {

		return new ResponseEntity<>(schoolMapper.findByDelYnAndRefId("N", id), HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 학력 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 학력 등록/수정")
	@Override
	public ResponseEntity<List<UserSchool>> save(@RequestBody List<UserSchool> t) {

		User user = UserSingleton.getInstance();

		for (UserSchool school : t) {

			if (school.getId() != null && school.getId() != 0) {
				school.setModifier(user.getId());
			} else {
				school.setCreater(user.getId());
			}
		}

		schoolMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 학력 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 학력 삭제")
	@Override
	public ResponseEntity<List<UserSchool>> delete(@RequestBody List<UserSchool> t) {

		User user = UserSingleton.getInstance();

		for (UserSchool school : t) {
			school.setDelYn("Y");
			school.setModifier(user.getId());
		}

		schoolMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 학력 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 학력 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserSchool> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();

		UserSchool result = schoolMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		schoolMapper.save(result);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
