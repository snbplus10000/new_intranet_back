package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Entity
@Table(name = "member_language")
@Data
public class UserLanguage {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer refId;
	
	private String cate;  //외국어구분
	
	private String acqDate;  // 취득일자
	
	private String testName; // 시험명
	
	private String acqScore;   // 취들일수
	
	private String score1;  //구슬/듣기점수
	
	private String score2;  // 필기점수
	
	private String ratio; // 환산비율
	
	private String vaildGrade; // 유효성적
	
	private String speakingRank; // 스피킹등급
	
	private String pubOffice; //인증기관
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
}
