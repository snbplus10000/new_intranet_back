package com.snbplus.madang.user;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


/**
 *  회원 - 출퇴근
 * @author bhshin
 *
 */
@Entity
@Table(name="member_commute")
@Data
public class UserCommute {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Date thedate; // 해당 날짜
	
	private Timestamp goWorkTime; //출근시간
	
	private Timestamp offWorkTime; // 퇴근시간
	
	private String exception; // 예외
	
	private Time nomalTime; // 정상근무시간
	
	private Time excessTime; // 초과 근무시간
	
	private Time totalTime; // 총 근무시간

}
