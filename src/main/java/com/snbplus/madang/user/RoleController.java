package com.snbplus.madang.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/")
    public ResponseEntity<List<Role>> myRole() {

        User user = UserSingleton.getInstance();

        UserRole userRole;
        List<Role> list = new ArrayList<>();

        Optional<UserRole> userRoleOptional = userRoleRepository.findByUserRoleId(user.getId());
        if (userRoleOptional.isPresent()) {
            userRole = userRoleOptional.get();
            list = userRole.getRoleList().stream().map(role -> {

                return role;
            }).collect(Collectors.toList());

        } else {
            list = null;
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/code")
    public ResponseEntity<List<Role>> roleCodeList() {

        return new ResponseEntity<>(roleRepository.findAll().stream().map(role -> {

            Role roles = new Role();
            roles.setRoleId(role.getRoleId());
            roles.setRoleName(role.getRoleName());
            return roles;
        }).collect(Collectors.toList()), HttpStatus.OK);
    }

}
