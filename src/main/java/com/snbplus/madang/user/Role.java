package com.snbplus.madang.user;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Collection;

@Data
@Entity
@Table(name = "role")
@ToString(exclude = "roleList")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer roleId;

    private String roleName;

    @ManyToMany(mappedBy = "roleList")
    @JsonIgnore
    private Collection<UserRole> roleList;

}
