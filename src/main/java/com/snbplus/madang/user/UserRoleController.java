package com.snbplus.madang.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userRole")
public class UserRoleController {

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	UserRoleRepository userRoleRepository;

	@Autowired
	UserMapper userMapper;

	@GetMapping("")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "유저 정보 pk", required = false, dataType = "number", defaultValue = "0"), })
	@ApiOperation(value = "유저 권한 전체 리스트", httpMethod = "GET", notes = "유저 권한 상세정보")
	public ResponseEntity<List<UserRole>> list(@RequestParam Integer id) {

		return new ResponseEntity<>(userRoleRepository.findByUserId(id), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "유저 권한 pk", required = false, dataType = "number", defaultValue = "0"), })
	@ApiOperation(value = "유저 권한 상세정보", httpMethod = "GET", notes = "유저 권한 상세정보")
	public ResponseEntity<UserRole> view(@PathVariable Integer id) {

		return new ResponseEntity<>(userRoleRepository.findByUserRoleId(id).get(), HttpStatus.OK);
	}

	@PutMapping("")
	@ApiOperation(value = "유저 권한 저장", httpMethod = "PUT", notes = "유저 권한 저장")
	public ResponseEntity<UserRole> save(@RequestBody UserRole t) throws Exception {

		User user = UserSingleton.getInstance();
		t.setUserId(user.getId());

		Integer[] roleIds = t.getRolesId();
		List<Role> roleList = new ArrayList<Role>();

		if (roleIds != null && roleIds.length > 0) {
			for (Integer no : roleIds) {

				Optional<Role> roleOptional = roleRepository.findByRoleId(no);
				roleOptional.orElseThrow(() -> new Exception("해당 권한에 대한 정보를 찾을수 없습니다."));
				roleList.add(roleOptional.get());

			}
		}

		t.setRoleList(roleList);
		return new ResponseEntity<>(userRoleRepository.save(t), HttpStatus.OK);
	}

	@PostMapping("/")
	@ApiOperation(value = "유저 권한 수정", httpMethod = "POST", notes = "유저 권한 수정")
	public ResponseEntity<UserRole> update(@RequestBody UserRole t) throws Exception {
		User user = UserSingleton.getInstance();

		List<UserRole> roleList = userRoleRepository.findByUserId(user.getId());

		if (roleList != null && roleList.size() > 0) {
			for (UserRole role : roleList) {
				userRoleRepository.delete(role.getUserRoleId());
			}

		}

		t.setUserId(user.getId());
		Integer[] roleIds = t.getRolesId();
		List<Role> roleList2 = new ArrayList<Role>();

		for (Integer no : roleIds) {

			Optional<Role> roleOptional = roleRepository.findByRoleId(no);
			roleOptional.orElseThrow(() -> new Exception("해당 권한에 대한 정보를 찾을수 없습니다."));
			roleList2.add(roleOptional.get());

		}

		t.setRoleList(roleList2);
		return new ResponseEntity<>(userRoleRepository.save(t), HttpStatus.OK);
	}

}
