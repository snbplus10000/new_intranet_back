package com.snbplus.madang.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userLanguage")
public class UserLanguageController implements JoinController<List<UserLanguage>> {

	@Autowired
	UserLanguageMapper languageMapper;

	@ApiOperation(value = "직원 리스트 - 담당자 언어 리스트", httpMethod = "GET", notes = "직원 리스트 - 담당자 언어 리스트")
	@Override
	public ResponseEntity<List<UserLanguage>> list(Integer id) {

		return new ResponseEntity<>(languageMapper.findByDelYnAndRefId("N", id), HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 언어 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 언어 등록/수정")
	@Override
	public ResponseEntity<List<UserLanguage>> save(@RequestBody List<UserLanguage> t) {

		User user = UserSingleton.getInstance();

		for (UserLanguage language : t) {

			if (language.getId() != null && language.getId() != 0) {
				language.setModifier(user.getId());
			} else {
				language.setCreater(user.getId());
			}
		}

		languageMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 언어 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 언어 삭제")
	@Override
	public ResponseEntity<List<UserLanguage>> delete(@RequestBody List<UserLanguage> t) {

		User user = UserSingleton.getInstance();

		for (UserLanguage language : t) {
			language.setDelYn("Y");
			language.setModifier(user.getId());
		}

		languageMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 언어 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 언어 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserLanguage> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();

		UserLanguage result = languageMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		languageMapper.save(result);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
