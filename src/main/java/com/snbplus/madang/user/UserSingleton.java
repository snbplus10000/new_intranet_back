package com.snbplus.madang.user;

import org.springframework.security.core.context.SecurityContextHolder;

public class UserSingleton {
	
	public static User getInstance() {
    	Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();   	
    	UserDetail userDetail = (UserDetail)principal; 
    	User user = userDetail.getUser();
    	
    	return user;
	}
	
	public static int getStep() {
		User user = UserSingleton.getInstance();
		Integer step = 0;
		
		if(user.getPosition().equals("팀장")) {
			step = 1;
		}else if(user.getPosition().equals("실장")) {
			step = 2;
		}
		
		return step;
		
	}
	


}
