package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSalaryMapper extends JpaRepository<UserSalary, Integer> {

	List<UserSalary> findByDelYnAndRefId(String string, Integer id);

}
