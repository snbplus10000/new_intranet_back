package com.snbplus.madang.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.snbplus.madang.auth.Auth;
import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import com.snbplus.madang.notification.NotificationMemberClone;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 회원 -인사정보 VO
 * 
 * @author ATU
 *
 */
@Entity
@Table(name = "member_clone")
@Data
public class User implements Cloneable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private String loginId;

	private String name;

	private String pass;

	private String salt;

	@OneToOne
	@JoinColumn(name = "auth")
	private Auth auth;

	private String extensionNo;

	private String sabun;

	private String position;

	private String rankName;

	private String rankCode;

	private String regNum1;

	private String regNum2;

	private String gender;

	private String cnName;

	private String enName;

	private String birth;

	private String workState;

	private String workPlace;

	private String memState;
	
	private String memKind;

	private String duty;

	private String joinDate;

	private String retireDate;

	private String homeZipcode;

	private String homeAddr1;

	private String homeAddr2;

	private String wpZipcode;

	private String wpAddr1;

	private String wpAddr2;

	private String bank;

	private String accNum;

	private String email;

	private String telNo;

	private String phoneNo;

	private Date lastLoginTime;

	private Integer creater; // 등록자

	@CreatedDate
	@Column(updatable = false, nullable = false)
	@CreationTimestamp
	private Timestamp created;

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;

	private Integer modifier; // 수정자

	private String kinds;

	private String companyName;

	private Integer orderNo;

	private String tesk;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	private List<ProjectManager> managers;

	/**
	 * 내/외부
	 */
	private String gubun;

	@Column(length = 512)
	private String etc;

	/*
	@ManyToOne
	@JoinColumn(name = "dept")
	private OrgChart deptChart;

	@ManyToOne
	@JoinColumn(name = "highDept")
	private OrgChart highDeptChart;
*/

	@ManyToOne
	@NotFound(action= NotFoundAction.IGNORE)
	@JoinColumn(name = "dept")
	private Dept deptChart;

	@ManyToOne
	@NotFound(action= NotFoundAction.IGNORE)
	@JoinColumn(name = "highDept")
	private Dept highDeptChart;

	private Integer approver;

	private Integer highApprover;

	@OneToOne
	@JoinColumn(name = "thumb_id")
	private UserFile thumbFile;

	@OneToOne
	@JoinColumn(name = "sign_id")
	private UserFile signFile;

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부

	/*
	@OneToMany(mappedBy = "receiver", fetch = FetchType.LAZY)
	@JsonIgnore
	@ToString.Exclude
	@OrderBy("id desc")
	private List<NotificationMemberClone> notificationList = new ArrayList<>();*/

	@Transient
	private MultipartFile thumbFiles;

	@Transient
	private MultipartFile signFiles;

	@Transient
	private UserHoli userHoli;

	@Transient
	private List<UserHoli> holiList;

	@Transient
	private List<UserCommute> userCommute;

	@Transient
	private List<UserSalary> userSalary;

	@Transient
	private List<UserSchool> userSchool;

	@Transient
	private List<UserLicense> userLicense;

	@Transient
	private List<UserLanguage> userLanguage;

	@Transient
	private List<UserInfo> userInfo;

	@Transient
	private List<UserFamily> userFamily;

	private String joinMilitaryDate;

	private String endMilitaryDate;


	/*
	 * @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	 * 
	 * @JoinColumn(name = "refId") private List<UserCommute> userCommute;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserSalary> userSalary;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserSchool> userSchool;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserLicense> userLicense;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserLanguage> userLanguage;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserInfo> userInfo;
	 * 
	 * @OneToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	 * 
	 * @JoinColumn(name="refId") private List<UserFamily> userFamily;
	 */

	@Transient
	private String newPass;

	@Transient
	private boolean result;

	private Integer firstApproval;

	private Integer secondApproval;

	private Integer emergencyApproval;

	private Integer approvalModifier;

	private Timestamp approvalModified;

	private Integer tenureFirstApproval;

	private Integer tenureSecondApproval;
}
