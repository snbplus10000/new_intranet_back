package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Entity
@Table(name="member_telecommute")
@Data
public class UserTeleCommute {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer creater; // 등록자

	@CreatedDate
	@Column(updatable = false, nullable = false)
	@CreationTimestamp
	private Timestamp created;

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified;

	private Integer modifier; // 수정자
	
	private String goclientIp; // 
	
	private String offclientIp; // 출근 ip
		
	@CreatedDate
	@Column(updatable = false, nullable = false)
	@CreationTimestamp
	private Timestamp gotowork;  // 출근 시간
	
	@LastModifiedDate
	@Column(insertable = false, updatable = true)
	@UpdateTimestamp
	private Timestamp offwork;
	
	private String etc; //비고
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부
	
	@Transient
	private User user;
	
	@Transient
	private Integer result;
	
	@Transient
	private String state;
}
