package com.snbplus.madang.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.snbplus.madang.common.FileAttach;

import lombok.Data;

@Entity
@Table(name="member_file")
@Data
public class UserFile {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String kind;
	
	@OneToOne
	@JoinColumn(name="file_id")
	private FileAttach fileAttach;


}
