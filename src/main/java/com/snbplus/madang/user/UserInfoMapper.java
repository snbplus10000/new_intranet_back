package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper extends JpaRepository<UserInfo, Integer> {

	List<UserInfo> findByDelYnAndRefId(String delYn, Integer refId);

}
