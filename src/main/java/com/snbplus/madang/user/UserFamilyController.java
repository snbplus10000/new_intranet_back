package com.snbplus.madang.user;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userFamily")
public class UserFamilyController implements JoinController<List<UserFamily>> {

	@Autowired
	UserFamilyMapper familyMapper;

	@ApiOperation(value = "직원 리스트 - 담당자 가족 관계 리스트", httpMethod = "GET", notes = "직원 리스트 - 담당자 가족 관계 리스트")
	@Override
	public ResponseEntity<List<UserFamily>> list(Integer id) {

		List<UserFamily> list = familyMapper.findByDelYnAndRefId("N", id);

		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 가족 관계 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 가족 관계 등록/수정")
	@Override
	public ResponseEntity<List<UserFamily>> save(@RequestBody List<UserFamily> t) {

		User user = UserSingleton.getInstance();

		for (UserFamily family : t) {

			if (family.getId() != null && family.getId() != 0) {
				family.setModifier(user.getId());
			} else {
				family.setCreater(user.getId());
			}
		}

		familyMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 가족 관계 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 가족 관계 삭제")
	@Override
	public ResponseEntity<List<UserFamily>> delete(@RequestBody List<UserFamily> t) {

		User user = UserSingleton.getInstance();

		for (UserFamily family : t) {

			UserFamily result = familyMapper.findOne(family.getId());

			result.setDelYn("Y");
			result.setModifier(user.getId());
			familyMapper.save(result);
		}

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 가족 관계 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 가족 관계 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserFamily> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();

		UserFamily result = familyMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		familyMapper.save(result);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
