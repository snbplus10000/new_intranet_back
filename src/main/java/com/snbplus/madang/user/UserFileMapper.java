package com.snbplus.madang.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFileMapper extends JpaRepository<UserFile, Integer> {
	

}
