package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserFamilyMapper extends JpaRepository<UserFamily, Integer> {

	List<UserFamily> findByDelYnAndRefId(String delYn, Integer refId);

}
