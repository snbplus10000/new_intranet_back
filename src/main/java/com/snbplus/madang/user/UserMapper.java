package com.snbplus.madang.user;

import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserMapper extends JpaRepository<User, Integer> {

    Optional<User> findByLoginId(String loginId);

	Page<User> findByGubunOrderByIdDesc(Pageable pageable, String gubun);

	@Query(value = "SELECT * FROM member_clone m WHERE id IN (SELECT DISTINCT creater FROM busi_card_calc where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findDistinctCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id IN (SELECT DISTINCT creater FROM holi_work where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findHoliCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM vacation where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findVacationCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM purchase where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findPurchaseCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM user_card_calc where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findUserCalcCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM outing where del_yn = 'N' AND cate = 'BT') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findApplyCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM outing where del_yn = 'N' AND cate = 'OW') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findOutingCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM outing_report where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findOutingReportCreater();

	@Query(value = "SELECT * FROM member_clone m WHERE id in (SELECT DISTINCT creater FROM tbl_busi_trip_car where del_yn = 'N') AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findBusiTripCreater();


	@Transactional
	@Modifying
	@Query(value = "UPDATE member_clone SET first_approval = :first, second_approval = :second, emergency_approval = :emer, modifier = :id, approval_modifier = :id WHERE id = :id", nativeQuery = true)
	void updateApproval(@Param("id") Integer id, @Param("first") Integer first, @Param("second") Integer second, @Param("emer") Integer emer);

	@Transactional
	@Modifying
	@Query(value = "UPDATE member_clone SET tenure_first_approval = :first, tenure_second_approval = :second where del_yn='N' and work_state='재직중'", nativeQuery = true)
	void updateTenureApproval(@Param("first") Integer first, @Param("second") Integer second);

	List<User> findByGubunOrderByIdDesc(String gubun);

	//List<User> findBydeptChartAndWorkState(Dept deptChart, String workState);

	List<User> findByWorkState(String workState);

	@Query(value="SELECT * FROM member_clone WHERE auth = :auth AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findByAuth(@Param("auth") Integer auth);

	Page<User> findByDelYn(Pageable pageable, String delYn);
	
	List<User> findByDelYn(String delYn);

	List<User> findByDelYnAndWorkState(String delYn, String workState, Sort sort);

	Page<User> findByDelYnAndWorkState(Pageable pageable, String delYn, String workState);

	List<User> findByNameLike(String name);
	
	List<User> findByMemKind(String memKind);

	List<User> findByDutyAndWorkState(String duty,String workState);

	User findByDelYnAndDeptChartAndPositionAndWorkState(String delYn, Dept dept, String position, String workState);

	@Query(value = "SELECT * FROM member_clone m WHERE curdate() < date_add(str_to_date(join_date, '%Y-%m-%d'), interval +1 year) AND del_yn = 'N' AND work_state='재직중'", nativeQuery = true)
	List<User> findLessThan1Year();
}
