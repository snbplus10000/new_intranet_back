package com.snbplus.madang.user;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSchoolMapper extends JpaRepository<UserSchool, Integer> {

	List<UserSchool> findByDelYnAndRefId(String delYn, Integer refId);

}
