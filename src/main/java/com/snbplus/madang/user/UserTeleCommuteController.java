package com.snbplus.madang.user;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Optional;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userTeleCommute")
public class UserTeleCommuteController  {
	
	@Autowired
	UserTeleCommuteMapper commuteMapper;
	
	@Autowired
	UserMapper userMapper;
	


	@ApiOperation(value = "자택근무 리스트", httpMethod = "GET", notes = "자택근무 리스트")
	@GetMapping("")
	public ResponseEntity<Page<UserTeleCommute>> list(@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable) {
		
		return new ResponseEntity<>(commuteMapper.findByDelYn(pageable,"N").map(commute ->{
			User searchUser = userMapper.findOne(commute.getCreater());
			User user = new User();
		 	user.setId(searchUser.getId());
		 	user.setName(searchUser.getName());
		 	user.setLoginId(searchUser.getLoginId());
		 	user.setDeptChart(searchUser.getDeptChart());
			//user.getDeptChart().setDeptUser(null);
			//user.getDeptChart().setHighDeptUser(null);
			commute.setUser(user);
			return commute;
		}), HttpStatus.OK);
	}

	@ApiOperation(value = "자택근무 상세화면", httpMethod = "GET", notes = "자택근무 상세화면")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "자택근무 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@GetMapping("/{id}")
	public ResponseEntity<UserTeleCommute> view(@PathVariable Integer id) {
			UserTeleCommute commute = commuteMapper.findOne(id);
			User user = userMapper.findOne(commute.getCreater());
			/*user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);*/
			commute.setUser(user);
		return new ResponseEntity<>(commute, HttpStatus.OK);
	}

	@ApiOperation(value = "자택근무 상세화면", httpMethod = "GET", notes = "자택근무 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "자택근무 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@GetMapping("/recent")
	public ResponseEntity<UserTeleCommute> viewRecent() {

		User user = UserSingleton.getInstance();
		LocalDateTime startDatetime = LocalDateTime.of(LocalDate.now(), LocalTime.of(0,0,0)); //오늘 00:00:00
		LocalDateTime endDatetime = LocalDateTime.of(LocalDate.now(), LocalTime.of(23,59,59)); //오늘 23:59:59
		Timestamp startTime = Timestamp.valueOf(startDatetime);
		Timestamp endTime = Timestamp.valueOf(endDatetime);

		Optional<UserTeleCommute> commuteOptional = commuteMapper.findByDelYnAndCreatedBetweenAndCreater("N",startTime,endTime, user.getId());

		UserTeleCommute commute = new UserTeleCommute();
		if(commuteOptional.isPresent()){
			commute = commuteOptional.get();
			commute.setResult(1);
		}else{
			commute.setResult(-1);
		}
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		commute.setUser(user);
		return new ResponseEntity<>(commute, HttpStatus.OK);
	}

	@ApiOperation(value = "자택근무 저장", httpMethod = "PUT", notes = "자택근무 저장")
	@PutMapping("")
	public ResponseEntity<UserTeleCommute> save(HttpServletRequest request, @RequestBody UserTeleCommute t) {

		System.out.println("test");
		User user = UserSingleton.getInstance();

		LocalDateTime startDatetime = LocalDateTime.of(LocalDate.now(), LocalTime.of(0,0,0)); //오늘 00:00:00
		LocalDateTime endDatetime = LocalDateTime.of(LocalDate.now(), LocalTime.of(23,59,59)); //오늘 23:59:59
		Timestamp startTime = Timestamp.valueOf(startDatetime);
		Timestamp endTime = Timestamp.valueOf(endDatetime);
		Optional<UserTeleCommute> commuteOptional = commuteMapper.findByDelYnAndCreatedBetweenAndCreater("N",startTime,endTime, user.getId());

		if(t.getState() != null && t.getState().equals("출근")) {
			if(commuteOptional.isPresent()) { // 해당 날짜에 출근이 이미 있는경우 출근을 다시 할수 없습니다.
				commuteOptional.get().setResult(-2);
				return new ResponseEntity<>(commuteOptional.get(), HttpStatus.OK);
			}
		}else {
			if(!commuteOptional.isPresent()) { // 해당날짜에 출근이 없는 경우 퇴근을 할수 없습니다.
				t.setResult(-2);
				return new ResponseEntity<>(t,HttpStatus.OK);
			}
		}
		
		UserTeleCommute commute = new UserTeleCommute();
		if(commuteOptional.isPresent()){
			commute = commuteOptional.get();
			commute.setOffclientIp(t.getOffclientIp());
			commute.setModifier(user.getId());
			commute.setOffwork(t.getOffwork());
		}else {
			commute = t;
			commute.setCreater(user.getId());
		}
		


		commuteMapper.save(commute);
		commute.setResult(1);
		
		return new ResponseEntity<>(commute,HttpStatus.OK);
	}

	/*
	@ApiOperation(value = "자택근무 수정", httpMethod = "POST", notes = "자택근무 수정")
	@PostMapping("/")
	public ResponseEntity<UserTeleCommute> update(HttpServletRequest request, @RequestBody UserTeleCommute t) {
		User user = UserSingleton.getInstance();
		String ip = getClientIP(request);
		t.setClientIp(ip);
		t.setModifier(user.getId());
		commuteMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

	@ApiOperation(value = "자택근무 삭제", httpMethod = "PUT", notes = "자택근무 삭제")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "자택근무 관련 pk", required = true, dataType = "number", defaultValue = "1")
	})
	@DeleteMapping("/{id}")
	public ResponseEntity<UserTeleCommute> delete(@PathVariable Integer id) {
		
		UserTeleCommute t = commuteMapper.findOne(id);
		User user = UserSingleton.getInstance();
		t.setDelYn("Y");
		t.setModifier(user.getId());
		commuteMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}

*/

}
