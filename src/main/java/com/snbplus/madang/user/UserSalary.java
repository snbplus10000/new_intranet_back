package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

@Entity
@Table(name = "member_salary")
@Data
public class UserSalary {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer refId;
	
	private String year;  //연도
	
	private Integer salary; // 연봉
	
	private Integer basicPay; // 기본급
	
	private Integer dutyBenefit; //직책수당
	
	private Integer overtimeBenefit; // 시간외수당
	
	private Integer transPay; // 교통비
	
	private Integer mealsPay; // 식비
	
	private Integer longBenefit; // 근속수당
	
	private Integer researchBenefit; //연구수당
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	

}
