package com.snbplus.madang.user;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;


@Entity
@Table(name = "member_school")
@Data
public class UserSchool {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private Integer refId;
	
	private String acAbil;
	
	private String finAcAbil; // 최종학력
	
	private String entDate; // 입학일
	
	private String grdDate; // 졸업(예정)일
	
	private String shName; // 학교명
	
	private String major; // 전공
	
	private String subMajor; // 부전공
	
	private String sciengiYn; // 이공계여부
	
	private String regionYn; // 지방대 여부
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	

	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부

}
