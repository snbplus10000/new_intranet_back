package com.snbplus.madang.user;

import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

@Repository
public interface UserTeleCommuteMapper extends JpaRepository<UserTeleCommute, Integer> {

	Page<UserTeleCommute> findByDelYn(Pageable pageable, String string);

	Optional<UserTeleCommute> findByDelYn(String string);

	Optional<UserTeleCommute> findByDelYnAndCreatedBetweenAndCreater(String string, Timestamp startTime,
			Timestamp endTime, Integer id);

}
