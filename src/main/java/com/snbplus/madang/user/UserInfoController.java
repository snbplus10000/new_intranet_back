package com.snbplus.madang.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userInfo")
public class UserInfoController implements JoinController<List<UserInfo>> {
	
	@Autowired
	UserInfoMapper infoMapper;
	
	@ApiOperation(value = "직원리스트 - 담당자 개인 정보 리스트", httpMethod = "GET", notes = "업체 리스트 - 담당자 개인 정보 리스트")
	@Override
	public ResponseEntity<List<UserInfo>> list(@PathVariable Integer id) {
		
		return new ResponseEntity<>(infoMapper.findByDelYnAndRefId("N",id), HttpStatus.OK);
	}
	
	@ApiOperation(value = "직원 리스트 - 담당자 개인 정보 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 개인 정보 등록/수정")
	@Override
	public ResponseEntity<List<UserInfo>> save(@RequestBody List<UserInfo> t) {
		
		User user = UserSingleton.getInstance();
		
		for(UserInfo info : t) {
			
			if(info.getId() != null && info.getId() != 0) {
				info.setModifier(user.getId());
			}else {
				info.setCreater(user.getId());
			}
		}
		
		
		infoMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}
	
	@ApiOperation(value = "직원 리스트 - 담당자 개인 정보 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 개인 정보 삭제")
	@Override
	public ResponseEntity<List<UserInfo>> delete(@RequestBody List<UserInfo> t) {
		
		User user = UserSingleton.getInstance();
		
		for(UserInfo info : t) {
			info.setDelYn("Y");
			info.setModifier(user.getId());
		}
		
		infoMapper.save(t);
		
		return new ResponseEntity<>(t,HttpStatus.OK);
	}


}
