package com.snbplus.madang.user;

import com.snbplus.madang.common.Dept;
import com.snbplus.madang.common.OrgChart;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

@Entity
@Table(name = "appointment")
@Data
public class Appointment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;  // pk

	@JoinColumn(name = "member_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private User member; // 멤버아이디(FK)

	@JoinColumn(name = "bef_dep")
	@ManyToOne(fetch = FetchType.LAZY)
	private Dept befDep; // 전부서

	@JoinColumn(name = "app_dep")
	@ManyToOne(fetch = FetchType.LAZY)
	private Dept appDep; // 발령부서
	
	private String rankName; // 발령직위

	@Column(nullable = false)
	private String befRankName; // 전직위

	private String  appDate;  // 발령일

	private String appCate; // 발령구분

	private String content; // 내용

	private String remark; // 비고

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여부
}
