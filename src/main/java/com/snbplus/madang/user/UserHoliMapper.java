package com.snbplus.madang.user;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserHoliMapper extends JpaRepository<UserHoli, Integer> {

	Optional<UserHoli> findByRefIdAndYear(Integer memberId, String year);

	List<UserHoli> findByYear(String year);

	Optional<UserHoli> findByDelYnAndYearAndRefId(String delYn, String year, Integer refId);

	List<UserHoli> findByDelYnAndRefId(String delYn, Integer refId);

}
