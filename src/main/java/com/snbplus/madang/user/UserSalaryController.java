package com.snbplus.madang.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.JoinController;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userSalary")
public class UserSalaryController implements JoinController<List<UserSalary>> {

	@Autowired
	UserSalaryMapper salaryMapper;

	@ApiOperation(value = "직원 리스트 - 담당자 봉급 리스트", httpMethod = "GET", notes = "직원 리스트 - 담당자 봉급 리스트")
	@Override
	public ResponseEntity<List<UserSalary>> list(Integer id) {

		return new ResponseEntity<>(salaryMapper.findByDelYnAndRefId("N", id), HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 봉급 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 봉급 등록/수정")
	@Override
	public ResponseEntity<List<UserSalary>> save(@RequestBody List<UserSalary> t) {
		
		User user = UserSingleton.getInstance();
		
		for(UserSalary salary : t) {
			
			if(salary.getId() != null && salary.getId() != 0) {
				salary.setModifier(user.getId());
			}else {
				salary.setCreater(user.getId());
			}
		}

		salaryMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 담당자 봉급 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 봉급 삭제")
	@Override
	public ResponseEntity<List<UserSalary>> delete(@RequestBody List<UserSalary> t) {
		
		User user = UserSingleton.getInstance();

		for (UserSalary salary : t) {
			salary.setDelYn("Y");
			salary.setModifier(user.getId());
		}

		salaryMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);

	}
	
	@ApiOperation(value = "직원 리스트 - 담당자 봉급 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 자격증 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserSalary> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();
		
		UserSalary result = salaryMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		salaryMapper.save(result);
			
		return new ResponseEntity<>(result,HttpStatus.OK);
	}

}
