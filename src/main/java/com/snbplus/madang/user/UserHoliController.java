package com.snbplus.madang.user;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/userHoli")
public class UserHoliController {

	@Autowired
	UserHoliMapper holiMapper;

	@Autowired
	UserMapper userMapper;

	@GetMapping("/list")
	public ResponseEntity<List<UserHoli>> holiList(@RequestParam(defaultValue = "2020") String years) {

		return new ResponseEntity<>(holiMapper.findByYear(years), HttpStatus.OK);

	}

	@GetMapping("/{id}")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "직원 해당 연도의 휴가 관련 api") })
	public ResponseEntity<UserHoli> holi(@PathVariable Integer id, @RequestParam(defaultValue = "2020") String years)
			throws Exception {

		Optional<UserHoli> userHoliOptional = holiMapper.findByDelYnAndYearAndRefId("N", years, id);
		userHoliOptional.orElseThrow(() -> new Exception("해당 정보를 찾을수 없습니다."));

		return new ResponseEntity<>(userHoliOptional.get(), HttpStatus.OK);
	}

	@ApiOperation(value = "관리자 - 직원 전체 리스트(퇴사자 포함)", httpMethod = "GET", notes = "관리자 - 직원 전체 리스트  ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/allList")
	public ResponseEntity<Page<User>> UserList(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable) {

		return new ResponseEntity<>(userMapper.findByDelYn(pageable, "N").map(user -> {

			Optional<UserHoli> holiOptional = holiMapper.findByDelYnAndYearAndRefId("N",
					Integer.toString(LocalDate.now().getYear()), user.getId());

			if (holiOptional.isPresent()) {
				user.setUserHoli(holiOptional.get());
			} else {
				user.setUserHoli(null);
			}

			/*user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);*/

			return user;

		}), HttpStatus.OK);

	}

	@ApiOperation(value = "관리자 - 직원 전체 리스트(퇴사자 제외)", httpMethod = "GET", notes = "관리자 - 직원 전체 리스트  ")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Server is Dead"),
			@ApiResponse(code = 200, message = "Run") })
	@GetMapping("/allWorker")
	public ResponseEntity<Page<User>> UserWorkPage(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable) {

		return new ResponseEntity<>(userMapper.findByDelYnAndWorkState(pageable, "N", "재직중").map(user -> {

			Optional<UserHoli> holiOptional = holiMapper.findByDelYnAndYearAndRefId("N",
					Integer.toString(LocalDate.now().getYear()), user.getId());

			if (holiOptional.isPresent()) {
				user.setUserHoli(holiOptional.get());
			} else {
				user.setUserHoli(null);
			}

			/*user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);*/

			return user;

		}), HttpStatus.OK);

	}

	@GetMapping("/allWorkerList")
	public ResponseEntity<List<User>> UserWorkList(@RequestParam(defaultValue = "id") String sort) {

		List<User> userList = userMapper.findByDelYnAndWorkState("N", "재직중", new Sort(Sort.Direction.ASC, sort));

		/*for(User user : userList){
			user.getDeptChart().setDeptUser(null);
			user.getDeptChart().setHighDeptUser(null);
			user.getHighDeptChart().setDeptUser(null);
			user.getHighDeptChart().setHighDeptUser(null);
		}*/

		return new ResponseEntity<>(userList, HttpStatus.OK);

	}

	@ApiOperation(value = "직원 리스트 - 휴가정보 등록/수정", httpMethod = "PUT", notes = "직원 리스트 - 담당자 휴가정보 등록/수정")
	@PutMapping("")
	public ResponseEntity<List<UserHoli>> save(@RequestBody List<UserHoli> t) {

		User user = UserSingleton.getInstance();

		for (UserHoli holi : t) {

			if (holi.getId() != null && holi.getId() != 0) {
				holi.setModifier(user.getId());
			} else {
				holi.setCreater(user.getId());
			}
		}

		holiMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 휴가 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 휴가 삭제")
	@DeleteMapping("")
	public ResponseEntity<List<UserHoli>> delete(@RequestBody List<UserHoli> t) {

		User user = UserSingleton.getInstance();

		for (UserHoli holi : t) {
			holi.setDelYn("Y");
			holi.setModifier(user.getId());
		}

		holiMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "직원 리스트 - 휴가 전체 불러오기", httpMethod = "GET", notes = "직원 리스트 - 휴가 전체 불러오기")
	@GetMapping("/all/{id}")
	public ResponseEntity<List<UserHoli>> userAllHoli(@PathVariable Integer id) {

		return new ResponseEntity<>(holiMapper.findByDelYnAndRefId("N", id), HttpStatus.OK);

	}

	@ApiOperation(value = "직원 리스트 - 휴가 개별 삭제", httpMethod = "DELETE", notes = "직원 리스트 - 담당자 자격증 개별 삭제")
	@DeleteMapping("/{id}")
	public ResponseEntity<UserHoli> deleteInit(@PathVariable Integer id) {
		User user = UserSingleton.getInstance();

		UserHoli result = holiMapper.findOne(id);
		result.setDelYn("Y");
		result.setModifier(user.getId());
		holiMapper.save(result);

		return new ResponseEntity<>(result, HttpStatus.OK);
	}

}
