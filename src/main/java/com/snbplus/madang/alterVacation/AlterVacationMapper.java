package com.snbplus.madang.alterVacation;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlterVacationMapper extends JpaRepository<AlterVacation, Integer> {

	Page<AlterVacation> findByDelYnAndCreaterIn(Pageable pageable, String delYn, List<Integer> userFilterList);

}
