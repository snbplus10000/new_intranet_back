package com.snbplus.madang.alterVacation;

import java.sql.Timestamp;

import javax.persistence.*;

import com.snbplus.madang.newApproval.ApprovalInfo;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.snbplus.madang.user.User;

import lombok.Data;

@Entity
@Table(name = "alter_vacation")
@Data
public class AlterVacation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	private String dept;  // 부서명
	
	private String regName; // 신청자명
	
	private String sabun;  // 사번
	
	private String position; // 직책
	
	private String regRank; // 직급
	
	private String year; // 연도
	
	private double holiCnt; // 대체 휴가 신청 일수
	
	private String overtime; // 초과 근무 시간
	
	@Column(length = 1024)
	private String reason; // 이유
	
	private String state; // 진행상태
	
	@ColumnDefault("''")
	private String deferReason=""; // 보류사유
	
	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp  created;  // 등록일
	
	private Integer creater; // 등록자
	
	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일
	
	private Integer modifier; // 수정자
	
	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn ="N"; // 삭제여부
	
	@Transient
	private User user;

	@OneToOne
	@JoinColumn(name = "approvalInfoNo")
	private ApprovalInfo approvalInfo;

}
