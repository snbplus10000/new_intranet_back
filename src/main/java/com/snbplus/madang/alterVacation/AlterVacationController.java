package com.snbplus.madang.alterVacation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.auth.UserAuthService;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/alterVacation")
public class AlterVacationController implements InterfaceController<AlterVacation> {

	@Autowired
	AlterVacationMapper vacationMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	UserAuthService userAuthService;

	@ApiOperation(value = "대체 휴가신청 리스트", httpMethod = "GET", notes = "대체 휴가신청 리스트")
	@Override
	public ResponseEntity<Page<AlterVacation>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15) Pageable pageable,
			@RequestParam(required = false) String conditionKey,
			@RequestParam(required = false) String conditionValue) {
		List<Integer> userFilterList = userAuthService.myTeamMemberList(UserSingleton.getInstance()).stream()
				.map(userItem -> userItem.getId()).collect(Collectors.toList());

		return new ResponseEntity<>(
				vacationMapper.findByDelYnAndCreaterIn(pageable, "N", userFilterList).map(AlterVacation -> {

					User searchUser;
					Integer id = 0;
					User user = new User();
					id = AlterVacation.getCreater();
					searchUser = userMapper.findOne(id);
					user.setId(searchUser.getId());
					user.setName(searchUser.getName());
					user.setLoginId(searchUser.getLoginId());
					AlterVacation.setUser(user);
					return AlterVacation;
				}), HttpStatus.OK);

	}

	@ApiOperation(value = "대체 휴가신청 상세화면", httpMethod = "GET", notes = "대체 휴가신청 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "대체 휴가신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<AlterVacation> view(@PathVariable Integer id) {
		AlterVacation t = vacationMapper.findOne(id);
		User user = userMapper.findOne(t.getCreater());
		/*user.getDeptChart().setDeptUser(null);
		user.getDeptChart().setHighDeptUser(null);
		user.getHighDeptChart().setDeptUser(null);
		user.getHighDeptChart().setHighDeptUser(null);*/
		t.setUser(user);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "대체 휴가신청 저장", httpMethod = "PUT", notes = "대체 휴가신청 저장")
	@Override
	public ResponseEntity<AlterVacation> save(@RequestBody AlterVacation t) {

		User user = UserSingleton.getInstance();
		t.setCreater(user.getId());
		vacationMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "대체 휴가신청 수정", httpMethod = "POST", notes = "대체 휴가신청 수정")
	@Override
	public ResponseEntity<AlterVacation> update(@RequestBody AlterVacation t) {

		User user = UserSingleton.getInstance();
		t.setModifier(user.getId());
		vacationMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "대체 휴가신청 삭제", httpMethod = "DELETE", notes = "대체 휴가신청 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "대체 휴가신청 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<AlterVacation> delete(@PathVariable Integer id) {

		User user = UserSingleton.getInstance();

		AlterVacation AlterVacation = vacationMapper.findOne(id);
		AlterVacation.setDelYn("Y");
		AlterVacation.setModifier(user.getId());

		vacationMapper.save(AlterVacation);

		return new ResponseEntity<>(AlterVacation, HttpStatus.OK);
	}

}
