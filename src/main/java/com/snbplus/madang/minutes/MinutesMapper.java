package com.snbplus.madang.minutes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MinutesMapper extends JpaRepository<Minutes, Integer> {

	Minutes findByRefId(Integer id);

}
