package com.snbplus.madang.minutes;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Data;

/**
 * 회의실 관련 vo
 * 
 * @author bhshin
 *
 */
@Entity
@Table(name = "minutes")
@Data
public class Minutes {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	private Integer refId; // busi_card_calc fk

	private String place; // 장소

	private String startDate; // 시작날짜시간

	private String endDate; // 종료날짜시간

	private String subject; // 주제

	private String attendee; // 참여자

	private String conclusion; // 결론

	private String contents; // 회의 내용

	@Column(nullable = false, updatable = false)
	@CreatedDate
	@CreationTimestamp
	private Timestamp created; // 등록일

	private Integer creater; // 등록자

	@LastModifiedDate
	@Column(updatable = true)
	@UpdateTimestamp
	private Timestamp modified; // 수정일

	private Integer modifier; // 수정자

	@Column(nullable = false)
	@ColumnDefault("'N'")
	private String delYn = "N"; // 삭제여
}
