package com.snbplus.madang.minutes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.snbplus.madang.InterfaceController;
import com.snbplus.madang.user.User;
import com.snbplus.madang.user.UserMapper;
import com.snbplus.madang.user.UserSingleton;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/minutes")
public class MinutesController implements InterfaceController<Minutes> {

	@Autowired
	MinutesMapper minutesMapper;

	@Autowired
	UserMapper userMapper;

	@Override
	public ResponseEntity<Page<Minutes>> list(
			@PageableDefault(sort = "id", direction = Sort.Direction.DESC, size = 15)Pageable pageable,
			@RequestParam(required = false) String conditionKey, @RequestParam(required = false) String conditionValue) {

		return null;
	}

	@ApiOperation(value = "회의 상세화면", httpMethod = "GET", notes = "회의 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "회의 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Minutes> view(@PathVariable Integer id) {

		return new ResponseEntity<>(minutesMapper.findByRefId(id), HttpStatus.OK);
	}

	@ApiOperation(value = "회의 저장", httpMethod = "PUT", notes = "회의 상세화면")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "회의 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Minutes> save(@RequestBody Minutes t) {

		minutesMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "회의 수정", httpMethod = "POST", notes = "회의 수정")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "회의 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Minutes> update(@RequestBody Minutes t) {

		User user = UserSingleton.getInstance();

		t.setModifier(user.getId());

		minutesMapper.save(t);

		return new ResponseEntity<>(t, HttpStatus.OK);
	}

	@ApiOperation(value = "회의 삭제", httpMethod = "DELETE", notes = "회의 삭제")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "회의 관련 pk", required = true, dataType = "number", defaultValue = "1") })
	@Override
	public ResponseEntity<Minutes> delete(@PathVariable Integer id) {

		Minutes minutes = minutesMapper.findOne(id);
		minutes.setDelYn("Y");
		minutesMapper.save(minutes);

		return new ResponseEntity<>(minutes, HttpStatus.OK);
	}

}
