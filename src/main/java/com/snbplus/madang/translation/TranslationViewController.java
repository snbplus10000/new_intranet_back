package com.snbplus.madang.translation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TranslationViewController {
	
	@GetMapping("/translation/list")
	public String translationList(Model model) {
		
		return "translation/translationList";
		
	}
	
	@GetMapping("/translation/view")
	public String translationView(Model model, Integer id) {
		
		
		return "translation/translationView";
	}
}
